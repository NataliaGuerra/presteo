import { AccessToken } from 'react-native-fbsdk';
import { Actions } from 'react-native-router-flux';
import { arrayPush, arrayRemoveAll } from 'redux-form';
import LoaderService from '../services/LoaderService';
import APIService from '.././services/APIService';
import ObjectValidator from '../helpers/ObjectValidator'; 

import { 
    SELECT_ROL,
    USER_DATA,
    CLEAR_DATA,
    ACCEPT_CONTRACT,
		SET_ROL_ID,
		REGISTER_DEVICE
} from './types';

// State 0 Login

export const login = (auth) => {
  return (dispatch) => {
    AccessToken.getCurrentAccessToken().then((data) => {
      loginUserSuccess(dispatch, data, auth);
    });
  };
};

export const registerDevice = (device) => {
	return {
		type: REGISTER_DEVICE,
		payload: device
	};
};

const loginUserSuccess = (dispatch, data, auth) => {
  const { accessToken, userID } = data;
  const fields = 'email,first_name,last_name,middle_name,name,name_format,picture,short_name';
  //Create loanding on facebook request
  LoaderService.setTimer();
  dispatch({
    type: 'CREATE_USER_FACEBOOK_REQUEST'
  });
   fetch(`https://graph.facebook.com/v3.1/${userID}?fields=${fields}&access_token=${accessToken}`)
    .then((response) => response.json())
    .then((json) => {
      //Adding facebook token to JSON data
      json['token'] = accessToken;
      fetch(`https://graph.facebook.com/v3.1/${userID}/picture?type=large&redirect=0`)
      .then((res) => res.json())
      .then((resJson) => {
          //Adding facebook 200px url to JSON data
          json['imgProfile'] = resJson.data.url;
          dispatch({
            type: USER_DATA,
            payload: json
          }); 
          //Go to verify if the user is logged in
          verifyIsLogIn(json, dispatch, auth); 
        }).catch((error) => {
          //Quit loading of facebook error
          LoaderService.removeTimer();
          dispatch({ 
            type: 'CREATE_USER_FACEBOOK_FAILURE', 
            payload: error
          });
        }); 
    }).catch((error) => {
      //Quit loading of facebook error
      LoaderService.removeTimer();
      dispatch({ 
        type: 'CREATE_USER_FACEBOOK_FAILURE', 
        payload: error
      });
    });
};

export const verifyIsLogIn = (obj, dispatch, auth) => {
  const url = 'v1/loginUsuario';
 	const user = {
		correo: obj.email,
		facebookId: obj.id,
		dispositivosUsuario: auth.dispositivosUsuario
  }; 
	
  /* const user = {
		correo: 'solicitanteJhon1@intrasoft.co',
		facebookId: '100011568109000',
		dispositivosUsuario: auth.dispositivosUsuario
	}; */ 
/* 	 const user = {
    correo: 'apps@intrasoft.co',
		facebookId: '100011568109176',
		dispositivosUsuario: auth.dispositivosUsuario
	};   */ 
	 
  return new Promise((resolve, reject) => {
    APIService.PutAPI(url, user)
    .then((response) => {
      console.log(response);
        let data = {};
        let rol = 1;
        let idRolUsuario = 0;

        //Check the JSON  sent by the API - Is bringing the 2 arrays or only one. 
        if (!('1' in response)) {
          data = response[0];
        } else {
          data = response[1];
        
          if (response[0].idUsuarioOtorgante) {
            rol = 2;
            idRolUsuario = response[0].idUsuarioOtorgante;
          } else {
            idRolUsuario = response[0].idUsuarioSolicitante;
          }
        }

        if (!ObjectValidator.ValidateFacebookRegister(data) || (data.loginEstado == '3' && !ObjectValidator.ValidateUserRegister(data))) {
          LoaderService.removeTimer();
          dispatch({ 
            type: 'CREATE_USER_FACEBOOK_FAILURE', 
            payload: 'Error'
          }); 

          dispatch({ 
            type: 'SHOW_MODAL',  
            modalType: 'alert',
            modalProps: {
              open: true,
              title: 'Alert Modal',
              message: 'Ups! Ha ocurrido un problema con los datos para el registro!',
            }
          });
        } else {
          LoaderService.removeTimer();
          dispatch({
            type: 'CREATE_USER_FACEBOOK_SUCCESS',
            payload: data
          });
          switch (data.loginEstado) {
            case '1':
              // Go to contract view
              Actions.contract();
              break;
            case '2':
              dispatch({
                type: SELECT_ROL,
                payload: rol
              });
              dispatch({
                type: SET_ROL_ID,
                payload: idRolUsuario
              });
  
              // Go to phone verify view
              Actions.phoneVerify(); 
              break;
            case '3':
              dispatch({
                type: 'SET_PHONE_NUMBER',
                payload: data.celular
              });
              dispatch({
                type: SELECT_ROL,
                payload: rol
              });
              dispatch({
                type: SET_ROL_ID,
                payload: idRolUsuario
              });
              dispatch({ 
                type: 'CREATE_USER_REGISTER_SUCCESS', 
                payload: data 
              });
    
              //Go to grantor or applicant view
              if (rol === 2) {
                Actions.layoutGrantor(); 
              } else {
                Actions.layoutApplicant(); 
              }
              
              break;
            default:
              createFacebookUser(obj, dispatch, auth);
          } 
          resolve(data); 
        } 
    })
    .catch((error) => { 
      createFacebookUser(obj, dispatch, auth);
      reject(error); 
    });
  });
};

const createFacebookUser = (data, dispatch, auth) => {

  //Create common variables
  const location = typeof data.location !== 'undefined' ? data.location : 'Bogota';
  const birthday = data.birthday == null ? null : new Date(data.birthday);
   
  const url = 'v1/crearUsuarioFacebook';
  //Create Array for API Request
  const userFacebook = {
    rangoEdad: data.age_range == null ? null : 
      data.age_range.min + '-' + data.age_range.max,
    genero: data.gender,
    locacion: location.name != null ? location.name : location,
    link: data.link, 
    facebookId: data.id,
    correo: data.email,
    fechaNacimiento: birthday == null ? null : birthday.toISOString().substr(0, 10),
    token: data.token,
    usuarioPerfil: {
			primerNombre: data.first_name,
			apellido: data.last_name,
			nombreCompleto: data.name,
			fotoPerfil: data.picture.data.url
		},
		dispositivosUsuario: auth.dispositivosUsuario
  };
  return new Promise((resolve, reject) => {
    APIService.PutAPI(url, userFacebook)
    .then((response) => {
      LoaderService.removeTimer();
      dispatch({
        type: 'CREATE_USER_FACEBOOK_SUCCESS',
        payload: response
      });
      Actions.contract();
      resolve(data); 
    })
    .catch((error) => {
      LoaderService.removeTimer();   
      dispatch({ 
        type: 'CREATE_USER_FACEBOOK_FAILURE', 
        payload: error
      });
      dispatch({ 
        type: 'SHOW_MODAL',  
        modalType: 'alert',
        modalProps: {
          open: true,
          title: 'Alert Modal',
          message: error,
        }
      });

      reject(error); 
    });
  });    
};

export const logOut = () => {
  return { type: CLEAR_DATA };
};

// State 1 AcceptContract
export const acceptContract = () => {
  return (dispatch) => {
    dispatch({
      type: ACCEPT_CONTRACT
    });
    Actions.roleSelection();
  };
};

// State 2 Select Role
export const selectRol = (rol, idUser) => {
  return (dispatch) => {
    dispatch({
      type: SELECT_ROL,
      payload: rol
    });
    registerUserRol(rol, idUser, dispatch);
  }; 
};

const registerUserRol = (rol, idUser, dispatch) => {
 
  let url = '';

  if (rol === 2) {
    url = 'v1/crearUsuarioOtorgante';
  } else {
    url = 'v1/crearUsuarioSolicitante';
  }

  const user = {
    usuario: { idUsuario: idUser }
  };
  
   
   
  return new Promise((resolve, reject) => {
    APIService.PutAPI(url, user)
    .then((data) => { 
       
       
        
        if (rol === 2) {
          dispatch({
            type: SET_ROL_ID,
            payload: data.idUsuarioOtorgante
          });
        } else {
          dispatch({
            type: SET_ROL_ID,
            payload: data.idUsuarioSolicitante
          });
        }
       
      Actions.phoneVerify(); 

      resolve(data); 
    })
    .catch((error) => { 
      dispatch({ 
        type: 'SHOW_MODAL',  
        modalType: 'alert',
        modalProps: {
          open: true,
          title: 'Alert Modal',
          message: error,
        }
      });
      reject(error); 
    });
  });
};


export const addImageForm = (image) => {
  return (dispatch) => {
    dispatch(arrayPush('UserForm', 'imagen', image));
  };
};

export const addBanksForm = (formBanks) => {
  return (dispatch) => {
    dispatch(arrayRemoveAll('UserForm', 'banks'));
    dispatch(arrayPush('UserForm', 'banks', formBanks));
  };
};

