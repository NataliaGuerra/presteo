import APIService from '.././services/APIService';
  
export const createInvestment = (formData, IdUserGrantor) => {
    return (dispatch) => {
        let invesment = '';
        if (formData.invesment !== undefined) {
            invesment = formData.invesment;
        }
        const url = 'v1/crearInversionUsuarioOtorgante';
        const invest = {
            idUsuarioOtorgante: IdUserGrantor,
            valorInversion: invesment,
            descripcionInversion: formData.Description
        };
        
        console.log(invest);
        return new Promise((resolve, reject) => {
          APIService.PutAPI(url, invest)
          .then((response) => {
              console.log(response);
              dispatch({ 
                type: 'SHOW_MODAL',  
                modalType: 'alert',
                modalProps: {
                  open: true,
                  icon: true,
                  title: 'Consulta éxitosa!',
                  message: response.message,
                }
              });
              resolve(response); 
          })
          .catch((error) => { 
            console.log(error);
            dispatch({ 
                type: 'SHOW_MODAL',  
                modalType: 'alert',
                modalProps: {
                  open: true,
                  title: 'Alert Modal',
                  message: error,
                }
              });
            reject(error); 
          });
        });      
    };
};
 
