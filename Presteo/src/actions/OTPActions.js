import axios from 'axios';
import { Actions } from 'react-native-router-flux';
import { 
    SET_CODE,
    NUMBER_VERIFIED_SUCCESS,
    NUMBER_VERIFIED_ERROR
} from './types';
import LoaderService from './../services/LoaderService';

export const sendMessage = (number, date) => {
  return (dispatch) => {
     
    dispatch({
      type: 'SET_PHONE_NUMBER',
      payload: { number: number.substring(2), date }
    });
    LoaderService.setTimer();
    dispatch({
      type: 'SEND_CODE_REQUEST'
    });
     const options = {
      method: 'POST',
      headers: { 
        //@TODO Change authorization with App
        Authorization: 'Basic c21hcnRsb2FuOjVtNFI3ITBhbg==', 
        'Content-Type': 'application/json'
      },
      data: {
        applicationId: '57AEF06A56A7304174C7763AF7154213',
        messageId: '51674C4E4B94F37842BDB19781740D17',
        from: 'Presteo',
        to: number
      },
      url: 'https://api.infobip.com/2fa/1/pin'
    };
    axios(options).then((response) => {
      LoaderService.removeTimer();
      dispatch({
        type: 'SEND_CODE_SUCCESS',
        payload: response.data
      });
      Actions.registerForm();
    }).catch((error) => {
      LoaderService.removeTimer();
      dispatch({
        type: 'SEND_CODE_FAILURE',
        payload: error
      }); 
      
       
    });
  };
};

export const reSendCode = (pinCode) => {
  return (dispatch) => {
    const options = {
      method: 'POST',
      headers: { 
        //@TODO Change authorization with App
        Authorization: 'Basic c21hcnRsb2FuOjVtNFI3ITBhbg==', 
        'Content-Type': 'application/json'
      },
      url: `https://api.infobip.com/2fa/1/pin/${pinCode}/resend`,
    };
    axios(options).then((response) => {
      LoaderService.removeTimer();
      dispatch({
        type: 'SEND_CODE_SUCCESS',
        payload: response.data
      });
    }).catch((error) => {
      LoaderService.removeTimer();
      dispatch({
        type: 'SEND_CODE_FAILURE',
        payload: error
      });
    });
  };
};

export const verifyCode = (verified) => {
  if (verified) {
    return {
      type: NUMBER_VERIFIED_SUCCESS,
      payload: true
    };
  } return {
    type: NUMBER_VERIFIED_ERROR,
    payload: false
  };
};

export const setCodeToVerify = (code) => {
  return {
    type: SET_CODE,
    payload: code
  };
};

export const verifyCodeRequest = () => {
  return {
    type: 'VERIFY_CODE_REQUEST'
  };
};
