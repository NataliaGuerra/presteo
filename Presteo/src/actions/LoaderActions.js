import { 
  DATA_GET_REQUEST,
  DATA_GET_SUCCESS,
  DATA_GET_FAILURE
} from './types';
import LoaderService from '../services/LoaderService';

export const dataRequest = (typeLoader = null) => {
  LoaderService.setTimer();
  return {
    type: DATA_GET_REQUEST,
    payload: typeLoader
  };
};
export const dataSuccess = () => {
  LoaderService.removeTimer();
  return {
    type: DATA_GET_SUCCESS
  };
};
export const dataFailure = (error) => {
  LoaderService.removeTimer();
  return {
    type: DATA_GET_FAILURE,
    payload: error
  };
};
