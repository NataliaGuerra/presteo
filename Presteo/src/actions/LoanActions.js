import { 
  SELECT_LOAN,
  SELECT_TIME,
  SELECT_MODALITY,
  UPDATE_LOAN_VALUE,
  SET_RECEIPT_FRONT_PHOTO,
  SET_RECEIPT_BACK_PHOTO,
  ADD_SUPPORT_IDENTITY,
  RESET_LOAN
} from './types';

export const selectLoan = (value, name) => {
  return {
    type: SELECT_LOAN,
    payload: { value, name }
  };
};
export const selectTime = (value, name) => {
  return {
    type: SELECT_TIME,
    payload: { value, name }
  };
};
export const selectModality = (value, name) => {
  return {
    type: SELECT_MODALITY,
    payload: { value, name }
  };
};
export const updateLoanValue = (value) => {
  return {
    type: UPDATE_LOAN_VALUE,
    payload: value
  };
};
export const setReceiptFrontPhoto = (value) => {
  return {
    type: SET_RECEIPT_FRONT_PHOTO,
    payload: value
  };
};
export const setReceiptBackPhoto = (value) => {
  return {
    type: SET_RECEIPT_BACK_PHOTO,
    payload: value
  };
};
export const addSupport = (value, index) => {
  return {
    type: ADD_SUPPORT_IDENTITY,
    payload: {
      value,
      index
    }
  };
};
export const resetLoan = () => {
  return {
    type: RESET_LOAN,
  };
};
