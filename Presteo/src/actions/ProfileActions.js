import { Actions } from 'react-native-router-flux';
import APIService from '.././services/APIService';
import LoaderService from './../services/LoaderService';

export const register = (formData, userData) => {
  LoaderService.setTimer();
  return (dispatch) => {
    dispatch({ type: 'CREATE_USER_REGISTER_REQUEST' });
   
    createUserRegister(formData, userData, dispatch);
  };
};

const createUserRegister = (formData, userData, dispatch) => {
  
    console.log(userData);
    //Create Array for API Request
    const userRegister = {
      idUsuario: userData.idUsuario,
      nombre: formData.first_name, 
      apellido: formData.last_name,
      correo: formData.email,
      celular: formData.celphone,
     /*  fechaNacimiento: userData.fechaNacimiento, */
     fechaNacimiento: "",
      numeroDocumento: formData.document,
      facebookId: userData.facebookId,
      tipoDocumento: {
        idTipoDocumento: formData.document_type
      },
      ciudad: {
        idCiudad: formData.city
      },
      direccion: formData.location,
      bancos: formData.banks[0],
      claveSeguridad: formData.password,
      codigoPostal: '000000',
     /*  link: userData.link,
      genero: userData.genero,
      rangoEdad: userData.rangoEdad, */
      link: "",
      genero: "",
      rangoEdad: "",
      locacion: formData.city,
      token: userData.token,
      usuarioPerfil: {
        idUsuarioPerfil: userData.usuarioPerfil.idUsuarioPerfil,
        fotoPerfil: formData.imagen[0]
			},
			dispositivosUsuario: userData.dispositivosUsuario
    };
    console.log(userRegister);
     
    const url = 'v1/crearUsuarioRegistro';
    return new Promise((resolve, reject) => {
    console.log(JSON.stringify(userRegister));
      APIService.PutAPI(url, userRegister)
      .then((response) => {
        LoaderService.removeTimer();
        resolve(response); 
        userRegister.pais = {
          idPais: formData.country
        };
        dispatch({ type: 'CREATE_USER_REGISTER_SUCCESS', payload: userRegister });
        if (userData.rol === 2) {
          Actions.reset('auth'); 
          Actions.replace('layoutGrantor'); 
        } else {
          Actions.reset('auth'); 
          Actions.replace('layoutApplicant');
        }
      })
      .catch((error) => {
        LoaderService.removeTimer();
        reject(error); 
        dispatch({ 
          type: 'CREATE_USER_REGISTER_FAILURE',
          payload: error
        });
        dispatch({ 
          type: 'SHOW_MODAL',  
          modalType: 'alert',
          modalProps: {
            open: true,
            title: 'Alert Modal',
            message: error,
          }
        });
      });
    });      
};


export const update = (formData, userData) => {
  return (dispatch) => {
    dispatch({ type: 'UPDATE_USER_REGISTER_REQUEST' });
   
    updateUserRegister(formData, userData, dispatch);
  };
};

const updateUserRegister = (formData, userData, dispatch) => {
  
    console.log(userData);
    console.log(formData);

    //Create Array for API Request
    const userRegister = {
      idUsuario: userData.idUsuario,
      nombre: formData.first_name, 
      apellido: formData.last_name,
      correo: formData.email,
      celular: formData.celphone,
      fechaNacimiento: '0000-00-00',
      numeroDocumento: formData.document,
      tipoDocumento: {
        idTipoDocumento: formData.document_type
      },
      ciudad: {
        idCiudad: formData.city
      },
      direccion: formData.location,
      bancos: formData.banks[0],
      claveSeguridad: formData.password,
      codigoPostal: '000000',
      link: userData.link,
      genero: userData.genero,
      rangoEdad: userData.rangoEdad,
      locacion: formData.city,
      usuarioPerfil: {
        idUsuarioPerfil: userData.usuarioPerfil.idUsuarioPerfil,
        fotoPerfil: formData.imagen[0]
      },
      dispositivosUsuario: userData.dispositivosUsuario
    };

    const url = 'v1/actualizarUsuario';
     
     
    return new Promise((resolve, reject) => {
      APIService.PutAPI(url, userRegister)
      .then((response) => {
          resolve(response); 
          userRegister.pais = {
            idPais: formData.country
          };
          dispatch({ type: 'UPDATE_USER_REGISTER_SUCCESS', payload: userRegister });
           
           
          dispatch({ 
            type: 'SHOW_MODAL',  
            modalType: 'success',
            modalProps: {
              open: true,
              title: 'Alert Modal',
              message: 'Perfil actualizado con éxito',
            }
          });
         /*  AlertService.showSuccess('Actualización', 'Perfil actualizado con éxito'); */
      })
      .catch((error) => { 
        reject(error); 
        dispatch({ 
          type: 'UPDATE_REGISTER_FAILURE',
          payload: error
        });
        dispatch({ 
          type: 'SHOW_MODAL',  
          modalType: 'alert',
          modalProps: {
            open: true,
            title: 'Alert Modal',
            message: error,
          }
        });
      });
    });      
};

export const setGuestState = (state) => {
  return (dispatch) => {
    dispatch({ type: 'GUEST_STATE', payload: state });
  };
};
