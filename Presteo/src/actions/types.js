/* AUTH */
export const SELECT_ROL = 'select_rol';
export const USER_DATA = 'user_data';
export const CLEAR_DATA = 'clear_data';
export const ACCEPT_CONTRACT = 'accept_contract';
export const SET_CODE = 'set_code';
export const NUMBER_VERIFIED_SUCCESS = 'number_verified_success';
export const NUMBER_VERIFIED_ERROR = 'number_verified_error';
export const GRANTOR_STATE = 'grantor_state';
export const SET_ROL_ID = 'set_rol_id';
export const REGISTER_DEVICE = 'register_devices';

/* CREATE LOAN */
export const SELECT_LOAN = 'select_loan';
export const SELECT_TIME = 'select_time';
export const SELECT_MODALITY = 'select_modality';
export const UPDATE_LOAN_VALUE = 'update_loan_value';
export const SET_RECEIPT_FRONT_PHOTO = 'set_receipt_front_photo';
export const SET_RECEIPT_BACK_PHOTO = 'set_receipt_back_photo';
export const ADD_SUPPORT_IDENTITY = 'add_support_identity';
export const RESET_LOAN = 'reset_loan';


/* REQUEST ACTIONS */
export const DATA_GET_REQUEST = 'DATA_GET_REQUEST';
export const DATA_GET_SUCCESS = 'DATA_GET_SUCCESS';
export const DATA_GET_FAILURE = 'DATA_GET_FAILURE';
export const DATA_GET = 'DATA_GET';

