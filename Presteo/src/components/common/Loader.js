import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Modal, View, Image } from 'react-native';
import { createLoadingSelector } from '../../reducers/Selector';
import { DATA_GET } from '../../actions/types';

const loanLoader = require('../../assets/images/loader.gif');
const commonLoader = require('../../assets/images/loader3.gif');

class Loader extends Component {
  render() {
    const { modalBackground, activityIndicatorWrapper } = styles;
    return (
      <Modal 
        visible={this.props.isFetching}  
        transparent animationType={'none'} 
        onRequestClose={() => {}} 
      >
        <View style={modalBackground}>
          <View style={activityIndicatorWrapper}>
          <Image 
            style={{ height: 250, width: 210 }} 
            source={this.props.gif == null ? commonLoader : loanLoader} 
          />
          </View>
        </View>
      </Modal>
    );
  }
}
const styles = {
  modalBackground: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000099',
    padding: 20
  },
  activityIndicatorWrapper: {
    backgroundColor: 'transparent',
    marginLeft: 20,
    marginRight: 20,
    borderRadius: 10,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around'
  }
};
/*
  * Se quitan estos elementos del arreglo 
  * para remover el loader inicial de autenticacion con facebook.
  * CREATE_USER_FACEBOOK
*/
const loadingSelector = createLoadingSelector(['SEND_CODE', DATA_GET, 'CREATE_USER_REGISTER']);
const mapStateToProps = (state) => ({ 
  isFetching: loadingSelector(state),
  gif: state.loader.gif
});
export default connect(mapStateToProps)(Loader);
