import React from 'react';
import { View } from 'react-native';
import { LoginButton } from 'react-native-fbsdk';

const FBLoginButton = ({ onLogin, onLogout, permissions }) => {
    const { viewStyle, defaultButtonStyle } = styles;
    return (
			<View style={viewStyle}>
				<LoginButton
					style={defaultButtonStyle}               
					readPermissions={permissions}
					onLoginFinished={onLogin}
					onLogoutFinished={onLogout}
				/>
			</View>
    );
};
const styles = {
	defaultButtonStyle: {
		backgroundColor: 'transparent',
		height: 20,
		width: 200,
		color: 'black',
	},
	viewStyle: {
		justifyContent: 'center',
		alignItems: 'center',
		height: 35,
		position: 'relative',
		backgroundColor: 'transparent',
		shadowColor: '#000',
		shadowOffset: { width: 2, height: 2 },
		backgroundColor: '#7c7c7c7'
	}
};
export { FBLoginButton };

