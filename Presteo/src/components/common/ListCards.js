
import React from 'react';
import { Content, Container, StyleProvider, H2 } from 'native-base';
import { RefreshControl } from 'react-native';
import { BackGround } from './BackGround';
import getTheme from '../../themes/components';
import platform from '../../themes/variables/platform';
import { h2Styles, contentStyles } from '../../assets/styles/component/common';


const ListCards = ({ children, title, onRefresh, refreshing }) => {
  return ( 
    <StyleProvider style={getTheme(platform)}>
      <Container>
          <BackGround>
            <H2 style={h2Styles} >{title}</H2>
            <Content 
             refreshControl={
              <RefreshControl
               refreshing={refreshing}
               onRefresh={onRefresh}
              />
             }
            style={contentStyles}
            >
              {children}
            </Content>
          </BackGround> 
      </Container>
    </StyleProvider>     
  );
};

export { ListCards };
