import React from 'react';
import { View } from 'react-native';
import { Icon, Title, Header as Head, Body, Left, Right, Thumbnail,
  StyleProvider } from 'native-base';
import { Actions } from 'react-native-router-flux';
import getTheme from '../../themes/components';
import platform from '../../themes/variables/platform';

const Header = ({ back, logo, roleView, children }) => {
  return (
    <StyleProvider style={getTheme(platform)}>
      <View>
        <View>
          <Head style={{ backgroundColor: '#2B2B2B' }}>
            <Left style={{ flex: 1 }}>
              { back && 
                <Icon 
                  name='ios-arrow-back' onPress={() => Actions.pop()} 
                  style={{ color: '#E4FF33', width: 40, height: 30 }} 
                />
              }
            </Left>
            <Body style={{ flex: 1 }}>
              <Title style={{ color: '#E4FF33' }}>P R E S T E O</Title>
            </Body>
            <Right style={{ flex: 1 }}>
            { logo &&  
              <Thumbnail
                style={{height: 45, width: 45, right: 60 }}
                source={require('../../assets/images/PresteoLogo.png')}
              /> 
            }
            </Right>
          </Head>
        </View>
        <View
          style={
            roleView ? {
              borderBottomColor: "#7c7c7c",
              borderBottomWidth: 50,
              backgroundColor: "#7c7c7c"
            } : 
            {
              borderBottomColor: 'gray',
              borderBottomWidth: 12,
          }}
        >
          {children}
        </View>
      </View>
    </StyleProvider>
  );
};

export { Header };
