import React from 'react';
import { Text, TouchableOpacity } from 'react-native';

const Button = ({ children, onPress, style }) => {
  const { textStyle, buttonStyle } = styles;
  return (
    <TouchableOpacity style={[buttonStyle, style]} onPress={onPress}>
      <Text style={textStyle}>
        {children}
      </Text>
    </TouchableOpacity>
  );
};

const styles = {
  textStyle: {
    alignSelf: 'center',
    color: '#fafdf6',
    fontSize: 18,
    fontWeight: '600',
    paddingTop: 10,
    paddingBottom: 10
  },
  buttonStyle: {
    alignSelf: 'center',
    borderRadius: 5,
    marginLeft: 5,
    marginRight: 5,
    marginTop: 20,
    width: 150,
  }
};


export { Button };
