import React from 'react';
import { Content, Card, CardItem, Text, Body, Button } from 'native-base';
import { View } from 'react-native';

const CardOne = () => {
    return (
      
        <Content padder>
          <Card>
            <CardItem header bordered>
              <Text>Prestamo 1</Text>
            </CardItem>
            <CardItem bordered>
              <Body>
                    <View style={{ alignItems: 'center', justifyContent: 'center', padding: 10, flex: 1, flexDirection: 'row' }} >
                       <View style={{ flex: 1 }}>
                            <Text>Ganacias:</Text>
                       </View>
                       <View style={{ flex: 1 }}>
                            <Text>$200.000</Text>
                       </View>
                    </View>
              </Body>
            </CardItem>
            <CardItem footer bordered>
                <View style={{ alignItems: 'center', justifyContent: 'center', padding: 10, flex: 1, flexDirection: 'row' }} >
                    <Button bordered>
                        <Text>Info</Text>
                    </Button>  
               </View>
            </CardItem>
          </Card>
        </Content>
   
    );
};

export { CardOne };

