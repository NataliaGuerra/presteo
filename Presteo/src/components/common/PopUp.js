import React from 'react';
import { View } from 'react-native';
import Modal from 'react-native-modal';
import { Text, Button } from 'native-base';

const PopUp = ({ children, onPress, closeFunction, visible, custom, buttonText }) => {
    const { textStyle, containerStyle, contentStyle } = styles;
    return (

        <Modal
          isVisible={visible}
          onBackdropPress={closeFunction}
          onSwipe={closeFunction}
          swipeDirection="left"
          style={containerStyle}
        >   
         
          {custom ? 
            <View style={{ height: 450 }}>
             <View style={{ flex: 1 }}>
              {children}
              </View>
           </View>
          :
          <View style={contentStyle}>
            <View>
              {children}
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
              <Button agree onPress={onPress}>
                {buttonText ? <Text style={textStyle}> {buttonText} </Text> : <Text>Aceptar</Text>}
              </Button>
            </View>
            
          </View>
          }
        </Modal>
     
    );
};

const styles = {
    containerStyle:{
      flex: 1, 
      flexDirection: 'column',
      justifyContent: 'center',
      padding: 15,
      alignItems: 'center'
    },
    contentStyle: {
      backgroundColor: '#ffffff', 
      borderRadius: 25,
      padding: 20
    },
  };

export { PopUp };

