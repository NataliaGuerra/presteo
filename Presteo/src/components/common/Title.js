import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

const Title = (props) => {
    const { viewStyle, textStyle } = styles;
    return (
        <View style={viewStyle}>
            <Text style={textStyle}>
                {props.titleText}
            </Text>
        </View>
    );
};

const styles = StyleSheet.create({
    viewStyle: {
      justifyContent: 'center',
      alignItems: 'center',
      height: 60,
      elevation: 2,
      position: 'relative',
      marginBottom: 15
    },
    textStyle: {
      fontSize: 25,
      fontFamily: 'Rubik-Medium',
      color: 'black'
    }
});
  
export { Title };
