import React from 'react';
import { View } from 'react-native';
import { Input } from 'native-base';

const SimpleInput = ({ onChangeText, keyboardType, style, placeholder, onBlur, onEndEditing }) => {
  const { inputStyle, containerStyle } = styles;
  return (
    <View style={containerStyle}>
        <Input 
          style={[inputStyle, style]} onChangeText={onChangeText} 
          keyboardType={keyboardType} placeholder={placeholder} 
          onBlur={onBlur} onEndEditing={onEndEditing}
        />
    </View>
  );
};

const styles = {
  inputStyle: {
    color: '#000',
    paddingRight: 5,
    paddingLeft: 5,
    fontSize: 18,
    lineHeight: 23,
    flex: 1
  },
  containerStyle: {
    height: 40,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center'
  }
};

export { SimpleInput };
