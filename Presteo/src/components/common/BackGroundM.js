import React from 'react';
import { ImageBackground, Dimensions } from 'react-native';
import { Container, StyleProvider, Content, H2 } from 'native-base';
import getTheme from '../../themes/components';
import platform from '../../themes/variables/platform';
import { h2Styles } from '../../assets/styles/component/common';

const bg = require('../../assets/images/YellowBackGround.png');

const BackGroundM = ({ children, container = false, title = false }) => {
    const { height, width } = Dimensions.get('window');
    return (
        <StyleProvider style={getTheme(platform)}>
            <Container>
                <ImageBackground
                    source={bg}
                    style={{ flex: 1, height, width }} 
                >
                    { title &&
                        <H2 style={h2Styles} >{title}</H2>
                    }
                    { container ? 
                        children
                    :
                        <Content>
                            {children}
                        </Content> 
                    }
                   
                </ImageBackground>
            </Container>
		</StyleProvider>
    );
};

export { BackGroundM };
