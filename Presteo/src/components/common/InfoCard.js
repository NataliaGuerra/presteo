
import React from 'react';
import { Container, StyleProvider } from 'native-base';
import { View } from 'react-native';
import { BackGround } from './BackGround';
import getTheme from '../../themes/components';
import platform from '../../themes/variables/platform';
import { centerRow } from '../../assets/styles/component/common';

const InfoCard = ({ children }) => {
  return ( 
    <StyleProvider style={getTheme(platform)}>
      <Container>
          <BackGround>
            <View style={centerRow}>
              {children}
            </View>
          </BackGround> 
      </Container>
    </StyleProvider>     
  );
};

export { InfoCard };
