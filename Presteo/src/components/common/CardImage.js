import React from 'react';
import { View, StyleSheet, Image } from 'react-native';
import { Thumbnail } from 'native-base';
import { scale, verticalScale } from '../../helpers/Scaler';
import Monedas5 from '../../assets/images/Monedas5.png';
import Monedas4 from '../../assets/images/Monedas4.png';
import Monedas3 from '../../assets/images/Monedas3.png';
import Monedas2 from '../../assets/images/Monedas2.png';
import Monedas1 from '../../assets/images/Monedas1.png';

const CardImage = ({ uri, rate = 5, large = true, showRate = true}) => {
	const { rateStyle, containerStyle,
		imageContainerStyle,
		rateContainerStyle, rateSmallStyle } = styles;

	return (
		<View style={containerStyle}>
			<View style={imageContainerStyle}>
				<Thumbnail 
					source={{ uri }} 
					large={large}
					style={{
						borderColor: '#B2C217', 
						borderWidth: 4
					}}
				/>
			</View>
			{showRate && 
				<View style={rateContainerStyle}>
				{Math.round(rate) >= 5 &&
				 <Image  
					style={large ? rateStyle : rateSmallStyle}
					source = {Monedas5}
					/>
				}
				{Math.round(rate) == 4 &&
					<Image  
						style={large ? rateStyle : rateSmallStyle}
						source = {Monedas4}
					/>
				}
				{Math.round(rate) == 3 &&
					<Image  
						style={large ? rateStyle : rateSmallStyle}
						source = {Monedas3}
					/>
				}
				{Math.round(rate) == 2 &&
					<Image  
						style={large ? rateStyle : rateSmallStyle}
						source = {Monedas2}
					/>
				}
				{Math.round(rate) <= 1 &&
					<Image  
						style={large ? rateStyle : rateSmallStyle}
						source = {Monedas1}
					/>
				}
			</View>
			}
		</View>
	);
}

const styles = StyleSheet.create({
	rateSmallStyle: {
		height: verticalScale(18),
		width: scale(90),
		resizeMode: 'stretch'
	},
	rateStyle: {
		height: verticalScale(20),
		width: scale(100),
		resizeMode: 'stretch'
	},
	containerStyle: {
		flex: 1, 
		flexDirection: 'column',
		justifyContent: 'center',
		alignContent: 'center',
	},
	imageContainerStyle: {
		flex: 3,
		flexDirection: 'column',
		justifyContent: 'flex-end',
		alignItems: 'center',
		padding: 4, 
	},
	rateContainerStyle: {
		flex: 1, 
		flexDirection: 'column',
		justifyContent: 'flex-start', 
		alignItems: 'center'
	}
});

export { CardImage };
