
import React from 'react';
import { View, ScrollView } from 'react-native';
import { Text, Button } from 'native-base';
import { BackGroundM } from './BackGroundM';

const Agreement = ({ title, onPress, onCancel, contract }) => {

        const { 
            viewContainerStyle, 
            scrollViewStyle, 
            scrollTextStyle, 
            sectionTitle, 
            sectionScroll,
            sectionScroll2,
            sectionsButtons,
            textButton,
            sizeButton,
        } = styles;
        return (
      
        <BackGroundM container>
          <View style={viewContainerStyle}>
            <View style={sectionTitle}>
              <Text style={{ textAlign: 'center', marginTop: 20, fontSize: 24 }}>{title}</Text>
            </View>
            
            <View style={sectionScroll}>
							<ScrollView 
								//removeClippedSubviews
								style={sectionScroll2}
								//showsVerticalScrollIndicator={false}
							>
								<View style={scrollViewStyle}>
									<Text style={scrollTextStyle}>
										{contract}
									</Text>
								</View>
							</ScrollView>
            </View>
            <View style={sectionsButtons}>

                  <Button style={[styles.blackButton, styles.sizeButton]} onPress={onPress}><Text style={textButton}>
                      Aceptar
                      </Text>
                  </Button>
                  <Button style={[sizeButton, { marginLeft: 30 }]} onPress={onCancel} ><Text ext style={textButton}>
                      Cancelar
                      </Text>
                  </Button>
              </View>

          </View> 
          </BackGroundM>
    
        );
};

const styles = {
    viewContainerStyle: {
        flex: 1, 
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
    },
    sectionTitle: {
      flex: 1, 
      justifyContent: 'center',
      alignItems: 'center',
      flowDirection: 'row',
      paddingLeft: 30,
      paddingRight: 30
    },
    sectionScroll: {
			flex: 3,
			padding: 30,
    },
    sectionScroll2: {
			borderRightWidth: 9,
			borderRightColor: 'gray',
			borderRadius: 10,
			backgroundColor: '#fafdf6',
			minWidth: '100%',
			minHeight: '100%',
			overflow: 'hidden',
    },
    sectionsButtons: {
        flex: 1, 
        flexDirection: 'row'
    },
    scrollViewStyle: {
      padding: 30,
    },
    scrollTextStyle: {
			textAlign: 'justify',
			fontSize: 18,
			color: 'gray',
			fontFamily: 'Rubik-Light',
    },
    blackButton: {
			backgroundColor: '#2b2b2b', 
			borderColor: '#fafdf6', 
			borderWidth: 3,
    },
    textButton: {
        fontSize: 18,
        justifyContent: 'center',
        alignItems: 'center',
    },
    sizeButton: {
        width: 118, 
        height: 40
    }
};

export { Agreement };
