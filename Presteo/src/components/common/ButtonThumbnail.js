import React from 'react';
import { TouchableOpacity } from 'react-native';

const ButtonThumbnail = ({ children, onPress, style }) => {
  const { buttonStyle } = styles;
  return (
    <TouchableOpacity style={[buttonStyle, style]} onPress={onPress}>
        {children}
    </TouchableOpacity>
  );
};

const styles = {
  buttonStyle: {
    alignSelf: 'center'
  }
};


export { ButtonThumbnail };
