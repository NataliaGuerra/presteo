import React from 'react';
import { Input as InputText, Label } from 'native-base';
import { View } from 'react-native';

const Input = (
  { 
    input: { onChange, ...restInput }, 
    meta: { touched, error }, ...inputProps 
  }) => {
     return (
      <View>
        <View >
          <InputText {...inputProps} onChangeText={onChange} {...restInput} />
         </View>
        {touched &&
         (error && 
              <Label style={{ color: '#ff0000', marginLeft: 20, fontSize: 15, marginBottom: 15 }}> * {error}</Label>
          )
        }
        </View>
  );
};

export { Input };
