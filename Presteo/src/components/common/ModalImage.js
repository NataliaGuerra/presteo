import React from 'react';
import Modal from 'react-native-modal';
import { View, Image, StyleSheet } from 'react-native';
import { Text, Button } from 'native-base';


const ModalImage = ({ visibility, url, acceptFunction }) => {
  const { buttonContainer, imageContainer, supportContainer } = styles;
  return (
    <Modal 
      isVisible={visibility}
      onBackdropPress={acceptFunction}
      onSwipe={acceptFunction}
      swipeDirection="left"
    >
      <View style={supportContainer}>
        <View style={imageContainer}>
          <Image
            style={{ flex: 1 }}
            resizeMode='stretch'
            source={{ uri: url }}
          /> 
        </View>
        <View style={buttonContainer}>
          <Button accept onPress={acceptFunction} >
            <Text>Aceptar</Text>
          </Button>
        </View>
      </View>
    </Modal>
  );
};
const styles = StyleSheet.create({
  supportContainer: {
    flex: 1, 
    flexDirection: 'column', 
    padding: 10,
    backgroundColor: 'white' 
  },
  imageContainer: {
    flex: 2,
    borderColor: 'gray'
  },
  buttonContainer: {
    flex: 0,
    flexDirection: 'row',
    padding: 10, 
    justifyContent: 'center',
    alignItems: 'center',
  }
}); 

export { ModalImage };
