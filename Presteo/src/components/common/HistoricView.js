import React from 'react';
import { TextMask } from 'react-native-masked-text';
import { View, StyleSheet, ScrollView } from 'react-native';
import { Item, Label, Text, Button, } from 'native-base';
import { CardImage } from './CardImage';
import { 
	textOpportunitiesCardStyles,
	labelOpportunitiesCardStyles,
	transparentCardItemsStyles
} from '../../assets/styles/component/cards';
import ImageService from '../../services/ImageService';

const HistoricView = ({ type = null, imageUrl, payments, user, acceptFunction }) => {
	 
	const { 
		containerStyle, 
		profileContainerStyle, 
		buttonContainerStyle, 
		photoContainerStyle, 
		titleContainer,
		descriptionContainerStyle,
		paymentsContainerStyle,
		paymentContainerStyle,
		dateContainerStyle,
		quaotaContainerStyle,
		textTitlteStyle,
		textStyle,
		scrollViewStyle,
		noPaymentsContainer,
		noPaymentText,
		blackButton
	} = styles;
  return (
		<View style={containerStyle}>
			<View style={[transparentCardItemsStyles, { flexDirection: 'column', flex: 1 }]}>
				<View style={titleContainer}>
					<Text>Historial</Text>
				</View>
				<View style={profileContainerStyle}>
					<View style={photoContainerStyle}>
						<CardImage 
							uri={ImageService.getUrlFormat(imageUrl)} 
							rate={user.calificacion == null ? 1 : user.calificacion }
						/>
					</View>
					<View	style={descriptionContainerStyle}>
						<Item style={{ borderBottomWidth: 0 }}>
							<Text style={labelOpportunitiesCardStyles}>{`${user != null ? user.usuario.nombre : 'Nicolas'} ${user != null ? user.usuario.apellido : 'Campuzano'}`}</Text>
						</Item>
						<Item style={{ borderBottomWidth: 0 }} last>
							<Label style={labelOpportunitiesCardStyles}>Tipo: </Label>
							<Text style={textOpportunitiesCardStyles}>{type == null ? -1 : type}</Text>
						</Item> 
					</View>
				</View>
				<View style={paymentsContainerStyle}>
					{payments != null && payments.length > 0 && 
					<ScrollView contentContainerStyle={scrollViewStyle}>
						{payments.map((element, index) => {
							if (element.pagosRealizados.length > 0) {
								return ( 
									element.pagosRealizados.map((payment) => {
										return(
											<View style={paymentContainerStyle} key={payment.idPagoRealizado}>
												<View style={quaotaContainerStyle}>
													<Text style={textTitlteStyle}>Cuota</Text>
													<Text style={textStyle}>Cuota {index+1}</Text>
													<Text style={textTitlteStyle}>Monto pagado</Text>
													<TextMask 
														type={'money'} 
														options={{ precision: 0, unit: '$', separator: ',', delimiter: '.' }}
														value={payment.montoPago}
														style={textStyle}
													/>
												</View>
												<View style={dateContainerStyle}>
													<Text style={textTitlteStyle}>Fecha planeada</Text>
													<Text style={textStyle}>{element.fechaPlaneada != null ?
														element.fechaPlaneada.substring(0, element.fechaPlaneada.indexOf('T')) : ''}
													</Text>
													<Text style={textTitlteStyle}>Fecha pago</Text>
													<Text style={textStyle}>{payment.fechaPago != null ?
														payment.fechaPago.substring(0, payment.fechaPago.indexOf('T')) : ''}
													</Text>
												</View>
												<View style={quaotaContainerStyle}>
													<Text style={textTitlteStyle}>Estado cuota</Text>
													<Text style={textStyle}>{element.estadoCuota != null ? element.estadoCuota.descripcion : '' }</Text>
													<Text style={textTitlteStyle}>Estado pago</Text>
													<Text style={textStyle}>{payment.estadoPago != null ? payment.estadoPago.nombre : '' }</Text>
												</View>
											</View>
										);
									})
								);
							}else{
								return (
									<View style={noPaymentsContainer}>
										<Text style={noPaymentText}>No hay pagos realizados</Text>
									</View>		
								);
							}
						})}
					</ScrollView>
					}
					{payments == null || !payments.length &&
						<View style={noPaymentsContainer}>
							<Text style={noPaymentText}>No hay pagos realizados</Text>
						</View>	
					}
				</View>
				<View style={buttonContainerStyle}>
					<Button style={blackButton} transparent onPress={acceptFunction}>
						<Text>Aceptar</Text>
					</Button>
				</View>
			</View>
		</View>
  );
};
const styles = StyleSheet.create({
	blackButton: {
		backgroundColor: '#2b2b2b', 
		borderColor: '#fafdf6', 
		borderWidth: 3,
	},
	noPaymentsContainer: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		flexDirection: 'column'
	},
	noPaymentText: {
		fontFamily: 'Rubik-Medium',
		fontSize: 15,

	},
	scrollViewStyle: {
		flexGrow: 1,
		
	},
  containerStyle: {
		flex: 1, 
		marginVertical: 15,
		borderRadius: 25,
		borderColor: '#636363',
		backgroundColor: '#fff',
		borderWidth: 3,
		marginHorizontal: 15,
	},
	titleContainer: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		flexDirection: 'column'
	},
  profileContainerStyle: {
    flex: 2,
		flexDirection: 'row',
		marginBottom: 10
	},
	photoContainerStyle: {
		flex: 2, 
	},
	descriptionContainerStyle: {
		flex: 3, 
		flexDirection: 'column', 
		justifyContent: 'center', 
		alignItems: 'flex-start'
	},
	paymentsContainerStyle: {
		flex: 6,
		paddingHorizontal: 10
	},	
  buttonContainerStyle: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
		justifyContent: 'center',
		padding: 10
	},
	paymentContainerStyle: {
		flexWrap: 'wrap',
		backgroundColor: '#f6f9f2',
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
		marginBottom: 10,
		borderBottomColor: '#7c7c7c',
		borderBottomWidth: 10
	},
	textTitlteStyle : {
		fontFamily: 'Rubik-light',
		fontSize: 11,
		color: '#7c7c7c'
	},
	textStyle : {
		fontFamily: 'Rubik-medium',
		fontSize: 13,
		fontWeight: 'bold',
		color: '#7c7c7c'
	},
	quaotaContainerStyle: {
		flex: 2,
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'center'
	},
	dateContainerStyle: {
		flex: 3,
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'center',
		borderLeftColor: '#7c7c7c',
		borderLeftWidth: 1,
		borderRightColor: '#7c7c7c',
		borderRightWidth: 1
	},
});
export { HistoricView };

