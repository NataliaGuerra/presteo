import React from 'react';
import { ImageBackground, Dimensions } from 'react-native';

const bg = require('../../assets/images/YellowBackGround.png');

const BackGround = ({ children }) => {
    const { height, width } = Dimensions.get('window');
    return (
        <ImageBackground
            source={bg}
            style={{ flex: 1, height, width }} 
        >
            {children}
        </ImageBackground>
    );
};

export { BackGround };
