import React from 'react';
import { View, TouchableWithoutFeedback, Platform, FlatList } from 'react-native';
import {
	Text,
	Icon,
	Content,
	Label, 
	Card,
	CardItem,
	Left, 
	Body, 
	Right,
	Button,
	Item,
	ListItem
} from 'native-base';
import ProgressBarAnimated from 'react-native-progress-bar-animated';
import { TextMask } from 'react-native-masked-text';
import { CardImage, ModalImage } from '../common';
import * as common from '../../assets/styles/component/common';
import * as cards from '../../assets/styles/component/cards';
import * as progress from '../../assets/styles/component/progress';

import { scale, verticalScale } from '../../helpers/Scaler';
import ImageService from '../../services/ImageService';

const RenderPenPays = ({ earnings, amount, time, type, title = null, img, qa = 0, large = false, onPress = null}) => {
  return (
		<Content>
			<Card transparent>
				{title != null && 
					<CardItem header style={cards.headerCardStyles}>
						<Body style={common.centerRow}>
							<Button title small>
								<Text style={cards.titleText}>{title}</Text>
							</Button>
						</Body>
					</CardItem>
				}
				<CardItem style={cards.bodyOpportunitiesCardStyles}>
					<Body style={cards.bodyRow}>
						<View style={cards.viewImageLeft}>
							<CardImage 
								uri={ ImageService.getUrlFormat(img)} 
								rate={qa == null ? 0 : qa}
								large={large}
							/>
						</View>
						<View 
							style={[{ 
								flex: 3, 
								flexDirection: 'column', 
								justifyContent: 'flex-start', 
								alignContent: 'flex-start'
							}, common.paddingTopiOS]}
						>
							<Item style={common.noBorderBottomWidth}> 
								<Label style={cards.labelOpportunitiesCardStyles}>Monto:</Label>
								<Text style={cards.textOpportunitiesCardStyles} >
									<TextMask 
										type={'money'} 
										options={{ precision: 0, unit: '$', separator: ',', delimiter: '.' }}
										value={amount === null ? -1 : amount}
									/>
								</Text>
							</Item>
							<Item style={common.noBorderBottomWidth}>
								<Label style={cards.labelOpportunitiesCardStyles}>Ganancia:</Label>
								<Text style={cards.textOpportunitiesCardStyles}>
									<TextMask 
											type={'money'} 
											options={{ precision: 0, unit: '$', separator: ',', delimiter: '.' }}
											value={earnings == null ? -1 : earnings}
									/>
								</Text>
							</Item>
							<Item style={common.noBorderBottomWidth}>
									<Label style={cards.labelOpportunitiesCardStyles}>Tiempo:</Label>
									<Text style={cards.textOpportunitiesCardStyles}>{time == null ? -1 : time}</Text>
							</Item>
							<Item style={common.noBorderBottomWidth} last>
									<Label style={cards.labelOpportunitiesCardStyles}>Tipo:</Label>
									<Text style={cards.textOpportunitiesCardStyles}>{type == null ? -1 : type}</Text>
							</Item>
						</View>
					</Body>
				</CardItem>
				{onPress != null && 
					<CardItem footer style={cards.footerCardStyles}>
						<Left />
						<Body />
						<Right>
							<Button style={cards.declineInfoButton} info onPress={onPress} bordered>
								<Text style={cards.declineTextInfoButton}>Info</Text>
							</Button>
						</Right>
					</CardItem>
				}
			</Card>
		</Content>
  );
};
const RenderInfoApplicant = ({ title, finished, canceled, total, later, maxAmount, minAmount }) => {
	return (
		<Content>
			<Card transparent>
				<CardItem header style={cards.headerCardStyles}>
					<Left style={common.centerRow}>
						<Button title small>
							<Text style={cards.titleText}>{title}</Text>
						</Button>
					</Left>
					<Body />
				</CardItem>
				<CardItem style={cards.bodyCardStyles}>
					<Body style={common.paddingTopiOS}>
						<Item style={cards.itemInfo}>
							<Label style={cards.labelInfo}>Préstamos finalizados:</Label>
							<View style={cards.blackTextInfoContainer}>
								<View style={cards.blackTextInfoBorderContainer }>
									<Text style={cards.blackTextInfo}>{finished == null ? -1 : finished}</Text>
								</View>
							</View>
						</Item>
						<Item style={cards.itemInfo}>
							<Label style={cards.labelInfo}>Préstamos cancelados:</Label>
							<View style={cards.blackTextInfoContainer}>
								<View style={cards.blackTextInfoBorderContainer}>
									<Text style={cards.blackTextInfo}>{canceled == null ? -1 : canceled}</Text>
								</View>
							</View>
						</Item>
						<Item style={cards.itemInfo}>
							<Label style={cards.labelInfo}>Préstamos totales:</Label>
							<View style={cards.blackTextInfoContainer}>
								<View style={cards.blackTextInfoBorderContainer}>
									<Text style={cards.blackTextInfo}>{total == null ? -1 : total}</Text>
								</View>
							</View>
						</Item>
						<Item style={cards.itemInfo}>
							<Label style={cards.labelInfo}>Préstamos de retraso:</Label>
							<View style={cards.blackTextInfoContainer}>
								<View style={cards.blackTextInfoBorderContainer}>
									<Text style={cards.blackTextInfo}>{later == null ? -1 : later}</Text>
								</View>
							</View>
						</Item>
						<Item style={cards.itemInfo}>
							<Label style={cards.labelInfo}>Monto máximo solicitado:</Label>
							<View style={cards.blackTextInfoContainer}>
								<View style={cards.blackTextInfoBorderContainer}>
									<TextMask 
		                type={'money'} 
		                options={{ precision: 0, unit: '$', separator: ',', delimiter: '.' }}
		                value={maxAmount === null ? -1 : maxAmount}
										style={cards.blackTextInfo}
		              />
								</View>
							</View>
						</Item>
						<Item style={cards.itemInfo}>
							<Label style={cards.labelInfo}>Monto mínimo solicitado:</Label>
							<View style={cards.blackTextInfoContainer}>
								<View style={cards.blackTextInfoBorderContainer}>
									<TextMask 
		                type={'money'} 
		                options={{ precision: 0, unit: '$', separator: ',', delimiter: '.' }}
		                value={minAmount === null ? -1 : minAmount}
										style={cards.blackTextInfo}
		              />
								</View>
							</View>
						</Item>
					</Body>
				</CardItem>
			</Card>
		</Content>
	);
};
const RenderSupportReceipt = ({ state, acceptFunction, onPress, visibility, url, title}) => {
	return (
		<Content>
			<Card transparent>
				<CardItem header style={cards.headerCardStyles}>
					<Left style={common.centerRow}>
						<Button title small>
							<Text style={cards.titleText}>{title}</Text>
						</Button>
					</Left>
					<Body />
				</CardItem>
				<CardItem style={cards.bodyCardStyles}>
					<Body style={[common.paddingTopiOS, common.centerColumn]}>
						<Item style={[common.noBorderBottomWidth, common.centerRow]}>
							<Label style={cards.labelOpportunitiesCardStyles}>Estado: </Label>
							<Text style={cards.textOpportunitiesCardStyles}>{state}</Text>
						</Item>
						<Item style={common.noBorderBottomWidth}>
							<Button
								onPress={onPress}
							  receipt
							>
								<Text style={cards.receiptText}>
									Ver soporte de factura
								</Text>
							</Button>
						</Item>
					</Body>
					<ModalImage 
						visibility={visibility}
						url={url == null ? '' : url}
						acceptFunction={acceptFunction}
					/>
				</CardItem>
			</Card>
		</Content>
	)
};

const RenderLatePays = ({ interestPercentage, capitalPercentage, img, expecDate, lateQuota, onPress, title, qa }) => {
    return (
        <Content>
          <Card transparent>
            <CardItem style={cards.bodyOpportunitiesCardStyles}>
             
                <Body style={cards.bodyRow}>
                <View style={cards.viewImageLeft}>
                    <CardImage 
                        uri={ ImageService.getUrlFormat(img)} 
                        rate={qa}
                    />
                </View>
                <View
                    style={{ 
                        flex: 3, 
                        flexDirection: 'column', 
                        justifyContent: 'flex-start', 
                        alignContent: 'flex-start'
                    }}
                >
                <Item style={{ borderBottomWidth: 0, marginBottom: 3, marginTop: 3}} >
								<View style={progress.progressViewStyles}>
									<ProgressBarAnimated
										{...progress.progressCustomStyles}
										height={15}
										width={scale(70)}
										value={capitalPercentage === null ? -1 : capitalPercentage}
										backgroundColorOnComplete="#E4FF33"
									/>
								</View>
								<View style={progress.progressViewTextStyle}>
									<Text style={progress.progressTextStyle}>
                                    {capitalPercentage === null ? 0 : capitalPercentage}% Capital
									</Text>
								</View>
							</Item>
							<Item style={{ borderBottomWidth: 0, marginBottom: 5 }} >
								<View style={progress.progressViewStyles}>
									<ProgressBarAnimated
										{...progress.progressCustomStyles}
										height={15}
										width={scale(70)}
										value={interestPercentage === null ? 0 : interestPercentage}
										backgroundColorOnComplete="#E4FF33"
									/>
								</View>
								<View style={progress.progressViewTextStyle}>
									<Text style={progress.progressTextStyle}>
                                    {interestPercentage === null ? 0 : interestPercentage}% Intereses
									</Text>
								</View>
							</Item>
                  
                    <Item style={common.noBorderBottomWidth} fixedLabel>
                        <Label style={cards.labelCardStyles}>Cuota atrasada:</Label>
                        <Text style={cards.textCardStyles} >{lateQuota != null ? lateQuota : -1 }</Text>
                    </Item>
                    <Item style={common.noBorderBottomWidth} fixedLabel last>
                        <Label style={cards.labelCardStyles}>Fecha esperada:</Label>
                        <Text style={cards.textCardStyles}>{expecDate != null ? expecDate.substr(0,10) : -1}</Text>
                    </Item>
                    </View>
                </Body>
            </CardItem>
            <CardItem footer style={cards.footerCardStyles}>
                <Left />
                <Body />
                <Right>
                    <Button style={cards.declineInfoButton} info onPress={onPress} small bordered>
                        <Text style={cards.declineTextInfoButton}>Info</Text>
                    </Button>
                </Right>
            </CardItem>
          </Card>
        </Content>
    );
};

const RenderActivePays = ({ interestPercentage, capitalPercentage, onPress, amount, time, type, title, img, qa }) => {
	return (
		<Content>
			<Card transparent>
				<CardItem style={cards.bodyOpportunitiesCardStyles}>
					<Body style={cards.bodyRow}>
						<View style={cards.viewImageLeft}>
							<CardImage 
								uri={ ImageService.getUrlFormat(img)} 
								rate={qa}
							/>
						</View>
						<View
							style={{ 
								flex: 3, 
								flexDirection: 'column', 
								justifyContent: 'flex-start', 
								alignContent: 'flex-start'
							}}
						>
							<Item style={common.noBorderBottomWidth} >
								<Label style={cards.labelActiveCardStyles}>Monto: </Label>
								<Text style={cards.textActiveCardStyles}>
									<TextMask 
										type={'money'} 
										options={{ precision: 0, unit: '$', separator: ',', delimiter: '.' }}
										value={amount === null ? -1 : amount}
									/>
								</Text>
							</Item>
							<Item style={common.noBorderBottomWidth} >
								<Label style={cards.labelActiveCardStyles}>Tiempo: </Label>
								<Text style={cards.textActiveCardStyles}>{time === null ? -1 : time }</Text>
							</Item>
							<Item style={common.noBorderBottomWidth} >
								<Label style={cards.labelActiveCardStyles}>Tipo: </Label>
								<Text style={cards.textActiveCardStyles}>{type === null ? -1 : type }</Text>
							</Item>
							<Item style={{ borderBottomWidth: 0, marginBottom: 3, marginTop: 3}} >
								<View style={progress.progressViewStyles}>
									<ProgressBarAnimated
										{...progress.progressCustomStyles}
										height={15}
										width={scale(70)}
										value={capitalPercentage === null ? -1 : capitalPercentage}
										backgroundColorOnComplete="#E4FF33"
									/>
								</View>
								<View style={progress.progressViewTextStyle}>
									<Text style={progress.progressTextStyle}>
										{capitalPercentage === null ? -1 : capitalPercentage}% Capital
									</Text>
								</View>
							</Item>
							<Item style={{ borderBottomWidth: 0, marginBottom: 5 }} >
								<View style={progress.progressViewStyles}>
									<ProgressBarAnimated
										{...progress.progressCustomStyles}
										height={15}
										width={scale(70)}
										value={interestPercentage === null ? 0 : interestPercentage}
										backgroundColorOnComplete="#E4FF33"
									/>
								</View>
								<View style={progress.progressViewTextStyle}>
									<Text style={progress.progressTextStyle}>
										{interestPercentage === null ? 0 : interestPercentage}% Intereses
									</Text>
								</View>
							</Item>
						</View>
					</Body>
				</CardItem>
				{onPress !== undefined &&
					<CardItem footer style={cards.footerCardStyles}>
						<Left />
						<Body />
						<Right>
							<Button style={cards.declineInfoButton} info onPress={onPress} small bordered>
								<Text style={cards.declineTextInfoButton}>Info</Text>
							</Button>
						</Right>
					</CardItem>
				}
			</Card>
		</Content>
	);
};

const RenderLoanOpportunities = ({ guest, img, qa, amount, earnings, time, type, onPress, onPressContract, onRejectLoan, title }) => {
	return (
		<Content style={{ padding: 0 }}>
			<Card transparent style={{ padding: 0 }}>
				<CardItem style={cards.bodyOpportunitiesCardStyles}>
					<Body style={cards.bodyRow}>
						<View style={cards.viewImageLeft}>
							<CardImage 
								uri={ ImageService.getUrlFormat(img)} 
								rate={qa}
							/>
						</View>
						<View 
							style={{ 
								flex: 3, 
								flexDirection: 'column', 
								justifyContent: 'flex-start', 
								alignContent: 'flex-start'
							}}
								
						>
							<Item style={common.noBorderBottomWidth}>
									<Label style={cards.labelOpportunitiesCardStyles}>Monto:</Label>
									<Text style={cards.textOpportunitiesCardStyles} >
										<TextMask 
											type={'money'} 
											options={{ precision: 0, unit: '$', separator: ',', delimiter: '.' }}
											value={amount === null ? -1 : amount}
										/>
									</Text>
								</Item>
							<Item style={common.noBorderBottomWidth}>
									<Label style={cards.labelOpportunitiesCardStyles}>Ganancia:</Label>
									<Text style={cards.textOpportunitiesCardStyles}>
											<TextMask 
													type={'money'} 
													options={{ precision: 0, unit: '$', separator: ',', delimiter: '.' }}
													value={earnings == null ? -1 : earnings}
											/>
									</Text>
							</Item>
							<Item style={common.noBorderBottomWidth}>
									<Label style={cards.labelOpportunitiesCardStyles}>Tiempo:</Label>
									<Text style={cards.textOpportunitiesCardStyles}>{time == null ? -1 : time}</Text>
							</Item>
							<Item style={common.noBorderBottomWidth} last>
									<Label style={cards.labelOpportunitiesCardStyles}>Tipo:</Label>
									<Text style={cards.textOpportunitiesCardStyles}>{type == null ? -1 : type}</Text>
							</Item>
							<TouchableWithoutFeedback onPress={onPress}>
								<Icon 
									type='FontAwesome' 
									name='angle-down' 
									style={cards.iconOpportunitiesStyle}
								/>
							</TouchableWithoutFeedback>
						</View>
					</Body>
				</CardItem>
				{guest !== 'INACTIVO' &&
					<CardItem footer style={cards.footerOpportunitiesCardStyles}>
						<Body style={{ flex: 1, flexDirection: 'row'}}>
							<View style={{ flex: 2 }} />
							<View style={{ flex: 3, flexDirection: 'row' }}>
								<Button 
									style={cards.blackButton} 
									onPress={onPressContract} 
									accept
								>
									<Text style={cards.textOpportunitiesButton}>Aceptar</Text> 
								</Button>
								{/* Button info onPress={onPress} small bordered> */}
								<Button style={cards.declineOpportunitiesButton} onPress={onRejectLoan} info small bordered>
									<Text style={cards.textOpportunitiesButton}>Rechazar</Text>
								</Button>
							</View>
						</Body>
					</CardItem>
				}
			</Card>
		</Content>
	);
};

const RenderInfoLoans = ({ title, state, capitalAmount, interestAmount, nextPayment, lastPayment, pendingPayments, paymentsMade, onPress, dayPast }) => {
	return (
		<Content>
              
			<Card transparent>
                <CardItem header style={cards.headerCardStyles}>
                    <Body style={{ marginLeft: 40 }}>
                        <Button title onPress={onPress} small>
                            <Text style={cards.titleText}>Credito 1</Text>
                        </Button>
                    </Body>
                </CardItem>
				<CardItem style={cards.bodyCardStyles}>
					<Body style={[common.paddingTopiOS, common.centerColumn]}>
						<Item style={[cards.itemInfo, { marginBottom: scale(4)}]}>
							<Label style={cards.labelActiveInfo}>Monto capital {capitalAmount === null ? 0 : capitalAmount}% </Label>
							<View style={cards.blackTextInfoContainer}>
								<View style={cards.progressViewInfoStyles}>
									<ProgressBarAnimated
										{...progress.progressCustomStyles}
										width={scale(90)}
										value={capitalAmount === null ? 0 : capitalAmount}
										backgroundColorOnComplete="#E4FF33"
									/>
								</View>
							</View>
						</Item>
						<Item style={[cards.itemInfo, { marginBottom: scale(4)}]}>
							<Label style={cards.labelActiveInfo}>Monto interes {interestAmount === null ? 0 : interestAmount}% </Label>
							<View style={cards.blackTextInfoContainer}>
								<View style={cards.progressViewInfoStyles}>
									<ProgressBarAnimated
										{...progress.progressCustomStyles}
										width={scale(90)}
										value={interestAmount === null ? 0 : interestAmount}
										backgroundColorOnComplete="#E4FF33"
									/>
								</View>
							</View>
						</Item>
						<Item style={cards.itemInfo}>
							<Label style={cards.labelActiveInfo}>Proximo Pago: </Label>
							<View style={cards.blackTextInfoContainer}>
								<View style={cards.blackTextInfoBorderContainer }>
									<Text style={cards.blackTextInfo}>{nextPayment === null ? -1 : nextPayment.substr(0,10) }</Text>
								</View>
							</View>
						</Item>
						<Item style={cards.itemInfo}>
							<Label style={cards.labelActiveInfo}>Últmo pago: </Label>
							<View style={cards.blackTextInfoContainer}>
								<View style={cards.blackTextInfoBorderContainer }>
									<Text style={cards.blackTextInfo}>{lastPayment === null ? 'Sin pagos' : lastPayment.substr(0,10) }</Text>
								</View>
							</View>
						</Item>
						<Item style={cards.itemInfo} >
							<Label style={cards.labelActiveInfo}>Pagos pendientes: </Label>
							<View style={cards.blackTextInfoContainer}>
								<View style={cards.blackTextInfoBorderContainer }>
									<Text style={cards.blackTextInfo} >{pendingPayments === null ? -1 : pendingPayments }</Text>
								</View>
							</View>
						</Item>
						<Item style={cards.itemInfo}>
							<Label style={cards.labelActiveInfo}>Pagos realizados: </Label>
							<View style={cards.blackTextInfoContainer}>
								<View style={cards.blackTextInfoBorderContainer }>
									<Text style={cards.blackTextInfo}>{paymentsMade === null ? -1 : paymentsMade }</Text>
								</View>
							</View>
						</Item>
						
						<Item style={{ borderBottomWidth: 0, paddingVertical: scale(6) }}>
							<Button onPress={onPress} historial>
								<Text style={cards.receiptText}>
									Historial de pagos
								</Text>
							</Button>
						</Item>

						{dayPast === undefined &&
							<Item style={cards.itemInfo}>
								<View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'flex-start'}}>
									<Label style={cards.statetitleText} >Estado: </Label>
									<Text style={cards.stateTextStyle}>{state === null ? -1 : state}</Text>
								</View>
							</Item>
						}
						{dayPast !== undefined && 
							<Item style={common.noBorderBottomWidth} fixedLabel last>
								<Text style={{ fontSize: 15 }}>Dias Mora: </Text>
								<Label style={{ fontSize: 15 }}>{dayPast != null ? dayPast : -1}</Label>
							</Item>
						}
					</Body>
				</CardItem>
			</Card>
		</Content>
	);
};

const RenderFinishedLoan = ({ earnings, invested, startDate, endDate, onPress }) => {
    return (
        <Content>
          <Card transparent>
          
            <CardItem style={cards.bodyCardMPStyles}>
                <Body style={cards.bodyCardItemStyles}>
                    <Item style={common.noBorderBottomWidth} fixedLabel>
                        <Label style={cards.labelCardItemStyles}>Ganancias:</Label>
                        <Label style={cards.textCardItemStyles} >
                            <TextMask 
                                type={'money'} 
                                options={{ precision: 0, unit: '$', separator: ',', delimiter: '.' }}
                                value={earnings === null ? -1 : earnings}
                            />
                        </Label>
                    </Item>
                    <Item style={common.noBorderBottomWidth} fixedLabel>
                        <Label style={cards.labelCardItemStyles}>Invertido:</Label>
                        <Label style={cards.textCardItemStyles} >
                            <TextMask 
                                type={'money'} 
                                options={{ precision: 0, unit: '$', separator: ',', delimiter: '.' }}
                                value={invested === null ? -1 : invested}
                            />
                        </Label>
                    </Item>
                    <Item style={common.noBorderBottomWidth} fixedLabel>
                        <Label style={cards.labelCardItemStyles}>Fecha Inicio:</Label>
                        <Label style={cards.textCardItemStyles} >{startDate != null ? startDate.substr(0,10) : -1}</Label>
                    </Item>
                    <Item style={common.noBorderBottomWidth} fixedLabel>
                        <Label style={cards.labelCardItemStyles}>Fecha Fin:</Label>
                        <Label style={cards.textCardItemStyles} >{endDate != null ? endDate.substr(0,10) : -1}</Label>
                    </Item>
                </Body>
            </CardItem>
            {onPress !== undefined &&
               <CardItem footer style={cards.footerCardStyles}>
                    <Left />
                    <Body />
                    <Right>
                        <Button style={cards.declineInfoButton} info onPress={onPress} small bordered>
                            <Text style={cards.declineTextInfoButton}>Info</Text>
                        </Button>
                    </Right>
                </CardItem>
            }
          </Card>
        </Content>
    );
};

const RenderSUFinishedLoan = ({ montoSolicitado, montoPagado, ultimoPago, onPress }) => {
    return (
        <Content>
          <Card transparent>
          
            <CardItem style={cards.bodyCardMPStyles}>
                <Body style={cards.bodyCardItemStyles}>
                    <Item style={common.noBorderBottomWidth} fixedLabel>
                        <Label style={cards.labelCardItemStyles}>Valor solicitado: </Label>
                        <Label style={cards.textCardItemStyles} >
                            <TextMask 
                                type={'money'} 
                                options={{ precision: 0, unit: '$', separator: ',', delimiter: '.' }}
                                value={montoSolicitado === null ? -1 : montoSolicitado}
                            />
                        </Label>
                    </Item>
                    <Item style={common.noBorderBottomWidth} fixedLabel>
                        <Label style={cards.labelCardItemStyles}>Valor pagado:</Label>
                        <Label style={cards.textCardItemStyles} >
                            <TextMask 
                                type={'money'} 
                                options={{ precision: 0, unit: '$', separator: ',', delimiter: '.' }}
                                value={montoPagado === null ? -1 : montoPagado}
                            />
                        </Label>
                    </Item>
                    <Item style={common.noBorderBottomWidth} fixedLabel>
                        <Label style={cards.labelCardItemStyles}>Último pago:</Label>
                        <Label style={cards.textCardItemStyles} >{ultimoPago != null ? ultimoPago.substr(0,10) : -1}</Label>
                    </Item>
                </Body>
            </CardItem>
            {onPress !== undefined &&
                <CardItem footer style={cards.footerCardStyles}>
                    <Left />
                    <Body />
                    <Right>
						<Button style={cards.declineInfoButton} info onPress={onPress} small bordered>
							<Text style={cards.declineTextInfoButton}>Info</Text>
						</Button>
                    </Right>
                </CardItem>
            }
          </Card>
        </Content>
    );
};

const RenderSummaryRequest = ({ title }) => {
    return (
        <Content>
            <Card>
                <CardItem header bordered>
                    <Left>
                        <Body>
                            <Text>
                               {title}
                            </Text>
                        </Body>
                    </Left>
                
                </CardItem>
                <CardItem bordered>
                    <Body style={{ flex: 1 }}>
                        <Item style={common.noBorderBottomWidth} fixedLabel>
                            <Label>Monto</Label>
                            <Text > $900.000</Text>
                        </Item>
                        <Item style={common.noBorderBottomWidth} fixedLabel last>
                            <Label>Modalidad de pago:</Label>
                            <Text> Mensual</Text>
                        </Item>
                        <Item style={common.noBorderBottomWidth} fixedLabel last>
                            <Label>Tiempo</Label>
                            <Text> 3 meses</Text>
                        </Item>
                        <Item style={common.noBorderBottomWidth} fixedLabel last>
                            <Label>Tipo</Label>
                            <Text>Servicios Públicos</Text>
                        </Item>
                        <Item style={common.noBorderBottomWidth} fixedLabel last>
                            <Label>Numero de cuotas</Label>
                            <Text>3</Text>
                        </Item>
                        <Item style={common.noBorderBottomWidth} fixedLabel last>
                            <Label>Días de pago</Label>
                            <Text> 5 de cada mes <Icon name="help-circle" /></Text> 
                        </Item>

                        <Item style={common.noBorderBottomWidth} fixedLabel last>
                            <Label>Documentos Adicionales</Label>
                            <Button small bordered>
                                <Text>Ver</Text>
                            </Button>
                        </Item>
                    </Body>
                </CardItem>
            </Card>
        </Content>
    );
};

const RenderPaymentPlan = ({ title, children }) => {
    return (
        <Content>
            <Card>
                <CardItem header bordered>
                    <Left>
                        <Body>
                            <Text>
                               {title}
                            </Text>
                        </Body>
                    </Left>
                
                </CardItem>
                <CardItem bordered>
                    <Left>
                        <Text>
                            Cuota
                        </Text>
                    </Left>
                    <Body>
                        <Text>
                            Total a pagar
                        </Text>
                    </Body>
                    <Right>
                        <Text>
                            Fecha de pago
                        </Text>
                    </Right>
                </CardItem>
                <CardItem bordered>
                    {children}
                </CardItem>
            </Card>
        </Content>
    );
};

const RenderPaymentPlanItems = () => {
    return (
        <CardItem bordered>
            <Left>
                <Text>
                    Cuota 1
                </Text>
            </Left>
            <Body>
                <Text>
                    $310.000
                </Text>
            </Body>
            <Right>
                <Text>
                    05/03/18
                </Text>
            </Right>
        </CardItem>    
    );
};


const RenderInfoFinished = ({ earnings, invested, startDate, endDate, img, fullName, loanType, onPress, applicant, qa }) => {
    return (
        <Content>
            <Card transparent>
            	<View style={cards.cardFinStyles}>
                <CardItem style={cards.transparentCardItemsStyles}>
                    <Left style={cards.leftCardItemStyles}>
                        <CardImage 
                            uri={ ImageService.getUrlFormat(img)} 
                            rate={qa}
						/>
                    </Left>
                    <Body style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                        <Item style={common.noBorderBottomWidth} fixedLabel>
                            <Label style={cards.labelFinStyles}>{fullName}</Label>
                        </Item>
                        <Item style={common.noBorderBottomWidth} fixedLabel last>
                            <Label style={cards.labelFinStyles}>{loanType}</Label>
                        </Item>
                    </Body>
                </CardItem>
                <CardItem style={cards.transparentCardItemsStyles}>
                    <Body style={{ flex: 1 }}>
                        <Item style={common.noBorderBottomWidth} fixedLabel>
                            <Label style={cards.labelFinStyles}>Fecha Inicio:</Label>
                            <Label style={cards.textFinStyles} >{startDate != null ? startDate.substr(0,10) : -1}</Label>
                        </Item>
                        <Item style={common.noBorderBottomWidth} fixedLabel>
                            <Label style={cards.labelFinStyles}>Fecha Fin:</Label>
                            <Label style={cards.textFinStyles} >{endDate != null ? endDate.substr(0,10) : -1}</Label>
                        </Item>
                        {
                            applicant ?
                            <Body>
                                <Item style={common.noBorderBottomWidth} fixedLabel>
                                    <Label style={cards.labelFinStyles}>Valor Solicitado:</Label>
                                    <Label style={cards.textFinStyles} >
                                        <TextMask 
                                            type={'money'} 
                                            options={{ precision: 0, unit: '$', separator: ',', delimiter: '.' }}
                                            value={invested === null ? -1 : invested}
                                        />
                                    </Label>
                                </Item>
                                <Item style={common.noBorderBottomWidth} fixedLabel>
                                    <Label style={cards.labelFinStyles}>Total Pagado:</Label>
                                    <Label style={cards.textFinStyles} >
                                        <TextMask 
                                            type={'money'} 
                                            options={{ precision: 0, unit: '$', separator: ',', delimiter: '.' }}
                                            value={earnings === null ? -1 : earnings}
                                        />
                                    </Label>
                                </Item>
                            </Body>
                      :
                            <Body>
                                <Item style={common.noBorderBottomWidth} fixedLabel>
                                    <Label style={cards.labelFinStyles}>Ganancias:</Label>
                                    <Label style={cards.textFinStyles} >
                                        <TextMask 
                                            type={'money'} 
                                            options={{ precision: 0, unit: '$', separator: ',', delimiter: '.' }}
                                            value={earnings === null ? -1 : earnings}
                                        />
                                    </Label>
                                </Item>
                                <Item style={common.noBorderBottomWidth} fixedLabel>
                                    <Label style={cards.labelFinStyles}>Invertido:</Label>
                                    <Label style={cards.textFinStyles} >
                                        <TextMask 
                                            type={'money'} 
                                            options={{ precision: 0, unit: '$', separator: ',', delimiter: '.' }}
                                            value={invested === null ? -1 : invested}
                                        />
                                    </Label>
                                </Item>
                            </Body>
                        }
                      
                        <Item style={common.noBorderBottomWidth}>
                        
                        <Body style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
                        <Button 
                        style={cards.buttonFinStyles}
                            info 
                            onPress={onPress} 
                            small 
                            bordered 
                        >
                            <Text style={butcards.textFinStyles}>Historial</Text>
                        </Button>
                        </Body>
                        </Item>
                    </Body>
                </CardItem>
                </View>
            </Card>
        </Content>
    );
};

const RenderGrantorSummary = ({ inversionActual, tasaInteresPromedio, saldoDisponible, addBalance, revBalance, addBalanceText }) => {
    return (
        <View>
            <Card transparent>
                <CardItem style={cards.bodyCardStyles}>
                    <Body style={{ flex: 2 }}>
                        <Item style={common.noBorderBottomWidth} fixedLabel last>
                            <Label style={cards.labelFinStyles}>Inversión Prestada:</Label>
                            <Text style={cards.labelFinStyles}>
                                <TextMask 
                                    type={'money'} 
                                    options={{ precision: 0, unit: '$', separator: ',', delimiter: '.' }}
                                    value={inversionActual === null ? 0 : inversionActual}
                                />
                            </Text>
                        </Item>
                        <Item style={common.noBorderBottomWidth} fixedLabel last>
                            <Label style={cards.labelFinStyles}>Tasa de interés (prom):</Label>
                            <Text style={cards.labelFinStyles}>{tasaInteresPromedio == null ? tasaInteresPromedio : 0 }</Text>
                        </Item>
                        <Item style={common.noBorderBottomWidth} fixedLabel last>
                            <Label style={cards.labelFinStyles}>Saldo disponible:</Label>
                            <Text style={cards.labelFinStyles}>
                                <TextMask 
                                    type={'money'} 
                                    options={{ precision: 0, unit: '$', separator: ',', delimiter: '.' }}
                                    value={saldoDisponible === null ? 0 : saldoDisponible}
                                />
                            </Text>
                        </Item>
                        <Item style={common.noBorderBottomWidth}>
                        
                        <Body style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
                        <Button 
                            info 
                            small 
                            onPress={addBalance}
                            style={{ backgroundColor: '#E4FF33', height: 40, borderColor: '#cdde33' }}
                            bordered 
                        >
                            <Text style={{ color: '#2b2b2b', fontSize: 16 }}>{addBalanceText}</Text>
                        </Button>
                        <Button 
                            info 
                            style={{ marginLeft: 10, height: 40 }}
                            small 
                            onPress={revBalance}
                            bordered 
                        >
                            <Text style={{ fontSize: 16 }}>Retirar Saldo</Text>
                        </Button>
                        </Body>
                        </Item>
                    </Body>
                </CardItem>
            </Card>
        </View>
    );
};

const RenderActiveLoansSummary = ({ finishedPress, processPress, activePress, processLoans, activeLoans, finishedLoans }) => {
    return (
        <Content>
            <Card transparent>
            <View style={cards.cardBottomStyles}>
                <CardItem style={{ borderBottomWidth: 3, borderColor: '#636363', borderTopLeftRadius: 10, borderTopRightRadius: 10, flex: 1, backgroundColor: '#636363' }}>
                    <Body style={{ flex: 1, justifyContent: 'center', alignContent: 'center', flexDirection: 'row' }}>
                        <Item style={common.noBorderBottomWidth} >
                            <Label style={{ fontSize: 18, color: '#fff', fontWeight: 'bold' }}>Préstamos activos</Label>
                        </Item>
                    </Body>
                </CardItem>
                <CardItem style={{ borderBottomWidth: 3, borderColor: '#636363' }}>
                    <Left style={{ flex: 5 }}>
                        <Item style={common.noBorderBottomWidth} >
                            <Label style={cards.labelFinStyles}>Préstamos en proceso</Label>
                        </Item>
                    </Left>
                    <Body style={{ flex: 1, justifyContent: 'center', alignContent: 'center', flexDirection: 'column' }}>
                        <Item style={common.noBorderBottomWidth} >
                            <Label style={cards.labelFinStyles}>{processLoans}</Label>
                        </Item>
                    </Body>
                    <Right style={{ flex: 1, justifyContent: 'center', alignContent: 'center', flexDirection: 'row' }}>
                        <Item style={common.noBorderBottomWidth} >
                            <TouchableWithoutFeedback onPress={processPress}> 
                                <Icon style={{ color: '#636363' }} name="ios-arrow-forward" />
                            </TouchableWithoutFeedback>
                        </Item>
                    </Right>
                </CardItem>
                <CardItem style={{ borderBottomWidth: 3, borderColor: '#636363' }}>
                    <Left style={{ flex: 5 }}>
                        <Item style={common.noBorderBottomWidth} >
                            <Label style={cards.labelFinStyles}>Préstamos activos en paz y salvo</Label>
                        </Item>
                    </Left>
                    <Body style={{ flex: 1, justifyContent: 'center', alignContent: 'center', flexDirection: 'column' }}>
                        <Item style={common.noBorderBottomWidth} >
                            <Label style={cards.labelFinStyles}>{activeLoans}</Label>
                        </Item>
                    </Body>
                    <Right style={{ flex: 1, justifyContent: 'center', alignContent: 'center', flexDirection: 'row' }}>
                        <Item style={common.noBorderBottomWidth} >
                            <TouchableWithoutFeedback onPress={activePress}> 
                                <Icon style={{ color: '#636363' }} name="ios-arrow-forward" />
                            </TouchableWithoutFeedback>
                        </Item>
                    </Right>
                </CardItem>
                <CardItem style={{ borderBottomWidth: 3, borderColor: '#636363' }}>
                    <Left style={{ flex: 5 }}>
                        <Item style={common.noBorderBottomWidth} >
                            <Label style={cards.labelFinStyles}>Préstamos Finalizados</Label>
                        </Item>
                    </Left>
                    <Body style={{ flex: 1, justifyContent: 'center', alignContent: 'center', flexDirection: 'column' }}>
                        <Item style={common.noBorderBottomWidth} >
                            <Label style={cards.labelFinStyles}>{finishedLoans}</Label>
                        </Item>
                    </Body>
                    <Right style={{ flex: 1, justifyContent: 'center', alignContent: 'center', flexDirection: 'row' }}>
                        <Item style={common.noBorderBottomWidth} >
                            <TouchableWithoutFeedback onPress={finishedPress}> 
                                <Icon style={{ color: '#636363' }} name="ios-arrow-forward" />
                            </TouchableWithoutFeedback>
                        </Item>
                    </Right>
                </CardItem>
                <View style={{ height: 130, paddingTop: 20 }}>

                    <View style={{ borderTopLeftRadius: 90, borderTopRightRadius: 90, height: 110, flex: 1, justifyContent: 'center', alignItems: 'center', flexDirection: 'column', backgroundColor: '#636363'}}>
                        <View style={common.noBorderBottomWidth} >
                            <TouchableWithoutFeedback onPress={finishedPress}> 
                                <Icon style={{ color: '#E4FF33' }} name="ios-arrow-up" />
                            </TouchableWithoutFeedback>
                        </View>
                        <View style={common.noBorderBottomWidth} >
                            <Label style={{ fontSize: 18, color: '#E4FF33' }}>Más Información</Label>
                        </View>
                    </View>
                </View>
            </View>
            </Card>
        </Content>
    );
};

const RenderMyLoans = ({ paymentPress = null, active = false, statusColor, monto, tiempoPrestamo, modalidad, fechaCreacion = '', nombreTipoPrestamo, nombreEstadoPrestamo, onPress })  => {
	return (
		<View>
			<View style={cards.ContentMyloans}>
				<View>
					<ListItem thumbnail>
						<Text style={cards.TextStyleMyloans}>Valor solicitado: </Text>
						<Text style={cards.TextBoldMyloans}>
							<TextMask
								type={'money'} 
								options={{ precision: 0, unit: '$', separator: ',', delimiter: '.' }}
								value={monto}
							/>
						</Text>
					</ListItem>
					<ListItem thumbnail>
						<Text style={cards.TextStyleMyloans}>Tiempo del crédito: </Text>
						<Text style={cards.TextBoldMyloans}>{tiempoPrestamo}</Text>
					</ListItem>
					<ListItem thumbnail>
						<Text style={cards.TextStyleMyloans}>Modalidad del pago: </Text>
						<Text style={cards.TextBoldMyloans}>{modalidad}</Text>
					</ListItem>
					<ListItem thumbnail>
						<Text style={cards.TextStyleMyloans}>Fecha de solicitud: </Text>
						<Text style={cards.TextBoldMyloans}>{fechaCreacion.substring(0, fechaCreacion.indexOf('T'))}</Text>
					</ListItem>
					<ListItem thumbnail>
						<Text style={cards.TextStyleMyloans}>Tipo de préstamo: </Text>
						<Text style={cards.TextBoldMyloans}>{nombreTipoPrestamo}</Text>
					</ListItem>
					<ListItem thumbnail>
						<Text style={cards.TextStyleMyloans}>Estado: </Text>
						<View style={{ borderRadius: 100, backgroundColor: statusColor, width: 15, height: 15, right: 20 }}>                      
					</View>
						<Text style={cards.TextBoldMyloans}>{nombreEstadoPrestamo}</Text>
					</ListItem>                   
				</View>
			</View>
			<View style={cards.MyLoansButtons}>
				{active && 
					<Button 
						style={cards.declineInfoButton} 
						onPress={paymentPress} 
					>
						<Text style={cards.declineTextInfoButton}>Pagar</Text>
					</Button>
				}
				<Button style={cards.declineInfoButton} onPress={onPress} >
					<Text style={cards.declineTextInfoButton}>Info</Text>
				</Button>
			</View>
		</View>
	);
}
const RenderSummaryInfo = ({ valueLoan, nameOfModality, nameOfTime, nameOfLoan, length, visibility, onPress, acceptFunction, url}) => {
	return (
		<Card transparent>
			<CardItem header style={cards.headerCardStyles}>
				<Left style={common.centerRow}>
					<Button title small>
						<Text style={cards.titleText}>Solicitud</Text>
					</Button>
				</Left>
				<Body />
			</CardItem>
			<CardItem style={cards.bodyOpportunitiesCardStyles}>
				<Body style={{ flex: 1, flexDirection: 'column' }}>
					<Item style={cards.itemInfo}>
						<Label style={cards.textStyle}>Valor: </Label>
						<Text style={cards.textInfoStyle}>
							<TextMask 
								type={'money'} 
								options={{ precision: 0, unit: '$', separator: ',', delimiter: '.' }}
								value={valueLoan}
							/>
						</Text>
					</Item>
					<Item style={cards.itemInfo}>
						<Label style={cards.textStyle}>Modalidad de pago: </Label>
						<Text style={cards.textInfoStyle}>{nameOfModality}</Text>
					</Item>
					<Item style={cards.itemInfo}>
						<Label style={cards.textStyle}>Tiempo: </Label>
						<Text style={cards.textInfoStyle}>{nameOfTime}</Text>
					</Item>
					<Item style={cards.itemInfo}>
						<Label style={cards.textStyle}>Tipo: </Label>
						<Text style={cards.textInfoStyle}>{nameOfLoan}</Text>
					</Item>
					<Item style={cards.itemInfo}>
						<Label style={[cards.labelInfo, cards.textStyle]}>Número de cuotas: </Label>
						<View style={cards.blackTextInfoContainer}>
							<View style={cards.blackTextInfoBorderContainer}>
								<Text style={cards.blackTextInfo}>{length}</Text>
							</View>
						</View>
					</Item>
			{/* 		<Item style={cards.itemInfo}>
						<Label style={[cards.labelInfo, textStyle]}>Dia pago: </Label>
						<View style={cards.blackTextInfoContainer}>
							<View style={cards.blackTextInfoBorderContainer }>
								<Text style={cards.blackTextInfo}>{length}</Text>
							</View>
						</View>
					</Item> */}
					<Item style={cards.itemInfo}>
						<Label style={[cards.labelInfo, cards.textStyle]}>Ver Factura: </Label>
						<View style={cards.blackTextInfoContainer}>
							<View style={cards.blackTextInfoBorderContainer}>
								<Text onPress={onPress} style={cards.blackTextInfo}>
									Ver
								</Text>
							</View>
						</View>
					</Item>
					<ModalImage 
						visibility={visibility}
						url={url == null ? '' : url}
						acceptFunction={acceptFunction}
					/>
				</Body>
			</CardItem>
		</Card>
	);
}
const RenderQuotas = ({ payments }) => {
	return (
		<View transparent>
			<CardItem header style={cards.headerCardStyles}>
				<Left style={common.centerRow}>
					<Button title small>
						<Text style={cards.titleText}>Plan de Pagos</Text>
					</Button>
				</Left>
				<Body />
			</CardItem>
			<View style={{ marginVertical: 2, marginHorizontal: 5 }}>
				<View 
					style={{
						borderRadius: Platform.Version == 24 ? null : 15,
						borderColor: '#636363',
						borderWidth: 3,
						marginRight: scale(20),
						marginLeft: scale(20),
						padding: 10,
						backgroundColor: 'white',
					}}
				>
					<View>
						<View style={{ flexDirection: 'row' }}>
							<View style={[cards.rowStyle, { flex: 1 }]}>
								<Text style={[cards.titleTextPayments, { color: 'black'}]} >Cuota</Text>
							</View>
							<View style={[cards.rowStyle, { flex: 2 }]}>
								<Text style={[cards.titleTextPayments, { color: 'black'}]}>Total a pagar</Text>
							</View>
							<View style={[cards.rowStyle, { flex: 2 }]}>
								<Text style={[cards.titleTextPayments, { color: 'black'}]}>Fecha de pago</Text>
							</View>
						</View>
						<FlatList 
							data={payments}
							renderItem={({ item, index }) =>
								<View style={{ flexDirection: 'row' }} key={index} >
									<View style={[cards.rowStyle, { flex: 1 }]}>
										<Text style={cards.titleTextPayments}>Cuota {index + 1}</Text>
									</View>
									<View style={[cards.rowStyle, { flex: 2 }]}>
										<Text style={cards.titleTextPayments}>
											<TextMask 
												type={'money'} 
												options={{ precision: 0, unit: '$', separator: ',', delimiter: '.' }}
												value={Math.trunc(item.montoPlaneado)}
											/>
										</Text>
									</View>
									<View style={[cards.rowStyle, { flex: 2 }]}>
										<Text style={cards.titleTextPayments}>
											{item.fechaPlaneada.substring(0, item.fechaPlaneada.indexOf('T')) }
										</Text>
									</View>
								</View>
							}
						/>
					</View>
				</View>
				
			</View>
		</View>
	);
}
export { 
	RenderQuotas,
	RenderSummaryInfo,
	RenderMyLoans,
	RenderPenPays, 
	RenderActivePays, 
	RenderLatePays, 
	RenderInfoLoans,
	RenderSummaryRequest,
	RenderGrantorSummary,
	RenderPaymentPlan,
	RenderPaymentPlanItems,
	RenderFinishedLoan,
	RenderSUFinishedLoan,
	RenderInfoFinished,
	RenderActiveLoansSummary,
	RenderLoanOpportunities,
	RenderInfoApplicant,
	RenderSupportReceipt
};
