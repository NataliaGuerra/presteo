import React from 'react';
import {
Text,
Icon,
Input,
ListItem,
Item,
InputGroup,
Form,
Content,
Label,
Left,
Right,
Radio,
Picker,	
} from 'native-base';
import { View, TouchableWithoutFeedback } from 'react-native';
import * as form from '../../assets/styles/component/form';
import * as common from '../../assets/styles/component/common';
//@COMPILE ...inputProps

	const renderInput = (
		{ 
			input: { onChange, ...restInput }, content = false,
			meta: { touched, error }, ...inputProps 
		}) => {
			if (content) {
				return (
					<Content>
						<View >
							<Input {...inputProps} onChangeText={onChange} {...restInput} />
						</View>
						{touched &&
						(error &&
							<Label style={form.labelInputError}> * {error}</Label>
						)
						}
						</Content>
				);
			} 
      return ( 
        <View style={{ flex: 1 }}>
          <View >
            <Input {...inputProps} onChangeText={onChange} {...restInput} />
            </View>
          {touched &&
            (error && 
                <Label style={form.labelInputError}> * {error}</Label>
            )
          }
          </View>
        );		 
    };
    
    //@COMPILE ...inputProps
const renderInputM = ({ 
    labelText, input: { onChange, ...restInput }, meta: { touched, error }, ...inputProps
}) => (
    <Form style={{ marginBottom: 20 }}>
        <Item fixedLabel last style={common.noBorderBottomWidth}>
            <Label style={form.inputMLabel}>{labelText}</Label>
            <Input style={form.inputM} {...inputProps} onChangeText={onChange} {...restInput} />
        </Item>
        {touched &&
        (error && 
            <Label 
                 style={form.labelInputErrorM}
            > 
                 * {error}
            </Label>
        )
        }
    </Form>
    
);
	

const renderTexbox = ({ 
    input: { onChange, ...restInput }, meta: { touched, error }, ...inputProps
}) => (
    <Form>
        <Item style={common.noBorderBottomWidth} >
          <Input style={form.texboxInput} {...inputProps} onChangeText={onChange} {...restInput} />
        </Item>
        {touched &&
        (error && 
            <Label 
                 style={form.labelInputError}
            > 
                 * {error}
            </Label>
        )
        }
    </Form>
);

const renderName = ({ input, placeholder }) => (
    <ListItem>
        <InputGroup iconRight>
            <Icon name="ios-person" />
            <Input placeholder={placeholder} {...input} />
        </InputGroup>
    </ListItem>
);

const renderNumber = ({ input, placeholder }) => (
    <ListItem>
        <InputGroup iconRight>
            <Icon name="ios-phone-portrait" />
            <Input placeholder={placeholder} {...input} />
        </InputGroup>
    </ListItem>
);

const renderPassword = ({ input, ...custom }) => (
    <ListItem>
        <InputGroup iconRight>
            <Icon name="ios-code-working" />
            <Input placeholder="Password" {...input} {...custom} />
        </InputGroup>
    </ListItem>
);

const renderCheckbox = ({ input, label, style, styleSelected, meta: { touched, error } }) => ( 
    <Content>
        
            <View 
                style={input.value ? styleSelected : style}
                
            >
            <TouchableWithoutFeedback onPress={() => input.onChange(!input.value)}> 
                <ListItem noBorder >
                    <Left>
                    
                        <Text style={input.value ? { fontSize: 17, color: '#E4FF33' } : { fontSize: 17 }} > {label}</Text>
                    </Left>
                    <Right >
                        <Radio
                            color={'#f0ad4e'}
                            selectedColor={'#5cb85c'}
                            style={{ height: 0, width: 0 }}
                            selected={input.value ? true : false}
                        />
                    </Right>
                </ListItem>
                </TouchableWithoutFeedback>
            </View>
        
        {touched &&
            (error && 
                <Label style={form.labelInputError}> * {error}</Label>
            )
        }
    </Content>

);

const renderSelect = ({ input, meta: { touched, error }, children, style, ...custom }) => (
    <Content>
        <View style={style}>
            <Picker 
                style={{ color: '#7c7c7c',borderTopWidth: 0, borderLeftWidth: 0, borderRightWidth: 0, borderBottomWidth: 0, width: '100%' }}
								{...input} 
								iosIcon={<Icon style={common.textGray} name="ios-arrow-down-outline" />}
              headerStyle={common.backgroundBlack}
              headerBackButtonTextStyle={common.textWhite}
							headerTitleStyle={common.textWhite}
							headerBackButtonText="Atras"
							textStyle={common.textGray}
                selectedValue={input.value} 
                onValueChange={(value) => input.onChange(value)} 
                children={children} {...custom} 
            />
        </View>
        {touched &&
        (error && 
            <Label style={form.labelInputError}> * {error}</Label>
        )
        }
    </Content>
);

const renderSelectCountry = ({ input, meta: { touched, error }, children, val, change, style, ...custom }) => (
    <Content>
        <View style={style}>
      
            <Picker 
            
                {...input} 
                style={{ color: '#7c7c7c', borderTopWidth: 0, borderLeftWidth: 0, borderRightWidth: 0, borderBottomWidth: 0, width: '100%'  }}
								selectedValue={val}
								iosIcon={<Icon style={common.textGray} name="ios-arrow-down-outline" />}
								headerStyle={common.backgroundBlack}
								headerBackButtonTextStyle={common.textWhite}
								headerBackButtonText="Atras"
								headerTitleStyle={common.textWhite} 
								textStyle={common.textGray}
                onValueChange={(value) => { input.onChange(value); change(value); }}
                children={children} {...custom} 
            />
        </View>
        {touched &&
        (error && 
            <Label style={form.labelInputError}> * {error}</Label>
        )
        }
    </Content>
);

export { 
	renderName, 
	renderNumber, 
	renderPassword, 
	renderCheckbox, 
	renderSelect,
    renderInput,
    renderInputM,
	renderTexbox,
	renderSelectCountry,
};
