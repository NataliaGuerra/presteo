import React, { Component } from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { View, Platform, StyleSheet, 
  Text, Vibration } from 'react-native';
import SmsListener from 'react-native-android-sms-listener';
import SMSVerifyCode from 'react-native-sms-verifycode';
import { setCodeToVerify, verifyCode, 
  reSendCode, verifyCodeRequest, register } from '../../actions';
import { Title, PopUp } from '../common';


class ModalPhoneVerify extends Component {

  state = {
    code: this.props.otp.code,
    resendCode: false,
    errorMessage: '',
    error: false,
    modalPhoneVisible: true
  }
  componentDidMount() {
    setTimeout(() => {
       
      if (this.props.otp.code != null ) {
        this.onInputCompleted(this.props.otp.code);
      } 
      const now  = new Date();
       
       
      let s = Math.round((((new Date() - this.props.otp.date) % 86400000) % 3600000) / 60000);
       
      /* if (s > 0) {
        this.props.register(this.props.form, this.props.auth); 
        this.showModalVerify();
      }; */
    }, 200);
  }

  onInputCompleted = (code) => {
    const options = {
      method: 'POST',
      headers: { 
        //@TODO Change authorization with App
        Authorization: 'Basic c21hcnRsb2FuOjVtNFI3ITBhbg==', 
        'Content-Type': 'application/json'
      },
      data: {
        pin: code
      },
      url: `https://api.infobip.com/2fa/1/pin/${this.props.otp.pinId}/verify`,
    };
    this.props.verifyCodeRequest();
    axios(options).then((response) => {
      if (response.data.verified) {
        this.props.verifyCode(true);
         
         
         
        this.props.register(this.props.form, this.props.auth); 
        this.showModalVerify();
      } else {
        this.setState({ error: true, 
          errorMessage: 'Este código no es correcto! Vuelve a intentar' });
        this.props.verifyCode(false);
        Vibration.vibrate(800);
      }
    }).catch((error) => {
       
      this.setState({ error: true, 
        errorMessage: 'Hemos tenido un problema, intenta de nuevo' });
        this.props.verifyCode(false);
      Vibration.vibrate(800);
    });
  }

  showModalVerify() {
    this.setState({ modalPhoneVisible: !this.state.modalPhoneVisible });
  } 
  sendCodeAgain() {
    this.setState({ resendCode: true });
    this.props.reSendCode(this.props.otp.pinId);
    if (Platform.OS === 'android') {
      const smsSuscription = SmsListener.addListener(message => {
        this.props.setCodeToVerify(message.body.match(/([\d]{5})/)[0]);
        smsSuscription.remove();
      });
    }
    setTimeout(() => {
      this.setState({ resendCode: false });
    }, 15000);
  }

  render() {
    return (
      <PopUp onPress={this.showModalVerify.bind(this)} closeFunction={() => this.showModalVerify()} visible={this.state.modalPhoneVisible} buttonText={'cancelar'} >
        <View>
          <View style={styles.textContainer}>
            <Title titleText="Verifica tu número" />
            <Text>Revisa tus mensajes de texto</Text>
            <Text>para encontrar el código</Text>
          </View>
          <SMSVerifyCode
            ref={ref => (this.verifycode = ref)}
            onInputCompleted={this.onInputCompleted}
            containerPaddingHorizontal={30}
            verifyCodeLength={6}
            autoFocus
            containerStyle={{ marginBottom: 20 }}
            onInputChangeText={() => {}}  
            initialCodes={
              this.props.otp.code === null ? [] : Array.from(this.props.otp.code.toString())
            }
          />
          {this.state.error &&
            <Text style={{ color: 'red', marginBottom: 50 }}>{this.state.errorMessage}</Text>
          }
          {this.state.resendCode ? 
          <Text>Revisa tú bandeja de nuevo</Text>
          : 
          <Text 
            style={{ borderBottomColor: 'gray', borderBottomWidth: 1 }} 
            onPress={this.sendCodeAgain.bind(this)}
          >No llega mi código de verificación</Text>
          }
        </View>
      </PopUp>
    );
  }
}
const styles = StyleSheet.create({
  textContainer: {
    flexDirection: 'column',
    alignItems: 'center',
    backgroundColor: '#FEFFFE',
    marginBottom: 50
  },
  modalBackground: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'
  },
  activityIndicatorWrapper: {
    backgroundColor: '#FFFFFF',
    height: 100,
    width: 100,
    borderRadius: 10,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around'
}
  
});
const mapStateToProps = state => {
  return { otp: state.otp, common: state.common, form: state.form.UserForm.values, auth: state.auth };
};

export default connect(mapStateToProps, 
  { setCodeToVerify, verifyCode, verifyCodeRequest, reSendCode, register })(ModalPhoneVerify);
