import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View } from 'react-native';
import UserForm from './UserForm';
import APIService from '../../services/APIService';

class RegisterUser extends Component {

  state = {
    banks: []
  };


  componentWillMount() {
    this.getData().then((data) => {
        this.setState({ banks: data });
    });
  }

   getData() {
    return new Promise((resolve, reject) => {
      const url = 'v1/buscarBancos';
      APIService.GetAPI(url)
      .then((data) => { resolve(data); })
      .catch((error) => { reject(error); });
    });
}
    
  render() { 
    const initData = {
        first_name: this.props.auth.usuarioPerfil.primerNombre,
        last_name: this.props.auth.usuarioPerfil.apellido,
        email: this.props.auth.correo,
        celphone: this.props.auth.celular,
        is_register: true,
        banks: this.state.banks
    };

    return (
      <View style={{flex:1}}>
        <UserForm initData={initData} buttonText={'Registrar'} isRegister />
      </View>
    );  
  }
}

const mapStateToProps = state => {
  return { auth: state.auth };
};

export default connect(mapStateToProps)(RegisterUser);
