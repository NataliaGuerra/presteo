import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View } from 'react-native';
import UserForm from './UserForm';
import APIService from '../../services/APIService';

class UpdateUser extends Component {
    
  state = {
    banks: []
  };

  componentWillMount() {
    this.getData().then((data) => {
        this.setState({ banks: data });
    });
  }

   getData() {
    return new Promise((resolve, reject) => {
      const url = 'v1/buscarBancos';
      APIService.GetAPI(url)
      .then((data) => { resolve(data); })
      .catch((error) => { reject(error); });
    });
}


  render() { 
    const initData = {
      first_name: this.props.auth.nombre,
      last_name: this.props.auth.apellido,
      email: this.props.auth.correo,
      document: this.props.auth.numeroDocumento,
      location: this.props.auth.direccion,
      celphone: this.props.auth.celular,
      document_type: this.props.auth.tipoDocumento,
      city: this.props.auth.ciudad,
      country: this.props.auth.pais,
      password: this.props.auth.claveSeguridad,
      is_register: false,
      banks: this.state.banks
  };

   for (let iState = 1; iState <= this.state.banks.length; iState++) {
      for (let iProps = 1; iProps <= this.props.auth.bancos.length; iProps++) {
       
       if (this.state.banks[iState - 1].idBanco === this.props.auth.bancos[iProps - 1].idBanco) {
          initData['bank'+iState] = true;
        } 
      }
    } 

    return (
      <View style={{flex:1}}>
        <UserForm initData={initData} buttonText={'Actualizar'} isRegister={false} />
      </View>
    );  
  }
}

const mapStateToProps = state => {
  return { auth: state.auth };
};

export default connect(mapStateToProps)(UpdateUser);
