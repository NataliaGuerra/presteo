import React, { Component } from 'react';
import {
  Text, View
} from 'react-native';


class ListTip extends Component {

  render() {
    const { descripcion } = this.props.tip;
    const { flexStyle, textStyle } = styles;

    return (
        <View style={flexStyle}>
            <Text style={textStyle}>
                {descripcion}
            </Text>
        </View>
    ); 
  }
}

const styles = {
	flexStyle: {
		justifyContent: 'center',
		alignItems: 'center',
		marginLeft: 30,
		marginRight: 30
	},
	textStyle: {
		textAlign: 'center',
		fontFamily: 'Rubik-Light',
		color: 'black',
		fontSize: 20,
	}
};

export default ListTip;
