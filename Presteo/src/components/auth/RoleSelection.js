import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Dimensions, ImageBackground } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { 
    Container, Body, Content, Thumbnail,
    Text, StyleProvider, Left, Right, CardItem, Card
} 
from 'native-base';
import { ButtonThumbnail, Header } from '../common';
import { selectRol } from '../../actions';
import getTheme from '../../themes/components';
import platform from '../../themes/variables/platform';
import { scale } from '../../helpers/Scaler';

class RoleSelection extends Component {

    onSelect(rol) {
        this.props.selectRol(rol, this.props.auth.idUsuario);
    }

    goBack() {
        Actions.pop();
    }

    render() {
        let {height, width} = Dimensions.get('window');
        const { cardApplicant, cardGrantor, imgApplicant, imgGrantor } = styles;
        return (
            <StyleProvider style={getTheme(platform)}>
                <Container>
                <Header back="true" roleView>
                    <Left style={{ flex: 1 }} />
                    <Body style={{ flex: 1 }}>
                        <Text
                        style={{ position: "absolute",
                        top: 10,
                        flexDirection: 'column',
                        color: "#fafdf6",
                        justifyContent: 'center',
                        alignItems: 'center' }}
                        >¿Qué quieres hacer?</Text>
                    </Body>
                    <Right style={{ flex: 1, alignItems: 'center' }} /> 
                </Header>
                <ImageBackground
                        source={require('../../assets/images/YellowBackGround.png')}
                        style={{ flex: 1, height, width }}>  
                    <Content contentContainerStyle={{ flex: 1, flexDirection: 'column', justifyContent: 'space-between' }}>
                        <Card transparent style={{ flex: 1}}>
                            <View style={cardApplicant}>
                            <CardItem cardBody>
                                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
                                <ButtonThumbnail onPress={this.onSelect.bind(this, 1)}>
                                    <Thumbnail
                                    style={imgApplicant}
                                    small
                                    source={require('../../assets/images/ApplicantRoleSection2.png')}
                                    />
                                    </ButtonThumbnail>
                                </View>
                            </CardItem>
                            <CardItem>
                                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
                                <ButtonThumbnail onPress={this.onSelect.bind(this, 1)}>
                                <Text style={{ textAlign: 'center', fontSize: 16 }}>Quiero Solicitar un préstamo!</Text>
                                </ButtonThumbnail>
                                </View>
                            </CardItem>
                            </View>
                        </Card>
                        <Card transparent style={{ flex: 1}}>
                            <View style={cardGrantor}>
                            <CardItem cardBody style={{ backgroundColor: 'transparent' }}>
                                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
                                <ButtonThumbnail onPress={this.onSelect.bind(this, 2)}>
                                    <Thumbnail
                                    style={imgGrantor}
                                        source={require('../../assets/images/GrantorRoleSection2.png')}
                                    />
                                    </ButtonThumbnail>
                                </View>
                            </CardItem>
                            <CardItem style={{ backgroundColor: 'transparent' }}>
                                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
                                <ButtonThumbnail onPress={this.onSelect.bind(this, 2)}>
                                <Text style={{ textAlign: 'center', fontSize: 16, color: '#e4ff33' }}>Quiero invertir!</Text>
                                </ButtonThumbnail>
                                </View>
                            </CardItem>
                            </View>
                            
                        </Card>
                    </Content>
                </ImageBackground>
                </Container>
            </StyleProvider>
        );
    }
}

const styles = {

    imgGrantor: {
        height: 80,
        width: 105,
    },

    imgApplicant: {
        height: 80,
        width: 105,
    },

    cardGrantor: {
        top: scale(20),
        borderRadius: 15,
        borderColor: '#636363',
        backgroundColor: '#7c7c7c',
        borderWidth: 3,
        marginRight: scale(60),
        marginLeft: scale(60),
        padding: scale(10)
    },
    cardApplicant: {
        top: scale(40),
        borderRadius: 15,
        borderColor: '#636363',
        backgroundColor: '#fff',
        borderWidth: 3,
        marginRight: scale(60),
        marginLeft: scale(60),
        padding: scale(10),
    },
};

const mapStateToProps = state => {
    return { auth: state.auth };
};

export default connect(mapStateToProps, { selectRol })(RoleSelection);
