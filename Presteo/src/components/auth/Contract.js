import { connect } from 'react-redux';
import { LoginManager } from 'react-native-fbsdk';
import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';
import { Agreement } from '../common';
import { 
  acceptContract, 
  logOut,
  dataRequest,
  dataSuccess,
  dataFailure,
} from '../../actions';
import APIService from '../../services/APIService';
import AlertService from '../../services/AlertService';

class Contract extends Component {
    state = { 
        contract: {  
            descripcion: ''
        }
    }

    componentWillMount() {
      //this.props.dataRequest();
      this.getData().then((response) => {
        //this.props.dataSuccess();
        this.setState({ contract: response });
      }).catch((error) => {
        //this.props.dataFailure(error);
      }); 
    }
        
    onCancel() {
      this.props.logOut();
      LoginManager.logOut();
      Actions.replace('register');
    }

    onAccept() {
      this.props.acceptContract(); 
    }

    getData() {
        return new Promise((resolve, reject) => {
          const url = 'v1/obtenerContratoTerm/';
          APIService.GetAPI(url)
          .then((data) => { resolve(data); })
          .catch((error) => { 
            reject(error);
            AlertService.showError();
          });
        });
    }

    render() {
        return (
            <Agreement 
                title={'Términos y Condiciones'}
                contract={this.state.contract.descripcion}
                onPress={this.onAccept.bind(this)}
                onCancel={this.onCancel.bind(this)}
            /> 
        );
    }
}

const mapStateToProps = state => {
    return { auth: state.auth };
};

const mapDispatchToProps = dispatch => ({
  dataRequest: () => dispatch(dataRequest()),
  dataSuccess: () => dispatch(dataSuccess()),
  dataFailure: () => dispatch(dataFailure()),
  acceptContract: () => dispatch(acceptContract()),
  logOut: () => dispatch(logOut()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Contract);
