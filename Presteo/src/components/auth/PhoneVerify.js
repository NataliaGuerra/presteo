import React, { Component } from 'react';
import { View, Alert, Platform, PermissionsAndroid, KeyboardAvoidingView   } from 'react-native';
import { Picker, Icon, 
  Container, Button, Input, 
  StyleProvider, Text, Content, Thumbnail
} from 'native-base';
import { connect } from 'react-redux';
import SmsListener from 'react-native-android-sms-listener';
import getTheme from '../../themes/components';
import { sendMessage, setCodeToVerify } from '../../actions';
import platform from '../../themes/variables/platform';

const logu = require('../../assets/images/GrayIcon.png');

class PhoneVerify extends Component {
  state = { 
    number: '', 
    imagePath: '', 
    imageWidth: 0, 
    imageHeight: 0, 
    numberToVerify: 0, 
    prefix: 57 
  };
  isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
  }
  sendSMS() {
    if (Platform.OS === 'android') {
      requestReadSmsPermission();
      const smsSuscription = SmsListener.addListener(message => {
         
        this.props.setCodeToVerify(message.body.match(/([\d]{5})/)[0]);
        smsSuscription.remove();
      });
    }
    if (this.isNumeric(this.state.numberToVerify) && 
      this.state.numberToVerify.toString().length == 10) {
      this.props.sendMessage(this.state.prefix + this.state.numberToVerify, new Date());
    } else {
      setTimeout(() => {
        Alert.alert(
          'Error',
          'Escriba un número valido',
          [{ text: 'Aceptar', 
            onPress: () => {}, 
            style: 'cancel' }],
          { cancelable: false }
        );
      }, 200);
    }
  }
  render() {
    const { containerStyle, sectionInputs, 
      sectionButton, sectionImage, inputStyle, pickerStyle, 
      sectionMiddleInputs, imageStyle } = styles;
    return (
      <StyleProvider style={getTheme(platform)}>
        <Container>
          <Content padder contentContainerStyle={containerStyle}>
            <View style={sectionInputs}>
              <View style={sectionMiddleInputs} >
                <View style={pickerStyle}>
                  <Picker
										style={{ borderTopWidth: 0, borderLeftWidth: 0, borderRightWidth: 0, borderBottomWidth: 0  }}
                    iosHeader='Prefijo'
                    selectedValue={this.state.prefix}
                    onValueChange={(itemValue) => this.setState({ prefix: itemValue })}
                    selectedValue={'57'}
                    placeholder='Prefijo'
                    iosIcon={<Icon name="ios-arrow-down-outline" />}
                  >
                    <Picker.Item label="+57" value="57" />
                  </Picker>
                </View>
                <View style={inputStyle}>
                  <Input
                  style={{ height: 35, top: 5, paddingTop: 5 }}
                    onChangeText={value => this.setState({ numberToVerify: value })}
                    keyboardType='numeric'
                  />
                </View>
              </View>
            </View>
            <View style={sectionButton}>
              <Button material onPress={this.sendSMS.bind(this)} style={{ height: 40, borderRadius: 7 }}>
                <Text style={{ fontSize: 18, fontFamily: 'Rubik-Medium' }}>Verificar</Text>
              </Button>
            </View>
						<View style={sectionImage}>
							<KeyboardAvoidingView>
								<Thumbnail style={imageStyle} source={logu} />
							</KeyboardAvoidingView>
						</View>
          </Content>
        </Container>
      </StyleProvider>
    );
  }
}
const styles = {
  imageStyle: {
    width: 80,
    height: 130,
    borderRadius: 0,
    overflow: 'visible'
  },
  titleStyle: {
    alignItems: 'center',
  },
  containerStyle: {
    justifyContent: 'center', 
    flex: 1, 
    flexDirection: 'column', 
    alignItems: 'center' 
  },
  sectionInputs: {
    justifyContent: 'center',
    flexDirection: 'column',
    paddingTop: 30,
  },
  sectionMiddleInputs: {
    width: '100%', 
    flexDirection: 'row',  
    justifyContent: 'center' 
  },
  sectionButton: {
    justifyContent: 'center',
    flexDirection: 'column', 
    flex: 0.7,
  },
  sectionImage: {
    justifyContent: 'center',
    flexDirection: 'column', 
    flex: 1,
  },
  inputStyle: {
    flex: 2,
    borderColor: '#7c7c7c',
    borderWidth: 3,
    borderRadius: 7,
    marginRight: 20,
    marginLeft: 15,
  },
  pickerStyle: {
    flex: 1,
    borderColor: '#7c7c7c',
    borderWidth: 3,
    borderRadius: 7,
    marginLeft: 20,
    height: 45,
    fontFamily: 'Rubik-Medium'
  }
};
const mapStateToProps = state => {
  return { otp: state.otp };
};

export default connect(mapStateToProps, { sendMessage, setCodeToVerify })(PhoneVerify);

async function requestReadSmsPermission() {
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.READ_SMS,{
        title: "Verificación automática",
        message: "Se necesita acceso a los SMS, para verificar automáticamente el celular."}
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
       
    } else {
       
    }
  } catch (err) {
    console.warn(err);
  }
}