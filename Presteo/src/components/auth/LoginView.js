import React, { Component } from 'react';
import { View, Alert, TouchableWithoutFeedback } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { Container, StyleProvider, Thumbnail } from 'native-base';
import { LoginManager } from 'react-native-fbsdk';
import { login } from '../../actions';
import getTheme from '../../themes/components';
import platform from '../../themes/variables/platform';
import NotificationService from '../../services/NotificationService';

const logu = require('../../assets/images/GrayIcon.png');
const fs = require('../../assets/images/LoginFacebookNew.png');

class LoginView extends Component {
	

  componentWillMount() {

   switch (this.props.auth.state) {
      case 0: break;
      case 1: Actions.replace('contract'); break;
      case 2: Actions.replace('roleSelection'); break; 
      case 3: Actions.replace('phoneVerify'); break; 
      case 4: 
        if (this.props.auth.rol === 2) {
          Actions.replace('layoutGrantor');
        } else {
          Actions.replace('layoutApplicant');
        }
        break; 
      default: Actions.auth({ type: 'reset' });
    } 
  }
  login() {
		LoginManager.logInWithReadPermissions(["public_profile", "email"])
		.then((result, error) => {
			if(error){
				setTimeout(() => {
					Alert.alert(
						'Error',
						'Inteta más tarde, tuvimos un error inesperado!',
						[{ text: 'Cancel', onPress: () => {}, style: 'cancel' }],
						{ cancelable: false }
					);
				}, 200);
			}
			if(result.isCancelled){
				setTimeout(() => {
					Alert.alert(
						'Disfruta de Presteo',
						'Tienes que registrarte con facebook para disfrutar de presteo',
						[{ text: 'Aceptar', 
							onPress: () => {}, 
							style: 'cancel' 
						}],
						{ cancelable: false }
					);
				}, 200);
			}else{
				this.props.login(this.props.auth);
			}
		});
  }
	logout() {}
	componentDidMount() {
		NotificationService.configure();
	}
  render() { 
		const { imageStyle, viewStyle, facebookIcon, facebookButton } = styles;
    return (     
      <StyleProvider style={getTheme(platform)} >
        <Container padder >
            <View style={viewStyle}>
              <View style={{ justifyContent: 'flex-end', alignItems: 'center', flex: 1, flexDirection: 'column' }}>
              <Thumbnail style={imageStyle} source={logu} />
              </View>
              <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1, flexDirection: 'row' }}>
								
								{/* <FBLoginButton
                  permissions={['public_profile email user_birthday user_age_range user_gender user_location user_link']}
                  onLogin={this.login.bind(this)}
                  onLogout={this.logout.bind(this)}
                /> */}
								<TouchableWithoutFeedback onPress={this.login.bind(this)}>
									<Thumbnail style={facebookIcon} large source={fs} />
								</TouchableWithoutFeedback>
              </View>
            </View>
        </Container>
      </StyleProvider>
    );  
  }
}
const styles = {
  imageStyle: {
    width: 140,
    height: 140,
    bottom: 20,
  },
  viewStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
		flexDirection: 'column',
		backgroundColor: '#fff'
  },

  facebookIcon: {
    width: 300,
    height: 90,
    position: 'absolute',

  },
};
const mapStateToProps = state => {
  return { auth: state.auth, common: state.common, loader: state.loader };
};
export default connect(mapStateToProps, { login })(LoginView);
