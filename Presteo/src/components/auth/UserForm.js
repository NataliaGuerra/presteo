import React, { Component } from 'react';
import { View, TouchableHighlight, ScrollView } from 'react-native';
import Swiper from 'react-native-swiper';
import ImagePicker from 'react-native-image-picker';
import { connect } from 'react-redux';
import { Field, reduxForm } from 'redux-form';
import { 
    Container, 
    Content, 
    Thumbnail, 
    Button, Text, 
    Spinner, 
    Picker, 
    StyleProvider,
    H2
} 
from 'native-base';
import Toast from 'react-native-easy-toast';

//Actions Method
import { 
    addBanksForm, 
    addImageForm, 
    update, 
    hideModal, 
    showModal, 
    dataRequest, 
    dataSuccess, 
    dataFailure 
} from '../../actions';

//Components
import { BackGround } from '.././common';
import * as renders from '.././render/renderComponents';

//Styles
import * as swiper from '../../assets/styles/component/swiper';
import * as common from '../../assets/styles/component/common';
import * as form from '../../assets/styles/component/form';
import getTheme from '../../themes/components';
import platform from '../../themes/variables/platform';

//Services
import APIService from '../../services/APIService';
import AWS3Service from '../../services/AWS3Service';

import { BUCKET_URL } from '../../config/variables';

import { 
    required, 
    email, 
    exactLength10,
    exactLength4, 
    number, 
    selectRequired,
    maxLength11,
    alphabetical 
} from '.././validation/validation';


let banksValidator = [];
let banksSelected = [];
let swiperObj = {};

class UserForm extends Component {
    
    state = {
        loading: false,
        loadingPsw: false,
        isCountrySelected: false,
        imagePath: this.props.auth.imagenPerfilPath, 
        countries: [],
        country: 'default',
        cities: [],
        banks: [],
        documentsTypes: [],
        modalPhoneVisible: false,
        modalUpdateVisible: false,
        modalPasswordVisible: false,
        image: [],
        password: this.props.auth.claveSeguridad
    };

    componentWillMount = async () => {
        this.props.dataRequest();
        try {
           const documents = await this.getData(0, null);
           const countries = await this.getData(2, null);
           banksValidator = this.props.initData.banks;
           const banks = await this.getData(1, null);
           this.setState({ banks, countries, documentsTypes: documents });
           this.handleInitialize();
           this.props.dataSuccess();
        } catch (err) {
            console.log(err);
            this.props.dataFailure(err);
        }   
    }

    componentWillUnmount() {
        this.props.reset();
    }
  
    //Value = idCountry
    onCountryChange(value) {
       
        if (value !== 'default') {
            this.getData(3, value).then((data) => {
                this.setState({ cities: data, isCountrySelected: true, country: value });
            });
        }
    }

    onSubmit(values) {
        const path = `perfil/${this.props.auth.idUsuario}/`;
        const file = this.setFile();
        let image = '';
        const form = values;
        form.banks = [];
        form.imagen = [];
        this.props.dataRequest();
      
        if (this.state.imagePath !== this.props.auth.imagenPerfilPath) {
          
            AWS3Service.UploadImage(file, path).then((data) => { 
                image = BUCKET_URL + data.postResponse.key;
                this.props.addImageForm(image); 
                    setTimeout(() => {
                        this.props.dataSuccess();
                        this.props.addBanksForm(banksSelected);
                        if (this.props.isRegister) {
                            this.popUpVerify();
                        } else {
                            form.banks.push(banksSelected);
                            form.imagen.push(image);
                            this.props.update(form, this.props.auth);
                        } 
                    }, 500);
            })
            .catch((error) => {  
                 
            });    
        } else {
            image = this.state.imagePath;
            
            this.props.addImageForm(image); 
            setTimeout(() => {
                this.props.addBanksForm(banksSelected);
                if (this.props.isRegister) {
                    this.popUpVerify();
                } else {
                    form.banks.push(banksSelected);
                    form.imagen.push(image);
                    this.props.update(form, this.props.auth);
                } 
                this.props.dataSuccess();
            }, 500);
        }
    }

    // i = Index 
    getData(i, value) {
        return new Promise((resolve, reject) => {
          let url = 'v1/buscarTipoDocumento';
          switch (i) {
            case 0: 
              url = 'v1/buscarTipoDocumento';
              break;
            case 1:
               url = 'v1/buscarBancos';
              break;
            case 2: 
              url = 'v1/listarPaises';
              break;
            case 3: 
              url = 'v1/listarCiudadesPais/' + value;
              break;
            default:           
               url = 'v1/buscarTipoDocumento';
          }
          APIService.GetAPI(url)
          .then((data) => { resolve(data); })
          .catch((error) => { reject(error); });
        });
    }

    setFile() {
        let file = {};
        if (this.state.image.length === 0) {
            file = {
                uri: this.state.imagePath,
                name: 'imagen-perfil',
                type: 'image/jpg'
            };
        } else {
            file = this.state.image;
        }
        return file;
    }

    getPhoto() {
        const options = {
            quality: 1.0,
            maxWidth: 520,
            maxHeight: 520,
            title: 'Seleccione una imagen',
            takePhotoButtonTitle: 'Tomar una foto',
            chooseFromLibraryButtonTitle: 'Obtener de la galería',
        };
        ImagePicker.showImagePicker(options, (response) => {
          if (!response.didCancel && !response.error) {
            this.setState({ 
              image: {
                uri: response.uri,
                name: response.fileName,
                type: response.type
              }, 
              imagePath: response.uri  
            });
          }
        });
    }

    closeModal(event) {
        this.props.hideModal();
    }
    
    handleInitialize() {
        console.log('Init data userform:');
        let initData = [];
        initData = this.props.initData;
        if (!this.props.isRegister) {
            this.onCountryChange(this.props.initData.country);
        } else {
            try {
                initData.document_type = this.state.documentsTypes.find(x => x.descripcion === 'C.C').idTipoDocumento;
            } catch (err) {
                console.log(err);
            }
        }
        this.props.initialize(initData);  
    }

    showModalVerify() {
        if (this.state.modalPhoneVisible) {
            this.setState({ modalPhoneVisible: !this.state.modalPhoneVisible });
        } 
        this.setState({ modalPhoneVisible: !this.state.modalPhoneVisible });
    } 

    popUpPassword() {
        this.props.showModal({
          open: true,
          title: 'Alert Modal',
          closeModal: this.closeModal.bind(this)
        }, 'password');
    }

    popUpVerify() {
        this.props.showModal({
            open: true,
            title: 'Alert Modal',
            closeModal: this.closeModal.bind(this)
        }, 'verify');
    }


    showModalPassword() {
        this.props.dataRequest();
        
        let image = '';
        if (this.state.imagePath !== this.props.auth.imagenPerfilPath) {
            const path = `perfil/${this.props.auth.idUsuario}/`;
            const file = this.setFile();
            AWS3Service.UploadImage(file, path).then((data) => { 
                image = BUCKET_URL + data.postResponse.key;
                this.props.addImageForm(image); 
                setTimeout(() => {
                    this.props.dataSuccess();
                    this.props.addBanksForm(banksSelected);
                    this.popUpPassword();
                }, 500);
            })
            .catch((error) => {  
                 
            });    
        } else {
            image = this.state.imagePath;
            this.props.addImageForm(image);
           
            setTimeout(() => {
                this.props.dataSuccess();
                this.props.addBanksForm(banksSelected);
                this.popUpPassword();
            }, 1000);
        }
    } 

    swiper(index){
        swiperObj.scrollBy(index);
    }

    renderCountries() {
        return this.state.countries.map(country =>
          <Picker.Item key={country.idPais} label={country.pais} value={country.idPais} />
        );
    }

    renderCities() {
        if (this.state.cities.length > 0) {
            return this.state.cities.map(city =>
                <Picker.Item key={city.idCiudad} label={city.nombreCiudad} value={city.idCiudad} />
            );
        } 
        return <Picker.Item label="Seleccione una ciudad" value="default" />;
    }

    renderBanks() {
        let indexBank = 1;
        banksValidator = this.state.banks;
        return this.state.banks.map(bank =>
            <Field style={form.checkBoxStyle} styleSelected={form.checkBoxSelectedStyle} key={bank.idBanco} name={`bank${indexBank++}`} component={renders.renderCheckbox} label={bank.nombre} />
        );
    }

    renderDocumentsTypes() {
        return this.state.documentsTypes.map(type =>
            <Picker.Item key={type.idTipoDocumento} label={type.descripcion} value={type.idTipoDocumento} />
          );
    }
    
    render() {
        const { handleSubmit } = this.props;
       
        return (
        <StyleProvider style={getTheme(platform)}>
            <View style={{ flex: 1 }}>
                <Container>
                    <BackGround>
                        <Swiper  
                            showsButtons={false}
                            ref={ref => { swiperObj = ref; }}
                            showsPagination={false}
                            scrollEnabled
                           /*  nextButton={<Icon style={{ color: 'gray', fontSize: 40 }} name='ios-arrow-forward' />}
                            prevButton={<Icon style={{ color: 'gray', fontSize: 40 }} name='ios-arrow-back' />} */
                            loop={false}
                        >
                            
                                <View style={swiper.slideAFormStyle}>
                                    <Content padder>
                                        <View 
                                            style={{ 
                                                alignItems: 'center', 
                                                justifyContent: 'center', 
                                                padding: 10, 
                                                flex: 1, 
                                                flexDirection: 'row' 
                                            }} 
                                        >
                                            <TouchableHighlight onPress={() => this.getPhoto()} badge vertical transparent light >
                                                <Thumbnail source={{ uri: this.state.imagePath }} style={{ height: 120, width: 120, borderColor: '#fff', borderWidth: 8, borderRadius: 60 }} />
                                            </TouchableHighlight>  
                                        </View> 
                                        <Field style={form.fieldFormStyle} name="first_name" validate={[required, alphabetical]} placeholder={'Nombres'} component={renders.renderInput} />
                                        <Field style={form.fieldFormStyle} name="last_name" validate={[required, alphabetical]} placeholder={'Apellidos'} component={renders.renderInput} />
                                        <Field style={form.fieldDisabledFormStyle} disabled name="email" validate={[required, email]} placeholder={'Email'} component={renders.renderInput} />
                                        <Field 
                                            style={this.props.isRegister ? form.fieldDisabledFormStyle : form.fieldFormStyle} 
                                            disabled={this.props.isRegister}
                                            name="celphone" 
                                            validate={[number, exactLength10]} 
                                            placeholder={'Celular'} 
                                            component={renders.renderInput} 
                                            keyboardType='phone-pad' 
                                        />   
                                        <View 
                                            style={{ 
                                                flex: 1,
                                                flexDirection: 'row',
                                                justifyContent: 'center',
                                                alignItems: 'center',
                                            }} 
                                        >
                                        <View style={{ flex: 1 }}>
                                        <Field 
                                            name="document_type" 
                                            mode="dropdown" 
                                            style={this.props.isRegister ? form.documentTypeStyle : form.documentTypeDisabledStyle}
                                            iosHeader='Documento'
                                            placeholder='Tipo de documento'
                                            validate={[required, selectRequired]}
                                            component={renders.renderSelect} 
                                            enabled={this.props.isRegister}
                                        >
                                            {this.renderDocumentsTypes()}
                                        </Field>
                                        </View>
                                        <View style={{ flex: 2 }}>
                                            <Field 
                                                style={this.props.isRegister ? form.documentStyle : form.documentDisabledStyle} 
                                                disabled={!this.props.isRegister} 
                                                name="document" 
                                                validate={[required, number, maxLength11]} 
                                                placeholder={'Número Documento'} 
                                                component={renders.renderInput} 
                                                keyboardType='phone-pad' 
                                            />
                                        </View> 
                                        </View> 
                                        { this.props.isRegister === false ? 
                                            <Field 
                                                style={form.fieldFormStyle}
                                                name="country" 
                                                mode="dropdown"  
                                                iosHeader='Seleccione su pais' 
                                                change={this.onCountryChange.bind(this)}
                                                placeholder='Seleccione su pais' 
                                                validate={[required, selectRequired]}
                                                val={this.state.country}
                                                component={renders.renderSelectCountry} 
                                            >
                                                {this.renderCountries()}
                                            </Field>  
                                            :
                                            <Field 
                                                style={form.fieldFormStyle}
                                                name="country" 
                                                mode="dropdown"  
                                                iosHeader='Seleccione su pais' 
                                                change={this.onCountryChange.bind(this)}
                                                placeholder='Seleccione su pais' 
                                                validate={[required, selectRequired]}
                                                val={this.state.country}
                                                component={renders.renderSelectCountry} 
                                            >
                                                <Picker.Item label="Seleccione un pais" value="default" />
                                                {this.renderCountries()}
                                            </Field>  
                                            
                                            }                                          
                                                       
                                        <Field 
                                            name="city" 
                                            mode="dropdown"
                                            style={form.fieldFormStyle} 
                                            iosHeader='Seleccione su ciudad'
                                            placeholder='Seleccione su ciudad'
                                            validate={[required, selectRequired]}
                                            enabled={this.state.isCountrySelected}
                                            component={renders.renderSelect} 
                                        >
                                            <Picker.Item label="Seleccione una ciudad" value="default" />
                                            {this.renderCities()}
                                        </Field>
                                        <Field style={form.fieldFormStyle} name="location" validate={[required]} placeholder={'Dirección'} component={renders.renderInput} />  
                                        {!this.props.isRegister ? 
                                         <View style={{ flex: 1 }}>
                                            <Button block bordered warning style={common.blackButtonStyle} onPress={this.showModalPassword.bind(this)}>
                                               {this.state.loadingPws ? <Spinner color='green' /> : <Text style={{ fontSize: 18 }} >Cambiar clave de seguridad</Text>}
                                            </Button> 
                                            <View style={{ height: 0, width: 0}}>        
                                                <Field
                                                    style={form.fieldFormStyle}
																										name="password" 
																										content
                                                    secureTextEntry
                                                    keyboardType='numeric'
                                                    validate={[required, exactLength4, number]} 
                                                    placeholder={'Dígite una clave de seguridad'} 
                                                    component={renders.renderInput} 
                                                />
                                            </View> 
                                         
                                         </View>
                                                      
                                        :
                                            <View>
                                               
                                                <Field
                                                    style={form.fieldFormStyle}
                                                    name="password" 
                                                    secureTextEntry
                                                    keyboardType='numeric'
                                                    onTouchStart={()=>{
                                                        this.refs.toast.show('Explicación clave de seguridad!');
                                                    }}
                                                    validate={[required, exactLength4, number]} 
                                                    placeholder={'Dígite una clave de seguridad'} 
                                                    component={renders.renderInput} 
                                                />
                                                <Field 
                                                    style={form.fieldFormStyle}
                                                    name="password_confirm" 
                                                    secureTextEntry
                                                    keyboardType='numeric'
                                                    validate={[required, exactLength4, number]} 
                                                    placeholder={'Confirme la clave de seguridad'} 
                                                    component={renders.renderInput} 
                                                />
                                            </View>
                                            
                                        }   

                                        <Toast
                                            ref="toast"
                                            position='top'
                                            fadeInDuration={750}
                                            positionValue={40}
                                            fadeOutDuration={1000}
                                            opacity={0.9}
                                        />
                                        <View style={{ height: 0, width: 0 }}>        
                                            <Field name="is_register" component={renders.renderCheckbox} />
                                        </View> 
                                        <View style={common.centerRow}>
                                            <Button primary onPress={this.swiper.bind(this, 1)}>
                                                <Text>Siguiente</Text>
                                            </Button>
                                        </View> 
                                    </Content>
                                </View>
                                <View style={swiper.slideAFormStyle}>
                                    
                                        <View style={[common.centerRow, { paddingLedf: 20, paddingRight: 20 }]}>
                                            <H2 style={common.h2Styles} >Selecciona tus Entidades Bancarias</H2>
                                        </View>
                                        <View style={{ flex: 2.6, paddingLedf: 20, paddingRight: 20 }}> 
                                            <ScrollView style={swiper.sectionScroll}>
                                                {this.renderBanks()}
                                            </ScrollView>
                                        </View>
                                        <View style={{ flex: 1.4, flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                                            <View style={common.centerRow}>
                                                <Button style={{ marginTop: 10, marginBottom: 10, marginRight: 15, width: 100, alignItems: 'center', justifyContent: 'center' }} primary onPress={this.swiper.bind(this, -1)}>
                                                    <Text style={{ textAlign: 'center' }}>Atras</Text>
                                                </Button>
                                                <Button style={{ marginTop: 10, marginBottom: 10 }} form onPress={handleSubmit(this.onSubmit.bind(this))}>
                                                    {this.state.loading ? <Spinner color='green' /> : <Text>{this.props.buttonText}</Text>}
                                                </Button>
                                            </View>
                                        </View>
                                        
                                </View>
                        </Swiper>
                    </BackGround>
                </Container>
            </View>
        </StyleProvider>
        );
    }
}

const mapStateToProps = state => {
    return { auth: state.auth, form: state.form };
};

const mapDispatchToProps = dispatch => ({
    dataRequest: () => dispatch(dataRequest()),
    dataSuccess: () => dispatch(dataSuccess()),
    dataFailure: () => dispatch(dataFailure()),
    addBanksForm: (banks) => dispatch(addBanksForm(banks)),
    addImageForm: (image) => dispatch(addImageForm(image)),
    update: (form, auth) => dispatch(update(form, auth)),
    hideModal: () => dispatch(hideModal()),
    showModal: (modalProps, modalType) => {
      dispatch(showModal({ modalProps, modalType }));
    }
});

UserForm = connect(
    mapStateToProps, mapDispatchToProps
)(UserForm);

export default reduxForm({

    form: 'UserForm',
    validate: values => {
      const errors = {};
      const password = values.password;
      const password_confirm = values.password_confirm;
      const is_register = values.is_register;
      let bank_error = true;
      banksSelected = [];
        for (let i = 1; i <= banksValidator.length; i++) {
          if (values['bank' + i] !== undefined && values['bank' + i] !== false){
            bank_error = false;
            banksSelected.push({'idBanco' : banksValidator[i - 1].idBanco });
          }
        }

        if (bank_error) {
          errors.bank1 = 'Debe seleccionar por lo menos un banco';
        }
        if (is_register && password !== password_confirm) {
          errors.password_confirm = 'Las credenciales no coinciden';
        }
   
        return errors;
    },
    onSubmitFail: (e) => {
        swiperObj.scrollBy(-1); // shows undefined
    },
    
  })(UserForm);
