import React, { Component } from 'react';
import { View, FlatList, ImageBackground, Dimensions, Image } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import ListTip from './auth/ListTip';
import { Title } from './common';
import APIService from '../services/APIService';

const bg = require('../assets/images/YellowBackGround.png');
const presteo = require('../assets/images/PresteoBigLogo.png');


class SplashScreen extends Component {
    state = { tips: [] }

    componentWillMount() {
       
      this.getData(0).then((data) => {
        this.setState({ tips: data });
      });
    }

    componentDidMount() {
      setTimeout(() => { 
        Actions.auth({ type: 'reset' });       
      }, 4000);
    }

    getData() {
      return new Promise((resolve, reject) => {
        const url = 'v1/obtenerConsejo';
        APIService.GetAPI(url)
        .then((data) => { resolve(data); })
        .catch((error) => { reject(error); });
      });
    }

    renderTips(tip) {
      return <ListTip tip={tip.item} />;
    }

    render() {
        const { height, width } = Dimensions.get('window');
        const { 
            viewContainerStyle, 
            titleStyke,
            flatListStyle

        } = styles;

        return ( 
            <View style={viewContainerStyle}>
                    <ImageBackground
                    source={bg}
                    style={{ flex: 1, height: height, width: width }}>
                <View style={titleStyke}>
                    <Image
                      style={{ height: 200, width: 100 }}
                      source={presteo}
                    />
                    <Title titleText={'P R E S T E O'} />
                </View>
                <View style={flatListStyle}>
                    <FlatList 
                        keyExtractor={tip => tip.descripcion}
                        renderItem={this.renderTips}
                        data={this.state.tips}
                    />
                </View>
                </ImageBackground>
            </View>
        );
    }
}


const styles = {
  viewContainerStyle: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  titleStyke: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center'
  },
  flatListStyle: {
    flex: 1,
    justifyContent: 'flex-start',
  }
};
const mapStateToProps = state => {
  return { auth: state.auth };
};

export default connect(mapStateToProps)(SplashScreen);
