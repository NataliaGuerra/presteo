import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import { Content, Text, StyleProvider, Container, H2, Button,} from 'native-base';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { dataRequest, dataSuccess, dataFailure, showModal, hideModal } from '../../actions';
import APIService from '../../services/APIService';
import ImageService from '../../services/ImageService';
import { BackGround } from '../common';
import getTheme from '../../themes/components';
import platform from '../../themes/variables/platform';
import { scale } from '../../helpers/Scaler';
import { RenderSummaryInfo, RenderQuotas} from './../render/renderCards';

class SummaryLoanAccepted extends Component {
  
  state = {
		modalVisible: false,
		loan: {
			modalidadPago: {
				modalidad: '',
			},
      tipoPrestamo: {
				nombre: '',
			},
      soporteFactura: {
				urlSoporte: '',
        estadoSoporte: {
					
				}
			},
			tiempoPrestamo: {
				duracion: '',
			},
      comprobantesDesembolso: [{
        urlDescripcion: '',
        estadoComprobante: {
          descripcion: ''
        }
			}],
			cuotasPlaneadas: []
		}
  }

  componentWillMount() {
		let url = `v1/obtenerSolicitudPrestamo/idPrestamo=${this.props.loanId}`;
		this.props.dataRequest();
    APIService.GetAPI(url).then((data) => {
			this.setState({ loan: data });
			this.props.dataSuccess();
    }).catch((error) => {
			this.props.dataFailure(error);
		});
  }


  render() {
		const { titleContainer, requestContainer, paymentsContainer,
			buttonsContainer, titleTextStyle, 
			buttonNext, acceptButtonStyle } = styles;
		const { loan } = this.state;
    return (
			<StyleProvider style={getTheme(platform)}>
				<Container>        
					<BackGround>
						<Content>
							<View style={titleContainer}>
								<H2 style={titleTextStyle}>Resumen solicitud</H2>
							</View>
							<View style={requestContainer}>
								<RenderSummaryInfo 
									valueLoan={loan.monto}
									nameOfModality={loan.modalidadPago.modalidad}
									nameOfTime={loan.tiempoPrestamo.duracion}
									nameOfLoan={loan.tipoPrestamo.nombre}
									length={loan.cuotasPlaneadas.length}
									visibility={this.state.modalVisible}
									onPress={() => this.setState({ modalVisible: true })}
									url={ImageService.getUrlFormat(loan.soporteFactura.urlSoporte)}
									acceptFunction={() => this.setState({ modalVisible: false })}
								/>
							</View>
							
							<View style={paymentsContainer}>
								<RenderQuotas 
									payments={loan.cuotasPlaneadas}
								/>
							</View>
							<View style={buttonsContainer}>
								<View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center' }}>
									<Button onPress={() => Actions.applicantHome()} navigation style={buttonNext}> 
										<Text>Rechazar</Text>
									</Button>
								</View>
								<View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center'}}>
									<Button onPress={() => Actions.applicantAgreement({ loanId: this.props.loanId, user: this.props.user })} navigation style={acceptButtonStyle} accept>
										<Text>Confirmar</Text>
									</Button>
								</View>
							</View>
						</Content>
					</BackGround>
				</Container>
			</StyleProvider>
    );
  }
}

const styles = StyleSheet.create({
	titleContainer: {
		flex: 2,
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'center'
	},
	buttonNext: {
    paddingVertical: 0
	},
	acceptButtonStyle: {
		color: 'white',	
		backgroundColor: '#2b2b2b', 
		borderColor: '#fafdf6', 
		borderWidth: 3,
	},
	requestContainer: {
		flex: 6,
	},
	paymentsContainer: {
    flex: 4,
	},
	buttonsContainer: {
		flex: 2,
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
		paddingHorizontal: scale(25),
		marginVertical: scale(10),
	},
	titleTextStyle: {
		paddingHorizontal: scale(30),
		fontFamily: 'Rubik-Medium',
		fontWeight: 'bold'
	}
});

const mapStateToProps = state => {
  return { auth: state.auth };
};
const mapDispatchToProps = dispatch => ({
  dataRequest: () => dispatch(dataRequest()),
  dataSuccess: () => dispatch(dataSuccess()),
  dataFailure: () => dispatch(dataFailure()),
  hideModal: () => dispatch(hideModal()),
  showModal: (modalProps, modalType) => {
    dispatch(showModal({ modalProps, modalType }));
  }
});
export default connect(mapStateToProps, mapDispatchToProps)(SummaryLoanAccepted);
