import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { Agreement } from '../common';
import APIService from '../../services/APIService';
import { dataRequest, dataSuccess, dataFailure,
  hideModal, showModal } from '../../actions';

class Contract extends Component {
  state = { 
    contract: {  
      descripcion: ''
    }
  }

  componentWillMount() {
    this.getData().then((response) => {
      this.setState({ contract: response });
    }); 
  }
      
  onCancel() {
    Actions.applicantHome();
  }

  onAccept() {
    const url = `v1/asignacionContratoAceptacionSolicitante/idPrestamo=${this.props.loanId}`;
     
    this.props.dataRequest();
    APIService.GetAPI(url).then((data) => {
       
      this.props.showModal({
        open: true,
        backdrop: false,
        userName: this.props.user,
        closeModal: () => {
          this.props.hideModal();
          Actions.applicantHome();
        }
      }, 'contract');
      this.props.dataSuccess();
    }).catch((error) => {
       
      this.props.dataFailure(error);
    });
  }

  getData() {
    return new Promise((resolve, reject) => {
      const url = 'v1/obtenerContratoAceptarPrestamo/';
      APIService.GetAPI(url)
      .then((data) => { resolve(data); })
      .catch((error) => { 
        reject(error);
      });
    });
  }

  render() {
    return (
      <Agreement 
        title={'Contrato'}
        contract={this.state.contract.descripcion}
        onPress={this.onAccept.bind(this)}
        onCancel={this.onCancel.bind(this)}
      /> 
    );
  }
}

const mapStateToProps = state => {
    return { auth: state.auth };
};
const mapDispatchToProps = dispatch => ({
  dataRequest: () => dispatch(dataRequest()),
  dataSuccess: () => dispatch(dataSuccess()),
  dataFailure: () => dispatch(dataFailure()),
  hideModal: () => dispatch(hideModal()),
  showModal: (modalProps, modalType) => {
    dispatch(showModal({ modalProps, modalType }));
  }
});
export default connect(mapStateToProps, mapDispatchToProps)(Contract);
