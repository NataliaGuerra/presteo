import { connect } from 'react-redux';
import { LoginManager } from 'react-native-fbsdk';
import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';
import { Agreement } from '../common';
import { acceptContract, logOut } from '../../actions';
import APIService from '../../services/APIService';
import AlertService from '../../services/AlertService';

class Contract extends Component {
    state = { 
        contract: {  
            descripcion: ''
        }
    }

    componentWillMount() {
        this.getData().then((response) => {
          this.setState({ contract: response });
        }); 
      }
        
    onCancel() {
      this.props.logOut();
      LoginManager.logOut();
      Actions.replace('register');
    }

    onAccept() {
      this.props.acceptContract();
    }

    getData() {
        return new Promise((resolve, reject) => {
          const url = 'v1/obtenerContratoTerm/';
          APIService.GetAPI(url)
          .then((data) => { resolve(data); })
          .catch((error) => { 
            reject(error);
            AlertService.showError();
          });
        });
    }

    render() {
        return (
            <Agreement 
                title={'Contrato'}
                contract={this.state.contract.descripcion}
                onPress={this.onAccept.bind(this)}
                onCancel={this.onCancel.bind(this)}
            /> 
        );
    }
}

const mapStateToProps = state => {
    return { auth: state.auth };
};

export default connect(mapStateToProps, { acceptContract, logOut })(Contract);
