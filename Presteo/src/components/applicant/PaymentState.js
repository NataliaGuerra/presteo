import React, { Component } from 'react';
import {  
    Text, List, ListItem, Icon,
    Button, Form, Item, Label
} 
from 'native-base';
import { View, TouchableWithoutFeedback, UIManager, Platform, LayoutAnimation } from 'react-native';
import { TextMask } from 'react-native-masked-text';
import { connect } from 'react-redux';
import ProgressBarAnimated from 'react-native-progress-bar-animated';
import { progressCustomStyles } from '../../assets/styles/component/progress';

import { Actions } from 'react-native-router-flux';
import { hideModal, showModal } from '../../actions';

class PaymentState extends Component {

  constructor() {
    super();

    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental && 
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
  }

  state = {
      information: false   
  };

  closeModal() {
    this.props.hideModal();
  }

  showModal() {
     this.props.showModal({
      open: true,
      title: 'Alert Modal',
      message: 'Funcionalidad en Proceso!',
      closeModal: this.closeModal.bind(this)
    }, 'maintenance'); 
    /* this.props.showModal({
      open: true,
      title: 'Alert Modal',
      closeModal: this.closeModal.bind(this)
    }, 'verify'); */
  }

  seeMore() {
    // Uncomment to animate the next state change.
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);

    // Or use a Custom Layout Animation
    //LayoutAnimation.configureNext(CustomLayoutAnimation);

    this.setState({ information: !this.state.information });
}
  
  render() {
    const { monto, proximoPago, nextMonto, capital, interest,
      cuotasPendientes, idPrestamo,
      idCuotaPlaneada, montoPlaneado, montoMora, diasMora, idCuotaPlaneadaMora } = this.props;

    const { 
      textStyle,
      buttonStyle,
      buttonStyleGray,
      boxStyleTop,
      progressViewStyles2,
      textBoldStyle
    } = styles;

    const position = this.state.information ? { top: -160, borderBottomLeftRadius: 0, borderBottomRightRadius: 0, height: 240 } : { top: 0, borderBottomLeftRadius: 7, borderBottomRightRadius: 7, height: 80 };
    return (

        <View>
          <View style={boxStyleTop}>   
            <Form>
                <ListItem thumbnail style={{ paddingTop: 15 }}>
                  <Text style={textStyle}>Valor: </Text> 
                  <Text style={textStyle}>     
                    <TextMask 
                      type={'money'} 
                      options={{ precision: 0, unit: '$', separator: ',', delimiter: '.' }}
                      value={monto}
                    />
                  </Text> 
                </ListItem>

                <ListItem thumbnail style={{ paddingTop: 10 }}>
                <Item style={{ borderBottomWidth: 0 }} fixedLabel last>
                <Text style={textStyle}>{capital !== null ? capital : 0}% Capital</Text>
                        <View style={ progressViewStyles2 }>
                            <ProgressBarAnimated
                                {...progressCustomStyles}
                                width={150}
                                height={15}
                                value={capital !== null ? capital : 0}
                                backgroundColorOnComplete="#E4FF33"
                            />
                        </View>                        
                    </Item>
                </ListItem>
                <ListItem thumbnail style={{ paddingTop: 10 }}>
                <Item style={{ borderBottomWidth: 0 }} fixedLabel last>
                <Text style={textStyle}>{interest !== null ? interest : 0}% Interes</Text>
                <View style={progressViewStyles2}>
                            <ProgressBarAnimated
                                {...progressCustomStyles}
                                width={150}
                                height={15}
                                value={interest !== null ? interest : 0}
                                backgroundColorOnComplete="#E4FF33"
                            />
                        </View> 
                        </Item>                       
                </ListItem>
                <ListItem thumbnail style={{ paddingTop: 10 }}>
                  <Text style={textStyle}>Cuotas Pendientes: </Text>
                  <Text style={[textStyle, textBoldStyle]}>{cuotasPendientes}</Text>
                </ListItem>
                <ListItem thumbnail style={{ paddingTop: 10 }}>
                  <Text style={textStyle}>Fecha Próximo pago:</Text>
                  <Text style={[textStyle, textBoldStyle]}>
                    <TextMask 
                      type={'datetime'}
                      value={proximoPago}
                      options={{ format: 'YYYY-MM-DD' }}
                    />
                  </Text>
                </ListItem>
                <ListItem thumbnail style={{ paddingTop: 10 }}>
                  <Text style={textStyle}>Monto Próximo pago:</Text>
                  <Text style={[textStyle, textBoldStyle]}>
                    <TextMask 
                      type={'money'} 
                      options={{ precision: 0, unit: '$', separator: ',', delimiter: '.' }}
                      value={nextMonto}
                    />
                  </Text>
                </ListItem>

                 <List style={{ flex: 1, justifyContent: 'center', alignItems: 'center', paddingTop: 20 }}>
                  <ListItem thumbnail>
                    <View>
                      <Button 
                        style={buttonStyle} 
                        onPress={idPrestamo !== 1 && idCuotaPlaneada !== 1 ?
                         () => Actions.paymentReceipt({ loanId: idPrestamo, quotaId: idCuotaPlaneada })
                         : () => {}}
                      >
                        <Text style={{ color: '#2b2b2b' }} >Pagar</Text>
                      </Button>
                    </View>
                  </ListItem>
                  <ListItem thumbnail style={{ paddingTop: 20, paddingBottom: 20 }}>
                    <View>
                      <Button 
                        style={buttonStyleGray} 
                        onPress={idPrestamo != null ?
                          () => Actions.infoLoanActive({ loanId: idPrestamo })
                          : () => {}}
                      >
                        <Text>Info</Text>
                      </Button>
                    </View>
                    <View style={{ paddingLeft: 20 }}>
                      <Button onPress={this.showModal.bind(this)} style={buttonStyle}>
                        <Text style={{ color: '#2b2b2b' }}>PQR</Text>
                      </Button>
                    </View>
                  </ListItem>
                </List>
                {this.props.isLate &&

                  <View style={{ height: 80, paddingTop: 20, flex: 1 }}>
                 
                 <View style={[position, { position: 'absolute', width: '100%', borderTopLeftRadius: 90, borderTopRightRadius: 90, justifyContent: 'center', alignItems: 'center', flexDirection: 'column', backgroundColor: '#636363' }]}>
                     <View style={{ flex: 1, marginTop: 10 }}>
                      <View style={{ alignItems: 'center', justifyContent: 'center' }} >
                          <TouchableWithoutFeedback onPress={this.seeMore.bind(this)} > 
                                <Icon style={{ color: '#E4FF33' }} name={this.state.information ? 'ios-arrow-down' : 'ios-arrow-up'}/>
                          </TouchableWithoutFeedback>
                      </View>
                      <View style={{ alignItems: 'center', justifyContent: 'center' }} >
                          <TouchableWithoutFeedback onPress={this.seeMore.bind(this)} > 
                              <Label style={{ fontSize: 18, color: '#E4FF33' }}>Pagos Atrasados</Label>
                          </TouchableWithoutFeedback>
                        
                      </View>
                     </View>
                     {this.state.information &&
                      <View style={{ flex: 2, paddingLeft: 10, paddingRight: 10 }}>
                      <View style={{ flexDirection: 'row', marginBottom: 10 }}>
                        <Text style={{ textAlign: 'left', color: '#fff', flex: 1, fontSize: 17, fontWeight: 'normal' }}>Valor:</Text>
                        <Text style={{ textAlign: 'left', color: '#fff', flex: 1, fontSize: 18, fontWeight: '900' }}>
                          <TextMask 
                            type={'money'} 
                            options={{ precision: 0, unit: '$', separator: ',', delimiter: '.' }}
                            value={montoPlaneado}
                          />
                        </Text>
                      </View>
                      <View style={{ flexDirection: 'row', marginBottom: 10 }}>
                        <Text style={{ textAlign: 'left', color: '#fff', flex: 1, fontSize: 17, fontWeight: 'normal' }}>Saldo en mora:</Text>
                        <Text style={{ textAlign: 'left', color: '#fff', flex: 1, fontSize: 18, fontWeight: '900' }}>
                          <TextMask 
                            type={'money'} 
                            options={{ precision: 0, unit: '$', separator: ',', delimiter: '.' }}
                            value={montoMora}
                          />
                        </Text>
                      </View>
                      <View style={{ flexDirection: 'row', marginBottom: 10 }}>
                        <Text style={{ textAlign: 'left', color: '#fff', flex: 1, fontSize: 17, fontWeight: 'normal' }}>Días en mora:</Text>
                        <Text style={{ textAlign: 'left', color: '#fff', flex: 1, fontSize: 18, fontWeight: '900' }}>{diasMora}</Text>
                      </View>
                      <View style={{ flexDirection: 'row', marginBottom: 10, alignItems: 'center', justifyContent: 'center' }}>
                        <Button 
                          style={buttonStyle} 
                          onPress={idPrestamo !== 1 && idCuotaPlaneadaMora !== 1 ?
                          () => Actions.paymentReceipt({ loanId: idPrestamo, quotaId: idCuotaPlaneadaMora })
                          : () => {}}
                        >
                          <Text style={{ color: '#2b2b2b' }} >Pagar</Text>
                        </Button>
                      </View>
                      
                    </View>
                        
                        }
                 </View>
             </View>
                }
              </Form>
          </View>      
        
            
        </View>   
    );
  }
}

const styles = {
  boxStyleTop: {
    borderRadius: 15,
    borderColor: 'gray',
    backgroundColor: '#fff',
    borderWidth: 3,
    marginRight: 20,
    marginLeft: 20,
    padding: 10,
    marginBottom: 20
  },
  progressViewStyles2: {
    backgroundColor: '#7c7c7c', 
    borderRadius: 10,
    width: 150,
  },

  textStyle: {
    fontSize: 16,
    color: '#7c7c7c',
    fontFamily: 'Rubik-Light',
    flex: 2,
  },
  textBoldStyle: { 
    fontFamily: 'Rubik-Medium',
    flex: 1,
  },
  orderInsideText: {
    bottom: 15, 
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonStyle: {
    borderColor: '#c7e600',
    borderWidth: 3,
    backgroundColor: '#e4ff33',
    height: 35,
  },
  buttonStyleGray: {
    borderColor: '#4a4a4a',
    borderWidth: 3,
    backgroundColor: '#7c7c7c',
    height: 35,
  },
  cardStyle: {
    borderColor: 'transparent',
    borderWidth: 0,
    backgroundColor: 'transparent',
  },

};

const mapDispatchToProps = dispatch => ({
  hideModal: () => dispatch(hideModal()),
  showModal: (modalProps, modalType) => {
    dispatch(showModal({ modalProps, modalType }));
  }
});

PaymentState = connect(
  null, mapDispatchToProps
)(PaymentState);

export default PaymentState;
