import React, { Component } from 'react';
import { 
    Button, Form, Item, Label, Input, Text, 
    Card, CardItem
} 
from 'native-base';
import { TextMask } from 'react-native-masked-text';
import { Actions } from 'react-native-router-flux';
import { View } from 'react-native';

export default class PaymentLatePay extends Component {
  render() {
    const { montoPlaneado, montoMora, diasMora, idPrestamo, idCuotaPlaneada } = this.props;
    return (
      <Card>
        <CardItem header bordered>
          <Text>Pagos atrasados</Text>
        </CardItem>
        <CardItem>
          <View style={{ flex: 1 }}>
          <Form>
              <Item inlineLabel>
                <Label>Monto:</Label>
                <TextMask 
                  type={'money'} 
                  options={{ precision: 0, unit: '$', separator: ',', delimiter: '.' }}
                  value={montoPlaneado}
                />
              </Item>
              <Item inlineLabel>
                <Label>Saldo por mora:</Label>
                <TextMask 
                  type={'money'} 
                  options={{ precision: 0, unit: '$', separator: ',', delimiter: '.' }}
                  value={montoMora}
                />
              </Item>
              <Item inlineLabel>
                <Label>Días de mora:</Label>
                <Input placeholder={`${diasMora}`} editable={false} />
              </Item>
              <View style={{ alignItems: 'center', justifyContent: 'center', padding: 10, flex: 1, flexDirection: 'row' }} >
                <Button onPress={() => Actions.paymentReceipt({ loanId: idPrestamo, quotaId: idCuotaPlaneada })}>
                  <Text>Carga Comprobante de pago</Text>
                </Button>
              </View>
              
            </Form>
          </View>
          
        </CardItem>
        
      </Card>
    );
  }
}

