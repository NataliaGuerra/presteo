
import React, { Component } from 'react';
import { Content, Container, Header, Body, Title, Text, Button } from 'native-base';
import { Actions } from 'react-native-router-flux';
import { 
    RenderSummaryRequest, 
    RenderPaymentPlan, 
    RenderPaymentPlanItems 
} from '../render/renderCards';
import { PopUp } from '../common';
import APIService from '../../services/APIService';

class SummaryRequest extends Component {

    state = {
        loans: [],
        modalVisible: false,
    };
    componentWillMount() {
        this.getData(0).then((data) => {
            this.setState({ loans: data });
        });
    }

    getData() {
        return new Promise((resolve, reject) => {
          const url = 'v1/listarPrestamosPorEstadoUO/idUsuarioOtorgante=7728384756/PENDIENTE%20DESEMBOLSO';
          APIService.GetAPI(url)
          .then((data) => { resolve(data); })
          .catch((error) => { reject(error); });
        });
    }
    getInfo(loan) {
        const req = {
            earnings: loan.ganancias,
            amount: loan.monto,
            time: loan.tiempoPrestamo.dias,
            type: loan.tipoPrestamo
        };
        const infoApp = {
            nextPayment: loan.proximoPago,
            lastPayment: loan.ultimoPago,
            pendingPayments: loan.cuotasPendientes,
            paymentsMade: loan.pagosRealizados,
            capitalAmount: loan.aporteCapital,
            interestAmount: loan.aporteIntereses,
        };
        Actions.push('infoactive', { req, infoApp });
    }

    setModalVisible(visible) {
        this.setState({ modalVisible: visible });
    }

    showPopUp() {
        this.setModalVisible(!this.state.modalVisible);
    }

    renderCards() {
        return this.state.loans.map(loan =>
            <RenderPaymentPlanItems 
                key={loan.idPrestamo} 
            />
          );
    }
    
    render() {
        return (
            <Container>
                <Header noShadow>
                    <Body>
                        <Title>RESUMEN SOLICITUD</Title>
                    </Body>
                </Header>
                <Content>
                    <RenderSummaryRequest title={'Solicitud'} />
                    <RenderPaymentPlan title={'Plan de Pagos'}>
                        {this.renderCards()}
                    </RenderPaymentPlan>

                    {this.renderCards()}
                    <Button onPress={this.showPopUp.bind(this)} small bordered>
                        <Text>Firmar contrato</Text>
                    </Button>
                </Content>
                <PopUp 
                    onPress={this.showPopUp.bind(this)} 
                    visible={this.state.modalVisible} 
                    title={'Contrato'}
                >
                    <Text>Texto de prueba</Text>
                </PopUp>
               
            </Container>
        );
    }
}

export default SummaryRequest;
