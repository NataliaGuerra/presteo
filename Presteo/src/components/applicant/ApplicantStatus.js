import React, { Component } from 'react';
import { 
    Body, 
    Text, Right,
    Button, Left, Card, CardItem, Icon,
} 
from 'native-base';
import { Actions } from 'react-native-router-flux';
import { View } from 'react-native';
import { 
	transparentCardItemsStyles,
	cardFinStyles,
} from '../../assets/styles/component/cards';

export default class ApplicantStatus extends Component {
  render() {
    const { solicitudesActivas, historialPagos, solicitudesEnProceso } = this.props;
    const { 
      buttonStyle
		} = styles;
		 
    return (
      <View>
				<Card transparent>
					<View style={[cardFinStyles, { padding: 0, borderColor: 'gray' }]}>
						<CardItem button onPress={solicitudesActivas > 0 ? () => Actions.loansActive() : () => {}} style={[transparentCardItemsStyles, { paddingLeft: 10, borderBottomWidth: 3, borderBottomColor: 'gray', borderBottomLeftRadius: 0, borderBottomRightRadius: 0 }]}>
							<Left style={{ flex: 3 }}>
								<Text style={[styles.textStyle, styles.orderInsideText]}>Solicitudes activas</Text>
							</Left>
							<Body style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
								<Text style={[styles.textStyle, { fontFamily: 'Rubik-Medium' }]}>{solicitudesActivas}</Text>
							</Body>
							<Right>
							<Button style={[buttonStyle, { height: 30 }]} transparent>
								<Icon style={[styles.iconStyle, styles.orderInsideText]} name='ios-arrow-forward' />
							</Button>
							</Right>
						</CardItem>
						<CardItem button onPress={solicitudesEnProceso > 0 ? () => Actions.loansOutstanding() : () => {}} style={[transparentCardItemsStyles, { paddingLeft: 10, borderBottomWidth: 3, borderBottomColor: 'gray', borderBottomLeftRadius: 0, borderBottomRightRadius: 0 }]}>
							<Left style={{ flex: 3 }}>
								<Text style={[styles.textStyle, styles.orderInsideText]}>Solicitudes pendientes</Text>
							</Left>
							<Body style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
								<Text style={[styles.textStyle, { fontFamily: 'Rubik-Medium' }]}>{solicitudesEnProceso}</Text>
							</Body>
							<Right>
								<Button style={[buttonStyle, { height: 30 }]} transparent>
									<Icon style={[styles.iconStyle, styles.orderInsideText]} name='ios-arrow-forward' />
								</Button>
							</Right>
						</CardItem>
						<CardItem button onPress={historialPagos > 0 ? () => Actions.applicantHistoric() : () => {}} style={[transparentCardItemsStyles, { paddingLeft: 10 }]}>
							<Left style={{ flex: 3 }}>
								<Text style={[styles.textStyle, styles.orderInsideText]}>Historial</Text>
							</Left>
							<Body style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
								<Text style={[styles.textStyle, { fontFamily: 'Rubik-Medium' }]}>{historialPagos}</Text>
							</Body>
							<Right>
								<Button style={[buttonStyle, { height: 30 }]} transparent>
									<Icon style={[styles.iconStyle, styles.orderInsideText]} name='ios-arrow-forward' />
								</Button>
							</Right>
						</CardItem>
					</View>
				</Card>
      </View>
    );
  }
}
const styles = {
  boxStyleTop: {
    borderColor: 'gray',
    backgroundColor: '#fafdf6',
    borderWidth: 3,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    height: 50,
    width: 320,
  },
  boxStyleBottom: {
   borderColor: 'gray',
   backgroundColor: '#fafdf6',
   borderLeftWidth: 3,
   borderRightWidth: 3,
   borderBottomWidth: 3,
   borderBottomLeftRadius: 10,
   borderBottomRightRadius: 10,
   height: 50,
   width: 320,
  },
  textStyle: {
    fontSize: 16,
    color: '#7c7c7c',
    fontFamily: 'Rubik-Light',
  },
  orderInsideText: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  buttonStyle: {
    borderColor: 'transparent',
    borderWidth: 0,
  },
  iconStyle: {
    color: 'gray',
    fontSize: 30
  },
  cardStyle: {
    borderColor: 'transparent',
    borderWidth: 0,
    backgroundColor: 'transparent',
  },

};
