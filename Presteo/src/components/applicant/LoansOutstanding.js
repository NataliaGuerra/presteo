import React, { Component } from 'react';
import { connect } from 'react-redux';
import { StyleProvider, Container, Content, View } from 'native-base';
import { RefreshControl } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { dataRequest, dataSuccess, dataFailure } from '../../actions';
import { RenderMyLoans } from '../render/renderCards';
import APIService from '../../services/APIService';
import { Title, BackGround } from '../common';
import getTheme from '../../themes/components';
import platform from '../../themes/variables/platform';

class LoansOutstanding extends Component {
	state = {
    myLoans: [],
    refreshing: false,
	}
	
	componentWillMount() {
    this.props.dataRequest();
    this.getLoans().then((response) => {
			this.props.dataSuccess();
			console.log(response);
      this.setState({ myLoans: response });
    }).catch((error) => {
      this.props.dataFailure(error);
		});
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.refresh) {
      this.props.dataRequest();
      this.getLoans().then((response) => {
        this.props.dataSuccess();
        this.setState({ myLoans: response });
      }).catch((error) => {
        this.props.dataFailure(error);
      });
    }
  }

  onRefresh() {
    this.setState({ refreshing: true });
    this.getLoans().then((response) => {
      this.setState({ refreshing: false });
			this.setState({ myLoans: response });
    }).catch(() => {
      this.setState({ refreshing: false });
    });
  }

	getLoans() {
    return new Promise((resolve, reject) => {
      const userId = this.props.auth.idRolUsuario;			
			let result = [];
			let url = `v1/listarPrestamosPorEstadoUS/${userId}/PENDIENTE DE APROBACION SOLICITANTE`;
			APIService.GetAPI(url).then((response) => {
				result = [ ...result, ...response ];
				url = `v1/listarPrestamosPorEstadoUS/${userId}/PENDIENTE DESEMBOLSO`;
				APIService.GetAPI(url).then((response2) => {
					result = [ ...result, ...response2 ];
					url = `v1/listarPrestamosPorEstadoUS/${userId}/PENDIENTE VERIFICACION`;
					APIService.GetAPI(url).then((response3) => {
						result = [ ...result, ...response3 ];
						url = `v1/listarPrestamosPorEstadoUS/${userId}/PENDIENTE DE APROBACION OTORGANTE`;
						APIService.GetAPI(url).then((response4) => {
							result = [ ...result, ...response4 ];
							resolve(result);
						}).catch((error) => {
							reject(error);
						});
					}).catch((error) => {
						reject(error);
					});
				}).catch((error) => {
					reject(error);
				});
      }).catch((error) => {
        reject(error);
      });
    });
  }
	
	render() {
    return (
      <StyleProvider style={getTheme(platform)}>      
      	<Container>
					<BackGround>
						<Content> 
							<View refreshControl={
								<RefreshControl
								refreshing={this.state.refreshing}
								onRefresh={this.onRefresh.bind(this)}
								/>
							}
								padder >
								<Title titleText={'Mis solicitudes pendientes'} />
								{
									this.state.myLoans.map(element => 
										<RenderMyLoans 
											key={element.idPrestamo}
											monto={element.montoSolicitado}
											tiempoPrestamo={element.tiempoCredito}
											modalidad={element.modalidadPago}
											fechaCreacion={element.fechaSolicitud}
											nombreTipoPrestamo={element.tipoPrestamo}
											nombreEstadoPrestamo={element.estadoPrestamo}
											onPress={() => element.estadoPrestamo == 'PENDIENTE DE APROBACION SOLICITANTE' ?
											 	Actions.summaryLoanAccepted({ loanId: element.idPrestamo }) : 
												Actions.infoLoanOutstanding({ loanId: element.idPrestamo })
											}
											statusColor='#FFA500'
										/>
									)
								}
							</View>
						</Content>
					</BackGround>
        </Container>
      </StyleProvider>  
    );
  }
};
const mapStateToProps = state => {
  return { auth: state.auth };

};
export default connect(mapStateToProps, { dataRequest, dataSuccess, dataFailure })(LoansOutstanding);
