
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { dataRequest, dataSuccess, dataFailure } from '../../actions';
import APIService from '../../services/APIService';
import { ListCards } from '../common';
import { RenderSUFinishedLoan } from '../render/renderCards';

class ApplicantHistoric extends Component {
  
  state = {
    payments: [],
    refreshing: false
  };

  componentWillMount() {
    this.props.dataRequest();
    this.getData().then((data) => {
        this.props.dataSuccess();
        this.setState({ payments: data });
    }).catch((error) => {
        this.props.dataFailure(error);
    });
  }

  getData() {
    return new Promise((resolve, reject) => {
      const id = this.props.auth.idRolUsuario;
      const url = `v1/listarPrestamosInfoBasicaFinalizadosUS/idUsuarioSolicitante=${id}`;
      APIService.GetAPI(url).then((data) => {
        resolve(data);
      }).catch((error) => {
        reject(error);
         
      });
    });
  }

  getInfo(loanId) {
    Actions.paymentFinalized({ loanId });
  }

  onRefresh() {
    this.setState({ refreshing: true });
    this.getData().then((data) => {
      this.setState({ refreshing: false });
      this.setState({ payments: data });
    }).catch(() => {
      this.setState({ refreshing: false });
    });
  }

	
  renderCards() {
    return this.state.payments.map(loan =>
      <RenderSUFinishedLoan 
        key={loan.idPrestamo} 
        montoSolicitado={loan.montoSolicitado}
        montoPagado={loan.montoPagado}
        ultimoPago={loan.ultimoPago}
        onPress={this.getInfo.bind(this, loan.idPrestamo)}
      />
    );
  }


  render() {
    return (
      <ListCards 
        onRefresh={this.onRefresh.bind(this)}
        refreshing={this.state.refreshing}
        title={'Historial de préstamos'}
      > 
    {/*   <RenderSUFinishedLoan 
            key={1} 
            montoSolicitado={20000}
            montoPagado={20000}
            ultimoPago={'20100625jkhjklhklh'}
            onPress={this.getInfo.bind(this, 1)}
          /> */}
        {this.renderCards()}
      </ListCards>
    );
  }
}
const mapStateToProps = state => {
  return { auth: state.auth };
};

export default connect(mapStateToProps, 
  { dataRequest, dataSuccess, dataFailure })(ApplicantHistoric);
