import React, { Component } from 'react';
import { Container,  Title, StyleProvider, Thumbnail, Card } from 'native-base';
import { View, Dimensions, ImageBackground, TouchableWithoutFeedback, Text } from 'react-native';
import { connect } from 'react-redux';
import getTheme from '../../../themes/components';
import { scale } from '../../../helpers/Scaler';
import { RenderSUFinishedLoan } from '../../render/renderCards';
import ApplicantStatus from '../ApplicantStatus';
import { Field, reduxForm } from 'redux-form';
//Componentes
import * as renders from '../.././render/renderComponents';
import {  required , alphabetical} from '../.././validation/validation';

const bg = require('../../../assets/images/YellowBackGround.png');
const logogreen = require('../../../assets/images/GreenV.png');

class FormPqr extends Component {

render(){
    return  <View>
                <View style={styles.viewTittle}>  
                    <Title style={styles.tittleStyle}>Peticiones, Quejas</Title>
                    <Title style={styles.tittleStyle}>y Reclamos</Title>
                </View>
                <View>
                    <Card transparent>
                        <View style={styles.cardFinStyles}>
                            <Container>
                            <View style={{flex: 1, flexDirection: 'row',  alignContent: 'center'}}>
                                <View style={styles.viewImageLeft}>
                                    <Text style={styles.textBody}>Asunto:</Text>
                                </View>
                                <View style={styles.viewImageRight}>
                                    <Field style={styles.fieldFormStyle} name="first_name" validate={[required]} placeholder='Natalia' component={renders.renderInput} />
                                    <Field style={styles.select} name="last_name" validate={[required]}  component={renders.renderSelect} />
                                </View>
                                
                            </View>
                                
                            </Container>
                        </View>
                    </Card>
                </View>

            </View>
                 
}

}

export default reduxForm({
 form: 'FormPqr',
})(FormPqr);

const styles = {
    fieldFormStyle: {
        backgroundColor: '#fff', 
        borderColor: '#7C7C7C', 
        borderWidth: 3, 
        borderRadius: 10, 
        marginBottom: 10, 
        marginLeft: 20, 
        marginRight: 20,
        height: 50,
        color: '#7c7c7c',
        textAlign: 'center',
    },
    select: {
        backgroundColor: '#fff', 
        borderColor: '#7C7C7C', 
        borderWidth: 3, 
        borderRadius: 10, 
        marginLeft: 20, 
        marginRight: 20,
        height: 50,
        color: '#7c7c7c',
    },
    textBody: {
        color: '#CAE63C',
        fontSize: 20,
        fontWeight: 'bold',
    },
    viewImageLeft: {
        flex: 1,        
        flexDirection: 'column',
        justifyContent: 'center',
        alignContent: 'center',
        backgroundColor: '#8F8F8F',
    },
    viewImageRight: {
        flex: 1,        
        flexDirection: 'column',
        justifyContent: 'center',
        alignContent: 'center',
        backgroundColor: '#8F8F8F',        
    },
    cardFinStyles: {
        borderRadius: 15,
        backgroundColor: '#8F8F8F',
        marginRight: 20,
        marginLeft: 20,
        padding: 20,
        height: 200,
    },
    tittleStyle: {
      fontSize: 20,
      color: '#2b2b2b',
      fontWeight: 'bold',
    },
    viewTittle : {
        paddingTop: 20,
        marginBottom: 20,
      },
  };
  
