import React, { Component } from 'react';
import { Container,  Title, StyleProvider, Thumbnail, Card } from 'native-base';
import { View, Dimensions, ImageBackground, TouchableWithoutFeedback, Text } from 'react-native';
import { connect } from 'react-redux';

import getTheme from '../../../themes/components';

import { scale } from '../../../helpers/Scaler';
import { RenderSUFinishedLoan } from '../../render/renderCards';
import ApplicantStatus from '../ApplicantStatus';
import { Field, reduxForm } from 'redux-form';
//Componentes
import * as renders from '../.././render/renderComponents';
//Estilos
import * as form from '../../../assets/styles/component/form';

import FormPqr from './FormPqr';

const bg = require('../../../assets/images/YellowBackGround.png');
const logogreen = require('../../../assets/images/GreenV.png');


class PQR extends Component {

render(){
    return <StyleProvider style={getTheme()}>
                <Container>    
                    <ImageBackground source={bg} style={{ flex: 1 }}>
                        <View>
                            <FormPqr/>
                        </View>                       
                    </ImageBackground>
                </Container>
            </StyleProvider>
    
}


}

const mapStateToProps = state => {
    return { auth: state.auth };
 };

export default connect(mapStateToProps)(PQR);