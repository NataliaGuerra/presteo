import React, { Component } from 'react';
import { View, StyleSheet, Alert } from 'react-native';
import { StyleProvider, Container, Text, Thumbnail, Button } from 'native-base';
import { connect } from 'react-redux';
import ImagePicker from 'react-native-image-picker';
import { Actions } from 'react-native-router-flux';
import { BackGround, CardImage } from '../common';
import { dataRequest, dataSuccess, dataFailure } from '../../actions';
import APIService from '../../services/APIService';
import AWS3Service from '../../services/AWS3Service';
import getTheme from '../../themes/components';
import platform from '../../themes/variables/platform';
import { scale } from '../../helpers/Scaler';
import ImageService from '../../services/ImageService';


const logogreen = require('../../assets/images/CheckGreen.png');
const logocloud = require('../../assets/images/Uploadlogo.png');
class PaymentReceipt extends Component {


  state = {
    bankInfo: {},
    paymentCharged: false,
    image: {}
  }
  componentWillMount() {
    this.getData(this.props.loanId, this.props.quotaId);
  }
  getData(loanId, quotaId) {
    const url = `v1/obtenerInfoBancaria/idPrestamo=${loanId}/idCuotaPlaneada=${quotaId}`;
    this.props.dataRequest();
     
     
    APIService.GetAPI(url).then((data) => {
      this.props.dataSuccess();
       
      this.setState({ bankInfo: data });
    }).catch((error) => {
      this.props.dataFailure(error);
       
    });
  }
  getImage() {
    const options = {
      quality: 1.0,
      maxWidth: 820,
      maxHeight: 820,
      title: 'Seleccione su comprobante',
      takePhotoButtonTitle: 'Tomar una foto',
      chooseFromLibraryButtonTitle: 'Obtener de la galería',
      mediaType: 'photo',
    };
    this.setState({ paymentCharged: false });
    ImagePicker.showImagePicker(options, (response) => {
      if (!response.didCancel && !response.error) {
        const file = {
          uri: response.uri,
          name: response.fileName,
          type: response.type
        };
        this.setState({ 
          paymentCharged: true, 
          image: file 
        });
      } 
    });
  }
  chargeNewPayment() {
    this.props.dataRequest();
    if (this.props.auth.usuarioPerfil.idUsuarioPerfil) {
      this.id = this.props.auth.usuarioPerfil.idUsuarioPerfil;
    }
    const path = `comprobantes/${this.id}/${this.props.loanId}`;
    AWS3Service.UploadImage(this.state.image, path).then((data) => {
      const newReceipt = {
        comprobantesPagos: [
          {
            descripcion: 'Nuevo pago',
            urlComprobante: data.postResponse.key
          }
        ]
      };
      const url = `v1/crearPagoRealizado/idPrestamo=${this.props.loanId}`;
       
      APIService.PutAPI(url, newReceipt).then((response) => {
         
        this.props.dataSuccess();
        setTimeout(() => {
          Alert.alert(
            'Comprobante', 
            'Su comprobante se ha subido satisfactoriamente',
            [{ text: 'Aceptar', 
              onPress: () => { Actions.applicantHome(); }, 
              style: 'cancel' 
            }],
            { cancelable: false }
          );
        }, 200);
      }).catch((error) => {
        this.props.dataFailure(error);
      });
    }).catch((error) => {
      this.props.dataFailure(error);
    });
  }
  renderInfo() {
    const { textContainerStyle } = styles;
    const { nombreOtor, entidadBancaria, apellidoOtor,
      numeroCuenta, urlImagen } = this.state.bankInfo;
    return (
      <View style={{ flex: 1 }}>
        <View style={{ flex: 1, flexDirection: 'row' }}>
          <View style={{ flex: 2, flexDirection: 'column' }}>
            <CardImage 
              uri={ImageService.getUrlFormat(urlImagen)}
              showRate={false}
            />
          </View>
          <View style={{ flex: 3, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
            <Text style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center', fontSize: 20, left: 10 }}>{nombreOtor} {apellidoOtor}</Text>
          </View>
        </View>
        <View style={textContainerStyle}>
          <Text>{entidadBancaria}</Text>
          <Text style={{ fontFamily: 'Rubik-Light', color:'#7c7c7c' }}># {numeroCuenta}</Text>
        </View>
      </View>
    );
  }
  render() {
    const { containerStyle,
      buttonStyle, footerContainerStyle, ContainerTop, ContainerBottom } = styles;
    const { fechaLimite } = this.state.bankInfo;
    return (
      <StyleProvider style={getTheme(platform)}>
        <Container>        
          <BackGround>
            <View style={containerStyle}>
              <View style={ContainerTop}>
                {this.renderInfo()}
              </View>
              <View style={[footerContainerStyle, { }]}>
                <Text style={{fontSize: 14, bottom: scale(20)}} >Límite para consignar: 
                  {fechaLimite != null ? fechaLimite.substring(0, fechaLimite.indexOf('T')) + ' ' +
                    fechaLimite.substring(fechaLimite.indexOf('T') + 1, fechaLimite.indexOf('T') + 6) : ''}
                </Text>

                <View style={ContainerBottom}>
                
                  {this.state.paymentCharged ? 
                    <Thumbnail style={{ height: 80, width: 80, }} name="check" source={logogreen} />
                    :
                    <Thumbnail style={{ height: 80, width: 80, }} name="cloud" source={logocloud} />
                  }
                  {this.state.paymentCharged ?
                  <View>
                    <Button style={buttonStyle} onPress={() => this.chargeNewPayment()}><Text style={{ fontSize: 16 }}>Subir comprobante</Text></Button>
                  </View> 
                    :
                    <View>
                    <Button style={buttonStyle} onPress={() => this.getImage()}><Text style={{ fontSize: 16 }}>Cargar comprobante</Text></Button>
                    </View>
                  }
                  </View>

              </View>
            </View>
          </BackGround>
        </Container>
      </StyleProvider>  
    );
  }
}

const styles = StyleSheet.create({
  containerStyle: {
    flex: 1,
    flexDirection: 'column',
  },
  ContainerTop: {
    borderColor: '#7c7c7c',
    backgroundColor: '#fafdf6',
    borderWidth: 3,
    borderRadius: 15,
    flexDirection: 'column',
    flex: 3,
    margin: scale(25),
  },
  ContainerBottom: {
    borderColor: '#7c7c7c',
    backgroundColor: '#fafdf6',
    borderWidth: 3,
    borderRadius: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    marginHorizontal: scale(10),
  },
  infoContainerStyle: {

    flexDirection: 'column',
    padding: 10,
    borderColor: 'gray',
    borderWidth: 0.5,
  },
  footerContainerStyle: {
    marginBottom: scale(20),
    paddingTop: 10,
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
  },
  photoContainerStyle: {
    flex: 1,
    flexDirection: 'row',
  },
  buttonStyle: {
    marginHorizontal: scale(10),
    width: scale(200),
    height: scale(40),
  },
  grayButton: {
    borderColor: 'grey',
    backgroundColor: '#7c7c7c'
  },
  iconStyle: {
    fontSize: 40,
    margin: 10
  },
  iconSuccessStyle: {
    fontSize: 40, 
    margin: 10, 
    color: 'green'
  },
  textContainerStyle: {
    flex: 2,
    bottom: scale(10),
    flexDirection: 'column', 
    justifyContent: 'flex-end', 
    alignItems: 'center',
  }
});
const mapStateToProps = state => {
  return { auth: state.auth };
};


export default connect(mapStateToProps, { dataRequest, dataSuccess, dataFailure })(PaymentReceipt);
