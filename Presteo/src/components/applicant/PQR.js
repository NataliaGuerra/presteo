import React, { Component } from 'react';
import { 
    Container, Content, Title, StyleProvider, CardItem, Left, Body, Right,
    Text, Thumbnail, Button
} 
from 'native-base';
import { View, Dimensions, ImageBackground, TouchableWithoutFeedback } from 'react-native';
import { connect } from 'react-redux';
import getTheme from '../../themes/components';
import { dataRequest, dataSuccess, dataFailure } from '../../actions';
import { scale } from '../../helpers/Scaler';

const bg = require('../../assets/images/YellowBackGround.png');
const logogreen = require('../../assets/images/GreenV.png');
const plus = require('../../assets/images/PlusGreen.png');


class PQR extends Component {

  render() {
    const { height, width } = Dimensions.get('window');
    const {
      tittleStyle, contentStyle, responseViewStyle, viewGray, viewWhite,
      leftStyle, bodyStyle, rightStyle, textStyleTop, textStyleSmall, textStyleGrey
    } = styles;
    return (
      <StyleProvider style={getTheme()}>
        <Container>

          <ImageBackground
            source={bg}
            style={{ flex: 1, height, width }} 
          >
          
            <View style={{ paddingTop: 20 }}>   
            <Title style={tittleStyle}>Peticiones, Quejas y</Title>
            <Title style={tittleStyle}>Reclamos</Title>
            </View>
          <Content style={contentStyle}>

            <View style={responseViewStyle}>

            <View style={{ left: scale(10), backgroundColor: 'black', height: 25, width: 25,  alignItems: 'center', justifyContent: 'center' }}>
              <Thumbnail style={{ height: 25, width: 25, }} name="check" source={logogreen} />
            </View>

            <View style={viewGray}>   
            <View style={viewWhite}>
            <CardItem>
            <Left style={leftStyle}>
            <Text style={textStyleTop}>fecha</Text>
            <Text style={textStyleGrey}>01/03/18</Text>
            <Text style={textStyleTop}>Número petición</Text>
            <Text style={textStyleGrey}>003</Text>
            </Left>
            <Body style={bodyStyle}>
            <Text style={[textStyleSmall, { fontWeight: '400', fontSize: 12, left: 20, color: 'black', fontFamily: 'Rubik-Medium' }]}>Respuesta</Text>
            <Content>
            <Text style={textStyleSmall}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</Text>
            </Content>
            <Text style={[textStyleSmall, { left: scale(50), top: scale(10) }]}>12:23:05 pm</Text>
            </Body>            
            <Right style={rightStyle}>
            <Text>V</Text>
            </Right>
            </CardItem>
            </View>
            </View>
            </View>      
 

          </Content>
          <View style={{ justifyContent: 'flex-end', padding: scale(15), flexDirection: 'row', alignItems: 'center' }}>
          <Button style={{ right: scale(40) }}>
          <Text>Archivar</Text>
          </Button>
          <TouchableWithoutFeedback>
						<Thumbnail style={{ height:65, width:65 }} source={plus} />
					</TouchableWithoutFeedback>
          </View>

          </ImageBackground>
        </Container>
    </StyleProvider>
    );
  }

}

const styles = {
  tittleStyle: {
    fontSize: 20,
    color: '#2b2b2b',
  },
  contentStyle: {
    width: scale(350),
  },
  responseViewStyle: {
    alignItems: 'center',
    flexDirection: 'row',
    paddingTop: 30,
  },

  viewGray: {
    width: scale(280),
    height: scale(160),
    alignItems: 'center',
    backgroundColor: 'grey',
    left: 20,
    top: 20,
  },

  viewWhite: {
    width: scale(270),
    height: scale(110),
    top: scale(5),
    backgroundColor: 'white'
  },
  leftStyle: {
    right: scale(20),
    height: scale(90),    
    flexDirection: 'column',
    flex: 8,
    justifyContent: 'flex-start',
    borderRightColor: 'grey',
    borderRightWidth: 2,

  },
  bodyStyle: {
    right: scale(10),
    height: scale(90),    
    flexDirection: 'column',
    flex: 12,
    justifyContent: 'flex-start',

  },
  rightStyle: {
    height: scale(90),    
    flexDirection: 'column',
    flex: 1,
    justifyContent: 'flex-start',

  },
  textStyleTop: {
    paddingTop: 5,
    fontSize: 10,
    color: '#7c7c7c',
  },
  textStyleGrey: {
    fontSize: 18,
    paddingTop: 5,
    fontWeight: '200',
    right: 2,
    color: '#7c7c7c',
  },
  textStyleSmall: {
    fontSize: 10,
    color: '#7c7c7c',
    fontFamily: 'Rubik-Light'
  },
};

const mapStateToProps = state => {
  return { auth: state.auth };
};

export default connect(mapStateToProps, { dataRequest, dataSuccess, dataFailure })(PQR);
