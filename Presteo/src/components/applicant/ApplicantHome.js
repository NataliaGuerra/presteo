import React, { Component } from 'react';
import { 
    Container, Content, Title, StyleProvider, Thumbnail
} 
from 'native-base';
import { View, Dimensions, ImageBackground, TouchableWithoutFeedback } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import getTheme from '../../themes/components';
import ApplicantStatus from './ApplicantStatus';
import PaymentState from './PaymentState';
import APIService from '../../services/APIService';
import { dataRequest, dataSuccess, dataFailure, registerDevice } from '../../actions';
import NotificationService from '../../services/NotificationService';


const bg = require('../../assets/images/YellowBackGround.png');
const icon = require('../../assets/images/CreateLoanIcon.png');

class ApplicantHome extends Component {

  state = {
    applicantSummary: {
      monto: '',
      cuotasPendientes: '',
      proximoPago: '',
      aporteCapital: '',
      aporteIntereses: '',
      usuarioSolicitante: {
        solicitudesActivas: '',
				creditosFinalizados: '',
				solicitudesEnProceso: '',
        usuario: {
          nombre: '',
          apellido: '',
        }    
      },
      cuotasPlaneadas: [],
      information: false   
    }
  };

  componentWillMount() {
    const id = this.props.auth.idRolUsuario;
		const url = `v1/obtenerResumenSolicitante/idUsuarioSolicitante=${id}`;
    this.props.dataRequest();
    APIService.GetAPI(url).then((response) => {
      this.setState({ applicantSummary: response });
      this.props.dataSuccess();
    }).catch((error) => {
      console.log(error);
      this.props.dataFailure(error);
    });
  }
	componentDidMount() {
		NotificationService.configure();
	}
  renderInfo() {
    const {
      monto,
      cuotasPendientes,
      proximoPago,
      aporteCapital,
      aporteIntereses,
      idPrestamo,
      cuotasPlaneadas
    } = this.state.applicantSummary;
    if (cuotasPlaneadas == null || cuotasPlaneadas.length === 0) {
      return (
        <View />
      );
    } else if (cuotasPlaneadas.length === 1) {
      const { montoPlaneado, idCuotaPlaneada } = cuotasPlaneadas[0];
      return (
        <PaymentState 
          monto={monto !== null ? monto.toString() : '0'} 
          proximoPago={proximoPago !== null ? proximoPago : ''} 
          cuotasPendientes={cuotasPendientes !== null ? cuotasPendientes : '0'} 
          capital={aporteCapital !== null ? aporteCapital / 100 : 0}
          interest={aporteIntereses !== null ? aporteIntereses / 100 : 0}
          nextMonto={montoPlaneado !== null ? Math.trunc(montoPlaneado) : 0}
          idPrestamo={idPrestamo != null ? idPrestamo : null}
          idCuotaPlaneada={idCuotaPlaneada != null ? idCuotaPlaneada : null}
        />
      );     
    }
    const { diasMora, montoMora, montoPlaneado, idCuotaPlaneada } = cuotasPlaneadas[0];
    return (
      <View>
        <PaymentState 
          monto={monto !== null ? Math.trunc(monto) : '0'} 
          proximoPago={proximoPago !== null ? proximoPago.substring(0, proximoPago.indexOf('T')) : ''} 
          cuotasPendientes={cuotasPendientes !== null ? cuotasPendientes : '0'} 
          capital={aporteCapital !== null ? aporteCapital / 100 : '0'}
          interest={aporteIntereses !== null ? aporteIntereses / 100 : '0'}
          nextMonto={cuotasPlaneadas[1].montoPlaneado !== null ? Math.trunc(cuotasPlaneadas[1].montoPlaneado) : 0}
          idPrestamo={idPrestamo != null ? idPrestamo : null}
          idCuotaPlaneada={cuotasPlaneadas[1].idCuotaPlaneada !== null ? cuotasPlaneadas[1].idCuotaPlaneada : 0}
          diasMora={diasMora !== null ? diasMora : 0}
          montoMora={montoMora !== null ? Math.trunc(montoMora) : 0}
          montoPlaneado={montoPlaneado !== null ? Math.trunc(montoPlaneado) : 0}
          idCuotaPlaneadaMora={idCuotaPlaneada !== null ? idCuotaPlaneada : null}
          isLate
        />
      </View>
    );
  }

  render() {
    const { height, width } = Dimensions.get('window');
    const {
      tittleStyle
    } = styles;
    const {
      usuarioSolicitante,
		} = this.state.applicantSummary;
    return (
      <StyleProvider style={getTheme()}>
        <Container>
          <View 
            style={{ 
              position: 'absolute',                                          
              bottom: 10,                                                    
              right: 10, 
              zIndex: 5,
            }}
          >
            <TouchableWithoutFeedback onPress={() => Actions.newLoan()} >
              <Thumbnail large source={icon} />
            </TouchableWithoutFeedback>  
          </View>
          <ImageBackground
            source={bg}
            style={{ flex: 1, height, width }} 
          >
          <Content>   
            <Title style={tittleStyle}>Bienvenido, {this.props.auth.nombre}</Title>
            <ApplicantStatus 
              solicitudesActivas={usuarioSolicitante.solicitudesActivas !== null ? usuarioSolicitante.solicitudesActivas : 0} 
              historialPagos={usuarioSolicitante.creditosFinalizados !== null ? usuarioSolicitante.creditosFinalizados : 0} 
							solicitudesEnProceso={usuarioSolicitante.solicitudesEnProceso !== null ? usuarioSolicitante.solicitudesEnProceso : 0}
						/>
            {this.renderInfo()}
          </Content>
          </ImageBackground>
        </Container>
    </StyleProvider>
    );
  }

}

const styles = {
  tittleStyle: {
    fontSize: 20,
    color: '#2b2b2b',
    paddingTop: 20,
    paddingBottom: 10,
  }
};

const mapStateToProps = state => {
  return { auth: state.auth };
};

export default connect(mapStateToProps, { dataRequest, dataSuccess, dataFailure, registerDevice })(ApplicantHome);
