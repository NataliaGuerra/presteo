
import React, { Component } from 'react';
import {  
    Text
} from 'native-base';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { RenderLatePays } from '../render/renderCards';
import APIService from '../../services/APIService';
import ImageService from '../../services/ImageService';
import { ListCards } from '../common';
import { dataRequest, dataSuccess, dataFailure } from '../../actions';

class LateLoans extends Component {
    state = {
        loans: [],
        refreshing: false
    };
    componentWillMount() {
        this.props.dataRequest();
        this.getData().then((data) => {
            this.props.dataSuccess();
            this.setState({ loans: data });
        }).catch((error) => {
            this.props.dataFailure(error);
        });
    }

    onRefresh() {
        this.setState({ refreshing: true });
        this.getData().then((data) => {
          this.setState({ refreshing: false });
          this.setState({ loans: data });
        }).catch(() => {
          this.setState({ refreshing: false });
        });
      }

    getData() {
        return new Promise((resolve, reject) => {
          const url = `v1/listarPrestamosInfoBasicaMoraUO/idUsuarioOtorgante=${this.props.auth.idRolUsuario}`;
          APIService.GetAPI(url)
          .then((data) => { resolve(data); })
          .catch((error) => { reject(error); });
        });
    }

    getInfo(loan) {
        let subtitle = '';
        let recoverCapital = false;
        const idLoan = loan.idPrestamo;

        if (loan.diasMora < 2) {
            subtitle = '(Día de gracia)';
        } else if (loan.diasMora > 1 && loan.diasMora < 16) {
            subtitle = '(Menor a quince)';
        } else if (loan.diasMora > 15 && loan.diasMora < 31) {
            subtitle = '(Mayor a quince)';
        } else if (loan.diasMora > 30 && loan.diasMora < 120) {
            subtitle = '(Mayor a treinta)';
        } else {
            subtitle = '(Defecto)';
            recoverCapital = true; 
        }
      
        Actions.push('infolate', { idLoan, subtitle, recoverCapital });
    }

    renderCards() {
    let index = 1; 
    const renderLatePays = [];

     switch (this.props.type) {
            case 0:
                for (const loan of this.state.loans) {
                    if (loan.diasMora < 2) {
                        const urlFotoPerfil = ImageService.getUrlFormat(loan.urlFotoPerfil);
                        renderLatePays.push(<RenderLatePays 
                            key={loan.idPrestamo} 
                            title={`Préstamo ${index++}`}
                            interestPercentage={Math.round(loan.intereses)}
                            capitalPercentage={Math.round(loan.capital)}
                            expecDate={loan.fechaEsperada}
                            lateQuota={loan.capital}
                            img={urlFotoPerfil}
                            qa={loan.calificacionUS}
                            onPress={this.getInfo.bind(this, loan)}
                        />);
                    }  
                }
                break;
            case 1:
                for (const loan of this.state.loans) {
                    if (loan.diasMora > 1 && loan.diasMora < 16) {
                        const urlFotoPerfil = ImageService.getUrlFormat(loan.urlFotoPerfil);
                        renderLatePays.push(<RenderLatePays 
                            key={loan.idPrestamo} 
                            title={`Préstamo ${index++}`}
                            interestPercentage={Math.round(loan.intereses)}
                            capitalPercentage={Math.round(loan.capital)}
                            expecDate={loan.fechaEsperada}
                            lateQuota={loan.capital}
                            img={urlFotoPerfil}
                            qa={loan.calificacionUS}
                            onPress={this.getInfo.bind(this, loan)}
                        />);
                    }  
                }
                break;
            case 2:
                for (const loan of this.state.loans) {
                    if (loan.diasMora > 15 && loan.diasMora < 31) {
                        const urlFotoPerfil = ImageService.getUrlFormat(loan.urlFotoPerfil);
                        renderLatePays.push(<RenderLatePays 
                            key={loan.idPrestamo} 
                            title={`Préstamo ${index++}`}
                            interestPercentage={Math.round(loan.intereses)}
                            capitalPercentage={Math.round(loan.capital)}
                            expecDate={loan.fechaEsperada}
                            lateQuota={loan.capital}
                            img={urlFotoPerfil}
                            qa={loan.calificacionUS}
                            onPress={this.getInfo.bind(this, loan)}
                        />);
                    }  
                }
                break;
            case 3:
                for (const loan of this.state.loans) {
                    if (loan.diasMora > 30 && loan.diasMora < 121) {
                        const urlFotoPerfil = ImageService.getUrlFormat(loan.urlFotoPerfil);
                        renderLatePays.push(<RenderLatePays 
                            key={loan.idPrestamo} 
                            title={`Préstamo ${index++}`}
                            interestPercentage={Math.round(loan.intereses)}
                            capitalPercentage={Math.round(loan.capital)}
                            expecDate={loan.fechaEsperada}
                            lateQuota={loan.capital}
                            img={urlFotoPerfil}
                            qa={loan.calificacionUS}
                            onPress={this.getInfo.bind(this, loan)}
                        />);
                    }  
                }
                break;
            case 4:
                for (const loan of this.state.loans) {
                    if (loan.diasMora > 120) {
                        const urlFotoPerfil = ImageService.getUrlFormat(loan.urlFotoPerfil);
                         
                        renderLatePays.push(<RenderLatePays 
                            key={loan.idPrestamo} 
                            title={`Préstamo ${index++}`}
                            interestPercentage={Math.round(loan.intereses)}
                            capitalPercentage={Math.round(loan.capital)}
                            expecDate={loan.fechaEsperada}
                            lateQuota={loan.capital}
                            img={urlFotoPerfil}
                            qa={loan.calificacionUS}
                            onPress={this.getInfo.bind(this, loan)}
                        />);
                    }  
                }
                break;
            default: 
                renderLatePays.push(<Text>No hay prestamos</Text>);
                break;
        } 
        return renderLatePays;
        
      
    }  

    render() {
        const loan = {
            diasMora: 123,
            idPrestamo: 1
        };
        return (
            <ListCards 
                onRefresh={this.onRefresh.bind(this)}
                refreshing={this.state.refreshing}
                title={'Préstamos en atraso'}
            >
            {this.renderCards()} 
             {/*   <RenderLatePays 
                    key={1} 
                    title={`Préstamo `}
                    interestPercentage={20}
                    capitalPercentage={20}
                    expecDate={'3434134134414'}
                    lateQuota={30000}
                    img={null}
                    qa={2}
                    onPress={this.getInfo.bind(this, loan)}
                /> */}
            </ListCards>
        );
    }
}


const mapStateToProps = state => {
    return { auth: state.auth };
  };
  export default connect(mapStateToProps, { dataRequest, dataSuccess, dataFailure })(LateLoans);
