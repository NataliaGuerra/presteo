
import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';
import { View } from 'react-native';
import {  
    Text, 
    Button, 
} from 'native-base';
import { connect } from 'react-redux';
//import { RefreshControl } from 'react-native';
import { RenderPenPays, RenderInfoApplicant, RenderSupportReceipt } from '../render/renderCards';
import APIService from '../../services/APIService';
import { BackGroundM } from '../common';
import { 
  blackButton,
	textOpportunitiesButton,
  declineOpportunitiesButton 
} from '../../assets/styles/component/cards';
import { hideModal, showModal, dataRequest, dataSuccess, dataFailure } from '../../actions';
import ImageService from '../../services/ImageService';

class InfoOpportunities extends Component {
    state = {
        loan: {
            tiempoPrestamo: [],
            tipoPrestamo: [],
            usuarioSolicitante: {
                usuario: {
                  usuarioPerfil: {}
                }
            },
            soporteFactura: {
              estadoSoporte: {}
            }
        },
        contract: '',
        secureCapital: '',
    };

    componentWillMount = async () => {
      this.props.dataRequest();
      try {
         const loan = await this.getData(0);
         const contract = await this.getData(1);
         const secureCapital = await this.getData(5);
         this.setState({ loan, secureCapital, contract });
         this.props.dataSuccess();
      } catch (err) {
          console.log(err);
          this.props.dataFailure(err);
      }   
    }
  
    getData(i) {
        return new Promise((resolve, reject) => {
          let url = '';
          switch (i) {
            case 0: 
              url = `v1/obtenerOportunidad/idPrestamo=${this.props.idLoan}`;
              break;
            case 1:
               url = 'v1/obtenerContratoAceptarPrestamo/';
              break;
            case 2:
              url = `v1/asignarOtorgantePrestamo/${this.props.idLoan}/${this.props.idRol}`;
             break;
            case 3:
              url = `v1/asignarContratoAceptacionOtorgante/${this.props.idLoan}`;
             break;
            case 4:
              url = `v1/ingresarPrestamoRechazado/idUsuarioOtorgante=${this.props.idRol}/idPrestamo=${this.props.idLoan}`;
               
              break;
            case 5:
              url = 'v1/obtenerInfoTipoContrato/tipoContrato/SEGURO%20CAPITAL';
              break;
            default:           
               url = 'v1/obtenerContratoAceptarPrestamo/';
          }
          APIService.GetAPI(url)
            .then((data) => { resolve(data); })
            .catch((error) => { 
              reject(error);
            });
        });
    }

    goBack() {
        Actions.pop();
    }

    acceptContSecureCapital() {
        this.props.hideModal();
        this.showModal(1);
    }

    acceptContract() {
        this.getData(2).then((data) => {
          this.getData(3).then((response) => {
             this.props.hideModal();
              this.showModal(2);  
          }).catch((error) => { 
             
          });
        });
      }
  
    rejectLoan() {
        this.getData(4).then((data) => {
          this.props.hideModal();
           Actions.pop();
        });
    }

    closeModal() {
      this.props.hideModal();
    }
  
    showImg(urlImg) {
       
      this.props.showModal({
              open: true,
        url: urlImg,
        pqr: true,
        closeModal: this.closeModal.bind(this),
        pqrModal: this.pqr.bind(this)
          }, 'image');
    }
  
    pqr() {
      this.props.showModal({
        open: true,
        title: 'Alert Modal',
        message: 'Funcionalidad en Proceso!',
        closeModal: this.closeModal.bind(this)
      }, 'maintenance');
    }

    showModal(index) {
       
      const fullName = (this.state.loan.usuarioSolicitante.usuario.nombre ? this.state.loan.usuarioSolicitante.usuario.nombre : '' ) + ' ' + (this.state.loan.usuarioSolicitante.usuario.apellido ? this.state.loan.usuarioSolicitante.usuario.apellido : 'Apellido');
        switch (index) {
          case 0: 
            this.props.showModal({
              open: true,
              title: 'Información sobre el seguro del capital en realización de préstamos.',
              message: this.state.secureCapital,
              closeModal: this.acceptContSecureCapital.bind(this)
            }, 'agreement');
            break;
          case 1:
            this.props.showModal({
              open: true,
              title: 'Aceptar contrato',
              message: this.state.contract,
              closeModal: this.acceptContract.bind(this)
            }, 'agreement');
            break;
          case 2:
            if (this.state.modalInfoVisible) {
              this.props.hideModal();
                Actions.pop();
            } else {
              this.setState({ modalInfoVisible: !this.state.modalInfoVisible });
              this.props.showModal({
                open: true,
                title: 'Alert Modal',
                message: 'Te enviaremos una notificación, cuándo ' + fullName + ' confirme  la aceptación del crédito.',
                closeModal: this.showModal.bind(this, 2)
              }, 'success');
            }
           break;
          case 3:
            this.props.showModal({
              open: true,
              title: 'Alert Modal',
              message: '¿Está seguro que desea rechazar el prestamo?',
              closeModal: this.rejectLoan.bind(this, 2)
            }, 'success');
           break;
          default:           
            this.props.showModal({
              open: true,
              title: 'Alert Modal',
              message: this.state.secureCapital,
              closeModal: this.acceptContSecureCapital.bind(this)
            }, 'agreement');
        }
    } 
  
	render() {
    const { loan } = this.state;
		return (

          <BackGroundM title='Resumen de la oportunidad'>
		
							<RenderPenPays 
								title={'Solicitado'}
								earnings={loan.ganancias}
								amount={loan.monto}
								time={loan.tiempoPrestamo.duracion !== null ? loan.tiempoPrestamo.duracion : 0}
								type={loan.tipoPrestamo.nombre}
								img={loan.usuarioSolicitante != null && 
									loan.usuarioSolicitante.usuario != null &&
									loan.usuarioSolicitante.usuario.usuarioPerfil != null ? 
                  loan.usuarioSolicitante.usuario.usuarioPerfil.fotoPerfil : ''}
                qa={loan.usuarioSolicitante != null && 
                  loan.usuarioSolicitante.calificacion != null ? 
                  loan.usuarioSolicitante.calificacion : null}
							/>
							<RenderInfoApplicant 
								title={'Info Solicitante'}
								finished={loan.usuarioSolicitante == null && loan.usuarioSolicitante.creditosFinalizados == null ? -1 : loan.usuarioSolicitante.creditosFinalizados}
								canceled={loan.usuarioSolicitante == null && loan.usuarioSolicitante.creditosCancelados == null ? -1 : loan.usuarioSolicitante.creditosCancelados}
								total={loan.usuarioSolicitante == null && loan.usuarioSolicitante.creditosTotales == null ? -1 : loan.usuarioSolicitante.creditosTotales}
								later={loan.usuarioSolicitante == null && loan.usuarioSolicitante.promedioRetraso == null ? -1 : loan.usuarioSolicitante.promedioRetraso}
								maxAmount={loan.usuarioSolicitante == null && loan.usuarioSolicitante.montoMaximo == null ? -1 : loan.usuarioSolicitante.montoMaximo}
								minAmount={loan.usuarioSolicitante == null && loan.usuarioSolicitante.montoMinimo == null ? -1 : loan.usuarioSolicitante.montoMinimo}
							/>
							<RenderSupportReceipt 
								state={loan.soporteFactura == null ? '' : loan.soporteFactura.estadoSoporte.descripcion}
								url={loan.soporteFactura == null ? '' : ImageService.getUrlFormat(loan.soporteFactura.urlSoporte)}
								onPress={() => this.showImg(loan.soporteFactura == null ? '' : ImageService.getUrlFormat(loan.soporteFactura.urlSoporte))}
								title={'Solicitado'}
							/>    
							{this.props.auth.estadoOU !== 'INACTIVO' &&
								<View 
									style={{ 
										flex: 1, 
										justifyContent: 'flex-end', 
										alignItems: 'flex-end', 
										flexDirection: 'row', 
										marginRight: 40,
										marginBottom: 30
									}}
								>
									<Button 
										style={blackButton} 
										onPress={this.showModal.bind(this, 0)} 
										accept
									>
										<Text style={textOpportunitiesButton}>Aceptar</Text>
									</Button>
									<Button 
										style={declineOpportunitiesButton} onPress={this.showModal.bind(this, 3)} 
									>
										<Text style={textOpportunitiesButton}>Rechazar</Text>
									</Button>
								</View>
							}		
					</BackGroundM> 
  
		);
	}
}

const mapDispatchToProps = dispatch => ({
  dataRequest: () => dispatch(dataRequest()),
	dataSuccess: () => dispatch(dataSuccess()),
	dataFailure: () => dispatch(dataFailure()),
  hideModal: () => dispatch(hideModal()),
  showModal: (modalProps, modalType) => {
    dispatch(showModal({ modalProps, modalType }));
  }
});

const mapStateToProps = state => {
  return { auth: state.auth };
};

InfoOpportunities = connect(
  mapStateToProps, mapDispatchToProps
)(InfoOpportunities);


export default connect(mapStateToProps, mapDispatchToProps)(InfoOpportunities);
