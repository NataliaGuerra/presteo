import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View } from 'react-native';
import ImagePicker from 'react-native-image-picker';
import { hideModal, showModal } from '../../actions';
import { RenderGrantorSummary } from '../render/renderCards';

//Services
import APIService from '../../services/APIService';
import AWS3Service from '../../services/AWS3Service';

//Variables
import { BUCKET_URL } from '../../config/variables';

class GrantorInvestments extends Component {

  state = {
    image: [],
  };

  getPhoto() {
    const options = {
        quality: 1.0,
        maxWidth: 520,
        maxHeight: 520,
        title: 'Seleccione una imagen',
        takePhotoButtonTitle: 'Tomar una foto',
        chooseFromLibraryButtonTitle: 'Obtener de la galería',
    };
      ImagePicker.launchCamera(options, (response) => {
        if (!response.didCancel && !response.error) {
          this.setState({ 
            image: {
              uri: response.uri,
              name: response.fileName,
              type: response.type
            },  
          });

          this.uploadProof();
        }
      });
  }

  closeModal() {
    this.props.hideModal();
  }

  showModalAdd() {
    this.props.showModal({
      open: true,
      title: 'Alert Modal',
      guest: this.props.guest,
      closeModal: this.closeModal.bind(this)
    }, 'invesment');
  }

  showModalSummary() {
    this.props.showModal({
      open: true,
      title: 'Alert Modal',
      initData: this.props.initData,
      closeModal: this.closeModal.bind(this)
    }, 'invesummary');
  }
  
  showModalRemov() {
    this.props.showModal({
      open: true,
      title: 'Alert Modal',
      message: 'Para retirar tu inversión necesitas comunicarte al siguiente número: 3553185.',
      closeModal: this.closeModal.bind(this)
    }, 'success');
  } 

  showImg(urlImg) {
    this.props.showModal({
            open: true,
      url: urlImg,
      pqr: true,
      closeModal: this.closeModal.bind(this),
      pqrModal: this.pqr.bind(this)
        }, 'image');
  }

  pqr() {
    this.props.showModal({
      open: true,
      title: 'Alert Modal',
      message: 'Funcionalidad en Proceso!',
      closeModal: this.closeModal.bind(this)
    }, 'maintenance');
  }

  selectButtonText() {
    let text = '';
    switch (this.props.investmentState) {
      case 'PENDIENTE DESEMBOLSO': 
        text = 'Cargar Comp.';
        break;
      case 'PENDIENTE APROBACION':
        text = 'Ver Resumen';
        break;
      case 'PEDIENTE REVISION':
        text = 'Ver Comp.';
        break;
      default:           
        text = 'Agregar Saldo';
    }
    return text;
  }

  selectFunction() {
    switch (this.props.investmentState) {
      case 'PENDIENTE DESEMBOLSO': 
        this.getPhoto();
        break;
      case 'PENDIENTE APROBACION':
        this.showModalSummary();
        break;
      case 'PEDIENTE REVISION':
        this.showImg(this.props.invesmentProff);
        break;
      default:      
       this.showModalAdd(); 
    }
  }

  async uploadProof() {
    try {
      const path = `perfil/${this.props.auth.idUsuario}/`;
      const file = this.state.image;
      const urlAPI = 'v1/cargarComprobanteInversion/';

      //Upload image to AWS3 Service
      const responseAWS = await AWS3Service.UploadImage(file, path);
      console.log(responseAWS);
      const imageUrl = BUCKET_URL + responseAWS.postResponse.key;
      /*  if (!responseAWS.ok) {
        throw Error(responseAWS.statusText);
      } */

      //Send image to API using PUT method
      const bodyAPI = {
        idInversion: this.props.investmentId,
        idUsuarioOtorgante: this.props.auth.idRolUsuario,
        url_comprobante: imageUrl
      };
      const responseApi = await APIService.PutAPI(urlAPI, bodyAPI);
      console.log(responseApi);

      //Show success modal
      this.props.showModal({
        open: true,
        title: 'Error!',
        icon: true,
        message: 'Se ha cargado el comprobante con éxito!',
        closeModal: this.closeModal.bind(this)
      }, 'alert');
    } catch (error) {

      //Show error modal
      this.props.showModal({
        open: true,
        title: 'Error!',
        message: 'Ups! Ha ocurrido un error inesperado, intenta más tarde.',
        closeModal: this.closeModal.bind(this)
      }, 'alert');
      console.log(error);
    }
  }

  render() {
    const { inversionActual, tasaInteresPromedio, saldoDisponible } = this.props;
    const buttonText = this.selectButtonText();
    return (
      <View>
        <RenderGrantorSummary 
          inversionActual={inversionActual}
          tasaInteresPromedio={tasaInteresPromedio}
          saldoDisponible={saldoDisponible}
          addBalanceText={buttonText}
          addBalance={this.selectFunction.bind(this)}
          revBalance={this.showModalRemov.bind(this)}
        />  
      </View>
    );
  }
}


const mapDispatchToProps = dispatch => ({
  hideModal: () => dispatch(hideModal()),
  showModal: (modalProps, modalType) => {
    dispatch(showModal({ modalProps, modalType }));
  }
});

const mapStateToProps = state => {
  return { auth: state.auth };
};

GrantorInvestments = connect(
  mapStateToProps, mapDispatchToProps
)(GrantorInvestments);


export default connect(mapStateToProps, mapDispatchToProps)(GrantorInvestments);

