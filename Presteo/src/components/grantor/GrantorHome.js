import React, { Component } from 'react';
import { 
  H2,
  Icon,
  Label,
  Card,
  CardItem,
  Left, 
  Body, 
  Right,
  Item,
} 
from 'native-base';
import { connect } from 'react-redux';

import {
    View,
    TouchableWithoutFeedback,
    Animated,
    LayoutAnimation,
    UIManager,
    Platform 
} from 'react-native';

import { Actions } from 'react-native-router-flux';

import GrantorInvestments from './GrantorInvestments';
import APIService from '../../services/APIService';
import { h2Styles } from '../../assets/styles/component/common';
import { setGuestState } from '../../actions';
import { BackGroundM } from '.././common';
import { cardBottomStyles, labelFinStyles } from '../../assets/styles/component/cards';
import NotificationService from '../../services/NotificationService';

class GrantorHome extends Component {
    constructor() {
        super();
    
        if (Platform.OS === 'android') {
          UIManager.setLayoutAnimationEnabledExperimental && 
          UIManager.setLayoutAnimationEnabledExperimental(true);
        }
      }
 
  state = {
    grantorSummary: {
      saldoDisponible: 0,
      inversionActual: 0,
      tasaInteresPromedio: 0,
      estadoUO: 'INACTIVO',
      enProceso: 0,
      prestamoPazSalvo: 0,
      prestamosActivos: 0,
      prestamoMoraUno: 0,
      prestamoMoraMenorAQuince: 0,
      prestamoMoraMayorAQuince: 0,
      prestamoMoraMayorATreinta: 0,
      prestamoMoraPorDefecto: 0,
      usuario: {
        nombre: '',
        apellido: '',
      }    
    },
    information: false,
  };

  
  componentWillMount() { 
   console.log(this.props.auth);
    this.position = new Animated.ValueXY(0, 0);
    Animated.spring(this.position, {
      toValue: { x: 200, y: 500 }
    }).start();
    this.getData().then((data) => {
      this.setState({ grantorSummary: data });
      this.props.setGuestState(data.estadoUO);
    });
  
  }
  componentDidMount() {
    NotificationService.configure();
  }
  getData() {
    return new Promise((resolve, reject) => {
      const url = 'v1/obtenerResumenOtorgante/idUsuarioOtorgante=' + this.props.auth.idRolUsuario;
      //const url = 'v1/obtenerResumenOtorgante/idUsuarioOtorgante=5b4395ebff87853865c126fc'; 
      APIService.GetAPI(url)
      .then((data) => { resolve(data); })
      .catch((error) => { reject(error); });
    });
  }

  seeMore() {
       // Uncomment to animate the next state change.
       LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);

       // Or use a Custom Layout Animation
       //LayoutAnimation.configureNext(CustomLayoutAnimation);
   
       this.setState({ information: !this.state.information });
  }

  render() {
    const {
      saldoDisponible,
      inversionActual,
      tasaInteresPromedio,
      enProceso,
      prestamoPazSalvo,
      estadoUO,
      prestamosFinalizados,
      prestamoMoraUno,
      prestamoMoraMenorAQuince,
      prestamoMoraMayorAQuince,
      prestamoMoraMayorATreinta,
      prestamoMoraPorDefecto,
      inversiones
    } = this.state.grantorSummary;
    let initData = [];
    if (typeof inversiones !== 'undefined' && inversiones != null) {
        initData = {
             invesment: inversiones[0].valorInversion,
             description: inversiones[0].descripcion,
        }; 
    } 
    return (
          <BackGroundM>
            <H2 style={h2Styles} >Bienvenido, {this.props.auth.nombre} {this.props.auth.apellido} </H2>
              <GrantorInvestments 
                guest={estadoUO}
                userFullName={this.props.auth.nombre + ' ' + this.props.auth.apellido}
                inversionActual={inversionActual != null ? inversionActual.toString() : '0'} 
                saldoDisponible={saldoDisponible != null ? saldoDisponible.toString() : '0'} 
                tasaInteresPromedio={tasaInteresPromedio != null ? tasaInteresPromedio.toString() : '0'} 
                investmentState={inversiones != null && inversiones.estadoInversion != null && inversiones.estadoInversion.nombre != null ? inversiones.estadoInversion.nombre : 'NO DISPONIBLE'}
                investmentId={inversiones != null && inversiones.idInversion != null ? inversiones.idInversion : null}
                invesmentProff={inversiones != null && inversiones.comprobanteInversion != null ? inversiones.comprobanteInversion : null}
                initData={initData}
              /> 
       
            
            { !this.state.information ?  
            <Card transparent>
            <View style={cardBottomStyles}>
                <CardItem style={{ borderBottomWidth: 3, borderColor: '#636363', borderTopLeftRadius: 10, borderTopRightRadius: 10, flex: 1, backgroundColor: '#636363' }}>
                    <Body style={{ flex: 1, justifyContent: 'center', alignContent: 'center', flexDirection: 'row' }}>
                        <Item style={{ borderBottomWidth: 0 }} >
                            <Label style={{ fontSize: 18, color: '#fff', fontWeight: 'bold' }}>Préstamos activos</Label>
                        </Item>
                    </Body>
              
                </CardItem>
              
                <CardItem style={{ borderBottomWidth: 3, borderColor: '#636363' }} button onPress={enProceso != null && enProceso != 0 ? () => Actions.outstandingloans() : () => {}}>
                    <Left style={{ flex: 5, zIndex: -1 }}>
                                <Label style={labelFinStyles}>Préstamos en proceso</Label>
                    </Left>
                    <Body style={{ flex: 1, justifyContent: 'center', alignContent: 'center', flexDirection: 'column' }}>
                                <Label style={labelFinStyles}>{enProceso != null ? enProceso : '0'}</Label>
                    </Body>
                    <Right style={{ flex: 1, justifyContent: 'center', alignContent: 'center', flexDirection: 'row' }}>
                                <Icon style={{ color: '#636363' }} name="ios-arrow-forward" />
                    </Right>
                </CardItem>
             
                <CardItem style={{ borderBottomWidth: 3, borderColor: '#636363' }} button onPress={prestamoPazSalvo != null && prestamoPazSalvo != 0 ? () => Actions.loans() : () => {}}>
                    <Left style={{ flex: 5 }}>
                        <Label style={[labelFinStyles, { paddingRight: 10 }]}>Préstamos activos en paz y salvo</Label>
                    </Left>
                    <Body style={{ flex: 1, justifyContent: 'center', alignContent: 'center', flexDirection: 'column' }}>
                        <Label style={labelFinStyles}>{prestamoPazSalvo != null ? prestamoPazSalvo : '0'}</Label>
                    </Body>
                    <Right style={{ flex: 1, justifyContent: 'center', alignContent: 'center', flexDirection: 'row' }}>
                        <Icon style={{ color: '#636363' }} name="ios-arrow-forward" />
                    </Right>
                </CardItem>
                <CardItem style={{ borderBottomWidth: 3, borderColor: '#636363' }} button onPress={prestamosFinalizados != null && prestamosFinalizados != 0 ? () => Actions.grantorhistoric() : () => {}}> 
                    <Left style={{ flex: 5 }}>
                                <Label style={labelFinStyles}>Préstamos Finalizados</Label>
                    </Left>
                    <Body style={{ flex: 1, justifyContent: 'center', alignContent: 'center', flexDirection: 'column' }}>
                                <Label style={labelFinStyles}>{prestamosFinalizados != null ? prestamosFinalizados : '0'}</Label>
                    </Body>
                    <Right style={{ flex: 1, justifyContent: 'center', alignContent: 'center', flexDirection: 'row' }}>
                                <Icon style={{ color: '#636363' }} name="ios-arrow-forward" />
                    </Right>
                </CardItem>
                <View style={{ height: 130, paddingTop: 20, flex: 1 }}>
                 
                    <View style={{ flex: 1, borderTopLeftRadius: 90, borderTopRightRadius: 90, height: 110, justifyContent: 'center', alignItems: 'center', flexDirection: 'column', backgroundColor: '#636363' }}>
                        <View style={{ borderBottomWidth: 0}} >
                            <TouchableWithoutFeedback onPress={this.seeMore.bind(this)}> 
                                    <Icon style={{ color: '#E4FF33' }} name="ios-arrow-up" />
                            </TouchableWithoutFeedback>
                        </View>
                        <View style={{ borderBottomWidth: 0 }} >
                            <TouchableWithoutFeedback onPress={this.seeMore.bind(this)}> 
                                <Label style={{ fontSize: 18, color: '#E4FF33' }}>Más Información</Label>
                            </TouchableWithoutFeedback>
                           
                        </View>
                    </View>
                
                </View>
            </View>
            </Card>

            :
         <Card transparent>
            <View style={cardBottomStyles}>
                 <CardItem style={{ borderBottomWidth: 3, borderColor: '#636363', borderTopLeftRadius: 10, borderTopRightRadius: 10, flex: 1, backgroundColor: '#636363' }}>
                    <View style={{ flex: 1, justifyContent: 'center', alignContent: 'center', flexDirection: 'row' }}>
                        <TouchableWithoutFeedback onPress={this.seeMore.bind(this)}> 
                            <Label style={{ fontSize: 18, fontWeight: 'bold', color: '#E4FF33' }}>Activos en retraso   </Label> 
                        </TouchableWithoutFeedback>
                        <TouchableWithoutFeedback onPress={this.seeMore.bind(this)}> 
                                <Icon style={{ color: '#E4FF33' }} name="ios-arrow-down" />
                        </TouchableWithoutFeedback>
                    </View>
              
                </CardItem> 
                 <CardItem style={{ borderBottomWidth: 3, borderColor: '#636363' }} button onPress={prestamoMoraUno != null && prestamoMoraUno != 0 ? () => Actions.push('lateloans', { type: 0 }) : () => {}}> 
                    <Left style={{ flex: 5 }}>
                            <Label style={labelFinStyles}>Día de gracia</Label>
                    </Left>
                    <Body style={{ flex: 1, justifyContent: 'center', alignContent: 'center', flexDirection: 'column' }}>
                            <Label style={labelFinStyles}>{prestamoMoraUno != null ? prestamoMoraUno : '0'}</Label>
                    </Body>
                    <Right style={{ flex: 1, justifyContent: 'center', alignContent: 'center', flexDirection: 'row' }}>
                                <Icon style={{ color: '#636363' }} name="ios-arrow-forward" />
                    </Right>
                </CardItem> 
             <CardItem style={{ borderBottomWidth: 3, borderColor: '#636363' }} button onPress={prestamoMoraMenorAQuince != null && prestamoMoraMenorAQuince != 0 ? () => Actions.push('lateloans', { type: 1 }) : () => {}}> 
                    <Left style={{ flex: 5 }}>
                            <Label style={labelFinStyles}>2 - 15 Días</Label>
                    </Left>
                    <Body style={{ flex: 1, justifyContent: 'center', alignContent: 'center', flexDirection: 'column' }}>
                            <Label style={labelFinStyles}>{prestamoMoraMenorAQuince != null ? prestamoMoraMenorAQuince : '0'}</Label>
                    </Body>
                    <Right style={{ flex: 1, justifyContent: 'center', alignContent: 'center', flexDirection: 'row' }}>
                                <Icon style={{ color: '#636363' }} name="ios-arrow-forward" />
                    </Right>
                </CardItem>
                <CardItem style={{ borderBottomWidth: 3, borderColor: '#636363' }} button onPress={prestamoMoraMayorAQuince != null && prestamoMoraMayorAQuince != 0 ? () => Actions.push('lateloans', { type: 2 }) : () => {}}> 
                    <Left style={{ flex: 5 }}>
                            <Label style={labelFinStyles}>16 - 30 días</Label>
                    </Left>
                    <Body style={{ flex: 1, justifyContent: 'center', alignContent: 'center', flexDirection: 'column' }}>
                            <Label style={labelFinStyles}>{prestamoMoraMayorAQuince != null ? prestamoMoraMayorAQuince : '0'} </Label>
                    </Body>
                    <Right style={{ flex: 1, justifyContent: 'center', alignContent: 'center', flexDirection: 'row' }}>
                                <Icon style={{ color: '#636363' }} name="ios-arrow-forward" />
                    </Right>
                </CardItem>
                <CardItem style={{ borderBottomWidth: 3, borderColor: '#636363' }} button onPress={prestamoMoraMayorATreinta != null && prestamoMoraMayorATreinta != 0 ? () => Actions.push('lateloans', { type: 3 }) : () => {}}> 
                    <Left style={{ flex: 5 }}>
                            <Label style={labelFinStyles}>31 - 120 días</Label>
                    </Left>
                    <Body style={{ flex: 1, justifyContent: 'center', alignContent: 'center', flexDirection: 'column' }}>
                            <Label style={labelFinStyles}>{prestamoMoraMayorATreinta != null ? prestamoMoraMayorATreinta : '0'} </Label>
                    </Body>
                    <Right style={{ flex: 1, justifyContent: 'center', alignContent: 'center', flexDirection: 'row' }}>
                                <Icon style={{ color: '#636363' }} name="ios-arrow-forward" />
                    </Right>
                </CardItem>
                <CardItem style={{ borderBottomWidth: 0 }} button onPress={prestamoMoraPorDefecto != null && prestamoMoraPorDefecto != 0 ? () => Actions.push('lateloans', { type: 4 }) : () => {}}> 
                    <Left style={{ flex: 5 }}>
                            <Label style={labelFinStyles}>Defecto</Label>
                    </Left>
                    <Body style={{ flex: 1, justifyContent: 'center', alignContent: 'center', flexDirection: 'column' }}>
                            <Label style={labelFinStyles}>{prestamoMoraPorDefecto != null ? prestamoMoraPorDefecto : '0'}</Label>
                    </Body>
                    <Right style={{ flex: 1, justifyContent: 'center', alignContent: 'center', flexDirection: 'row' }}>
                                <Icon style={{ color: '#636363' }} name="ios-arrow-forward" />
                    </Right>
                </CardItem> 
            </View>
            </Card>
            }   
          </BackGroundM>
    );
  }
}

const mapStateToProps = state => {
  return { auth: state.auth };
};

export default connect(mapStateToProps, { setGuestState })(GrantorHome);
