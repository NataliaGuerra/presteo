
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { dataRequest, dataSuccess, dataFailure } from '../../actions';
import APIService from '../../services/APIService';
import { InfoCard } from '../common';
import { RenderInfoFinished } from '../render/renderCards';
import ImageService from '../../services/ImageService';

class InfoFinished extends Component {

  state = {
    loan: {
      usuarioSolicitante: {
        usuario: {
          usuarioPerfil: {},
          nombre: '',
          apellido: ''
        }
      },
      historialPagos: []
    }
  }

  componentWillMount() {
    const { loanId } = this.props; 
    const url = `v1/obtenerPrestamoFinalizado/idPrestamo=${loanId}`;
    this.props.dataRequest();
    APIService.GetAPI(url).then((data) => {
      this.setState({ loan: data });
       
      this.props.dataSuccess();
    }).catch((error) => {
      this.props.dataFailure(error);
    });
  }

  render() {
    const { usuarioSolicitante, tipoPrestamo } = this.state.loan;
    return (
      <InfoCard>
          <RenderInfoFinished
            img={usuarioSolicitante != null 
              && usuarioSolicitante.usuario != null 
              && usuarioSolicitante.usuario.usuarioPerfil != null ? ImageService.getUrlFormat(usuarioSolicitante.usuario.usuarioPerfil.fotoPerfil) : null}
            earnings={this.state.loan.ganancias}
            qa={usuarioSolicitante != null && usuarioSolicitante.calificacion != null ? usuarioSolicitante.calificacion : 0 }
            invested={this.state.loan.monto}
            loanType={tipoPrestamo != null && tipoPrestamo.nombre != null ? tipoPrestamo.nombre : 'null'}
            startDate={this.state.loan.fechaCreacion != null ? this.state.loan.fechaCreacion.substring(0, this.state.loan.fechaCreacion.indexOf('T')) : ''}
            endDate={this.state.loan.fechaFinalizacion != null ? this.state.loan.fechaFinalizacion.substring(0, this.state.loan.fechaFinalizacion.indexOf('T')) : ''}
            fullName={`${usuarioSolicitante != null ? usuarioSolicitante.usuario.nombre : ''} ${usuarioSolicitante != null ? usuarioSolicitante.usuario.apellido : ''}`}
            onPress={this.state.loan.historialPagos != null ? () => Actions.paymentHistoricGrantor({ 
              history: this.state.loan.historialPagos, 
              user: this.state.loan.usuarioSolicitante
            }) : () => {}}
          />
      </InfoCard>
    );
  }
}

export default connect(null, { dataRequest, dataSuccess, dataFailure })(InfoFinished);
