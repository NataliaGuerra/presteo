import React, { Component } from 'react';
import { 
    Body, 
    Text, List, ListItem, Right,
    Button, Left, Card, CardItem
} 
from 'native-base';
import { Actions } from 'react-native-router-flux';
import { View } from 'react-native';

export default class GrantorDalys extends Component {
  state = {
    online: true
  };
  
  render() {
    const { 
      prestamoMoraUno, prestamoMoraMenorAQuince, 
      prestamoMoraMayorAQuince, prestamoMoraMayorATreinta, 
      prestamoMoraPorDefecto 
    } = this.props;
    return (
      <View>
        <Card>
        <CardItem header bordered>
          <Text>Activos En Retraso</Text>
        </CardItem>
          <CardItem>
            <View style={{ flex: 1 }}>
              <List>
                <ListItem thumbnail>
                  <Left>
                  <Text>Dia de gracia</Text>
                  </Left>
                  <Body>
                    <Text>{prestamoMoraUno}</Text>
                    <Text note numberOfLines={1} />
                  </Body>
                  <Right>
                    <Button onPress={() => Actions.push('lateloans', { type: 0 })} disabled={prestamoMoraUno <= 0} transparent>
                      <Text>Ver</Text>
                    </Button>
                  </Right>
                </ListItem>
                <ListItem thumbnail>
                  <Left>
                  <Text>2 - 15</Text>
                  </Left>
                  <Body>
                    <Text>{prestamoMoraMenorAQuince}</Text>
                    <Text note numberOfLines={1} />
                  </Body>
                  <Right>
                    <Button onPress={() => Actions.push('lateloans', { type: 1 })} disabled={prestamoMoraMenorAQuince <= 0}transparent>
                      <Text>Ver</Text>
                    </Button>
                  </Right>
                </ListItem>
                <ListItem thumbnail>
                  <Left>
                  <Text>16 - 30</Text>
                  </Left>
                  <Body>
                    <Text>{prestamoMoraMayorAQuince}</Text>
                    <Text note numberOfLines={1} />
                  </Body>
                  <Right>
                    <Button onPress={() => Actions.push('lateloans', { type: 2 })} disabled={prestamoMoraMayorAQuince <= 0} transparent>
                      <Text>Ver</Text>
                    </Button>
                  </Right>
                </ListItem>
                <ListItem thumbnail>
                  <Left>
                  <Text>31 - 120</Text>
                  </Left>
                  <Body>
                    <Text>{prestamoMoraMayorATreinta}</Text>
                    <Text note numberOfLines={1} />
                  </Body>
                  <Right>
                    <Button onPress={() => Actions.push('lateloans', { type: 3 })} disabled={prestamoMoraMayorATreinta <= 0} transparent>
                      <Text>Ver</Text>
                    </Button>
                  </Right>
                </ListItem>
                <ListItem thumbnail>
                  <Left>
                  <Text>Defecto</Text>
                  </Left>
                  <Body>
                    <Text>{prestamoMoraPorDefecto}</Text>
                    <Text note numberOfLines={1} />
                  </Body>
                  <Right>
                    <Button onPress={() => Actions.push('lateloans', { type: 4 })} disabled={prestamoMoraPorDefecto <= 0} transparent>
                      <Text>Ver</Text>
                    </Button>
                  </Right>
                </ListItem>
              </List>
            </View>
          </CardItem>
        </Card>
      </View>
    
    );
  }
}

