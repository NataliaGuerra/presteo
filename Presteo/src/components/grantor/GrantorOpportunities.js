
import React, { Component } from 'react';
import { View } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { RenderLoanOpportunities } from '../render/renderCards';
import APIService from '../../services/APIService';
import { ListCards } from '../common';
import { dataRequest, dataSuccess, dataFailure, hideModal, showModal } from '../../actions';


class GrantorOpportunities extends Component {

    state = {
      loans: [],
      contract: '',
      secureCapital: '',
      idLoan: 0,
      applicantFullname: '',
      refreshing: false
    };
    componentWillMount() {
      this.props.dataRequest();
      this.getData(0).then((data) => {
          this.props.dataSuccess();
          this.setState({ loans: data });
      }).catch((error) => {
          this.props.dataFailure(error);
      });

      this.getData(1).then((data) => {
        this.setState({ contract: data.descripcion });
      });

      this.getData(5).then((data) => {
        this.setState({ secureCapital: data.descripcion });
      });
    }

    onRefresh() {
      this.setState({ refreshing: true });
      this.getData(0).then((data) => {
        this.setState({ refreshing: false });
        this.setState({ loans: data });
      }).catch(() => {
        this.setState({ refreshing: false });
      });
    }

    //i = index
    getData(i) {
      return new Promise((resolve, reject) => {
        let url = '';
        switch (i) {
          case 0: 
            url = `v1/listarSolicitudesPrestamo/idUsuarioOtorgante=${this.props.auth.idRolUsuario}`;
            break;
          case 1:
             url = 'v1/obtenerContratoAceptarPrestamo/';
            break;
          case 2:
            url = `v1/asignarOtorgantePrestamo/${this.state.idLoan}/${this.props.auth.idRolUsuario}`;
             
           break;
          case 3:
            url = `v1/asignarContratoAceptacionOtorgante/${this.state.idLoan}`;
             
           break;
          case 4:
            url = `v1/ingresarPrestamoRechazado/idUsuarioOtorgante=${this.props.auth.idRolUsuario}/idPrestamo=${this.state.idLoan}`;
            break;
          case 5:
            url = 'v1/obtenerInfoTipoContrato/tipoContrato/SEGURO%20CAPITAL';
            break;
          default:           
             url = 'v1/obtenerContratoAceptarPrestamo/';
        }
        APIService.GetAPI(url)
        .then((data) => { resolve(data); })
        .catch((error) => { 
          reject(error); 
        });
      });
    }

    getInfo(idLoan) {
      const idRol = this.props.auth.idRolUsuario;
      Actions.push('infoopportunities', { idLoan, idRol });
    }

    acceptContract() {
      this.getData(2).then((data) => {
        this.getData(3).then((response) => {
          this.props.hideModal();
          this.showModal(2);
        });
      });
    }

    acceptContSecureCapital() {
      this.props.hideModal();
        this.showModal(1);
    }

    rejectLoan() {
      this.getData(4).then((data) => {
        this.getData(0).then((response) => {
          this.setState({ loans: response });
          this.props.hideModal();
        });
      });
    }
    
    showModal(index, loan = null) {
       
      
      if (loan != null) {
        this.setState({ idLoan: loan.idPrestamo });
        this.setState({ applicantFullname: `${loan.usuarioSolicitante.usuario.nombre} ${loan.usuarioSolicitante.usuario.apellido}` });
      }

      switch (index) {
        case 0: 
          this.props.showModal({
            open: true,
            title: 'Información sobre el seguro del capital en realización de préstamos.',
            message: this.state.secureCapital,
            closeModal: this.acceptContSecureCapital.bind(this)
          }, 'agreement');
          break;
        case 1:
          this.props.showModal({
            open: true,
            title: 'Aceptar contrato',
            message: this.state.contract,
            closeModal: this.acceptContract.bind(this)
          }, 'agreement');
          break;
        case 2:
          if (this.state.modalInfoVisible) {
            this.props.hideModal();
            this.getData(0).then((res) => { this.setState({ loans: res }); });
          } else {
            this.setState({ modalInfoVisible: !this.state.modalInfoVisible });
            this.props.showModal({
              open: true,
              title: 'Alert Modal',
              message: 'Te enviaremos una notificación, cuándo ' + this.state.applicantFullname + ' confirme  la aceptación del crédito.',
              closeModal: this.showModal.bind(this, 2, null)
            }, 'success');
          }
         break;
        case 3:
          this.props.showModal({
            open: true,
            title: 'Alert Modal',
            message: '¿Está seguro que desea rechazar el prestamo?',
            closeModal: this.rejectLoan.bind(this, 2)
          }, 'success');
         break;
        default:           
          this.props.showModal({
            open: true,
            title: 'Alert Modal',
            message: this.state.secureCapital,
            closeModal: this.acceptContSecureCapital.bind(this)
          }, 'agreement');
      }
    } 

    renderCards() {
      let index = 1;
       
      if (this.state.loans.length > 0) {
        return this.state.loans.map(loan =>
          <RenderLoanOpportunities
            key={loan.idPrestamo} 
            title={`Préstamo ${index++}`}
            amount={loan.monto} 
            earnings={loan.ganancias} 
            time={loan.tiempoPrestamo != null && loan.tiempoPrestamo.duracion != null ? loan.tiempoPrestamo.duracion : ''} 
            type={loan.tipoPrestamo && loan.tiempoPrestamo.nombre != null ? loan.tiempoPrestamo.nombre : ''} 
            img={loan.usuarioSolicitante != null && 
              loan.usuarioSolicitante.usuario != null &&
              loan.usuarioSolicitante.usuario.usuarioPerfil != null ? 
              loan.usuarioSolicitante.usuario.usuarioPerfil.fotoPerfil : ''}
            qa={loan.usuarioSolicitante && loan.usuarioSolicitante.calificacion != null ? loan.usuarioSolicitante.calificacion : 0}
            onPress={this.getInfo.bind(this, loan.idPrestamo)}
            guest={this.props.auth.estadoOU}
            onRejectLoan={this.showModal.bind(this, 3, loan)}
            onPressContract={this.showModal.bind(this, 0, loan)}
          />
        );
      }
      return (<View />);
    }
    
    render() {
      return ( 
        <ListCards 
          onRefresh={this.onRefresh.bind(this)}
          refreshing={this.state.refreshing}
          title={'Oportunidades de Inversión'}
        >
          {this.renderCards()}
        </ListCards>
      );
    }
}

const mapDispatchToProps = dispatch => ({
  dataRequest: () => dispatch(dataRequest()),
  dataSuccess: () => dispatch(dataSuccess()),
  dataFailure: () => dispatch(dataFailure()),
  hideModal: () => dispatch(hideModal()),
  showModal: (modalProps, modalType) => {
    dispatch(showModal({ modalProps, modalType }));
  }
});

const mapStateToProps = state => {
  return { auth: state.auth };
};

GrantorOpportunities = connect(
  mapStateToProps, mapDispatchToProps
)(GrantorOpportunities);


export default connect(mapStateToProps, mapDispatchToProps)(GrantorOpportunities);
