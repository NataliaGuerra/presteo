
import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { RenderPenPays } from '../render/renderCards';
import APIService from '../../services/APIService';
import { ListCards } from '../common';
import { dataRequest, dataSuccess, dataFailure } from '../../actions';
import ImageService from '../../services/ImageService';

class OutstandingLoans extends Component {

	state = {
		loans: [],
		refreshing: false
	};
	componentWillMount() {
		this.props.dataRequest();
		this.getData().then((data) => {
			this.props.dataSuccess();
			 
			this.setState({ loans: data });
		}).catch((error) => {
			this.props.dataFailure(error);
		});
	}

	onRefresh() {
		this.setState({ refreshing: true });
		this.getData().then((data) => {
			this.setState({ refreshing: false });
			this.setState({ loans: data });
		}).catch(() => {
			this.setState({ refreshing: false });
		});
	}

	getData() {
		return new Promise((resolve, reject) => {
			const url = `v1/listarSolicitudPendientes/idUsuarioOtorgante=${this.props.auth.idRolUsuario}`;
			APIService.GetAPI(url)
			.then((data) => { resolve(data); })
			.catch((error) => { reject(error); });
		});
	}

	getInfo(idLoan) {
		Actions.push('infoutstanding', { idLoan });
	}

	renderCards() {
		let index = 1;
		 
		return this.state.loans.map(loan =>
			<RenderPenPays 
				key={loan.idPrestamo} 
				earnings={loan.ganancias}
				amount={loan.monto}
				time={loan.tiempoPrestamo}
				type={loan.tipoPrestamo}
				img={loan.usuarioSolicitante != null && loan.usuarioSolicitante.fotoPerfil != null ? ImageService.getUrlFormat(loan.usuarioSolicitante.fotoPerfil) : null}
				qa={loan.usuarioSolicitante.calificacion}
				onPress={this.getInfo.bind(this, loan.idPrestamo)}
				large
			/>
		);
	}
	
	render() {
		return (
			<ListCards 
				onRefresh={this.onRefresh.bind(this)}
				refreshing={this.state.refreshing}
				title={'Préstamos pendientes'}
			>
				{this.renderCards()}
			</ListCards>
		);
	}
}

const mapStateToProps = state => {
	return { auth: state.auth };
};
export default connect(mapStateToProps, { dataRequest, dataSuccess, dataFailure })(OutstandingLoans);
  
