
import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';
import Modal from 'react-native-modal';
import { connect } from 'react-redux';
import { RenderPenPays, RenderInfoLoans } from '../render/renderCards';
import APIService from '../../services/APIService';
import { HistoricView, BackGroundM } from '../common';
import ImageService from '../../services/ImageService';
import { dataRequest, dataSuccess, dataFailure } from '../../actions';

class InfoActive extends Component {

  state = {
    loan: {
      tiempoPrestamo: [],
      tipoPrestamo: [],
      historialPagos: [],
      usuarioSolicitante: {
        usuario: {
          usuarioPerfil: {}
        }
      },
      estadoPrestamo: [],
      proximoPago: '',
      ultimoPago: ''
    },
    modalHistoricVisibility: false,
  };
  
  componentWillMount() {
    this.props.dataRequest();
    this.getData().then((data) => {
      this.props.dataSuccess();
      this.setState({ loan: data });
    }).catch((error) => {
      this.props.dataFailure(error);
    });
  }

  getData() {
     
    return new Promise((resolve, reject) => {
      const url = `v1/obtenerPrestamoActivo/idPrestamo=${this.props.idLoan}`;
      APIService.GetAPI(url)
      .then((data) => { resolve(data); })
      .catch((error) => { reject(error); });
    });
  }

  goBack() {
    Actions.pop();
  }

  render() {
    const { loan } = this.state;
    return (
      <BackGroundM title='Préstamo Activo'>
          <RenderPenPays 
            earnings={loan.ganancias}
            amount={loan.monto}
            time={loan.tiempoPrestamo.duracion}
            type={loan.tipoPrestamo.nombre}
            img={loan.usuarioSolicitante != null && 
              loan.usuarioSolicitante.usuario != null &&
              loan.usuarioSolicitante.usuario.usuarioPerfil != null ? 
              ImageService.getUrlFormat(loan.usuarioSolicitante.usuario.usuarioPerfil.fotoPerfil) : ''}
            title={'Solicitado'}
            qa={loan.usuarioSolicitante != null && 
              loan.usuarioSolicitante.calificacion != null ? 
              loan.usuarioSolicitante.calificacion : null}
          />
          <RenderInfoLoans
            capitalAmount={loan.aporteCapital}
            interestAmount={loan.aporteIntereses}
            nextPayment={loan.proximoPago}
            lastPayment={loan.ultimoPago}
            pendingPayments={loan.cuotasPendientes}
            paymentsMade={loan.pagosRealizados}
            state={loan.estadoPrestamo.nombre}
            onPress={() => this.setState({ modalHistoricVisibility: true })}
            title={'Crédito 1'}
          />
          <Modal
            isVisible={this.state.modalHistoricVisibility}
            onBackdropPress={() => this.setState({ modalHistoricVisibility: false })}
            style={{ margin: 0, flex: 1 }}
          >
            <HistoricView
              payments={loan.historialPagos}
              type={loan.tipoPrestamo.nombre}
              user={loan.usuarioSolicitante} 
              imageUrl={loan.usuarioSolicitante.usuario != null &&
                loan.usuarioSolicitante.usuario.usuarioPerfil != null ?
                loan.usuarioSolicitante.usuario.usuarioPerfil.fotoPerfil : ''}
              acceptFunction={() => this.setState({ modalHistoricVisibility: false })}
            />
          </Modal>
      </BackGroundM>
    );
  }
}

export default connect(null, { dataRequest, dataSuccess, dataFailure })(InfoActive);
