
import React, { Component } from 'react';
import { View } from 'react-native';
import { Actions } from 'react-native-router-flux';
import Modal from 'react-native-modal';
import { connect } from 'react-redux';
import { Text, Button, Icon, H2 } from 'native-base';
import { HistoricView, PopUp, BackGroundM } from '../common';
import APIService from '../../services/APIService';
import AlertService from '../../services/AlertService';
import { RenderPenPays, RenderInfoLoans } from '../render/renderCards';
import { BUCKET_URL } from '../../config/variables';
import { hideModal, showModal, dataRequest, dataSuccess, dataFailure } from '../../actions';
import ImageService from '../../services/ImageService';


class InfoLate extends Component {

  state = {
    loan: {
      tiempoPrestamo: [],
      tipoPrestamo: [],
      modalSuccess: false,
      usuarioSolicitante: {
        usuario: {
          usuarioPerfil: {
            fotoPerfil: ''
          }
        }
      },
      proximoPago: '',
      ultimoPago: ''
    },
    modalHistoricVisibility: false
  };
  
  componentWillMount() {
    this.props.dataRequest();
    this.getData(0).then((data) => {
      this.props.dataSuccess();
      this.setState({ loan: data });
    }).catch((error) => {
      this.props.dataFailure(error);
    });
  }

  //i = index
  getData(i) {
    return new Promise((resolve, reject) => {
      let url = '';
      switch (i) {
        case 0: 
          url = `v1/obtenerPrestamoEnRetraso/idPrestamo=${this.props.idLoan}`;
          break;
        case 1:
          url = `v1/actualizarEstadoPrestamoPagosAtrasados/${this.props.idLoan}`;
          break;
        default:           
          url = `v1/obtenerPrestamoEnRetraso/idPrestamo=${this.props.idLoan}`;
      }
      APIService.GetAPI(url)
      .then((data) => { resolve(data); })
      .catch((error) => { 
        reject(error);
        AlertService.showError();
      });
    });
  }

  rejectLoan() {
 /*    Actions.rejectloan(); */
    Actions.push('rejectloan', { idLoan: this.props.idLoan });
   /*  this.getData(1).then((data) => {
       
      this.showModal();
    }); */
  }

  closeModal() {
    this.props.hideModal();
  }

  pqr() {
    this.props.showModal({
      open: true,
      title: 'Alert Modal',
      message: 'Funcionalidad en Proceso!',
      closeModal: this.closeModal.bind(this)
    }, 'maintenance'); 
  }

  showModal(back = null) {
    this.setState({ modalSuccess: !this.state.modalSuccess });  
    if (back != null) {
      Actions.pop();
    }
  } 

  render() {
    const { subtitle, recoverCapital } = this.props;
    const { loan } = this.state;
    return (

        <BackGroundM>
      
          <H2 style={{ fontSize: 22, fontWeight: 'bold', color: '#2b2b2b', textAlign: 'center', marginTop: 20 }} >Préstamo Atrasado</H2>
          <H2 style={{ fontSize: 18, fontWeight: 'bold', color: '#2b2b2b', textAlign: 'center' }} > {subtitle}</H2>
    
       <RenderPenPays 
            earnings={loan.ganancias}
            amount={loan.monto}
            time={loan.tiempoPrestamo.duracion}
            type={loan.tipoPrestamo.nombre}
            qa={loan.usuarioSolicitante != null && loan.usuarioSolicitante.calificacion != null ? loan.usuarioSolicitante.calificacion : 0}
            img={loan.usuarioSolicitante != null && loan.usuarioSolicitante.usuario != null &&
              loan.usuarioSolicitante.usuario.usuarioPerfil.fotoPerfil != undefined && loan.usuarioSolicitante.usuario.usuarioPerfil.fotoPerfil != null ?
              ImageService.getUrlFormat(loan.usuarioSolicitante.usuario.usuarioPerfil.fotoPerfil) : null}
            title={'Solicitado'}
          /> 

          <RenderInfoLoans
            capitalAmount={loan.aporteCapital}
            interestAmount={loan.aporteIntereses}
            nextPayment={loan.proximoPago}
            lastPayment={loan.ultimoPago}
            pendingPayments={loan.cuotasPendientes}
            paymentsMade={loan.pagosRealizados}
            state={loan.estadoMora}
            dayPast={loan.diasMora}
            onPress={() => this.setState({ modalHistoricVisibility: true })}
          /> 
     {/*    <RenderPenPays 
            earnings={30000}
            amount={30000}
            time={'1 MES'}
            type={'PUBLICOS'}
            img={null}
            qa={3}
            title={'Solicitado'}
          />

          <RenderInfoLoans
            capitalAmount={50}
            interestAmount={50}
            nextPayment={'234234234354'}
            lastPayment={'12341423423423434'}
            pendingPayments={300000}
            paymentsMade={5}
            state={'a'}
            dayPast={15}
            onPress={() => this.setState({ modalHistoricVisibility: true })}
          />  */}
          {recoverCapital === true &&
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', flexDirection: 'row', marginBottom: 20, marginTop: 20 }}>
              <Button accept onPress={this.rejectLoan.bind(this)} small>
                <Icon sdtyle={{ color: '#E4FF33' }} name='md-refresh' />
                <Text>Recuperar Capital</Text>
              </Button>
              <Button accept onPress={this.pqr.bind(this)} style={{ marginLeft: 10 }} small>
                <Text>PQR</Text>
              </Button>
            </View>
          }
     			<Modal
            isVisible={this.state.modalHistoricVisibility}
						onBackdropPress={() => this.setState({ modalHistoricVisibility: false })}
						style={{ margin: 0, flex: 1 }}
          >
						<HistoricView
							payments={loan.historialPagos}
							user={loan.usuarioSolicitante} 
							imageUrl={loan.usuarioSolicitante.usuario != null &&
								loan.usuarioSolicitante.usuario.usuarioPerfil.fotoPerfil != null ?
								BUCKET_URL + loan.usuarioSolicitante.usuario.usuarioPerfil.fotoPerfil : ''}
							acceptFunction={() => this.setState({ modalHistoricVisibility: false })}
						/> 
					</Modal> 
        <PopUp onPress={this.showModal.bind(this, true)} closeFunction={() => this.showModal()} visible={this.state.modalSuccess} >
          <Text style={{ fontSize: 15 }}>
            Se realizo correctamente la recuperación del capital 
          </Text>
        </PopUp>  
      
      	</BackGroundM>

    );
  }
}


const mapDispatchToProps = dispatch => ({
  dataRequest: () => dispatch(dataRequest()),
	dataSuccess: () => dispatch(dataSuccess()),
	dataFailure: () => dispatch(dataFailure()),
  hideModal: () => dispatch(hideModal()),
  showModal: (modalProps, modalType) => {
    dispatch(showModal({ modalProps, modalType }));
  }
});

const mapStateToProps = state => {
  return { auth: state.auth };
};

InfoLate = connect(
  mapStateToProps, mapDispatchToProps
)(InfoLate);


export default InfoLate;

