import React, { Component } from 'react';
import { StyleProvider, Container } from 'native-base';
import { Actions } from 'react-native-router-flux';
import getTheme from '../../themes/components';
import platform from '../../themes/variables/platform';
import ImageService from '../../services/ImageService';
import { HistoricView, BackGround } from '../common';

class PaymentFinalizedHistoricGrantor extends Component {
  
  render() {
		const { user } = this.props;
    return (
      <StyleProvider style={getTheme(platform)}>
      	<Container>
          <BackGround>
						<HistoricView
							payments={this.props.history}
							user={user} 
							imageUrl={user.usuario != null && user.usuario.usuarioPerfil != null ?
								ImageService.getUrlFormat(user.usuario.usuarioPerfil.fotoPerfil) : ''}
							acceptFunction={() => Actions.pop()}
						/>
					</BackGround> 
				</Container>
			</StyleProvider>
    );
  }
}
export default PaymentFinalizedHistoricGrantor;
