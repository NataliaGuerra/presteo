
import React, { Component } from 'react';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { Agreement } from '../common';
import APIService from '../../services/APIService';
import AlertService from '../../services/AlertService';
import { hideModal, showModal } from '../../actions';

class RejectLoan extends Component {

  state = { 
    contract: {
      contrato: ''
    }
  }
  
  componentWillMount() {
    this.getData(1).then((response) => {
      this.setState({ contract: response });
    }); 
  }

  onCancel() {
    Actions.pop();
  }

  getData(index) {
    return new Promise((resolve, reject) => {
  
      let url = '';
      switch (index) {
        case 0: 
          url = `v1/actualizarEstadoPrestamoPagosAtrasados/${this.props.idLoan}`;
          break;
        case 1:
           url = 'v1/obtenerContratoCancelacion/';
          break;
        default:           
           url = `v1/actualizarEstadoPrestamoPagosAtrasados/${this.props.idLoan}`;
      }
      APIService.GetAPI(url)
      .then((data) => { resolve(data); })
      .catch((error) => { 
        reject(error);
        AlertService.showError();
      });
    });
  }

  closeModal() {
    this.props.hideModal();
    Actions.pop();
}

    rejectLoan() {
    this.getData(0).then(() => {
      this.props.showModal({
        open: true,
        title: 'Alert Modal',
        message: 'Tu solicitud ha sido aprobada, te invitamos a que crees una nueva solicitud.',
        closeModal: this.closeModal.bind(this)
      }, 'success');
    }); 
  }

  render() {
    return (
      <Agreement 
            title={'Términos de cancelación de préstamo'}
            contract={this.state.contract.contrato}
            onPress={this.rejectLoan.bind(this)}
            onCancel={this.onCancel.bind(this)}
      /> 
    );
  }
}

const mapDispatchToProps = dispatch => ({
    hideModal: () => dispatch(hideModal()),
    showModal: (modalProps, modalType) => {
      dispatch(showModal({ modalProps, modalType }));
    }
});

const mapStateToProps = state => {
    return { auth: state.auth };
};

export default connect(mapStateToProps, mapDispatchToProps)(RejectLoan);
