
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { RenderPenPays, RenderInfoApplicant, RenderSupportReceipt } from '../render/renderCards';
import APIService from '../../services/APIService';
import { BackGroundM } from '../common';
import ImageService from '../../services/ImageService';
import { hideModal, showModal, dataRequest, dataSuccess, dataFailure } from '../../actions'; 

class InfoOutstanding extends Component {
	state = {
		loan: {
			tiempoPrestamo: [],
			tipoPrestamo: [],
			usuarioSolicitante: {
				usuario: {
					usuarioPerfil: {}
				}
			},
			soporteFactura: {}
		},
		supportVisibility: false,
	};
	
	componentWillMount() {
		this.props.dataRequest();
		this.getData().then((data) => {
			this.props.dataSuccess();
			this.setState({ loan: data });
		}).catch((error) => {
			this.props.dataFailure(error);
		});
	}

	getData() {
		 
		return new Promise((resolve, reject) => {
			const url = 'v1/obtenerPrestamoPendiente/idPrestamo=' + this.props.idLoan;
			APIService.GetAPI(url)
			.then((data) => { resolve(data); })
			.catch((error) => { reject(error); });
		});
	}

	closeModal() {
		this.props.hideModal();
	}

	showModal(urlImg) {
		 
		this.props.showModal({
            open: true,
			url: urlImg,
			pqr: true,
			closeModal: this.closeModal.bind(this),
			pqrModal: this.pqr.bind(this)
        }, 'image');
	}

	pqr() {
		this.props.showModal({
			open: true,
			title: 'Alert Modal',
			message: 'Funcionalidad en Proceso!',
			closeModal: this.closeModal.bind(this)
		}, 'maintenance');
	}

	render() {
		const { loan } = this.state;
		 
		return (
          <BackGroundM title='Resumen del préstamo'>
			<RenderPenPays 
				title={'Solicitado'}
				earnings={loan.ganancias}
				amount={loan.monto}
				time={loan.tiempoPrestamo.duracion !== null ? loan.tiempoPrestamo.duracion : 0}
				type={loan.tipoPrestamo.nombre}
				img={loan.usuarioSolicitante != null && 
					loan.usuarioSolicitante.usuario != null &&
					loan.usuarioSolicitante.usuario.usuarioPerfil != null ? 
					ImageService.getUrlFormat(loan.usuarioSolicitante.usuario.usuarioPerfil.fotoPerfil) : ''}
				qa={loan.usuarioSolicitante != null && 
					loan.usuarioSolicitante.calificacion != null ? 
					loan.usuarioSolicitante.calificacion : null}
			/>
			<RenderInfoApplicant 
				title={'Info Solicitante'}
				finished={loan.usuarioSolicitante.creditosFinalizados == null ? -1 : loan.usuarioSolicitante.creditosFinalizados}
				canceled={loan.usuarioSolicitante.creditosCancelados == null ? -1 : loan.usuarioSolicitante.creditosCancelados}
				total={loan.usuarioSolicitante.creditosTotales == null ? -1 : loan.usuarioSolicitante.creditosTotales}
				later={loan.usuarioSolicitante.promedioRetraso == null ? -1 : loan.usuarioSolicitante.promedioRetraso}
				maxAmount={loan.usuarioSolicitante.montoMaximo == null ? -1 : loan.usuarioSolicitante.montoMaximo}
				minAmount={loan.usuarioSolicitante.montoMinimo == null ? -1 : loan.usuarioSolicitante.montoMinimo }
			/>
			<RenderSupportReceipt 
				state={loan.soporteFactura != null &&
					loan.soporteFactura.estadoSoporte != null ?
					loan.soporteFactura.estadoSoporte.descripcion : ''}
				url={loan.soporteFactura == null ? '' : ImageService.getUrlFormat(loan.soporteFactura.urlSoporte)}
				onPress={() => this.showModal(loan.soporteFactura == null ? '' : ImageService.getUrlFormat(loan.soporteFactura.urlSoporte))}
				title={'Solicitado'}
			/>  
					
		</BackGroundM>  
		);
	}
}

const mapDispatchToProps = dispatch => ({
	dataRequest: () => dispatch(dataRequest()),
	dataSuccess: () => dispatch(dataSuccess()),
	dataFailure: () => dispatch(dataFailure()),
  hideModal: () => dispatch(hideModal()),
  showModal: (modalProps, modalType) => {
    dispatch(showModal({ modalProps, modalType }));
  }
});

const mapStateToProps = state => {
  return { auth: state.auth };
};

InfoOutstanding = connect(
  mapStateToProps, mapDispatchToProps
)(InfoOutstanding);


export default InfoOutstanding;
