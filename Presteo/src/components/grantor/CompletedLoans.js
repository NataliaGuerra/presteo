
import React, { Component } from 'react';
import { Container, Header, Body, Title } from 'native-base';
import { CardOne } from '../common';

class CompletedLoans extends Component {
    render() {
        return (
            <Container>
                <Header noShadow>
                
                    <Body>
                        <Title>PRÉSTAMOS FINALIZADOS</Title>
                    </Body>
              
            </Header>
                <CardOne />
            </Container>
        );
    }
}

export default CompletedLoans;
