

const required = value => (value ? undefined : 'Campo obligatorio.');

const maxLength = max => value =>
  value && value.length > max ? `Debe tener ${max} caracteres o menos` : undefined;


const maxLength4 = maxLength(4);
const maxLength11 = maxLength(11);

const minLength = min => value =>
  value && value.length < min ? `Debe tener ${min} caracteres o más` : undefined;

const minLength4 = minLength(4);

const exactLength = val => value =>
  value && (value.length < val || value.length > val) ? `Debe tener  ${val} dígitos exactamente` : undefined;

const exactLength4 = exactLength(4);

const exactLength10 = exactLength(10);

const number = value =>
  value && isNaN(Number(value)) ? 'Debe ser númerico' : undefined;


const minValue = min => value =>
  value && value < min ? `Must be at least ${min}` : undefined;

const minValue18 = minValue(18);

const email = value =>
  value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
    ? 'Dirección de correo electrónico no válida'
    : undefined;


const tooOld = value =>
  value && value > 65 ? 'You might be too old for this' : undefined;


const aol = value =>
  value && /.+@aol\.com/.test(value)
    ? 'Really? You still use AOL for your email?'
    : undefined;


const alphaNumeric = value =>
  value && /[^a-zA-Z0-9 ]/i.test(value)
    ? 'Solo caracteres alfanuméricos'
    : undefined;
    
const alphabetical = value =>
    value && /[^a-zA-Z ]/i.test(value)
      ? 'Solo caracteres alfabéticos'
      : undefined;

const phoneNumber = value =>
  value && !/^(0|[1-9][0-9]{9})$/i.test(value)
    ? 'El número de teléfono no es válido, debe tener 10 dígitos'
    : undefined;

    
const selectRequired = value =>
value && value === 'default' ? `Campo obligatorio.` : undefined;

export { 
    required, 
    maxLength4, 
    minLength, 
    minLength4, 
    number,
    minValue, 
    exactLength4,
    exactLength,
    minValue18, 
    email, 
    tooOld, 
    aol,
    alphaNumeric, 
    phoneNumber,
    selectRequired,
    alphabetical,
    maxLength11,
    exactLength10
}; 
