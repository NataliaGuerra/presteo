import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Content, Text, StyleProvider, Container, H2, Button } from 'native-base';
import { connect } from 'react-redux';
import { dataRequest, dataSuccess, dataFailure, showModal, hideModal } from '../../actions';
import APIService from '../../services/APIService';
import { BackGround } from '../common';
import getTheme from '../../themes/components';
import platform from '../../themes/variables/platform';
import { scale } from '../../helpers/Scaler';
import { RenderSummaryInfo, RenderQuotas } from './../render/renderCards';

class SummaryCreateLoan extends Component {
  
  state = {
    payments: [],
    modalVisible: false
  }

  componentWillMount() {
    const url = 'v1/obtenerCuotasEsperadas/';
    const { loan, auth } = this.props;
    const id = auth.idRolUsuario;
    const body = {
      usuarioSolicitante: {
        idUsuarioSolicitante: id
      },
      monto: loan.valueLoan,
      tiempoPrestamo: {
        idTiempoPrestamo: loan.kindOfTime
      },
      modalidadPago: {
        idModalidadPago: loan.kindOfModality  
      },
      tipoPrestamo: {
        idTipoPrestamo: loan.kindOfLoan
      }
		};
		this.props.dataRequest();
    APIService.PutAPI(url, body).then((data) => {
			this.props.dataSuccess();
			this.setState({ payments: data });
    }).catch((error) => {
			this.props.dataFailure(error);
		});
  }

  closeModal() {
    this.props.hideModal();
  }

  verifyIdentity() {
    this.props.hideModal();
    Actions.verifyIdentity();
  }

  showInformation() {
    this.props.dataRequest();
    const url = 'v1/obtenerInfoTipoContrato/tipoContrato/RESUMEN%20SOLICITUD';
    APIService.GetAPI(url).then((data) => {
      this.props.dataSuccess();
      setTimeout(() => {
        this.props.showModal({
          open: true,
          title: 'Alert Modal',
          message: data.descripcion,
          closeModal: this.closeModal.bind(this)
        }, 'info');
      }, 200);
    }).catch((error) => {
      this.props.dataFailure(error);
    });
  }
  
  confirmLoan() {
    setTimeout(() => {
      this.props.showModal({
        open: true,
        title: 'Estás a un paso de solicitar tu préstamo',
        message: 'Por último, valida tu identidad para continuar!',
        closeModal: this.verifyIdentity.bind(this)
      }, 'confirm');
    }, 200);
  }

  render() {
		const { titleContainer, requestContainer, paymentsContainer,
			buttonsContainer, titleTextStyle, 
      buttonNext } = styles;
    return (
			<StyleProvider style={getTheme(platform)}>
				<Container>        
					<BackGround>
						<Content>
							<View style={titleContainer}>
								<H2 style={titleTextStyle}>Resumen solicitud</H2>
							</View>
							<View style={requestContainer}>
								<RenderSummaryInfo 
									valueLoan={this.props.loan.valueLoan}
									nameOfModality={this.props.loan.nameOfModality}
									nameOfTime={this.props.loan.nameOfTime}
									nameOfLoan={this.props.loan.nameOfLoan}
									length={this.state.payments.length}
									visibility={this.state.modalVisible}
									onPress={() => this.setState({ modalVisible: true })}
									url={this.props.loan.receiptFront.uri}
									acceptFunction={() => this.setState({ modalVisible: false })}
								/>
							</View>
							
							<View style={paymentsContainer}>
								<RenderQuotas 
									payments={this.state.payments}
								/>
							</View>
							<View style={buttonsContainer}>
								<View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-start' }}>
									<Button navigation style={buttonNext} onPress={() => this.showInformation()}> 
										<Text>Más info</Text>
									</Button>
								</View>
								<View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-end'}}>
									<Button navigation style={buttonNext} onPress={() => this.confirmLoan()}>
										<Text>Aceptar</Text>
									</Button>
								</View>
							</View>
						</Content>
					</BackGround>
				</Container>
			</StyleProvider>
    );
  }
}

const styles = StyleSheet.create({
	titleContainer: {
		flex: 2,
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'center'
	},
	buttonNext: {
    paddingVertical: 0
	},
	requestContainer: {
		flex: 6,
	},
	paymentsContainer: {
    flex: 4,
	},
	buttonsContainer: {
		flex: 2,
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
		paddingHorizontal: scale(25)
	},
	titleTextStyle: {
		//textAlign: 'center',
		paddingHorizontal: scale(30),
		fontFamily: 'Rubik-Medium',
		fontWeight: 'bold'
	}
});

const mapStateToProps = state => {
  return { loan: state.loan, auth: state.auth };
};
const mapDispatchToProps = dispatch => ({
	dataRequest: () => dispatch(dataRequest()),
	dataFailure: () => dispatch(dataFailure()),
	dataSuccess: () => dispatch(dataSuccess()),
	hideModal: () => dispatch(hideModal()),
	showModal: (modalProps, modalType) => {
		dispatch(showModal({ modalProps, modalType }));
	}
});

export default connect(mapStateToProps, mapDispatchToProps)(SummaryCreateLoan);
