import React, { Component } from 'react';
import { View, StyleSheet, Text, Image, TouchableWithoutFeedback } from 'react-native'; 
import { connect } from 'react-redux';
import { H2, StyleProvider, Card, CardItem, Left,
	Button, Body, Container } from 'native-base';
import { Actions } from 'react-native-router-flux';
import ImagePicker from 'react-native-image-picker';
import { dataRequest, dataSuccess,
   dataFailure, addSupport, resetLoan, hideModal, showModal } from '../../actions';
import AWS3Service from '../../services/AWS3Service';
import { BackGround } from '../common';
import APIService from '../../services/APIService';
import getTheme from '../../themes/components';
import platform from '../../themes/variables/platform';
import { headerCardStyles, bodyOpportunitiesCardStyles } from '../../assets/styles/component/cards';
import { centerRow, titleText, leftRow } from '../../assets/styles/component/common';
  
import { scale } from '../../helpers/Scaler';
import Photo1 from '../../assets/images/Photo1.png';
import Photo2 from '../../assets/images/Photo2.png';
import Photo3 from '../../assets/images/Photo3.png';
import GreenCamera from '../../assets/images/GreenCamera.png';

class VerifyIdentity extends Component {
  getImage(index) {
    const options = {
      quality: 1.0,
      maxWidth: 820,
      maxHeight: 820,
      title: 'Seleccione una imagen',
      takePhotoButtonTitle: 'Tomar una foto',
      chooseFromLibraryButtonTitle: 'Obtener de la galería',
    };
    if (index === 1 && this.props.loan.selfie === null) {
      setTimeout(() => {
        this.props.showModal({
          open: true,
          message: 'Sube la foto de selfie primero',
          closeModal: this.closeModal.bind(this)
        }, 'alert');
      }, 200);
      return false;
    }  
    if (index === 2 && this.props.loan.idPhoto === null) {
      setTimeout(() => {
        this.props.showModal({
          open: true,
          message: 'Sube la foto de tu cédula primero',
          closeModal: this.closeModal.bind(this)
        }, 'alert');
      }, 200);
      return false;
    }
    this.props.addSupport(null, index);
    ImagePicker.launchCamera(options, (response) => {
      if (response.didCancel || response.error) {
        this.props.addSupport(null, index);
      } else {
        const file = {
          uri: response.uri,
          name: response.fileName,
          type: response.type
        };
        this.props.addSupport(file, index);
      }
    });
  }

  validate() {
    if (this.props.loan.selfie != null && 
      this.props.loan.idPhoto != null && 
      this.props.loan.document != null) {
      return true;
    }
    return false;
  }

  
  closeModal() {
    this.props.hideModal();
  }

  
  actionModal() {
    this.props.hideModal();
    Actions.reset('layoutApplicant'); 
    Actions.loansActive({ refresh: true }); 
  }

  submitForm() {
    if (!this.validate()) {
      setTimeout(() => {
        this.props.showModal({
          open: true,
          message: 'Debe subir las 3 fotos solicitadas',
          closeModal: this.closeModal.bind(this)
        }, 'alert');
      }, 200);
      return;
    }
    this.props.dataRequest('creatingLoan'); 
    const id = this.props.auth.idRolUsuario;
    const path = `prestamos/${id}/`;
    const { loan } = this.props;
    this.newLoan = {
      usuarioSolicitante: {
        idUsuarioSolicitante: id,
      },
      monto: loan.valueLoan,
      tiempoPrestamo: {
        idTiempoPrestamo: loan.kindOfTime
      },
      modalidadPago: {
        idModalidadPago: loan.kindOfModality
      },
      tipoPrestamo: {
        idTipoPrestamo: loan.kindOfLoan
      },
    };
    let files = [
      loan.receiptFront,
      loan.receiptBack
    ]
    AWS3Service.UploadMultipleImages(files, path).then((data) => {
      const pathMultiple = `identidad/${id}/`;
      this.newLoan.soportesFactura = [
        {
          descripcion: 'Factura cargada',
          urlSoporte: data[0].postResponse.key
        },
        {
          descripcion: 'Factura cargada2',
          urlSoporte: data[1].postResponse.key
        }
      ];
      files = [
        this.props.loan.selfie,
        this.props.loan.idPhoto,
        this.props.loan.document
      ];
      AWS3Service.UploadMultipleImages(files, pathMultiple).then((multiple) => {
        const soportes = [
          { 
            tipoSoporte:
              { idTipoSoporte: '5b6bc02df6178364486fcadd' } 
          },
          { 
            tipoSoporte:
              { idTipoSoporte: '5b6bc04ff6178364486fcade' } 
          },
          {
            tipoSoporte:
              { idTipoSoporte: '5b6bc07cf6178364486fcadf' } 
          }
        ];
        let counter = 0;
        for (const file of multiple) {
          soportes[counter].urlSoporte = file.postResponse.key;
          counter++;
        }
        this.newLoan.usuarioSolicitante.soportes = soportes;
        const url = 'v1/crearPrestamo';
        APIService.PutAPI(url, this.newLoan).then(() => {
          this.props.dataSuccess();
          this.props.resetLoan();
          setTimeout(() => {
            this.props.showModal({
              open: true,
              title: 'Alert Modal',
              icon: true,
              message: 'Serás notificado cuando alguno de nuestros inversionistas acepte tu solicitud',
              closeModal: this.actionModal.bind(this)
            }, 'alert');
          }, 200);
        }).catch((error) => {
          setTimeout(() => {
            this.props.showModal({
              open: true,
              message: error,
              closeModal: this.actionModal.bind(this)
            }, 'alert');
          }, 200);
          this.props.dataFailure(error);
        });
      }).catch((error) => {
        this.props.dataFailure(error);
      });
    }).catch((error) => {  
      this.props.dataFailure(error);
    });    
  }

  render() {
    const { containerStyle, photoContainer, 
			buttonContainer, buttonStyle, 
				textStyle,
			textContainer, iconStyle, buttonAcceptStyle,
			titleContainer, titleTextStyle } = styles;
    return (
			<StyleProvider style={getTheme(platform)}>
				<Container>        
					<BackGround>
						<View style={containerStyle}>
							<View style={titleContainer}>
								<H2 style={titleTextStyle}>Cuéntanos más de ti</H2>
							</View>
							<View style={photoContainer}>
								<Card transparent style={{ marginBottom: 0 }}>
									<CardItem header style={headerCardStyles}>
										<Left style={centerRow}>
											<Button title small>
												<Text style={[titleText, { color: 'white', paddingHorizontal: 6 }]}>
													Tómate una foto
												</Text>
											</Button>
										</Left>
										<Body />
									</CardItem>
									<CardItem identity style={[bodyOpportunitiesCardStyles, { marginBottom: 0 }]}>
										<Body style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
                      <TouchableWithoutFeedback onPress={() => this.getImage(0)}>
                        {this.props.loan.selfie != null ?
                          <Image source={Photo1} style={iconStyle}/>
                          :
                          <Image source={GreenCamera} 
                            style={iconStyle}
                          />
                        }
                      </TouchableWithoutFeedback>
										</Body>
									</CardItem>
								</Card>
							</View>
							<View style={photoContainer}>
								<Card transparent>
									<CardItem header style={headerCardStyles}>
										<Left style={[leftRow, { marginLeft: 10 }]}>
											<Button title small>
												<Text style={[titleText, { color: 'white', paddingHorizontal: 6 }]}>
													Toma una foto a tu cédula
												</Text>
											</Button>
										</Left>
									</CardItem>
									<CardItem identity style={bodyOpportunitiesCardStyles}>
										<Body style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
                    <TouchableWithoutFeedback onPress={() => this.getImage(1)}>
                      {this.props.loan.idPhoto != null ? 
                        <Image source={Photo2} style={iconStyle}/>
                        :
                        <Image source={GreenCamera} 
                          style={iconStyle}
                        />
                      }
                    </TouchableWithoutFeedback>
										</Body>
									</CardItem>
								</Card>
							</View>
							<View style={photoContainer}>
								<Card transparent>
									<CardItem header style={headerCardStyles}>
										<Left style={centerRow}>
											<Button title small>
												<Text style={[titleText, { color: 'white', paddingHorizontal: 6 }]}>
													Ahora a un documento con foto (no CC)
												</Text>
											</Button>
										</Left>
									</CardItem>
									<CardItem identity style={bodyOpportunitiesCardStyles}>
										<Body style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
                      <TouchableWithoutFeedback onPress={() => this.getImage(2)}>
                        {this.props.loan.document != null ? 
                          <Image source={Photo3} style={iconStyle}/>
                          :
                          <Image source={GreenCamera} 
                            style={iconStyle}
                          />
                        }
                      </TouchableWithoutFeedback>
										</Body>
									</CardItem>
								</Card>
							</View>
							<View style={textContainer}>
								<View style={{ width: '70%' }}>
									<Text style={[textStyle, { top: scale(10) }]}>
										Estas imágenes se usarán para validar tu identidad
									</Text>
								</View>
							</View>
							<View style={buttonContainer}>
								<Button style={buttonStyle} 
										accept onPress={() => this.submitForm()}>
									<Text style={buttonAcceptStyle}>Aceptar</Text>
								</Button>
							</View>
						</View>
					</BackGround>
				</Container>
			</StyleProvider>
		);
  }
}
const styles = StyleSheet.create({
  containerStyle: {
    flex: 1,
	},
	buttonAcceptStyle:{
		color: 'white',
		fontFamily: 'Rubik-Medium',
		paddingHorizontal: 15,
		fontSize: 16
	},
	titleTextStyle: {
		textAlign: 'center',
		paddingHorizontal: scale(30),
		fontFamily: 'Rubik-Medium',
		fontWeight: 'bold'
	},
	titleContainer: {
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'center'
	},	
  iconStyle: { 
		width: scale(70), 
		height: scale(70),
		paddingTop: 5,
		paddingBottom: 0
	},
	buttonStyle: {
		color: 'white',	
		backgroundColor: '#2b2b2b', 
		borderColor: '#fafdf6', 
		borderWidth: 3,
	},
  iconSuccessStyle: {
    fontSize: 40, 
    margin: 10, 
    color: 'green'
  },
  textStyle: {
		fontSize: 16,
		color: 'black',
		fontFamily: 'Rubik-Medium',
		textAlign: 'center'
  },
  textContainer: {
    justifyContent: 'center',
		alignItems: 'center',
		flex: 1.5,
		flexDirection: 'column',
		paddingTop: 10,
  },
  photoContainer: {
    flex: 2,
  },
  buttonContainer: {
    flex: 1.5,
    flexDirection: 'row', 
    paddingRight: 10,
    paddingLeft: 10,
    paddingBottom: 10,
		justifyContent: 'center',
		alignItems: 'center'
  },

});

const mapStateToProps = state => {
  return { loan: state.loan, auth: state.auth };
};

const mapDispatchToProps = dispatch => ({
    dataRequest: (typeLoader) => dispatch(dataRequest(typeLoader)),
    dataFailure: () => dispatch(dataFailure()),
    dataSuccess: () => dispatch(dataSuccess()),
    resetLoan: () => dispatch(resetLoan()),
    addSupport: (file, index) => dispatch(addSupport(file, index)),
    hideModal: () => dispatch(hideModal()),
    showModal: (modalProps, modalType) => {
      dispatch(showModal({ modalProps, modalType }));
    }
  });
  
  VerifyIdentity = connect(
    mapStateToProps, mapDispatchToProps
  )(VerifyIdentity);
  
  export default VerifyIdentity;
