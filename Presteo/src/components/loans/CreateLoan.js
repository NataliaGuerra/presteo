import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { Container, StyleProvider, 
  Text, Thumbnail, H2, Button, Card, CardItem, Left, Body } from 'native-base';
import { View, StyleSheet, Keyboard, Vibration, Platform,
  Image, TouchableWithoutFeedback } from 'react-native';
import Picker from 'react-native-wheel-picker';
import Swiper from 'react-native-swiper';
import ImagePicker from 'react-native-image-picker';
import { TextInputMask } from 'react-native-masked-text';
import { selectLoan, selectTime, 
  selectModality, updateLoanValue, setReceiptBackPhoto, setReceiptFrontPhoto, hideModal, showModal,
  dataRequest, dataSuccess, dataFailure } from '../../actions';
import APIService from '../../services/APIService';
import getTheme from '../../themes/components';
import platform from '../../themes/variables/platform';
import ProgressBarAnimated from 'react-native-progress-bar-animated';
import { progressCustomStyles } from '../../assets/styles/component/progress';
import { scale, verticalScale } from '../../helpers/Scaler';
import { BackGround } from '../common';
import { headerCardStyles, bodyOpportunitiesCardStyles } from '../../assets/styles/component/cards';
import { centerRow, titleText, leftRow } from '../../assets/styles/component/common';

const logocheck = require('../../assets/images/CheckGreen.png');
import ReceiptFront from '../../assets/images/ReceiptFront.png';
import ReceiptBack from '../../assets/images/ReceiptBack.png';


const PickerItem = Picker.Item;

class CreateLoan extends Component {
  
  state = {
    progress: 0,
    billFront: false,
    billBack: false,
    index: 0,
    kindLoans: [],
    kindTimes: [],
    kindModality: [],
    showError: false,
    errorMessage: '',
    valueInput: '',
  }

  componentWillMount() {
    this.getData(4).then((res) => {
      this.getData(0).then((data) => {
        this.props.dataSuccess();
        this.setState({ kindLoans: data });
        this.props.selectLoan(this.state.kindLoans[1].idTipoPrestamo, this.state.kindLoans[1].nombre);
      }).catch((error) => {
        this.dataFailure(error);
      });
       
    }).catch((error) => {
      this.showModal(error);
      this.dataFailure(error);
    });
    
  }

  getData(index) {  
    return new Promise((resolve, reject) => {
      let url = '';
      switch (index) {
        case 0: 
          this.state.kindLoans.length > 0 ? resolve(true) : url = 'v1/buscarTipoPrestamo';
          break;
        case 2:
          this.state.kindTimes.length > 0 ? resolve(true) : url = 'v1/buscarTiempoPrestamo';
          break;
        case 3: 
          url = `v1/buscarModalidadPago/TiempoPrestamo=${this.props.loan.nameOfTime}`; 
          break;
        case 4:
          url = `v1/validarSolicitanteCrearPrestamo/idUsuarioSolicitante=${this.props.auth.idRolUsuario}`; 
           
          break;
        default:
      }
      this.props.dataRequest();
      APIService.GetAPI(url).then((data) => { 
        resolve(data); 
      }).catch((error) => { 
        this.props.dataFailure(error);
        reject(error); 
      });
    });
  }

  closeModal() {
    this.props.hideModal();
    Actions.pop();
  }

  showModal(text) {
    this.props.showModal({
      open: true,
      title: 'Error!',
      message: 'Ya tienes un préstamo activo. Cuando termines de pagarlos podrás solicitar uno nuevo. Próximamente podrás tener más uno a la vez!',
      closeModal: this.closeModal.bind(this)
    }, 'alert');
  }

  validate(index) {
    switch (index) {
      case 0: 
        if (this.props.loan.kindOfLoan != this.state.kindLoans[1].idTipoPrestamo) {
          return 'Escoge un tipo de préstamo valido';
        }
        break;
      case 1: 
        if (this.props.loan.valueLoan > 20000 || this.props.loan.valueLoan < 1000) {
          return 'Máximo de préstamo es $20.000';
        }
        break;
      case 2:
        if (this.props.loan.kindOfTime < 0) {
          return 'Escoge un tiempo de préstamo';
        }
        break;
      case 3:
        if (this.props.loan.kindOfModality < 0) {
          return 'Escoge una modalidad de pago';
        }
        break;
      case 4:
        if (!this.state.billFront || !this.state.billBack) {
          return 'Debes adjuntar ambas caras de la factura';
        }
        break;
      default: 
        return false;
    }
    return true;
  }
  
  navigate(direction) {
    const { index } = this.state;
    this.setState({ showError: false });
    Keyboard.dismiss();
    if (direction === 1) {
      const validate = this.validate(index);
      if (validate !== true) {
        this.setState({ errorMessage: validate });
        this.setState({ showError: true });
        Vibration.vibrate(200);
        return false;
      }
      if (index >= 4) {
        Actions.summaryCreateLoan();
        return false;
      }
      if (index > 0 && index < 3) {
        this.getData(index + 1).then((data) => {
          if (data !== true) {
            this.props.dataSuccess();
            switch (index + 1) {
              case 2: 
                this.setState({ kindTimes: data });
                this.props.selectTime(this.state.kindTimes[0].idTiempoPrestamo, this.state.kindTimes[0].duracion);
              break;
              default: 
                this.setState({ kindModality: data });
                this.props.selectModality(this.state.kindModality[0].idModalidadPago, this.state.kindModality[0].modalidad);
            }
          } else {
            this.props.dataFailure('Error obteniendo los datos');
          }
        });
      }
    } else {
      switch (index) {
        case 3:
          this.setState({ kindModality: [] });
          break;
        default:
      }
    }
    this.swiper.scrollBy(direction);
  }
  updateIndex(index) {
    let currentIndex = index;
    if (index < 0) {
      currentIndex = 0;
    } else if (index > 4) {
      currentIndex = 4;
    }
		this.setState({ progress: currentIndex * (25 / 100) });
		this.setState({ progressAnimated: currentIndex * (25) });
    this.setState({ index: currentIndex });
  }

  chargeReceipt(type) {
    const options = {
      quality: 1.0,
      maxWidth: 820,
      maxHeight: 820,
      title: 'Seleccione su factura',
      takePhotoButtonTitle: 'Tomar una foto',
      chooseFromLibraryButtonTitle: 'Obtener de la galería',
      mediaType: 'photo',
    };
    type === 0 ? this.setState({ billFront: false }) : this.setState({ billBack: false });
    ImagePicker.showImagePicker(options, (response) => {
      Keyboard.dismiss();
      if (!response.didCancel && !response.error) {
        const file = {
          uri: response.uri,
          name: response.fileName,
          type: response.type
        };
        if (type === 0) {
          this.props.setReceiptFrontPhoto(file);
          this.setState({ billFront: true });
        } else {
          this.props.setReceiptBackPhoto(file);
          this.setState({ billBack: true });
        }
      }
    });
  }

  render() {
    const { containerStyle, swiperContainer, slideStyle,
      barContainer, inputStyle, contentContainer,
      buttonUploadText, buttonContainer, 
      imageStyle, buttonNext, buttonTextStyle,
      textStyle, buttonUpload, photoContainer, iconStyle,
      progressViewStyles2, borderPicker,
			titleContainer, warningContainer, titleTextStyle, pickerStyle,
      containerBorderStyle, errorTextStyle, inputContainerStyle } = styles;
    return (
      <StyleProvider style={getTheme(platform)}>
				<Container>        
					<BackGround>
						<View style={containerStyle}>
							<View style={barContainer}>
								<View style={progressViewStyles2}>
									<ProgressBarAnimated
										{...progressCustomStyles}
										width={scale(270)}
										height={20}
										borderColor='black'
										borderWidth={3}
										value={this.state.progressAnimated}
										backgroundColorOnComplete='white'
										backgroundColor='white'
										maxValue={100}
									/> 
								</View>
							</View>
							<View style={swiperContainer}>
								<Swiper 
									scrollEnabled={false}
									onIndexChanged={(index) => this.updateIndex(index)} 
									loop={false}
									showsPagination={false}
									ref={ref => { this.swiper = ref; }}
								>
									<View style={slideStyle}>
										<View style={titleContainer}>
											<H2 style={titleTextStyle}>¿Qué tipo de préstamo necesitas?</H2>
										</View>
										<View style={contentContainer}>
										{this.state.kindLoans.length > 0 && 
											<Picker 
												style={pickerStyle}
												selectedValue={Platform.OS === 'ios' ? this.props.loan.kindOfLoan : 1}
												itemStyle={{ 
													fontSize: 26, 
													color: '#fff'
												}}
												onValueChange={(index, i) => 
													this.props.selectLoan(
														Platform.OS === 'ios' ? index : this.state.kindLoans[index].idTipoPrestamo , 
														Platform.OS === 'ios' ? this.state.kindLoans[i].nombre : this.state.kindLoans[index].nombre
                        )}
											>
												{this.state.kindLoans.map((element, i) => (
													<PickerItem 
														label={element.nombre} 
														value={Platform.OS === 'ios' ? element.idTipoPrestamo : i} 
														key={element.idTipoPrestamo}
													/>
												))}
											</Picker>
										}
										</View>
										<View style={warningContainer}>
                      <Text 
                        style={this.state.showError ? [textStyle, { color: 'red' }] 
                          : textStyle}
                      >
												Próximamente podrás pedir préstamos estudiantiles y de libre inversión
											</Text>
										</View>
										<View style={containerBorderStyle}>
											<View style={borderPicker} />
										</View>
									</View>
									<View style={slideStyle}>
										<View style={titleContainer}>
											<H2 style={titleTextStyle}>¿Cuánto necesitas?</H2>
										</View>   
										<View style={[contentContainer, { width: '60%' }]}>
											<View style={inputContainerStyle}>
                        <TextInputMask 
                          type={'money'} 
                          style={inputStyle} 
                          ref={ref => (this.myTextInputMask = ref)}
                          options={{ precision: 0, unit: '$', separator: ',', delimiter: '.' }}
                          value={this.state.valueInput}
                          onChangeText={value => {
														this.setState({ valueInput: value });
                            this.props.updateLoanValue(this.myTextInputMask.getRawValue());
													}}
													
                          placeholder={'$100.000'}
                        />
											</View>
										</View>
										<View style={{ flex: 1 }}/>
									</View>
									<View style={slideStyle}>
										<View style={titleContainer}>
											<H2 style={titleTextStyle}>¿Por cuánto tiempo?</H2>
										</View>  
										<View style={contentContainer}>
											{this.state.kindTimes.length > 0 && 
												<Picker 
													style={pickerStyle}
													selectedValue={this.props.loan.kindOfTime}
													itemStyle={{ 
														fontSize: 26, 
														color: '#fff'
													}}
													onValueChange={(index, i) => 
														this.props.selectTime(
															Platform.OS === 'ios' ? index : this.state.kindTimes[index].idTiempoPrestamo, 
															Platform.OS === 'ios' ? this.state.kindTimes[i].duracion : this.state.kindTimes[index].duracion
														)}
												>
													{this.state.kindTimes.map((element, i) => (
														<PickerItem 
															label={element.duracion} 
															value={Platform.OS === 'ios' ? element.idTiempoPrestamo : i} 
															key={element.idTiempoPrestamo}
														/>
													))}
												</Picker>
											}
										</View>
										<View style={{ flex: 1 }}/>
										<View style={containerBorderStyle}>
											<View style={borderPicker} />
										</View>
									</View>
									<View style={slideStyle}>
										<View style={titleContainer}>
											<H2 style={titleTextStyle}>¿Modalidad de pago?</H2>
										</View>  
										<View style={contentContainer}>
											{this.state.kindModality.length > 0 && 
												<Picker 
													style={pickerStyle}
													selectedValue={this.props.loan.kindOfModality}
													itemStyle={{ 
														fontSize: 26, 
														color: '#fff'
													}}
													onValueChange={(index, i) => 
                            this.props.selectModality(
                              Platform.OS === 'ios' ? index : this.state.kindModality[index].idModalidadPago, 
                              Platform.OS === 'ios' ? this.state.kindModality[i].modalidad : this.state.kindModality[index].modalidad
                            )}
                          
                          ref={ref => { this.pickerModality = ref; }}
												>
													{this.state.kindModality.map((element, i) => (
														<PickerItem 
															label={element.modalidad} 
															value={Platform.OS === 'ios' ? element.idModalidadPago : i} 
															key={element.idModalidadPago}
														/>
													))}
												</Picker>
											}
										</View>
										
										<View style={{ flex: 1 }}/>
										<View style={containerBorderStyle}>
											<View style={borderPicker} />
										</View>
									</View>
									<View style={{ flex: 1 }}>
                    <View style={{ flex: 0.5 }}>
										  <H2 style={titleTextStyle}>
                        Carga tu factura
                      </H2>
										</View>
                    <View style={{ flex: 2 }}>
                      <Card transparent>
                        <CardItem header style={headerCardStyles}>
                          <Left style={centerRow}>
                            <Button title small>
                              <Text style={[titleText, { color: 'white', paddingHorizontal: 6 }]}>
                                Cargar factura por el frente
                              </Text>
                            </Button>
                          </Left>
                        </CardItem>
                        <TouchableWithoutFeedback onPress={() => this.chargeReceipt(0)}>
                          <CardItem identity style={[bodyOpportunitiesCardStyles, { marginBottom: 0 }]}>
                            <Body style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
                              {this.state.billFront ? 
                                <Image source={logocheck} style={iconStyle}/>
                                :
                                <Image source={ReceiptFront} style={iconStyle}/>
                              }
                            </Body>
                          </CardItem>
                        </TouchableWithoutFeedback>
                      </Card>
                    </View>
                    <View style={{ flex: 2 }}>
                      <Card transparent>
                        <CardItem header style={headerCardStyles}>
                          <Left style={centerRow}>
                            <Button title small>
                              <Text style={[titleText, { color: 'white', paddingHorizontal: 6 }]}>
                                Cargar factura por el respaldo
                              </Text>
                            </Button>
                          </Left>
                        </CardItem>
                        <TouchableWithoutFeedback onPress={() => this.chargeReceipt(1)}>
                          <CardItem identity style={[bodyOpportunitiesCardStyles, { marginBottom: 0 }]}>
                            <Body style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
                              {this.state.billBack ? 
                                <Image source={logocheck} style={iconStyle}/>
                                :
                                <Image source={ReceiptBack} style={[iconStyle, { width: scale(120) }]}/>
                              }
                            </Body>
                          </CardItem>
                        </TouchableWithoutFeedback>
                      </Card>
                    </View>
									</View>
								</Swiper>
								{this.state.showError && 
									<View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
										<Text style={errorTextStyle}>
											{this.state.errorMessage}
										</Text>
									</View>
                }
              </View>
							<View style={buttonContainer}>
								{this.state.index > 0 && 
									<View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center'}}>
										<Button navigation style={buttonNext} onPress={() => this.navigate(-1)}>
											<Text style={buttonTextStyle}>Anterior</Text>
										</Button>
									</View>
								}
								<View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center' }}>
									<Button navigation style={buttonNext} onPress={() => this.navigate(1)}> 
										<Text style={buttonTextStyle}>Siguiente</Text>
									</Button>
								</View>
							</View>
						</View>
					</BackGround> 
				</Container>
			</StyleProvider>  
    );
  }
}

const styles = StyleSheet.create({
  photoContainer: {
    flex: 2
  },  
	titleContainer: {
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'center'
  },	
  iconStyle: {
    width: scale(70), 
    height: scale(85),
    paddingTop: 5,
    paddingBottom: 0
  },
	errorTextStyle: {
		fontSize: 14,
		color: 'red',
		textAlign: 'center',
		marginBottom: 10,
		width: '80%'
	},	
	containerBorderStyle: {
		marginTop: Platform.OS === 'ios' ? verticalScale(31) : 0,
		flex: 1, 
		position: 'absolute', 
		top: 0, 
    left: 0, 
    right: 0, 
		bottom: 0, 
		justifyContent: 'center', 
		alignItems: 'center', 
		zIndex: -1
	},
	warningContainer: {
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'center'
	},	
	titleTextStyle: {
		textAlign: 'center',
		paddingHorizontal: scale(30),
		fontFamily: 'Rubik-Medium',
		fontWeight: 'bold'
	},	
  swiperContainer: {
    flex: 4
	},
	pickerStyle: { 
		width: scale(300), 
		height: 180 
	},
	imageStyle: {
		width: 95,
		height: 95,
		borderRadius: 0,
		bottom: scale(10),
		overflow: 'visible'
	},
  buttonNext: {
    paddingVertical: 0,
    width: scale(112),
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center'
	},
	buttonTextStyle: {
		fontSize: 18,
		color: 'white',
		fontWeight: 'bold',
    fontFamily: 'Rubik-Medium',
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center'
	},
  buttonContainer: {
    flex: 1,
    flexDirection: 'row', 
    paddingHorizontal: scale(30),
		justifyContent: 'flex-end',
		alignItems: 'center'
  },
  progressViewStyles2: {
    backgroundColor: 'black', 
    borderRadius: 10,
    width: scale(270),
  },
  buttonUpload: {
    backgroundColor: '#2b2b2b', 
		borderColor: '#fafdf6', 
		borderWidth: 1,
		height: verticalScale(43),
		paddingHorizontal: scale(25),
  },
  borderPicker: {
    borderColor: 'grey',
    backgroundColor: '#2b2b2b',
    borderTopWidth: 3,
    borderBottomWidth: 3,
    height: verticalScale(45),
		width: scale(300),
  },
  contentContainer: {
    flex: 3, 
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  titleSection: {
    flex: 1,
    paddingTop: 20,
    justifyContent: 'center',
    paddingBottom: 20
  },
  buttonUploadText: {
    fontSize: 20,
    fontFamily: 'Rubik-Medium',
		color: '#fff',
		fontWeight: '700'
  },
  barContainer: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center'
  },
  containerStyle: { 
    flex: 1, 
    flexDirection: 'column',
    justifyContent: 'center',
  }, 
  slideStyle: {
    flex: 1,
    alignItems: 'center',
  },
  text: {
    color: '#fff',
    fontSize: 30,
    fontWeight: 'bold',
  },
  textStyle: {
    fontSize: 14,
    color: '#ffc04d',
    fontFamily: 'Rubik-Light',
		flexWrap: 'wrap',
		textAlign: 'center',
		color: '#96ab2f',
		paddingHorizontal: scale(30)
  }, 
  inputStyle: {
    color: '#000',
		height: verticalScale(42),
		fontFamily: 'Rubik-Medium',
		fontWeight: 'bold',
		fontSize: 16,
		textAlign: 'right'
	},
	iconMoneyStyle: {
		padding: 10
	},
	inputContainerStyle: {
		flexDirection: 'row',
		height: verticalScale(42),
    justifyContent: 'center',
    alignItems: 'center',
		width: '100%',
		borderColor: '#7c7c7c',
    borderWidth: 2,
    borderRadius: 9,
		backgroundColor: "#fff",
	}
});
const mapStateToProps = state => {
  return { loan: state.loan, auth: state.auth };
};

const mapDispatchToProps = dispatch => ({
  dataRequest: (typeLoader) => dispatch(dataRequest(typeLoader)),
  dataSuccess: () => dispatch(dataSuccess()),
  dataFailure: () => dispatch(dataFailure()),
  selectLoan: (value, name) => {
    dispatch(selectLoan(value, name));
  },
  selectTime: (value, name) => {
    dispatch(selectTime(value, name));
  },
  selectModality: (value, name) => {
    dispatch(selectModality(value, name));
  },
  updateLoanValue: (value) => {
    dispatch(updateLoanValue(value));
  },
  setReceiptFrontPhoto: (file) => {
    dispatch(setReceiptFrontPhoto(file));
  },
  setReceiptBackPhoto: (file) => {
    dispatch(setReceiptBackPhoto(file));
  },
  hideModal: () => dispatch(hideModal()),
  showModal: (modalProps, modalType) => {
    dispatch(showModal({ modalProps, modalType }));
  }
});


export default connect(mapStateToProps, mapDispatchToProps)(CreateLoan);
