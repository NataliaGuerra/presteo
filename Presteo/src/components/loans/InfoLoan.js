import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { Content, Card, Text, Body, Container, StyleProvider,
  ListItem, View, Button } from 'native-base';
import { connect } from 'react-redux';
import { TextMask } from 'react-native-masked-text';
import { dataRequest, dataSuccess, dataFailure, hideModal, showModal } from '../../actions';
import APIService from '../../services/APIService';
import { Title, ModalImage, BackGround } from '../common';
import { scale } from '../../helpers/Scaler';
import getTheme from '../../themes/components';
import platform from '../../themes/variables/platform';
import ImageService from '../../services/ImageService';
import AWS3Service from '../../services/AWS3Service';
import { Actions } from 'react-native-router-flux';

class InfoLoan extends Component {

  state = {
    activeLoan: {
      modalidadPago: {},
      tipoPrestamo: {
        nombre: ''
      },
      estadoPrestamo: {},
      soporteFactura: {
        estadoSoporte: {}
      },
      comprobantesDesembolso: [{
        urlDescripcion: '',
        estadoComprobante: {
          descripcion: ''
        }
      }]
    },
    supportVisibility: false,
    receiptVisibility: false,
  }
  componentWillMount() {
    this.getData(this.props.loanId);
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.loanId !== nextProps.loanId) {
      this.getData(nextProps.loanId);
    }
  }

  getData(loanId) {
    const url = `v1/obtenerResumenSolComprobanteP/idPrestamo=${loanId}`;
		this.props.dataRequest();
    APIService.GetAPI(url).then((data) => {
      this.props.dataSuccess();
      console.log(data);
      this.setState({ activeLoan: data });
    }).catch((error) => {
      this.props.dataFailure(error);
       
    });
  }
  
	closeModal() {
		this.props.hideModal();
	}

  reUploadReceipt = async() => {

    const id = this.props.auth.idRolUsuario;
    const path = `prestamos/${id}/`;
    try {
      //Obtener imagen de la galeria o cámara
      let image = await ImageService.GetImage();
      this.props.dataRequest();
      //Subirla a AWS3 para poder retornar la url de la imagen
      let url = await AWS3Service.UploadImage(image, path);
      //Generar JSON de actualizacion
      let newReceipt = {
        idPrestamo: this.props.loanId,
        urlSoporteFactura: url.postResponse.key,
      };
      console.log(newReceipt);
      console.log(JSON.stringify(newReceipt));
      //Subir la imagen
      const putPath = 'v1/actualizarSoporteFactura';
      let response = await APIService.PutAPI(putPath, newReceipt);
      console.log(response);
      this.props.showModal({
        open: true,
        title: 'Factura cargada',
        message: 'Haz subido de nuevo tu factura',
        closeModal: () => { 
          this.props.hideModal();
          Actions.pop();
        },
      }, 'success');
      this.props.dataSuccess();
    } catch(e) {
      this.props.dataFailure(e);
      console.log('entre por acá');
      console.log(e);
    }
  }

	showModal(urlImg) {
    this.props.showModal({  
      open: true,
      url: urlImg,
      pqr: true,
      closeModal: this.closeModal.bind(this),
      pqrModal: this.pqr.bind(this)
    }, 'image');
  }

	pqr() {
    this.props.hideModal();
    Actions.createPqr();
	}

  renderInfo() {
    const { contentViewTop, headerItem, contentViewMid, 
      textStyle, textStyleB, buttonStyle, listItemStyle, 
      viewBlackStyle } = styles;
    const { fechaCreacion, estadoPrestamo, monto, modalidadPago,
      tipoPrestamo, soporteFactura, comprobantesDesembolso } = this.state.activeLoan;
    return (
      <View>
        <View> 
        <View style={[headerItem, {width: scale(120), bottom: scale(-10), zIndex: 20 }]}>
            <Text style={{ color: 'white'}}>Solicitado</Text>
          </View>
          <View style={contentViewTop}>
            <View style={{ paddingVertical: scale(10) }}>
              <ListItem thumbnail style={listItemStyle}>
                <Text style={textStyle}>Fecha de solicitud: </Text>
                <Text style={textStyleB}>{fechaCreacion != null ? fechaCreacion.substring(0, fechaCreacion.indexOf('T')) : ''}</Text>
              </ListItem>
              <ListItem thumbnail style={listItemStyle}>
                <Text style={textStyle}>Estado: </Text>
                <Text style={textStyleB}>{estadoPrestamo.nombre}</Text>
              </ListItem>
              <ListItem thumbnail style={listItemStyle}>
                <Text style={textStyle}>Valor: </Text>
                <Text style={textStyleB}>
                  <TextMask 
                    type={'money'} 
                    options={{ precision: 0, unit: '$', separator: ',', delimiter: '.' }}
                    value={monto}
                  />
                </Text>
              </ListItem>
              <ListItem thumbnail style={listItemStyle}>
                <Text style={textStyle}>Modalidad de Pago: </Text>
                <Text style={textStyleB}>{modalidadPago.modalidad}</Text>  
              </ListItem>
              <ListItem thumbnail style={listItemStyle}>
                <Text style={textStyle}>Tipo: </Text>
                <Text style={textStyleB}>{tipoPrestamo.nombre}</Text>   
              </ListItem>
            </View>
            { estadoPrestamo.nombre != null && 
              !estadoPrestamo.nombre.includes('PENDIENTE') &&
              <View style={viewBlackStyle}>
                <View style={{ alignItems: 'center', paddingTop: scale(5)}}>
                  <Button 
                    style={{ height: scale(32), alignItems: 'center', borderRadius: 0 }} 
                    onPress={() => this.pqr() }
                  >
                    <Text>Pagar</Text>                
                  </Button>
                </View>
              </View>
            }
            
          </View>
        </View> 

        <View style={contentViewMid}>
          <ListItem thumbnail style={{alignItems: 'center', paddingTop: scale(10)}}>
          <Text style={textStyle} >Estado: </Text>
            <Text style={[textStyle, {fontWeight: 'bold'}]} >{soporteFactura != null && soporteFactura.estadoSoporte != null ? 
              soporteFactura.estadoSoporte.descripcion : ''}</Text>
          </ListItem>
          <Body style={{alignItems: 'center'}}>
          {soporteFactura != null && soporteFactura.estadoSoporte != null &&
          soporteFactura.estadoSoporte.descripcion === 'RECHAZADO' ? 
            <View style={{alignItems: 'center', paddingTop: scale(5)}}>
              <Button style={buttonStyle} onPress={this.reUploadReceipt}>
                <Text>Volver a cargar</Text>                
              </Button>
            </View>
            :
            <View style={{alignItems: 'center', paddingTop: scale(5)}}>
              <Button style={buttonStyle} onPress={() => this.showModal(soporteFactura === null ? '' : ImageService.getUrlFormat(soporteFactura.urlSoporte))}>
                <Text>Ver</Text>                
              </Button>
            </View>
          } 
            
            <ModalImage 
              visibility={this.state.receiptVisibility}
              url={soporteFactura === null ? '' : ImageService.getUrlFormat(soporteFactura.urlSoporte)}
              acceptFunction={() => this.setState({ receiptVisibility: false })}
            />
          </Body> 
        </View>
        <View style={[headerItem, {width: scale(200), bottom: scale(115), }]}>
            <Text style={{ color: 'white'}}>Imagen de factura</Text>
        </View>


        {comprobantesDesembolso !== null && comprobantesDesembolso[0] != null &&
          comprobantesDesembolso[0].estadoComprobante.descripcion !== 'PENDIENTE' &&  
          <Card transparent>
          <View style={contentViewMid}>
              <ListItem thumbnail style={{alignItems: 'center', paddingTop: scale(10)}}>
                <Text style={textStyle}>Estado: </Text>
               <Text style={[textStyle, {fontWeight: 'bold'}]}>{comprobantesDesembolso[0] != null && 
                  comprobantesDesembolso[0].estadoComprobante != null ? 
                  comprobantesDesembolso[0].estadoComprobante.descripcion : ''}</Text>              
              </ListItem>
              <Body style={{alignItems: 'center'}}>
              <View style={{alignItems: 'center', paddingTop: scale(5)}}> 
                <Button style={buttonStyle} onPress={() => this.showModal(comprobantesDesembolso[0] == null ? '' : ImageService.getUrlFormat(comprobantesDesembolso[0].urlComprobante))}>
                <Text>Verificar soporte</Text>                  
                </Button>
               <ModalImage 
                  visibility={this.state.supportVisibility}
                  url={comprobantesDesembolso[0] == null ? '' : ImageService.getUrlFormat(comprobantesDesembolso[0].urlComprobante)}
                  acceptFunction={() => this.setState({ supportVisibility: false })}
      /> 
              </View>
              </Body>
          </View>
          <View style={[headerItem, {width: scale(180), bottom: scale(110) }]}>
              <Text style={{ color: 'white', fontSize: 14 }}>Pago de factura</Text>
          </View>
        
          </Card>
               }   
      </View>
    );
  }


  render() {
    return (
      <StyleProvider style={getTheme(platform)}>
      <Container>        
        <BackGround>
      <Content padder>
        <Title titleText={'Resumen solicitud'} />
        {this.renderInfo()}
      </Content>
      </BackGround>
        </Container>
      </StyleProvider>  
    );
  }
}
const styles = StyleSheet.create({
  listItemStyle: {
    paddingLeft: 0
  },
  contentViewTop: {
    borderColor: '#7c7c7c',
    backgroundColor: '#fafdf6',
    borderWidth: 3,
    borderRadius: 20,
    marginBottom: 45, 
    paddingTop: scale(10),
  },
  viewBlackStyle: {
    backgroundColor: 'black', 
    height: scale(45),
    width: '100%',
    borderBottomLeftRadius: 17,
    borderBottomRightRadius: 17,
    alignItems: 'center'
  },
  contentViewMid: {
    borderColor: '#7c7c7c',
    backgroundColor: '#fafdf6',
    borderWidth: 2,
    borderRadius: 15,
    padding: scale(10)
  },

  contentViewBot: {
    borderColor: '#7c7c7c',
    backgroundColor: '#fafdf6',
    borderWidth: 2,
    borderRadius: 15,
    padding: scale(10)
  },

  headerItem: {
    backgroundColor: '#7c7c7c',
    left: scale(35),
    height: scale(35),
    borderRadius: 10,
    borderWidth: 3,
    borderColor: '#fafdf6',
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textStyle: {
    fontSize: 15,
    flex: 1,
    fontFamily: 'Rubik-Light',
    color: '#7c7c7c',
  },
  textStyleB: {
    flex: 1,
    fontSize: 13,
    fontFamily: 'Rubik-Medium',
    color: '#2b2b2b',
    fontWeight: 'bold'
  },
  buttonStyle: {
    height: scale(35),
    alignItems: 'center',
  },
  iconStyle: { 
		width: scale(70), 
		height: scale(70),
		paddingTop: 5,
		paddingBottom: 0
	},
});

const mapDispatchToProps = dispatch => ({
  dataSuccess: () => dispatch(dataSuccess()),
  dataRequest: () => dispatch(dataRequest()),
  dataFailure: () => dispatch(dataFailure()),
  hideModal: () => dispatch(hideModal()),
  showModal: (modalProps, modalType) => {
    dispatch(showModal({ modalProps, modalType }));
  }
});

const mapStateToProps = state => {
  return { auth: state.auth };
};

InfoLoan = connect(
  mapStateToProps, mapDispatchToProps
)(InfoLoan);


export default InfoLoan;
