import React, { Component } from 'react';
import { connect } from 'react-redux';
import { StyleProvider, Container, Content, View } from 'native-base';
import { RefreshControl } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { dataRequest, dataSuccess, dataFailure, hideModal, showModal } from '../../actions';
import { RenderMyLoans } from '../render/renderCards';
import APIService from '../../services/APIService';
import { Title, BackGround } from '../common';
import getTheme from '../../themes/components';
import platform from '../../themes/variables/platform';

class GetLoans extends Component {
  state = {
    myLoans: [],
    refreshing: false,
  }

  componentWillMount() {
    this.props.dataRequest();
    this.getData().then((response) => {
      this.props.dataSuccess();
      this.setState({ myLoans: response });
    }).catch((error) => {
      this.props.dataFailure(error);
    });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.refresh) {
      this.props.dataRequest();
      this.getData().then((response) => {
        this.props.dataSuccess();
        this.setState({ myLoans: response });
      }).catch((error) => {
        this.props.dataFailure(error);
      });
    }
  }

  onRefresh() {
    this.setState({ refreshing: true });
    this.getData().then((response) => {
      this.setState({ refreshing: false });
      this.setState({ myLoans: response });
    }).catch(() => {
      this.setState({ refreshing: false });
    });
  }

  getData() {
    return new Promise((resolve, reject) => {
      //const userId = '1587489652';
       
      const userId = this.props.auth.idRolUsuario;
      const url = `v1/listarPrestamosInfoBasicaUS/idUsuarioSolicitante=${userId}`;
      APIService.GetAPI(url).then((response) => {
         
        resolve(response);
      }).catch((error) => {
        reject(error);
      });
    });
  }
  closeModal() {
		this.props.hideModal();
  }
  pqr() {
		this.props.showModal({
			open: true,
			title: 'Alert Modal',
			message: 'Funcionalidad en Proceso!',
			closeModal: this.closeModal.bind(this)
		}, 'maintenance');
	}
  render() {
    return (
      <StyleProvider style={getTheme(platform)}>      
      	<Container>
					<BackGround>
						<Content> 
							<View refreshControl={
								<RefreshControl
								refreshing={this.state.refreshing}
								onRefresh={this.onRefresh.bind(this)}
								/>
							}
								padder >
								<Title titleText={'Mis solicitudes'} />
								{
									this.state.myLoans.map((element) => {
										return (
											<RenderMyLoans 
												key={element.idPrestamo}
												monto={element.monto}
												tiempoPrestamo={element.tiempoPrestamo}
												modalidad={element.modalidad}
												fechaCreacion={element.fechaCreacion}
												nombreTipoPrestamo={element.nombreTipoPrestamo}
												nombreEstadoPrestamo={element.nombreEestadoPrestamo}
                        onPress={() => Actions.infoLoanActive({ loanId: element.idPrestamo })}
                        statusColor={element.nombreEestadoPrestamo == 'ACTIVO AL DIA' ? 
                        "#00FF00" : "#FF0000"}
                        active
                        paymentPress={() => this.pqr() }
											/>
										);
									})
								}
							</View>
					</Content>
					</BackGround>
        </Container>
      </StyleProvider>  
    );
  }
}
const mapDispatchToProps = dispatch => ({
  dataSuccess: () => dispatch(dataSuccess()),
  dataRequest: () => dispatch(dataRequest()),
  dataFailure: () => dispatch(dataFailure()),
  hideModal: () => dispatch(hideModal()),
  showModal: (modalProps, modalType) => {
    dispatch(showModal({ modalProps, modalType }));
  }
});
const mapStateToProps = state => {
  return { auth: state.auth };
};

GetLoans = connect(
  mapStateToProps, mapDispatchToProps
)(GetLoans);
export default GetLoans;
