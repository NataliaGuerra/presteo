import React from 'react';
import { Text, Button, Thumbnail } from 'native-base';
import { View } from 'react-native';



const ContractModal = ({ closeModal, userName }) => {
 const { contentStyle, boxStyle, TextStyle } = styles;
  return (
    <View style={boxStyle}>
        
        <View style={contentStyle}>
            <View style={{ alignItems: 'center', width: '100%', height: 20, overflow: 'hidden' }}>
            </View>
            <View style={{ flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                <Text style={TextStyle} >Te enviaremos una</Text>
                <Text style={TextStyle} >notificación cuando</Text>
                <Text style={[TextStyle, {color: 'black'}]} >{ userName }</Text>
                <Text style={TextStyle} >realice el desembolso de tu préstamo.</Text>
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
                <Button agree onPress={closeModal}>
                    <Text>Aceptar</Text>
                </Button>
            </View>
        </View>
    </View> 
  );
};

const styles = {
    containerStyle: {
      flexDirection: 'column',
      justifyContent: 'center',
      padding: 15,
      alignItems: 'center'
    },
    contentStyle: {
    height: 250,
    width: '100%',
      backgroundColor: '#ffffff', 
      borderRadius: 25
    },
    boxStyle: {
        flex: 1, 
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
      },
    TextStyle:{
        color: '#7c7c7c',
        textAlign: 'center',
        marginTop: 5
    },

  };

export default ContractModal;
