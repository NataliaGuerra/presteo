import React from 'react';
import { Text, Button, Thumbnail } from 'native-base';
import { View, Image } from 'react-native';

const error = require('../../assets/images/RedEquis.png');


const BillErrorModal = ({ closeModal, message }) => {
 const { contentStyle, boxStyle, TextStyle } = styles;
  return (
    <View style={boxStyle}> 
      <View style={contentStyle}>
          <View style={{ alignItems: 'center', width: '100%', height: 100, overflow: 'hidden' }}>
            <Thumbnail large style={{ height: 90, width: 90, top: 10 }} source={error} />
          </View>
          <View style={{ flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
            <Text style={TextStyle}>{message}</Text>
          </View>
          <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
            <Button agree onPress={closeModal}>
              <Text>Aceptar</Text>
            </Button>
          </View>
      </View>
    </View> 
  );
};

const styles = {
  containerStyle: {
    flexDirection: 'column',
    justifyContent: 'center',
    padding: 15,
    alignItems: 'center'
  },
  contentStyle: {
    height: 310,
    width: '100%',
    backgroundColor: '#ffffff', 
    borderRadius: 25
  },
  boxStyle: {
    flex: 1, 
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center'
  },
  TextStyle:{
    color: '#7c7c7c',
    textAlign: 'center',
    marginTop: 5
  },  
};

export default BillErrorModal;
