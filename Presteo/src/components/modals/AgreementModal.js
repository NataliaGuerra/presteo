import React from 'react';
import { Text, Button } from 'native-base';
import { View, ScrollView } from 'react-native';

const AgreementModal = ({ closeModal, title, message }) => {
    const { 
        viewContainerStyle, 
        scrollTextStyle, 
        sectionTitle, 
        sectionScroll,
        sectionScroll2,
        sectionsButtons,
    } = styles;
  return (
    <View style={viewContainerStyle}>
    <View style={sectionTitle}>
        <Text style={{ textAlign: 'center', marginTop: 20, fontSize: 20 }}>{title}</Text>
    </View>
    
    <View style={sectionScroll}>
        <ScrollView 
            removeClippedSubviews
            style={sectionScroll2}
            showsVerticalScrollIndicator={false}
        >
            <Text style={scrollTextStyle}>
                {message}
            </Text>
        </ScrollView>
    </View>
    <View style={sectionsButtons}>

          <Button agree onPress={closeModal}>
           <Text>Aceptar</Text>
        </Button>
      </View>

  </View> 
    
  );
};


const styles = {
    viewContainerStyle: {
        flex: 0.8, 
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#ffffff', 
      borderRadius: 25,

    },
    sectionTitle: {
      flex: 1, 
      justifyContent: 'center',
      alignItems: 'center',
      flowDirection: 'row',
      paddingLeft: 30,
      paddingRight: 30,
      paddingTop: 10
    },
    sectionScroll: {
        flex: 3,
        padding: 30,
        elevation: 3,
        zIndex: 5
    },
    sectionScroll2: {
        borderRightWidth: 10,
        borderColor: 'gray',
        borderRadius: 10,
        backgroundColor: '#fff',
        minWidth: '100%',
        maxHeight: '100%',
     
    },
    sectionsButtons: {
        flex: 1, 
        flexDirection: 'row'
    },
    scrollTextStyle: {
        textAlign: 'justify',
        fontSize: 18,
        color: 'gray',
        fontFamily: 'Rubik-Light',
    },
    blackButton: {
			backgroundColor: '#2b2b2b', 
			borderColor: '#fafdf6', 
			borderWidth: 3,
    },
    textButton: {
        fontSize: 18,
        justifyContent: 'center',
        alignItems: 'center',
    },
    sizeButton: {
        width: 118, 
        height: 40
    }
};
export default AgreementModal;
