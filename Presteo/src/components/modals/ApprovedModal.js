import React from 'react';
import { Text, Button, Thumbnail } from 'native-base';
import { View, Image } from 'react-native';
import { imageStyleBorder, imageStyleBorderOutside } from '../../assets/styles/component/common';

const check = require('../../assets/images/CheckGreen.png');


const ApprovedModal = ({ closeModal, message, url }) => {
 const { contentStyle, boxStyle, TextStyle } = styles;
  
  return (
    <View style={boxStyle}>
      <View style={contentStyle}>
        <View style={{ flexDirection: 'row', alignItems: 'center', width: '100%', height: 110, overflow: 'hidden', justifyContent: 'space-between' }}>
          <View style={[imageStyleBorderOutside, { left: 20, top: 5 }]}>
            <View style={imageStyleBorder}>
              <Thumbnail large style={{ height: 80, width: 80 }} source={{ uri: url }} />
            </View>
          </View>
          <Image style={{ height: 100, width: 100, top: 5, right: 20 }} source={check} thumbnail />
          </View>
          <View style={{ flexDirection: 'column', justifyContent: 'center', alignItems: 'center', top: 15, padding: 10 }}>
            <Text style={TextStyle} >{message}</Text>
          </View>
          <View style={{ flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center', padding: 10, top: 15 }}>
            <Button agree onPress={closeModal}>
              <Text>Aceptar</Text>
            </Button>
          </View>
      </View>
    </View> 
  );
};

const styles = {
    containerStyle: {
      flexDirection: 'column',
      justifyContent: 'center',
      padding: 15,
      alignItems: 'center'
    },
    contentStyle: {
    height: 410,
    width: '100%',
      backgroundColor: '#ffffff', 
      borderRadius: 25
    },
    boxStyle: {
        flex: 1, 
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
      },
    TextStyle:{
        color: '#7c7c7c',
        textAlign: 'center',
        marginTop: 5
    },    
  };

export default ApprovedModal;
