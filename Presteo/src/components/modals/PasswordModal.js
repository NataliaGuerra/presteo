import React, { Component } from 'react';
import { View } from 'react-native';
import { connect } from 'react-redux';
import { Text, Button, Content } from 'native-base';
import { Field, reduxForm } from 'redux-form';
import { update } from '../../actions';
import { fieldFormStyle } from '../../assets/styles/component/form';
import { 
    required,
    exactLength4, 
    number
} from '.././validation/validation';
import * as renders from '.././render/renderComponents';
import { rowCenterStyle } from '../../assets/styles/component/common';


class PasswordModal extends Component {

    componentDidMount() {
        this.handleInitialize();
    }

    onSubmit(values) {
        const formData = this.props.form; 
        formData.password = values.password;

        this.props.update(formData, this.props.auth);
        return new Promise((resolve) => {
            setTimeout(() => {
                this.props.closeModal();
                resolve();
            }, 500);
        }); 
    }
 
    handleInitialize() {
        const initData = {
            old_password: this.props.auth.claveSeguridad,
        };
        this.props.initialize(initData);  
    }

  render() { 
    const { handleSubmit } = this.props;
    return (
        <View style={rowCenterStyle}>
            <Content>
                <View style={{ height: 0, width: 0 }}>        
                    <Field style={fieldFormStyle} secureTextEntry name="old_password" keyboardType='numeric' validate={[required, exactLength4, number]} content placeholder={'Clave Anterior'} component={renders.renderInput} />
                </View>
                
                    <Field style={fieldFormStyle} secureTextEntry name="old_confirm" keyboardType='numeric' validate={[required, exactLength4, number]} placeholder={'Clave Anterior'} component={renders.renderInput} />
            
            
                    <Field style={fieldFormStyle} secureTextEntry name="password" keyboardType='numeric' validate={[required, exactLength4, number]}  placeholder={'Nueva Clave'} component={renders.renderInput} />
        
            
                <Field style={fieldFormStyle} secureTextEntry name="password_confirm" keyboardType='numeric' validate={[required, exactLength4, number]}  placeholder={'Confirmar Clave'} component={renders.renderInput} />
            
                <View style={[rowCenterStyle, { marginBottom: 10 }]}>
                <Button yellow onPress={handleSubmit(this.onSubmit.bind(this))} >
                    <Text style={{ color: '#2b2b2b' }}>Guardar cambios</Text>
                </Button>
                </View>
            </Content>
         </View>
    );
  }
}

const mapStateToProps = state => {
  return { form: state.form.UserForm.values, auth: state.auth };
};

PasswordModal = connect(
    mapStateToProps, { update }
)(PasswordModal);

export default reduxForm({

    form: 'PasswordForm',
    validate: values => {
        const errors = {};
        const password = values.password;
        const password_confirm = values.password_confirm;
        const old = values.old_password;
        const old_confirm = values.old_confirm;
        
        if (password !== password_confirm) {
            errors.password_confirm = 'Las credenciales no coinciden';
        }

        if (old !== old_confirm) {
            errors.old_confirm = 'El campo no coincide con la clave de seguridad';
        }
 
        return errors;
    }
    
  })(PasswordModal);
