import React, { Component } from 'react';
import { Text, Button, Form } from 'native-base';
import { connect } from 'react-redux';
import { View } from 'react-native';
import { Field, reduxForm } from 'redux-form';
import { required, number } from '.././validation/validation';
import { renderInputM, renderTexbox } from '../render/renderComponents';
import { createInvestment } from '../../actions';
import { commonStyles, invesmentStyles } from '../../assets/styles/component/modal';

class InvesmentModal extends Component {
    onSubmit(values) {
         
         
       const idGrantorUser = this.props.auth.idRolUsuario;
          return new Promise((resolve) => {
              setTimeout(() => {
                this.props.createInvestment(values, idGrantorUser);
                this.props.closeModal();
                  resolve();
              }, 500);
          });      
      }


    render() {
    const { guest, handleSubmit } = this.props; 
    const { textGrayStyle, textBalckStyle, textGrayMStyle } = invesmentStyles;
    const { contentStyle, footerStyle } = commonStyles;
    return (
      <View style={contentStyle}>
        <View style={{ alignItems: 'center' }}>
          <Form>
              <Text style={textGrayStyle}>
              Hola <Text style={textBalckStyle}>{this.props.auth.nombre} {this.props.auth.apellido} </Text>
              </Text>
              {guest !== 'INACTIVO' ?
                <Text style={textGrayMStyle}>
                    Por favor escribenos para solicitar invertir nuevamente en la aplicación, pronto nos comunicaremos contigo. 
                </Text>
              :
                <Text style={textGrayMStyle}>
                    Actuamente solo recibimos inveriones de invitados. Si quieres recibir una invitación informanos cuanto sería el valor que deseas invertir y que tipo de préstamos te gustaría otorgar. 
                </Text>
              } 
              <Field name="invesment" validate={[required, number]} placeholder={'$50.000'} labelText={'Valor Inversión:'} component={renderInputM} keyboardType='phone-pad'  /> 
              <Field name="Description" validate={[required]} placeholder={'Préstamos educativos, libre inversión, pagos de facturas, ayudar amigos...etc'} component={renderTexbox} />
          </Form>
        </View>
        <View style={footerStyle}>
            <Button agree onPress={handleSubmit(this.onSubmit.bind(this))}>
                <Text>Aceptar</Text>
            </Button>
        </View>
      </View>  
    );
    }
}

  const mapStateToProps = state => {
    return { auth: state.auth };
  };
  
  InvesmentModal = connect(
    mapStateToProps, { createInvestment }
  )(InvesmentModal);
  
  export default reduxForm({
  
    form: 'GrantorInvestmentsForm',
    validate: values => {
      
      const errors = {};
    
      return errors;
    },
    destroyOnUnmount: true
    
  })(InvesmentModal);
