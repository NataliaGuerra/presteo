import React, { Component } from 'react';
import { Text, Button, Form } from 'native-base';
import { connect } from 'react-redux';
import { View } from 'react-native';
import { Field, reduxForm } from 'redux-form';
import { renderInputM, renderTexbox } from '../render/renderComponents';
import { createInvestment } from '../../actions';
import { commonStyles, invesmentStyles } from '../../assets/styles/component/modal';

class InveSummaryModal extends Component {
    componentDidMount() {
      this.handleInitialize();
    }

    handleInitialize() {
      this.props.initialize(this.props.initData);  
    }

    render() {
    const { closeModal } = this.props; 
    const { textGrayStyle, textBalckStyle, textGrayMStyle } = invesmentStyles;
    const { contentStyle, footerStyle } = commonStyles;
    return (
      <View style={contentStyle}>
        <View style={{ alignItems: 'center' }}>
          <Form>
            <Text style={textGrayStyle}>
            Hola <Text style={textBalckStyle}>{this.props.auth.nombre} {this.props.auth.apellido} </Text>
            </Text>
            <Text style={textGrayMStyle}>
                Este es el resumen de tu inversión actual:
            </Text>
            <Field disabled name="invesment" labelText={'Valor Inversión:'} component={renderInputM} /> 
            <Field disabled name="description" component={renderTexbox} />
          </Form>
        </View>
        <View style={footerStyle}>
            <Button agree onPress={closeModal}>
                <Text>Aceptar</Text>
            </Button>
        </View>
      </View>  
    );
    }
}

  const mapStateToProps = state => {
    return { auth: state.auth };
  };
  
  InveSummaryModal = connect(
    mapStateToProps, { createInvestment }
  )(InveSummaryModal);
  
  export default reduxForm({
  
    form: 'GrantorInvestmentsForm',
    validate: values => {
      
      const errors = {};
    
      return errors;
    },
    destroyOnUnmount: true
    
  })(InveSummaryModal);
