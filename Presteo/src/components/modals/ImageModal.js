import React from 'react';
import { Button, Text } from 'native-base';
import { View, Image } from 'react-native';

const ImageModal = ({ closeModal, pqr, url, pqrModal }) => {
    const { buttonContainer, imageContainer, supportContainer } = styles;
  return (
    <View style={supportContainer}>
        <View style={imageContainer}>
          <Image
            style={{ flex: 1 }}
            resizeMode='stretch'
            source={{ uri: url }}
          /> 
        </View>
        <View style={buttonContainer}>
        { pqr &&
         <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'flex-end', alignItems: 'flex-start' }}>
            <Button style={{ marginBottom: -25 }} small pqr onPress={pqrModal}>
               <Text style={{ color: '#000' }}>PQR</Text>
             </Button>
             </View>
        }  
        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
          <Button agree onPress={closeModal}>
          <Text>Aceptar</Text>
          </Button>
          </View>
        </View>
    </View>
  );
};

const styles = {
    supportContainer: {
        backgroundColor: '#ffffff', 
        borderRadius: 25,
        padding: 20,
        flex: 0.9
      },
      imageContainer: {
        flex: 3.5,
        borderColor: 'gray'
      },
      buttonContainer: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
      }
  };

export default ImageModal;
