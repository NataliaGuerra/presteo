import React from 'react';
import { Text, Button } from 'native-base';
import { View, Image } from 'react-native';

const error = require('../../assets/images/YellowBackGround.png');


const VoucherImageModal = ({ closeModal }) => {
 const { contentStyle, boxStyle, ButtonPqr } = styles;
  return (
    <View style={boxStyle}>
        
        <View style={contentStyle}>
            <View style={{ alignItems: 'center', width: '100%', height: 370, overflow: 'hidden' }}>

            <Image style={{ height: 370, width: 300, top: 20 }} source={error} thumbnail />
            </View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center', padding: 10 }}>
                <Button style={ButtonPqr}>
                    <Text style={{color: 'black'}}>PQR</Text>
                </Button>
                <Button agree onPress={closeModal}>
                    <Text>Aceptar</Text>
                </Button>
            </View>
        </View>
    </View> 
  );
};

const styles = {
    containerStyle: {
      flexDirection: 'column',
      justifyContent: 'center',
      padding: 15,
      alignItems: 'center'
    },
    contentStyle: {
    height: 450,
    width: '100%',
      backgroundColor: '#ffffff', 
      borderRadius: 25
    },
    boxStyle: {
        flex: 1, 
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
      },  
    ButtonPqr: {
		color: 'white',	
		backgroundColor: 'yellow', 
		borderColor: 'orange', 
		borderWidth: 3,
    },
  };

export default VoucherImageModal;
