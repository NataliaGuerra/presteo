import React, { Component } from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { 
    View, 
    Platform, 
    StyleSheet,  
    Vibration 
} from 'react-native';
import { Button, Text } from 'native-base';
import SmsListener from 'react-native-android-sms-listener';
import SMSVerifyCode from 'react-native-sms-verifycode';
import { setCodeToVerify, verifyCode, 
  reSendCode, verifyCodeRequest, register } from '../../actions';

class VerifyModal extends Component {

  state = {
    code: this.props.otp.code,
    resendCode: false,
    errorMessage: '',
    error: false,
  }
  componentDidMount() {
    if(Platform.OS === 'android') {
      setTimeout(() => {
        if (this.props.otp.code != null ) {
          this.onInputCompleted(this.props.otp.code);
        } else {
          const now  = new Date();
          let s = Math.round((((now - this.props.otp.date) % 86400000) % 3600000) / 60000);
          if (s > 0) {
            this.props.register(this.props.form, this.props.auth); 
            this.props.closeModal();
          }
        }
      }, 200);
    }
  }

  onInputCompleted = (code) => {
    const options = {
      method: 'POST',
      headers: { 
        //@TODO Change authorization with App
        Authorization: 'Basic c21hcnRsb2FuOjVtNFI3ITBhbg==', 
        'Content-Type': 'application/json'
      },
      data: {
        pin: code
      },
      url: `https://api.infobip.com/2fa/1/pin/${this.props.otp.pinId}/verify`,
		};
    this.props.verifyCodeRequest();
    axios(options).then((response) => {
      if (response.data.verified) {
        this.props.verifyCode(true);
        this.props.register(this.props.form, this.props.auth); 
        this.props.closeModal();
      } else {
        this.setState({ error: true, 
          errorMessage: 'Este código no es correcto! Vuelve a intentar' });
        this.props.verifyCode(false);
        Vibration.vibrate(800);
      }
    }).catch((error) => {
       
      this.setState({ error: true, 
        errorMessage: 'No hemos podido realizar tu registro' });
        this.props.verifyCode(false);
      Vibration.vibrate(800);
    }); 
  }

  sendCodeAgain() {
    this.setState({ resendCode: true });
    this.props.reSendCode(this.props.otp.pinId);
    if (Platform.OS === 'android') {
      const smsSuscription = SmsListener.addListener(message => {
        this.props.setCodeToVerify(message.body.match(/([\d]{5})/)[0]);
        smsSuscription.remove();
      });
    }
    setTimeout(() => {
      this.setState({ resendCode: false });
    }, 15000);
  }

  render() {
      const { closeModal } = this.props; 
    return (
        <View style={styles.contentStyle}>
        <View>
          <View style={styles.textContainer}>
          <Text style={{ color: '#2b2b2b', fontSize: 23, fontWeight: 'bold', marginBottom: 10, paddingLeft: -10 }}>Código de verificación</Text>
          </View>
          <View style={{ felx: 1, marginBottom: 20 }}>
          <SMSVerifyCode
            
            ref={ref => (this.verifycode = ref)}
            onInputCompleted={this.onInputCompleted}
            containerPaddingHorizontal={0}
            verifyCodeLength={5}
           
            // codeViewStyle={}
            codeViewBorderWidth={0}
            codeViewBackgroundColor="#2b2b2b"
            codeViewBorderColor="#fff"
            focusedCodeViewBorderColor="#fff"
            codeFontSize={20}
            codeColor="#e4ff33"
            containerPaddingHorizontal={10}

            onInputChangeText={() => {}}  
            initialCodes={
              this.props.otp.code === null ? [] : Array.from(this.props.otp.code.toString())
            }
          />
          </View>
          {this.state.error &&
            <Text style={{ color: 'red', fontSize: 14, marginBottom: 20, textAlign: 'center' }}>{this.state.errorMessage}</Text>
          }
          {this.state.resendCode ? 
          <Text
          style={{ textAlign: 'center', color: '#7c7c7c7c', fontSize: 14 }} 
          >Revisa tú bandeja de nuevo</Text>
          : 
          <Text 
            style={{ textAlign: 'center', color: '#7c7c7c7c', fontSize: 14 }} 
            onPress={this.sendCodeAgain.bind(this)}
          >No llega mi código de verificación</Text>
          }
          </View>
           <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
            <Button accept onPress={closeModal}>
            <Text>Cancelar</Text>
            </Button>
        </View>
        </View>
    );
  }
}
const styles = StyleSheet.create({
  textContainer: {
    flexDirection: 'column',
    alignItems: 'center',
    backgroundColor: '#FEFFFE',
    marginBottom: 0
  },
  modalBackground: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'
  },
  activityIndicatorWrapper: {
    backgroundColor: '#FFFFFF',
    height: 100,
    width: 100,
    borderRadius: 10,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around'
    },
    contentStyle: {
        backgroundColor: '#ffffff', 
        borderRadius: 25,
        paddingTop: 20,
        paddingBottom: 20
      },
});

const mapStateToProps = state => {
  return { otp: state.otp, common: state.common, form: state.form.UserForm.values, auth: state.auth };
};

export default connect(mapStateToProps, 
  { setCodeToVerify, verifyCode, verifyCodeRequest, reSendCode, register })(VerifyModal);
