import React from 'react';
import { Text, Button, Thumbnail } from 'native-base';
import { View } from 'react-native';
import { commonStyles } from '../../assets/styles/component/modal';

const error = require('../../assets/images/RedEquis.png');
const check = require('../../assets/images/CheckGreen.png');

const AlertModal = ({ closeModal, message, icon }) => {
const {
  contentStyle,
  thumbnailStyle,
  footerStyle,
  textStyle,
  alingCenterStyle
} = commonStyles;

  return (

    <View style={contentStyle}>
        <View style={alingCenterStyle}>
          <Thumbnail large style={thumbnailStyle} source={icon ? check : error} />
            <Text style={textStyle} >{message}</Text>
        </View>
        <View style={footerStyle}>
        <Button error onPress={closeModal}>
           <Text>Aceptar</Text>
        </Button>
        </View>
    </View>
    
  );
};

export default AlertModal;
