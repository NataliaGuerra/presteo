import React from 'react';
import { Text, Button, Thumbnail } from 'native-base';
import { View, Image } from 'react-native';
import { maintenanceStyles, commonStyles } from '../../assets/styles/component/modal';

const wIcon = require('../../assets/images/FixIcon.png');
const wBard = require('../../assets/images/warningbar.png');

const MaintenanceModal = ({ closeModal, message }) => {
    
const { 
    contentStyle,
    boxStyle,
    imgStye,  
    bodyStyle,
} = maintenanceStyles;

const { 
    thumbnailStyle,
    footerStyle,
    textMTopStyle 
} = commonStyles;

  return (
    <View style={boxStyle}>
        <Thumbnail large style={thumbnailStyle} source={wIcon} />
        <View style={contentStyle}>
            <View style={imgStye}>
                <Image
                    source={wBard}
                />
            </View>
            <View style={bodyStyle}>
                <Text style={textMTopStyle} >{message}</Text>
            </View>
            <View style={footerStyle}>
                <Button agree onPress={closeModal}>
                    <Text>Aceptar</Text>
                </Button>
            </View>
        </View>
    </View> 
  );
};

export default MaintenanceModal;
