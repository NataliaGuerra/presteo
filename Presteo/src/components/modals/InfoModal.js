import React from 'react';
import { Text, Button, Icon } from 'native-base';
import { View } from 'react-native';
import * as common from '../../assets/styles/component/common';
import { commonStyles } from '../../assets/styles/component/modal';

const InfoModal = ({ closeModal, message }) => {
 const { contentStyle, textMTopStyle, footerStyle, iconInfoStyle } = commonStyles;
  return (

    <View style={contentStyle}>
        <View style={common.columnCenterStyle}>
            <View style={common.rowCenterStyle}>
                <Button fab style={common.noBorderStyle}>
                    <Icon style={iconInfoStyle} name="ios-information" />
                </Button>
            </View>
            <View style={common.rowCenterStyle}>
                <Text style={textMTopStyle} >
                    {message}
                </Text>
            </View>
        </View>
        <View style={footerStyle}>
        <Button agree onPress={closeModal}>
           <Text>Aceptar</Text>
        </Button>
        </View>
    </View>
    
  );
};

export default InfoModal;
