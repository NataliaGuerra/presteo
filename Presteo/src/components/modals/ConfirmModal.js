import React from 'react';
import { Text, Button, Icon, Thumbnail } from 'native-base';
import { View } from 'react-native';

const check = require('../../assets/images/CheckGreen.png');

const ConfirmModal = ({ closeModal, title, message }) => {
 const { contentStyle  } = styles;
  return (

    <View style={contentStyle}>
        <View style={{ alignItems: 'center' }}>
            <Text style={{ color: '#000', textAlign: 'center', marginTop: 20 }} >Confirmación</Text>
            <Text style={{ color: '#7c7c7c', textAlign: 'center', marginTop: 20, marginBottom: 10 }} >{title}</Text>
            <Thumbnail large style={{ height: 100, width: 100, borderRadius: 50 }} source={check} />
            <Text style={{ color: '#7c7c7c', textAlign: 'center', marginTop: 10 }} >{message}</Text>
        </View>
        <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginTop: 20 }}>
        <Button agree onPress={closeModal}>
           <Text>Aceptar</Text>
        </Button>
        </View>
    </View>
    
  );
};

const styles = {
    containerStyle:{
      flex: 1, 
      flexDirection: 'column',
      justifyContent: 'center',
      padding: 15,
      alignItems: 'center'
    },
    contentStyle: {
      backgroundColor: '#ffffff', 
      borderRadius: 25,
      padding: 20
    },
  };

export default ConfirmModal;
