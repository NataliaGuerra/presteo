import alertModal from './AlertModal';
import successModal from './SuccessModal';
import invesmentModal from './InvesmentModal';
import passwordModal from './PasswordModal';
import verifyModal from './VerifyModal';
import agreementModal from './AgreementModal';
import maintenanceModal from './MaintenanceModal';
import infoModal from './InfoModal';
import confirmModal from './ConfirmModal';
import imageModal from './ImageModal';
import approvedModal from './ApprovedModal';
import billErrorModal from './BillErrorModal';
import reviewModal from './ReviewModal';
import contractModal from './ContractModal';
import internetModal from './InternetModal';
import inveSummaryModal from './InveSummaryModal';

const modalTypes = {
  alertModal,
  successModal,
  invesmentModal,
  passwordModal,
  verifyModal,
  agreementModal,
  maintenanceModal,
  infoModal,
  confirmModal,
	imageModal,
	approvedModal,
	billErrorModal,
  reviewModal,
  contractModal,
  internetModal,
  inveSummaryModal
};

export default modalTypes;
