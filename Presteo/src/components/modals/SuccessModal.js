import React from 'react';
import { Text, Button } from 'native-base';
import { View } from 'react-native';
import { commonStyles } from '../../assets/styles/component/modal';

const SuccessModal = ({ closeModal, message }) => {
 const {
  contentStyle,
  footerStyle,
  textMTopStyle,
  alingCenterStyle
} = commonStyles;

  return (

    <View style={contentStyle}>
        <View style={alingCenterStyle}>
            <Text style={textMTopStyle} >{message}</Text>
        </View>
        <View style={footerStyle}>
        <Button agree onPress={closeModal}>
           <Text>Aceptar</Text>
        </Button>
        </View>
    </View>
    
  );
};

export default SuccessModal;
