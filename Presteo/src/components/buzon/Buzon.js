import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { BackGroundM } from '../common';
import { hideModal, showModal } from '../../actions';

class Buzon extends Component {

  componentWillMount() {
     
   this.props.showModal({
      open: true,
      title: 'Alert Modal',
      message: 'Funcionalidad en Proceso!',
      closeModal: this.closeModal.bind(this)
    }, 'maintenance'); 
  }
    
  closeModal() {
    this.props.hideModal();
    Actions.pop();
  }

  render() { 
    return (
      <BackGroundM title="Buzón" />
    );  
  }
}


const mapDispatchToProps = dispatch => ({
    hideModal: () => dispatch(hideModal()),
    showModal: (modalProps, modalType) => {
      dispatch(showModal({ modalProps, modalType }));
    }
  });
  
  Buzon = connect(
    null, mapDispatchToProps
  )(Buzon);
  
  export default Buzon;
