import { 
  SET_CODE,
  NUMBER_VERIFIED_SUCCESS,
  NUMBER_VERIFIED_ERROR
} from '../actions/types';


const INITIAL_STATE = { 
  pinId: '',
  to: '',
  ncStatus: '',
  smsStatus: '',
  code: null,
  success: false,
  verified: null,
  date: null,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'SEND_CODE_SUCCESS':
      return { 
        ...state,
        pinId: action.payload.pinId,
        to: action.payload.to,
        ncStatus: action.payload.ncStatus,
        smsStatus: action.payload.smsStatus,
        success: true,
        verified: false,
        code: null
      };
    case SET_CODE:
      return { ...state, code: action.payload };
    case 'SEND_CODE_FAILURE':
      return INITIAL_STATE;
    case NUMBER_VERIFIED_SUCCESS:
      return { ...INITIAL_STATE, verified: action.payload };
    case NUMBER_VERIFIED_ERROR:
      return { ...state, verified: action.payload };
    case 'SET_PHONE_NUMBER':
      return { ...state, date: action.payload.date };
    default:
      return state;
  }
};

