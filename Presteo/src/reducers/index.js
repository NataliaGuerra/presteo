import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import AuthReducer from './AuthReducer';
import OTPReducer from './OTPReducer';
import LoanReducer from './LoanReducer';
import LoaderReducer from './LoaderReducer';
import ModalReducer from './ModalReducer';

export default combineReducers({
    form: formReducer,
    auth: AuthReducer,
    otp: OTPReducer,
    loan: LoanReducer,
    loader: LoaderReducer,
    modal: ModalReducer
});
