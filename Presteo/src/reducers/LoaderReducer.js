export default (state = { gif: null }, action) => {
  const { type, payload } = action;
  const matches = /(.*)_(REQUEST|SUCCESS|FAILURE)/.exec(type);

  if (!matches) return state;  
  
  const [, requestName, requestState] = matches;

  return {
    ...state,
    [requestName]: requestState === 'REQUEST',
    gif: requestState === 'REQUEST' && payload != null ? payload : null,
  };
};
