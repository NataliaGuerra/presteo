import { 
  SELECT_ROL,
  USER_DATA,
  CLEAR_DATA,
  GRANTOR_STATE,
  ACCEPT_CONTRACT,
  SET_ROL_ID,
	NUMBER_VERIFIED_SUCCESS,
	REGISTER_DEVICE
} from '../actions/types';

const INITIAL_STATE = { 
  rol: null, 
  state: 0, 
  age_range: {},
  name_format: {},
  imagenPerfil: {},
  imagenPerfilPath: '',
  contract: false,
  idUsuario: 0,
  idRolUsuario: 0,
  nombre: '',
  apellido: '',
  correo: '',
  celular: '',
  fechaNacimiento: '0000-00-00',
  codigoSeguridad: '',
  numeroDocumento: '',
  tipoDocumento: '',
  estadoOU: '',
  pais: '',
  ciudad: '',
  direccion: '',
  fechaCreacion: '',
  fechaModificacion: '',
  cuentas: '',
  bancos: [],
  contratos: '',
  claveSeguridad: '',
  codigoPostal: '',
  rangoEdad: '',
  genero: '',
  locacion: '',
  link: '',
  usuarioPerfil: {
    idUsuarioPerfil: '',
    primerNombre: '',
    apellido: '',
    segundoNombre: '',
    nombreCompleto: '',
    nombreCorto: ''
  },
  token: '',
	facebookId: '',
	dispositivosUsuario: []
};

/**
 * State: 0 Login - 1 Contract - 2 RoleSelection
 * - 3 PhoneVerify - 4 Loged 
 */

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case USER_DATA:

      return { ...state, 
        correo: action.payload.email,
        rangoEdad: action.payload.age_range == null ? null :
          action.payload.age_range.min + '-' + action.payload.age_range.max,
        usuarioPerfil: {
          primerNombre: action.payload.first_name,
          apellido: action.payload.last_name,
        },
        genero: action.payload.gender == null ? null : action.payload.gender, 
        link: action.payload.link == null ? null : action.payload.link,
        locacion: action.payload.location == null ? null : action.payload.location,
        name_format: action.payload.name_format,
        imagenPerfil: action.payload.picture,
        imagenPerfilPath: action.payload.imgProfile,
        token: action.payload.token,
        facebookId: action.payload.id
      };
    case ACCEPT_CONTRACT:
      return { ...state, state: 2, contract: true, };
    case SELECT_ROL:
      return { ...state, rol: action.payload, state: 3 };
    case 'SET_PHONE_NUMBER':
     return { ...state, celular: action.payload.number };
    case 'CREATE_USER_FACEBOOK_SUCCESS':
      return { ...state, 
        idUsuario: action.payload.idUsuario,
        usuarioPerfil: {
          primerNombre: action.payload.usuarioPerfil.primerNombre,
          apellido: action.payload.usuarioPerfil.apellido,
          idUsuarioPerfil: action.payload.usuarioPerfil.idUsuarioPerfil,
          nombreCompleto: action.payload.usuarioPerfil.nombreCompleto,
        },
        state: 1 
      };
    case 'CREATE_USER_REGISTER_SUCCESS':
      return { ...state, 
        correo: action.payload.correo, 
        nombre: action.payload.nombre,
        apellido: action.payload.apellido,
        numeroDocumento: action.payload.numeroDocumento,
        claveSeguridad: action.payload.claveSeguridad,
        tipoDocumento: action.payload.tipoDocumento.idTipoDocumento,
        imagenPerfilPath: action.payload.usuarioPerfil.fotoPerfil,
        pais: action.payload.pais.idPais,
        ciudad: action.payload.ciudad.idCiudad,
        direccion: action.payload.direccion,
        bancos: action.payload.bancos,
        state: 4
      };
    case 'UPDATE_USER_REGISTER_SUCCESS':
      return { ...state, 
        correo: action.payload.correo,
        nombre: action.payload.nombre,
        apellido: action.payload.apellido,
        numeroDocumento: action.payload.numeroDocumento,
        claveSeguridad: action.payload.claveSeguridad,
        tipoDocumento: action.payload.tipoDocumento.idTipoDocumento,
        imagenPerfilPath: action.payload.usuarioPerfil.fotoPerfil,
        pais: action.payload.pais.idPais,
        ciudad: action.payload.ciudad.idCiudad,
        direccion: action.payload.direccion,
        bancos: action.payload.bancos,
      };
    case 'GUEST_STATE':
      return { ...state, 
        estadoOU: action.payload,
      };
    case SET_ROL_ID:
      return { ...state, idRolUsuario: action.payload };
    case NUMBER_VERIFIED_SUCCESS:
      return { ...state, state: 3 };
    case CLEAR_DATA:
			return INITIAL_STATE;
		case REGISTER_DEVICE:
			return { ...state, dispositivosUsuario: [action.payload]}
    default:
      return state;
  }
};

