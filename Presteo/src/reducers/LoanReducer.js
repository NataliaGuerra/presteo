import { 
  SELECT_LOAN,
  SELECT_TIME,
  SELECT_MODALITY,
  UPDATE_LOAN_VALUE,
  SET_RECEIPT_FRONT_PHOTO,
  SET_RECEIPT_BACK_PHOTO,
  ADD_SUPPORT_IDENTITY,
  RESET_LOAN
} from '../actions/types';


const INITIAL_STATE = { 
  kindOfLoan: 0,
  nameOfLoan: '',
  kindOfTime: 0,
  nameOfTime: '',
  kindOfModality: 0,
  nameOfModality: '',
  valueLoan: 0,
  index: 0,
  receiptFront: {},
  receiptBack: {},
  selfie: null,
  document: null,
  idPhoto: null,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SELECT_LOAN:
      return { ...state, 
          kindOfLoan: action.payload.value,  
          nameOfLoan: action.payload.name
        };
    case SELECT_TIME:
      return { ...state, 
        kindOfTime: action.payload.value,
        nameOfTime: action.payload.name
      };
    case SELECT_MODALITY:
      return { ...state, 
        kindOfModality: action.payload.value,
        nameOfModality: action.payload.name
      };
		case UPDATE_LOAN_VALUE:
      return { ...state, valueLoan: action.payload };
    case SET_RECEIPT_FRONT_PHOTO:
      return { ...state, receiptFront: action.payload };
    case SET_RECEIPT_BACK_PHOTO:
      return { ...state, receiptBack: action.payload };
    case ADD_SUPPORT_IDENTITY:
      switch (action.payload.index) {
        case 0:
          return { ...state, selfie: action.payload.value };
        case 1:
          return { ...state, idPhoto: action.payload.value }; 
        default:
          return { ...state, document: action.payload.value };
      }
    case RESET_LOAN:
      return INITIAL_STATE;
    default:
      return state;
  }
};

