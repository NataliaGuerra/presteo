import React from 'react';
import { connect } from 'react-redux';
import Modal from 'react-native-modal';
import { StyleProvider } from 'native-base';
import getTheme from './themes/components';
import platform from './themes/variables/platform';

import { default as modalTypes } from './components/modals';

const MODAL_TYPES = {
  alert: modalTypes.alertModal,
  success: modalTypes.successModal,
  invesment: modalTypes.invesmentModal,
  password: modalTypes.passwordModal,
  verify: modalTypes.verifyModal,
  agreement: modalTypes.agreementModal,
  maintenance: modalTypes.maintenanceModal,
  info: modalTypes.infoModal,
  confirm: modalTypes.confirmModal,
	image: modalTypes.imageModal,
	approved: modalTypes.approvedModal,
	billError: modalTypes.billErrorModal,
  review: modalTypes.reviewModal,
  contract: modalTypes.contractModal,
  internet: modalTypes.internetModal,
  invesummary: modalTypes.inveSummaryModal,
};

const mapStateToProps = state => ({
  ...state.modal
});

class ModalContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      modalIsOpen: false
    };
    this.closeModal = this.closeModal.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps !== this.props) {
      this.setState({
        modalIsOpen: nextProps.modalProps.open
      });
    }
  }

  closeModal() {
    this.setState({ modalIsOpen: false });
  }

  render() {
    if (!this.props.modalType) {
      return null;
    }
		const SpecifiedModal = MODAL_TYPES[this.props.modalType];
		const { backdrop = true } = this.props.modalProps;
    return (
      <StyleProvider style={getTheme(platform)}>
        <Modal
          isVisible={this.state.modalIsOpen}
          onBackdropPress={backdrop ? this.closeModal : null}
         
        >
          <SpecifiedModal
            closeModal={this.closeModal}
            {...this.props.modalProps}
          />
        </Modal>  
      </StyleProvider> 
    );
  }
}

export default connect(mapStateToProps, null)(ModalContainer);
