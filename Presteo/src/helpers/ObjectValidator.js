class ObjectValidator {
    static ValidateFacebookRegister = (obj) => {
      if (typeof (obj.idUsuario) == 'undefined' || obj.idUsuario === null) {
        return false; 
      }
      if (typeof (obj.usuarioPerfil.primerNombre) == 'undefined' || obj.usuarioPerfil.primerNombre === null) {
        return false; 
      }
      if (typeof (obj.usuarioPerfil.apellido) == 'undefined' || obj.usuarioPerfil.apellido === null) {
        return false; 
      }
      if (typeof (obj.usuarioPerfil.idUsuarioPerfil) == 'undefined' || obj.usuarioPerfil.idUsuarioPerfil === null) {
        return false; 
      }
      if (typeof (obj.usuarioPerfil.nombreCompleto) == 'undefined' || obj.usuarioPerfil.nombreCompleto === null) {
        return false; 
      }
      return true;
    };
 
    static ValidateUserRegister = (obj) => {
      if (typeof (obj.correo) == 'undefined' || obj.correo === null) {
        return false; 
      }
      if (typeof (obj.nombre) == 'undefined' || obj.nombre === null) {
        return false; 
      }
      if (typeof (obj.apellido) == 'undefined' || obj.apellido === null) {
        return false; 
      }
      if (typeof (obj.numeroDocumento) == 'undefined' || obj.numeroDocumento === null) {
        return false; 
      }
      if (typeof (obj.claveSeguridad) == 'undefined' || obj.claveSeguridad === null) {
        return false; 
      }
      if (typeof (obj.direccion) == 'undefined' || obj.direccion === null) {
        return false; 
      }
      if (typeof (obj.bancos) == 'undefined' || obj.bancos === null) {
        return false; 
      }
      if (typeof (obj.tipoDocumento.idTipoDocumento) == 'undefined' || obj.tipoDocumento.idTipoDocumento === null) {
        return false; 
      }
      if (typeof (obj.usuarioPerfil.fotoPerfil) == 'undefined' || obj.usuarioPerfil.fotoPerfil === null) {
        return false; 
      }
      if (typeof (obj.pais.idPais) == 'undefined' || obj.pais.idPais === null) {
        return false; 
      }
      if (typeof (obj.ciudad.idCiudad) == 'undefined' || obj.ciudad.idCiudad === null) {
        return false; 
      }
      return true;
    };
  }
  
  export default ObjectValidator;
