import variable from "./../variables/platform";

export default (variables = variable) => {
  const textTheme = {
    fontSize: variables.SelectionRoleFontSize,
    fontFamily: "Rubik-Medium",
    color: variables.textColor,
    ".note": {
      color: "#a7a7a7",
    }
  };

  return textTheme;
};
