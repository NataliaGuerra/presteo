import variable from "./../variables/platform";

export default (variables = variable) => {
  const platformStyle = variables.platformStyle;
  const platform = variables.platform;
  const darkCommon = {
    "NativeBase.Text": {
      color: variables.brandDark
    },
    "NativeBase.Icon": {
      color: variables.brandDark
    },
    "NativeBase.IconNB": {
      color: variables.brandDark
    }
  };
  const lightCommon = {
    "NativeBase.Text": {
      color: '#fafdf6'
    },
    "NativeBase.Icon": {
      color: variables.brandLight
    },
    "NativeBase.IconNB": {
      color: variables.brandLight
    }
  };
  const primaryCommon = {
    "NativeBase.Text": {
      color: '#fafdf6'     
    },
    "NativeBase.Icon": {
      color: variables.btnPrimaryBg
    },
    "NativeBase.IconNB": {
      color: variables.btnPrimaryBg
    }
  };

  const formCommon = {
    "NativeBase.Text": {
      color: '#fff'     
    },
    "NativeBase.Icon": {
      color: variables.btnPrimaryBg
    },
    "NativeBase.IconNB": {
      color: variables.btnPrimaryBg
    }
  };

  const successCommon = {
    "NativeBase.Text": {
      color: '#fafdf6'
    },
    "NativeBase.Icon": {
      color: variables.btnSuccessBg
    },
    "NativeBase.IconNB": {
      color: variables.btnSuccessBg
    }
  };
  const infoCommon = {
    "NativeBase.Text": {
      color: '#fafdf6'
    },
    "NativeBase.Icon": {
      color: variables.btnInfoBg
    },
    "NativeBase.IconNB": {
      color: variables.btnInfoBg
    }
	};
	const acceptCommon = {
		"NativeBase.Text": {
			paddingHorizontal: 10,
			marginLeft: 0,
      marginRight: 0,
    },
  };
  const titleCommon = {
		"NativeBase.Text": {
			paddingHorizontal: 10,
			marginLeft: 0,
      marginRight: 0,
		}
	};
	
  const agreeCommon = {
		"NativeBase.Text": {
			paddingHorizontal: 10,
			marginLeft: 0,
      marginRight: 0,
      fontSize: 17
    },
  };

  const errorCommon = {
		"NativeBase.Text": {
      fontSize: 17
    },
  };

  const yellowCommon = {
		"NativeBase.Text": {
      fontSize: 18
    },
  };

  const pqrCommon = {
		"NativeBase.Text": {
      fontSize: 18,
    },
  };


  const fabCommon = {
		"NativeBase.Text": {
      fontSize: 18
    },
  };
	
	const navigationCommon = {
		"NativeBase.Text": {
			paddingHorizontal: 15,
			marginLeft: 0,
			marginRight: 0,
		},
		height: 38
	}
	const receiptCommon = {
		"NativeBase.Text": {
			paddingHorizontal: 10,
			marginLeft: 0,
      marginRight: 0,
		},
		paddingHorizontal: 25,
		paddingVertical: 2,
		height: 35
	}
	const historialCommon = {
		"NativeBase.Text": {
			paddingHorizontal: 10,
			marginLeft: 0,
      marginRight: 0,
		},
		paddingVertical: 2,
		height: 30,
		paddingHorizontal: 10
	}
	

  const warningCommon = {
    "NativeBase.Text": {
      color: '#fff'
    },
    "NativeBase.Icon": {
      color: variables.btnWarningBg
    },
    "NativeBase.IconNB": {
      color: variables.btnWarningBg
    }
  };
  const dangerCommon = {
    "NativeBase.Text": {
      color: variables.btnDangerBg
    },
    "NativeBase.Icon": {
      color: variables.btnDangerBg
    },
    "NativeBase.IconNB": {
      color: variables.btnDangerBg
    }
  };
  const buttonTheme = {
    ".disabled": {
      ".transparent": {
        backgroundColor: null,
        "NativeBase.Text": {
          color: variables.btnDisabledBg
        },
        "NativeBase.Icon": {
          color: variables.btnDisabledBg
        },
        "NativeBase.IconNB": {
          color: variables.btnDisabledBg
        }
      },
      "NativeBase.Icon": {
        color: variables.brandLight
      },
      "NativeBase.IconNB": {
        color: variables.brandLight
      },
      backgroundColor: variables.btnDisabledBg
    },
    ".bordered": {
      ".dark": {
        ...darkCommon,
        backgroundColor: "#e4ff33",
        borderColor: variables.brandDark,
        borderWidth: variables.borderWidth * 2
      },
      ".light": {
        ...lightCommon,
        backgroundColor: '#7c7c7c',
        borderColor: '#fafdf6',
        borderWidth: 3,
      },
      ".primary": { 
        ...primaryCommon,
        backgroundColor: '#7c7c7c',
        borderColor: variables.btnBorder,
        borderWidth: variables.borderWidth * 2
      },
      ".success": {
        ...successCommon,
        backgroundColor: "transparent",
        borderColor: variables.btnSuccessBg,
        borderWidth: variables.borderWidth * 2
      },
      ".info": {
        ...infoCommon,
        backgroundColor: '#7c7c7c',
        borderColor: variables.btnBorder,
        borderWidth: 3,
      },
      ".warning": {
        ...warningCommon,
        backgroundColor: '#2b2b2b',
        borderColor: '#fafdf6',
        borderWidth: 3
      },
      ".danger": {
        ...dangerCommon,
        backgroundColor: "transparent",
        borderColor: variables.btnDangerBg,
        borderWidth: variables.borderWidth * 2
      },
      ".disabled": {
        backgroundColor: null,
        borderColor: variables.btnDisabledBg,
        borderWidth: variables.borderWidth * 2,
        "NativeBase.Text": {
          color: variables.btnDisabledBg
        }
			},
      ...primaryCommon,
      borderWidth: variables.borderWidth * 2,
      elevation: null,
      shadowColor: "#e4ff33",
      shadowOffset: null,
      shadowOpacity: null,
      shadowRadius: 10,
      backgroundColor: "#e4ff33"
    },

		".navigation": {
			...navigationCommon
		},
    ".yellow": {
      ...yellowCommon,
      backgroundColor: '#e4ff33',
      borderWidth: 0,
    },

    ".form": {
      ...formCommon,
      backgroundColor: '#000',
      borderWidth: 3,
      borderColor: '#ffffff'
    },

    ".pqr": {
      ...pqrCommon,
      backgroundColor: '#e4ff33',
      borderWidth: 3,
      borderColor: '#cdde33'
    },

    ".fab": {
      ...fabCommon,
      backgroundColor: '#2b2b2b',
      width: 60,
			height: 60,
      borderRadius: 30,
      borderWidth: 2,
    },


    ".error": {
      ...errorCommon,
      backgroundColor: '#7c7c7c',
      borderWidth: 3,
    },

    ".agree": {
      ...agreeCommon,
      backgroundColor: '#2b2b2b',
      borderWidth: 0
    },
    
		".accept": {
			...acceptCommon
		},
		".receipt": {
			...receiptCommon,
		},
		".historial": {
			...historialCommon,
		},
		".title": {
			...titleCommon,
			backgroundColor: '#7c7c7c',
			borderColor: 'white',
			borderWidth: 1,
			elevation: 0,
      shadowColor: null,
      shadowOffset: null,
      shadowRadius: null,
      shadowOpacity: null,
    },
    
    ".dark": {
      ".bordered": {
        ...darkCommon
      },
      backgroundColor: variables.brandDark
    },

    ".light": {
      ".transparent": {
        ...lightCommon,
        backgroundColor: null
      },
      ".bordered": {
        ...lightCommon
      },
      ...darkCommon,
      backgroundColor: variables.brandLight
    },

    ".primary": {
      ".bordered": {
        ...primaryCommon
      },
      backgroundColor: variables.btnPrimaryBg
    },

    ".success": {
      ".bordered": {
        ...successCommon
      },
      backgroundColor: variables.btnSuccessBg
    },

    ".info": {
      ".bordered": {
        ...infoCommon
      },
      backgroundColor: variables.btnInfoBg
    },

    ".warning": {
      ".bordered": {
        ...warningCommon
      },
      backgroundColor: variables.btnWarningBg
    },

    ".danger": {
      ".bordered": {
        ...dangerCommon
      },
      backgroundColor: variables.btnDangerBg
    },

    ".block": {
      justifyContent: "center",
      alignSelf: "stretch"
    },

    ".full": {
      justifyContent: "center",
      alignSelf: "stretch",
      borderRadius: 0
    },

    ".rounded": {
      // paddingHorizontal: variables.buttonPadding + 20,
      borderRadius: variables.borderRadiusLarge
    },

    ".transparent": {
      backgroundColor: "transparent",
      elevation: 0,
      shadowColor: null,
      shadowOffset: null,
      shadowRadius: null,
      shadowOpacity: null,
      ...primaryCommon,
      ".dark": {
        ...darkCommon,
        backgroundColor: null
      },
      ".danger": {
        ...dangerCommon,
        backgroundColor: null
      },
      ".warning": {
        ...warningCommon,
        backgroundColor: null
      },
      ".info": {
        ...infoCommon,
        backgroundColor: null
      },
      ".primary": {
        ...primaryCommon,
        backgroundColor: null
      },
      ".success": {
        ...successCommon,
        backgroundColor: null
      },
      ".light": {
        ...lightCommon,
        backgroundColor: null
      },
      ".disabled": {
        backgroundColor: "transparent",
        borderColor: variables.btnDisabledBg,
        borderWidth: variables.borderWidth * 2,
        "NativeBase.Text": {
          color: variables.btnDisabledBg
        },
        "NativeBase.Icon": {
          color: variables.btnDisabledBg
        },
        "NativeBase.IconNB": {
          color: variables.btnDisabledBg
        }
      }
    },

    ".small": {
			height: 30,
      "NativeBase.Text": {
        fontSize: 14
      },
      "NativeBase.Icon": {
        fontSize: 20,
        paddingTop: 0
      },
      "NativeBase.IconNB": {
        fontSize: 20,
        paddingTop: 0
      }
    },

    ".large": {
      height: 60,
      "NativeBase.Text": {
        fontSize: 22,
      }
    },

    ".capitalize": {},

    ".vertical": {
      flexDirection: "column",
      height: null
    },

    "NativeBase.Text": {
      fontFamily: variables.btnFontFamily,
      marginLeft: 0,
      marginRight: 0,
      color: "#fafdf6",
      fontSize: variables.btnTextSize,
      paddingHorizontal: 16,
      backgroundColor: "transparent"
      // childPosition: 1
    },

    "NativeBase.Icon": {
      color: variables.inverseTextColor,
      fontSize: 24,
      marginHorizontal: 16,
      paddingTop: platform === "ios" ? 2 : undefined
    },
    "NativeBase.IconNB": {
      color: variables.inverseTextColor,
      fontSize: 24,
      marginHorizontal: 16,
      paddingTop: platform === "ios" ? 2 : undefined
    },

    ".iconLeft": {
      "NativeBase.Text": {
        marginLeft: 0
      },
      "NativeBase.IconNB": {
        marginRight: 0,
        marginLeft: 16
      },
      "NativeBase.Icon": {
        marginRight: 0,
        marginLeft: 16
      }
    },
    ".iconRight": {
      "NativeBase.Text": {
        marginRight: 0
      },
      "NativeBase.IconNB": {
        marginLeft: 0,
        marginRight: 16
      },
      "NativeBase.Icon": {
        marginLeft: 0,
        marginRight: 16
      }
    },
    ".picker": {
      "NativeBase.Text": {
        ".note": {
          fontSize: 16,
          lineHeight: null
        }
      }
    },

    paddingVertical: variables.buttonPadding,
    // paddingHorizontal: variables.buttonPadding + 10,
    backgroundColor: variables.btnPrimaryBg,
    borderRadius: 7,
    borderColor: variables.btnBorder,
    borderWidth: 3,
    height: 50,
    alignSelf: "flex-start",
    flexDirection: "row",
    elevation: 2,
    shadowColor: platformStyle === "material" ? variables.brandDark : undefined,
    shadowOffset:
      platformStyle === "material" ? { width: 0, height: 2 } : undefined,
    shadowOpacity: platformStyle === "material" ? 0.2 : undefined,
    shadowRadius: platformStyle === "material" ? 1.2 : undefined,
    alignItems: "center",
    justifyContent: "space-between"
  };
  return buttonTheme;
};
