import variable from "./../variables/platform";

export default (variables = variable) => {
  const pickerTheme = {
    ".note": {
      color: "#8F8E95",
      border: 4px solid gray,
      padding: 1px,
    },
    // width: 90,
    marginRight: -4,
    flexGrow: 1,
    height: 50,
    width: 10 
  };

  return pickerTheme;
};
