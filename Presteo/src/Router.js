import React from 'react';
import { Scene, Router, Tabs, ActionConst, Actions } from 'react-native-router-flux';
import { Thumbnail } from 'native-base';
import { tabBarStyles } from './assets/styles/component/common';

//NAVBAR
import { Header } from './components/common';

//Login Views
import SplashScreen from './components/SplashScreen';
import RoleSelection from './components/auth/RoleSelection';
import Contract from './components/auth/Contract';
import LoginView from './components/auth/LoginView';
import PhoneVerify from './components/auth/PhoneVerify';
import RegisterUser from './components/auth/RegisterUser';

// Application Views
import ApplicationHome from './components/applicant/ApplicantHome';
import ApplicantHistoric from './components/applicant/ApplicantHistoric';
import PaymentReceipt from './components/applicant/PaymentReceipt';
import PaymentFinalizedInfo from './components/applicant/PaymentFinalizedInfo';
import PaymentFinalizedHistoric from './components/applicant/PaymentFinalizedHistoric';

// Grantor Views
import GrantorHome from './components/grantor/GrantorHome';
import OutstandingLoans from './components/grantor/OutstandingLoans';
import LateLoans from './components/grantor/LateLoans';
import ActiveLoans from './components/grantor/ActiveLoans';
import GrantorHistoric from './components/grantor/GrantorHistoric';
import InfoOutstanding from './components/grantor/InfoOutstanding';
import InfoOpportunities from './components/grantor/InfoOpportunities';
import InfoActive from './components/grantor/InfoActive';
import InfoLate from './components/grantor/InfoLate';
import InfoFinished from './components/grantor/InfoFinished';
import SummaryRequest from './components/applicant/SummaryRequest';
import CompletedLoansG from './components/grantor/CompletedLoansG';
import GrantorOpportunities from './components/grantor/GrantorOpportunities';
import PaymentFinalizedHistoricGrantor from './components/grantor/PaymentFinalizedHistoricGrantor';
import RejectLoan from './components/grantor/RejectLoan';


//CreateLoan
import CreateLoan from './components/loans/CreateLoan';
import SummaryCreateLoan from './components/loans/SummaryCreateLoan';
import VerifyIdentity from './components/loans/VerifyIdentity';
import UpdateUser from './components/auth/UpdateUser';

//Loans
import GetLoans from './components/loans/GetLoans';
import InfoLoan from './components/loans/InfoLoan';
import LoansOutstanding from './components/applicant/LoansOutstanding';
import SummaryLoanAccepted from './components/notifications/SummaryLoanAccepted';
import ApplicantAgreement from './components/notifications/ApplicantAgreement';

//PQR
import CreatePqr from './components/applicant/pqr/CreatePqr';

//Buzon
import Buzon from './components/buzon/Buzon';

const home = require('./assets/images/homeIconFooter.png');
const envelope = require('./assets/images/MailFooterIcon.png');
const cash = require('./assets/images/CashFooterIcon.png');
const manuscript = require('./assets/images/CreateLoanFooterIcon.png');
const profile = require('./assets/images/UserFooterIcon.png');
const wallet = require('./assets/images/WalletFooterIcon.png');

const RouterComponent = () => {
  return (
    <Router>
      <Scene key="root" hideNavBar>  
        <Scene key="splashscreen" component={SplashScreen} hideNavBar initial />
        <Scene key="auth"> 
          <Scene key="register" component={LoginView} initial navBar={Header} logo />
          <Scene key="contract" component={Contract} navBar={Header} logo /> 
          <Scene 
            key="roleSelection" 
            component={RoleSelection} 
            title="Seleccione un rol" 
            hideNavBar
          />
          <Scene key="phoneVerify" component={PhoneVerify} navBar={Header} logo />
          <Scene key="registerForm" component={RegisterUser} navBar={Header} logo back /> 
        </Scene>
        <Tabs 
          key="layoutGrantor" tabs tabBarStyle={tabBarStyles} 
          inactiveTintColor='#7C7C7C' activeTintColor='#E4FF33' 
        >
          <Scene 
            key="grantor" 
            icon={() => { return <Thumbnail small source={home} />; }} 
            title='Home'
            
            initial
          > 
            <Scene key="summaryrequest" component={SummaryRequest} hideNavBar />
            <Scene key="grantorhome" onEnter={() => Actions.layoutGrantor({ type: 'reset' })} component={GrantorHome} navBar={Header} logo initial />
            <Scene key="grantorhistoric" component={GrantorHistoric} logo navBar={Header} back />
            <Scene key="lateloans" component={LateLoans} logo navBar={Header} back />
            <Scene key="outstandingloans" component={OutstandingLoans} logo navBar={Header} back />
            <Scene key="infoutstanding" component={InfoOutstanding} logo navBar={Header} back />
            <Scene key="infolate" component={InfoLate} logo navBar={Header} back />
            <Scene key="infofinished" component={InfoFinished} logo navBar={Header} back />
            <Scene key="completedLoansG" component={CompletedLoansG} hideNavBar />
            <Scene key="grantoroppotunities" component={GrantorOpportunities} logo hideNavBar />
            <Scene key='paymentHistoricGrantor' component={PaymentFinalizedHistoricGrantor} navBar={Header} back />
            <Scene key='rejectloan' component={RejectLoan} navBar={Header} back />
            
          </Scene>
          <Scene 
            key="loansTab" title="Prestamos" 
            icon={() => { return <Thumbnail small source={cash} />; }}
          >
            <Scene key="loans" title="Prestamos" component={ActiveLoans} logo navBar={Header} initial />
            <Scene key="infoactive" component={InfoActive} logo navBar={Header} back />
          </Scene> 
          <Scene 
            key="searchTab" title="Prestar"
            icon={() => { return <Thumbnail small source={wallet} />; }} 
          >
            <Scene key="search" component={GrantorOpportunities} logo navBar={Header} initial />
            <Scene key="infoopportunities" component={InfoOpportunities} logo navBar={Header} back />
          </Scene>
          <Scene 
            key="buzonuo" title="Buzón" 
						component={Buzon}
            navBar={Header} logo back
            //onEnter={() => { setTimeout(() => { Actions.pop(); }, 2000); }} 
            icon={() => { return <Thumbnail small source={envelope} />; }} 
            type={ActionConst.RESET}
          />  
          <Scene 
            key="profile" title="Perfil" 
            component={UpdateUser} 
            navBar={Header} logo back
            icon={() => { return <Thumbnail small source={profile} />; }} 
          />
        </Tabs>
        <Tabs 
          key="layoutApplicant" tabs tabBarStyle={tabBarStyles} 
          inactiveTintColor='#7C7C7C' activeTintColor='#E4FF33' 
        > 
          <Scene 
            key="applicant" 
            title='Inicio'
            icon={() => { return <Thumbnail small source={home} />; }} 
            initial
            type={ActionConst.RESET}
          > 
            <Scene key="applicantHome" onEnter={() => Actions.layoutApplicant({ type: 'reset' })} component={ApplicationHome} navBar={Header} initial />
            <Scene key="paymentFinalized" component={PaymentFinalizedInfo} navBar={Header} back />
            <Scene key="applicantHistoric" component={ApplicantHistoric} navBar={Header} back />
            <Scene key="paymentReceipt" component={PaymentReceipt} navBar={Header} back />
            <Scene key='paymentHistoric' component={PaymentFinalizedHistoric} navBar={Header} back />
						<Scene key="loansOutstanding" component={LoansOutstanding} navBar={Header} logo back />
						<Scene key="infoLoanOutstanding" component={InfoLoan} forceRenderOnFocus navBar={Header} logo back />
						<Scene key="summaryLoanAccepted" component={SummaryLoanAccepted} navBar={Header} logo />
            <Scene key="applicantAgreement" component={ApplicantAgreement} navBar={Header} logo />
            <Scene key='createPqr' component={CreatePqr} navBar={Header} back />
          </Scene>
          <Scene 
            key="loansApplicant" title="Prestamos" 
            icon={() => { return <Thumbnail small source={cash} />; }}
          >
            <Scene key="loansActive" component={GetLoans} navBar={Header} logo init />
            <Scene key="infoLoanActive" component={InfoLoan} forceRenderOnFocus navBar={Header} logo back />
					</Scene> 
          <Scene 
            key="newLoan" title="Solicitar" 
            icon={() => { return <Thumbnail small source={manuscript} />; }} 
          >
            <Scene key="createLoan" component={CreateLoan} navBar={Header} logo />  
            <Scene key="summaryCreateLoan" component={SummaryCreateLoan} navBar={Header} logo back />
            <Scene key="verifyIdentity" component={VerifyIdentity} navBar={Header} logo back />
          </Scene>  
          <Scene 
            key="buzonus" title="Buzón" 
						component={Buzon}
            navBar={Header} logo back
            //onEnter={() => { setTimeout(() => { Actions.pop(); }, 2000); }} 
            icon={() => { return <Thumbnail small source={envelope} />; }} 
            type={ActionConst.RESET}
          />  
          <Scene 
            key="profile" title="Perfil" 
						component={UpdateUser}
						navBar={Header} logo back
            icon={() => { return <Thumbnail small source={profile} />; }} 
          />  
          
        </Tabs>
			</Scene>
    </Router>
  );
};

export default RouterComponent;
