//All components to config redux and redux-persist
import { createStore, applyMiddleware, compose } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2';
import ReduxThunk from 'redux-thunk';
import reducers from './reducers';

const persistConfig = {
    key: 'root',
    storage,
    stateReconciler: autoMergeLevel2,
    blacklist: ['loan', 'loader']
  };
const pReducer = persistReducer(persistConfig, reducers);
export const store = createStore(pReducer, {}, compose(applyMiddleware(ReduxThunk)));
export const persistor = persistStore(store);
