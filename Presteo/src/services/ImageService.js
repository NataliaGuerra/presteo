
import ImagePicker from 'react-native-image-picker';
import { Keyboard } from 'react-native';
import { BUCKET_URL } from './../config/variables';

class ImageService {

  static getUrlFormat(url) {
    const re = /^(http|https):\/\//;
    if (re.test(url)) {
        return url;
    } 
    return BUCKET_URL + url;
  }

  static GetImage(title = null){ 
    return new Promise((resolve, reject) => {
      const options = {
        quality: 1.0,
        maxWidth: 820,
        maxHeight: 820,
        title: title == null ? 'Seleccione su factura' : title,
        takePhotoButtonTitle: 'Tomar una foto',
        chooseFromLibraryButtonTitle: 'Obtener de la galería',
        mediaType: 'photo',
      };
      ImagePicker.showImagePicker(options, (response) => {
        Keyboard.dismiss();
        if (!response.didCancel && !response.error) {
          const file = {
            uri: response.uri,
            name: response.fileName,
            type: response.type
          };
          resolve(file);
        } else {
          reject('canceled');
        }
      });
    });
  }

}

export default ImageService;
