import { store } from './../Store'; 

class LoaderService { 
  static timer = null;
  static isLoading = false;

  static setTimer() {
    clearTimeout(LoaderService.timer);
    LoaderService.isLoading = true;
    LoaderService.timer = setTimeout(() => {
      if (LoaderService.isLoading) {
        store.dispatch({
          type: 'DATA_GET_FAILURE'
        });
        store.dispatch({
          type: 'SHOW_MODAL',
          modalProps: {
            open: true,
            message: 'Revisa tu acceso a internet, tenemos problemas de conexión.',
            closeModal: () => {
              store.dispatch({
                type: 'HIDE_MODAL'
              });
            }
          },
          modalType: 'alert'
        });
      }
      LoaderService.isLoading = false;
    }, 15000);
  }
  static removeTimer() {
    if (!this.isLoading) {
      clearTimeout(LoaderService.timer);
    }
    LoaderService.isLoading = false;
  }
}
export default LoaderService;

