import { RNS3 } from 'react-native-aws3';

const options = {
  bucket: 'bd-info-smart-loan',
  region: 'us-east-1',
  accessKey: 'AKIAIN7ZP5F5JEQPN52Q',
  secretKey: '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',
  successActionStatus: 201
};

class AWS3Service {

  static UploadImage(file, path) {
    return new Promise((resolve, reject) => {
      options.keyPrefix = `images/${path}`;
      this.myFile = file;
      if (this.myFile.name == null || this.myFile.name === undefined) {
        this.myFile.name = file.uri.substring(file.uri.lastIndexOf('/') + 1, file.uri.lastIndexOf('.'));
      }
      if (this.myFile.type == null || this.myFile.type === undefined) {
        this.myFile.type = `image/${file.uri.substring(file.uri.lastIndexOf('.') + 1)}`;
      }

      RNS3.put(this.myFile, options).then(response => {
        if (response.status !== 201) {
           
          reject('Failed to upload image to S3');
        }
        resolve(response.body);
      });
    });
  }
  static UploadMultipleImages(files, path) {
    return new Promise((resolve, reject) => {
      const result = [];
      const puts = [];
      options.keyPrefix = `images/${path}`;
      for (const file of files) {
        this.myFile = file; 
        if (this.myFile.name == null || this.myFile.name === undefined) {
          this.myFile.name = file.uri.substring(file.uri.lastIndexOf('/') + 1, file.uri.lastIndexOf('.'));
        }
        if (this.myFile.type == null || this.myFile.type === undefined) {
          this.myFile.type = `image/${file.uri.substring(file.uri.lastIndexOf('.') + 1)}`;
        }
        puts.push(
          RNS3.put(this.myFile, options).then(response => {
            if (response.status !== 201) {
               
              reject('Failed to upload image to S3');
            }
            result.push(response.body);
          })
        );
      }
      Promise.all(puts).then(() => {
        resolve(result);
      });
    });
  } 
}

export default AWS3Service; 
