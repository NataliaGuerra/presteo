import PushNotification from 'react-native-push-notification';
import DeviceInfo from 'react-native-device-info';
import { PushNotificationIOS, Platform } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { store } from './../Store'; 
import { REGISTER_DEVICE } from './../actions/types';
import ImageService from '../services/ImageService';

class NotificationService {

  static configure() {
    PushNotification.configure({

			// Register token logic
      onRegister: function(token) {
				 
				const device = {
					tokenDispositivo: token.token,
					plataforma: token.os,
					imei: DeviceInfo.getUniqueID(),
				};
				store.dispatch({
					type: REGISTER_DEVICE,
					payload: device
				});
			}, 
			
			// Logic process notification
      onNotification: function(notification) {
				// Notification 
				 
				const type = Platform.OS === 'ios' ? notification.data.type : notification.type;		
				const alert = Platform.OS === 'ios' ? notification.data.alert : notification.message;
				const title = Platform.OS === 'ios' ? notification.data.title : notification.title;		
				switch (type) {
					case 'PrestamoAceptadoOtorgante':
						const url = Platform.OS === 'ios' ? notification.data.fotoPerfil.replace( /\s\s+/g, ' ' ).trim() : notification.fotoPerfil.replace( /\s\s+/g, ' ' ).trim();	
						const user = Platform.OS === 'ios' ? notification.data.usuario.replace( /\s\s+/g, ' ' ).trim() : notification.usuario.replace( /\s\s+/g, ' ' ).trim();	
						store.dispatch({
							type: 'SHOW_MODAL',
							modalProps: {
								open: true,
								title: title,
								message: alert,
								url: ImageService.getUrlFormat(url),
								backdrop: false,
								closeModal: () => {
									store.dispatch({
										type: 'HIDE_MODAL'
									});
									const loanId = Platform.OS === 'ios' ? notification.data.idPrestamo.replace( /\s\s+/g, ' ' ).trim() : notification.idPrestamo.replace( /\s\s+/g, ' ' ).trim();
									PushNotification.cancelAllLocalNotifications();
									Actions.summaryLoanAccepted({ loanId, user });
								}
							},
							modalType: 'approved'
						});
						break;
					case 'SoporteFacturaRechazado':
						store.dispatch({
							type: 'SHOW_MODAL',
							modalProps: {
								open: true,
								backdrop: false,
								message: alert,
								title: title,
								closeModal: () => {
									store.dispatch({
										type: 'HIDE_MODAL'
									});
									const loanId = Platform.OS === 'ios' ? notification.data.idPrestamo.replace( /\s\s+/g, ' ' ).trim() : notification.idPrestamo.replace( /\s\s+/g, ' ' ).trim();
									PushNotification.cancelAllLocalNotifications();
									Actions.infoLoanOutstanding({ loanId: loanId });
								}
							},
							modalType: 'billError'
						});
						break;
					case 'DesembolsoCargado':
						const urlComprobante = Platform.OS === 'ios' ? notification.data.urlComprobante.replace( /\s\s+/g, ' ' ).trim() : notification.urlComprobante.replace( /\s\s+/g, ' ' ).trim();
						store.dispatch({
							type: 'SHOW_MODAL',
							modalProps: {
								open: true,
								backdrop: false,
								message: alert,
								closeModal: () => {
									store.dispatch({
										type: 'HIDE_MODAL'
									});
									store.dispatch({
										type: 'SHOW_MODAL',
										modalProps: {
											open: true,
											url: ImageService.getUrlFormat(urlComprobante),
											backdrop: false,
											closeModal: () => {
												store.dispatch({
													type: 'HIDE_MODAL'
												});
											},
										}, 
										modalType: 'image'
									});
									PushNotification.cancelAllLocalNotifications();
								}
							},
							modalType: 'review'
						});
						break;
					default:
						 
				}
				notification.finish(PushNotificationIOS.FetchResult.NoData);		
			}, 

      // Android
      senderID: '63853790814',

      // iOS
      permissions: {
        alert: true,
        badge: true,
        sound: true
      },

      popInitialNotification: true,

      requestPermissions: true,
		});
  }
};
export default NotificationService;