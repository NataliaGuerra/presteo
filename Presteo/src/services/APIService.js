import axios from 'axios';
import { BASE_URL } from '../config/variables';

class APIService {

  static GetAPI(url) {
    return new Promise((resolve, reject) => {
      const options = {
        method: 'GET',
        url: BASE_URL + url,
        timeout: 14500,
      };
      axios(options).then((response) => {
        if (response.data === '') {
          throw new Error('Ha ocurrido un error inesperado en la solicitud!');
        }
        resolve(response.data);
      }).catch((error) => {
        if (typeof (error.response) === 'undefined' 
          || typeof (error.response.data) === 'undefined') {
          reject(error);
        } else {
          reject(error.response.data.message);
        }
      });
    });
  }

  static PutAPI(url, body) {
    return new Promise((resolve, reject) => {
      const absolutUrl = BASE_URL + url;
      axios.put(absolutUrl, body, 
        { headers: { 'Content-Type': 'application/json' }, timeout: 14500, }).then((response) => {
        resolve(response.data);
      }).catch((data) => {
        reject(data.response.data.message);
      });
    });
  }
}

export default APIService;
