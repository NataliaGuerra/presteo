import { Alert } from 'react-native';
import { Actions } from 'react-native-router-flux';

class AlertService {

  static showError() {
    setTimeout(() => {
      Alert.alert(
        'Error',
        'Ups! Ha ocurrido un error en la busqueda. Por favor intente de nuevo más tarde',
        [
          { text: 'OK', onPress: () => Actions.pop() },
        ],
        { cancelable: false }
      );
    }, 200);
  }

  static showCustomError(text) {
    setTimeout(() => {
      Alert.alert(
        'Error',
            text,
        [
          { text: 'OK', onPress: () => {} },
        ],
        { cancelable: false }
      );
    }, 200);
  }

  static showSuccess(title, text) {
    setTimeout(() => {
      Alert.alert(
        title,
        text,
        [
          { text: 'OK', onPress: () => {} },
        ],
        { cancelable: false }
      );
    }, 200);
  }

}

export default AlertService;
