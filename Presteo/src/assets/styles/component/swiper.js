
const slideAFormStyle = {
    flex: 1
};


const sectionScroll = {
    minWidth: '100%',
    maxHeight: '100%',
    borderRightWidth: 9,
    borderColor: 'gray',
    borderRadius: 10
};

const dot = {
    backgroundColor: '#FAFDF6', 
    width: 25, 
    height: 25,
    borderRadius: 12,
    borderColor: '#7C7C7C',
    borderWidth: 2, 
    marginLeft: 3, 
    marginRight: 3, 
    marginTop: 3, 
    marginBottom: 0 
};

const activeDot = {
    backgroundColor: '#7C7C7C', 
    width: 25, 
    height: 25,
    borderRadius: 12,
    borderColor: '#2B2B2B',
    borderWidth: 2, 
    marginLeft: 3, 
    marginRight: 3, 
    marginTop: 3, 
    marginBottom: 0 
};

export { sectionScroll, slideAFormStyle, dot, activeDot };
