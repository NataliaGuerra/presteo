import { scale, moderateScale } from '../../../helpers/Scaler';

const progressViewStyles = {
	backgroundColor: '#7C7C7C', 
	borderRadius: 10,
	width: scale(70)
};
const progressCustomStyles = {
	backgroundColor: '#E4FF33',
	borderRadius: 10, 
};

const progressViewTextStyle = {
	flex: 1,
	marginLeft: scale(3)
};

const progressTextStyle = {
	fontSize: moderateScale(12, 1),
	color: '#7C7C7C',
	fontFamily: 'Rubik-Light'
};

export { 
	progressViewStyles,
	progressViewTextStyle,
	progressTextStyle,
	progressCustomStyles
};
