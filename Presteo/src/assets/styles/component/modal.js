const commonStyles = {
    thumbnailStyle: {
        height: 100,
        width: 100,
        borderRadius: 50,
    },
    footerStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 20 
    },
    textStyle: {
        color: '#7c7c7c',
        textAlign: 'center',
    },
    textMTopStyle: {
        color: '#7c7c7c',
        textAlign: 'center',
        marginTop: 20,
    },
    contentStyle: {
        backgroundColor: '#ffffff', 
        borderRadius: 25,
        padding: 20
    },
    iconInfoStyle: {
        color: '#e4ff33',
        fontSize: 80,
        marginLeft: 23 
    },
    alingCenterStyle: {
        alignItems: 'center'
    },
};

const maintenanceStyles = {
    imgStye: {
        alignItems: 'center',
        marginTop: 30,
        width: '100%',
        height: 50,
        overflow: 'hidden'
    },
    contentStyle: {
        height: 250,
        width: '100%',
      backgroundColor: '#ffffff', 
      borderRadius: 25
    },
    bodyStyle: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 20 
    },
    boxStyle: {
        flex: 1, 
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
};

const invesmentStyles = {
    textGrayStyle: { 
        fontSize: 17,
        color: '#7c7c7c',
        textAlign: 'center' 
    },
    textBalckStyle: { 
        fontSize: 17,
        color: '#2b2b2b',
        textAlign: 'center' 
    },
    textGrayMStyle: { 
        fontSize: 17,
        color: '#7c7c7c',
        textAlign: 'center',
        marginBottom: 10
    },
};

export { maintenanceStyles, commonStyles, invesmentStyles };
