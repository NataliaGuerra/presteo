import { Platform } from 'react-native';
import { scale, verticalScale, moderateScale } from '../../../helpers/Scaler';


const bodyRow = {
    flex: 1,
    flexDirection: 'row',
};

const viewImageLeft = {
    flex: 2,
    flexDirection: 'column',
    justifyContent: 'center',
    alignContent: 'center'
};

const labelCardStyles = {
	fontSize: 12,
	color: '#7C7C7C',
	fontWeight: 'normal'
};

const textCardStyles = {
    fontSize: 12,
    color: '#7C7C7C',
    fontWeight: 'bold',
};
const textCardStyles2 = {
	fontSize: 12,
	color: '#7C7C7C',
};
const labelCardItemStyles = {
    fontSize: 15,
    color: '#7C7C7C',
    fontWeight: 'normal',
    marginBottom: 10,
    flex: 1.8
};

const textCardItemStyles = {
    fontSize: 16,
    color: '#7C7C7C',
    fontWeight: 'bold',
    marginBottom: 10,
    flex: 1.2
};


const labelFinStyles = {
    fontSize: 17,
    paddingTop: 5,
    paddingBottom: 5,
    color: '#7C7C7C',
    fontWeight: '100'
};

const textFinStyles = {
    fontSize: 17,
    paddingTop: 5,
    paddingBottom: 5,
    color: '#7C7C7C',
    fontWeight: 'bold'
};


const headerCardStyles = {
    backgroundColor: 'transparent',
    borderWidth: 0,
    borderColor: 'transparent',
    shadowOffset: null,
    shadowOpacity: null,
    marginBottom: -30,
    zIndex: 1
};

const bodyCardStyles = {
	borderRadius: 15,
	borderColor: '#636363',
	borderWidth: 3,
	marginRight: 20,
	marginLeft: 20,
};

const bodyCardMPStyles = {
    borderRadius: 15,
    borderColor: '#636363',
    borderWidth: 3,
    marginRight: 20,
    marginLeft: 20,
    paddingLeft: 30,
    paddingRight: 30,
    paddingTop: 20,
    paddingBottom: 30
};

const cardFinStyles = {
	borderRadius: 15,
	borderColor: '#636363',
	backgroundColor: '#fff',
	borderWidth: 3,
	marginRight: 20,
	marginLeft: 20,
	padding: 20
};

const buttonFinStyles = {
    marginTop: 30,
};

const butTextFinStyles = {
    fontSize: 17
};

const transparentCardItemsStyles = {
	shadowColor: null,
	shadowOffset: null,
	shadowOpacity: null,
	shadowRadius: null,
	elevation: null,
	backgroundColor: "transparent",
	borderWidth: 0
};

const infoButtonStyles = {
  height: verticalScale(30)
};

const footerCardStyles = {
	backgroundColor: 'transparent',
	borderWidth: 0,
	marginTop: -27,
	marginRight: 30,
	zIndex: 1
};

const infoCardStyles = {
  fontSize: 15,
  height: 30
};

const bodyCardItemStyles2 = {
	fontSize: 10,
	flex: 2, 
    flexDirection: 'column', 
    justifyContent: 'center', 
    alignItems: 'center' 
};
const lightCardStyles = {
  fontSize: 18
};

const bodyCardItemStyles = {
    flex: 2, 
    flexDirection: 'column', 
    justifyContent: 'center', 
    alignItems: 'center' 
};

const leftCardItemStyles = { 
    flex: 1, 
    justifyContent: 'center', 
		alignItems: 'center',
};

const cardBottomStyles = {
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    borderColor: '#636363',
    backgroundColor: '#fff',
    borderWidth: 3,
    marginRight: 20,
    marginLeft: 20,
    padding: 0
};

/**
 * GRANTOR OPPORTUNITIES STYLES
 */
const blackButton = {
	backgroundColor: '#2b2b2b', 
	borderColor: '#fafdf6', 
	borderWidth: 3,
	height: verticalScale(30),
	paddingHorizontal: scale(2),
}

const declineOpportunitiesButton = {
	height: verticalScale(30),
	marginLeft: scale(10),
	backgroundColor: '#7c7c7c',
}
const textOpportunitiesButton = {
	fontSize: moderateScale(12, 1),
	color: 'white',
	fontFamily: 'Rubik-Medium'
}

const declineInfoButton = {
	zIndex: 5,
	height: verticalScale(43),
	marginLeft: scale(10),
	backgroundColor: '#7c7c7c',
};

const declineTextInfoButton = {
	fontSize: moderateScale(14, 1),
	color: 'white',
	fontFamily: 'Rubik-Medium'
};

const bodyCardOpportunitiesItemStyles = {
	flex: 3, 
	flexDirection: 'column', 
	justifyContent: 'flex-start', 
	alignItems: 'flex-start', 
	padding: scale(10)
};
const labelOpportunitiesCardStyles = {
	fontSize: moderateScale(16, 0.8),
	color: '#7C7C7C',
	fontWeight: 'normal'
};
const textOpportunitiesCardStyles = {
	fontSize: moderateScale(16, 0.8),
	color: '#7C7C7C',
	fontWeight: 'bold'
};
const footerOpportunitiesCardStyles = {
	backgroundColor: 'transparent',
	borderWidth: 0,
	marginTop: Platform.OS === 'ios' ? verticalScale(-25) : verticalScale(-30),
	zIndex: 1
};
const bodyOpportunitiesCardStyles = {
	borderRadius: 15,
	borderColor: '#636363',
	borderWidth: 3,
	marginRight: scale(20),
	marginLeft: scale(20),
};

const iconOpportunitiesStyle = {
	position: 'absolute',
	right: 0,
	top: 0,
};

const blackTextInfo = {
	color: 'white',
	backgroundColor: '#7c7c7c',
	fontSize: moderateScale(14, 1),
	fontFamily: 'Rubik-Medium',
	fontWeight: '600',
	paddingLeft: scale(5),
	paddingRight: scale(5),
}
const labelInfo = {
	fontSize: moderateScale(13,1),
	color: '#7C7C7C',
	fontFamily: 'Rubik-Light',
	position: null,
	top: null,
	left: null,
	right: null,
	flex: 2,
	height: null,
	width: null,
};
const itemInfo = {
	flex: 1, 
	flexDirection: 'row',
	borderBottomWidth: 0,
	justifyContent: 'flex-start',
	alignItems: 'flex-start'
}
const blackTextInfoContainer = {
	flex: 1	
}
const blackTextInfoBorderContainer = {
	overflow: 'hidden',
	borderRadius: scale(6),
	marginBottom: scale(3),
	alignSelf: 'flex-start'
}
const receiptText = {
	fontFamily: 'Rubik-light',
	fontSize: moderateScale(12,1),
}
const stateTitleText = {
	fontFamily: 'Rubik-Medium',
	fontSize: moderateScale(13,1),
	fontWeight: 'bold',
	color: 'black'
}
const stateTextStyle = {
	fontFamily: 'Rubik-light',
	fontSize: moderateScale(12,1),
	color: '#7C7C7C',
	fontWeight: 'bold'
}
/* GRANTOR OPPORTUNITIES STYLES */

/* GRANTOR ACTIVE LOANS  */
const textActiveCardStyles = {
	fontSize: moderateScale(15, 0.8),
	color: '#7C7C7C',
	fontWeight: 'bold'
};
const labelActiveCardStyles = {
	fontSize: moderateScale(15, 0.8),
	color: '#7C7C7C',
	fontWeight: 'normal'
};

const labelActiveInfo = {
	fontSize: moderateScale(15,1),
	color: '#7C7C7C',
	fontFamily: 'Rubik-Light',
	position: null,
	top: null,
	left: null,
	right: null,
	flex: 2,
	height: null,
	width: null,
};
const progressViewInfoStyles = {
	backgroundColor: '#7C7C7C', 
	borderRadius: 10,
	width: scale(90)
};
/* GRANTOR ACTIVE LOAN */
/* MY LOANS */
const ContentMyloans = {
	borderColor: '#7c7c7c',
	backgroundColor: '#fafdf6',
	borderWidth: 3,
	borderRadius: 15,
	paddingLeft: scale(20),
	paddingRight: scale(20),
	paddingTop: scale(20),
	paddingBottom: scale(20),
};
const TextStyleMyloans = {
	fontSize: 15,
	fontFamily: 'Rubik-Light',
	color: '#7c7c7c',
	flex: 1,
	right: scale(20),
};
const TextBoldMyloans = {
	fontSize: moderateScale(16, 0.8),
	fontFamily: 'Rubik-Light',
	color: '#7c7c7c',
	fontWeight: 'bold',
	flex: 1,
	right: scale(15),
	marginBottom: scale(5),
};
const ButtonOrderMyloans = {
	top: scale(-20),
	left: scale(220),
};
const MyLoansButtons = {
	top: scale(-20),
	left: scale(140),
	flex: 1, 
	flexDirection: 'row',
}
/* MY LOANS */
/* SUMMARY LOAN */
const textInfoStyle = {
	color: 'grey',
	fontSize: 18,
	fontFamily: 'Rubik-Medium'
};
const textStyle = {
	fontSize: 18,
	color: '#7c7c7c',
	fontFamily: 'Rubik-Light',
};
const rowStyle = {
	borderColor: 'black',
	borderWidth: 1,
	backgroundColor: '#7c7c7c',
	flexDirection: 'row',
	justifyContent: 'center',
	textAlign: 'center'
};
const titleTextPayments = {
	fontFamily: 'Rubik-light',
	fontSize: 12,
	color: 'white',
	textAlign: 'center'
};
/* SUMMARY LOAN */
export { 
	MyLoansButtons,
	rowStyle,
	titleTextPayments,
	textStyle,
	textInfoStyle,
	ContentMyloans,
	TextStyleMyloans,
	TextBoldMyloans,
	ButtonOrderMyloans,
	blackTextInfo,
	labelInfo,
	itemInfo,
	receiptText,
	textActiveCardStyles,
	labelActiveCardStyles,
	blackTextInfoContainer,
	blackTextInfoBorderContainer,
	labelActiveInfo,
	progressViewInfoStyles,
	stateTitleText,
	stateTextStyle,
    infoButtonStyles,
    labelCardStyles,
    textCardStyles,
    headerCardStyles,
    labelFinStyles,
    textFinStyles,
    bodyCardStyles,
    cardBottomStyles,
		footerCardStyles,
		footerOpportunitiesCardStyles,
		declineOpportunitiesButton,
    infoCardStyles,
    lightCardStyles,
    buttonFinStyles,
		bodyCardItemStyles,
		bodyCardOpportunitiesItemStyles,
		labelOpportunitiesCardStyles,
		textOpportunitiesCardStyles,
		bodyOpportunitiesCardStyles,
		iconOpportunitiesStyle,
    leftCardItemStyles,
    butTextFinStyles,
    cardFinStyles,
    bodyCardMPStyles,
    transparentCardItemsStyles,
    labelCardItemStyles,
    textCardItemStyles,
	blackButton,
	bodyCardItemStyles2,
	textCardStyles2,	
        textOpportunitiesButton,
        bodyRow,
		viewImageLeft,
		declineInfoButton,
		declineTextInfoButton,
};
