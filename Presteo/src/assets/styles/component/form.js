const labelInputError = {
    color: '#ff0000',
    marginLeft: 20,
    fontSize: 15,
    marginBottom: 15
};

const inputMLabel = {
    flex: 1,
    fontSize: 17,
    color: '#7c7c7c',
    fontFamily: 'Rubik-Medium'
};

const inputM = {
    flex: 2,
    borderWidth: 3,
    borderColor: '#7c7c7c',
    height: 50
};

const labelInputErrorM = {
    color: '#ff0000',
    marginLeft: 20,
    fontSize: 15
};

const texboxInput = {
    borderWidth: 3,
    borderColor: '#7c7c7c',
    height: 100
};

const fieldFormStyle = {
    backgroundColor: '#fff', 
    borderColor: '#7C7C7C', 
    borderWidth: 3, 
    borderRadius: 10, 
    marginBottom: 10, 
    marginLeft: 20, 
    marginRight: 20,
    height: 50,
    color: '#7c7c7c'
};


const checkBoxStyle = {
    backgroundColor: '#fff', 
    borderColor: '#7C7C7C', 
    borderWidth: 3, 
    borderRadius: 20, 
    marginBottom: 10, 
    marginLeft: 20, 
    marginRight: 20,
    height: 50,
    color: '#7c7c7c'
};

const checkBoxSelectedStyle = {
    backgroundColor: '#2B2B2B', 
    borderColor: '#2B2B2B', 
    borderWidth: 3, 
    borderRadius: 20, 
    marginBottom: 10, 
    marginLeft: 20, 
    marginRight: 20,
    height: 50
};

const documentStyle = {
    backgroundColor: '#fff', 
    borderColor: '#7C7C7C', 
    borderWidth: 3, 
    borderRadius: 10, 
    marginBottom: 10, 
    marginLeft: 2, 
    marginRight: 20,
    height: 50,
    color: '#7c7c7c'
};

const documentTypeStyle = {
    backgroundColor: '#fff', 
    borderColor: '#7C7C7C', 
    borderWidth: 3, 
    borderRadius: 10, 
    marginBottom: 10, 
    marginLeft: 20, 
    marginRight: 2,
    height: 50,
    color: '#7c7c7c'
};

const documentTypeDisabledStyle = {
    backgroundColor: '#ebebe4', 
    borderColor: '#7C7C7C', 
    borderWidth: 3, 
    borderRadius: 10, 
    marginBottom: 10, 
    marginLeft: 20, 
    marginRight: 2,
    height: 50,
    color: '#7c7c7c'
};

const fieldDisabledFormStyle = {
    backgroundColor: '#ebebe4', 
    borderColor: '#7C7C7C', 
    borderWidth: 3, 
    borderRadius: 10, 
    marginBottom: 10, 
    marginLeft: 20, 
    marginRight: 20,
    color: '#7C7C7C',
    height: 50,
};

const documentDisabledStyle = {
    backgroundColor: '#ebebe4', 
    borderColor: '#7C7C7C', 
    borderWidth: 3, 
    borderRadius: 10, 
    marginBottom: 10, 
    marginLeft: 2, 
    marginRight: 20,
    color: '#7C7C7C',
    height: 50,
};

export {
    labelInputError,
    inputMLabel,
    texboxInput,
    inputM,
    labelInputErrorM,
    documentStyle,
    documentTypeStyle,
    documentTypeDisabledStyle,
    documentDisabledStyle,
    fieldDisabledFormStyle,
    fieldFormStyle,
    checkBoxStyle,
    checkBoxSelectedStyle
};
