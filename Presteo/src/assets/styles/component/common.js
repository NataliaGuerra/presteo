import { scale, moderateScale } from '../../../helpers/Scaler';


const textBlack = {
    color: '#000'
};

const textWhite = {
    color: '#fff'
};

const textGray = {
	color: '#7c7c7c'
};

const backgroundBlack = {
    backgroundColor: '#000'
};

const backgroundWhite = {
    backgroundColor: '#fff'
};

const backgroundGray = {
	backgroundColor: '#7c7c7c'
};


const rowCenterStyle = {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
};

const columnCenterStyle = {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
};

const noBorderStyle = {
    borderWidth: 0,
};

const noBorderBottomWidth = {
    borderBottomWidth: 0,
};

const titleText = {
	fontSize: moderateScale(14, 1),
	fontFamily: 'Rubik-Medium'
};
const centerRow = {
	flex: 1, 
	flexDirection: 'row',
	justifyContent: 'center',
	alignItems: 'center'
};
const rightRow = {
	flex: 1, 
	flexDirection: 'row',
	justifyContent: 'flex-end',
	alignItems: 'flex-end'
};
const leftRow = {
	flex: 1, 
	flexDirection: 'row',
	justifyContent: 'flex-start',
	alignItems: 'flex-start'
};
const centerColumn = 	{ 
	flex: 1, 
	flexDirection: 'column', 
	justifyContent: 'center', 
	alignItems: 'center'
};

const paddingTopiOS = {
	paddingTop: scale(10) ,
};
const imageStyleBorder = {
	borderWidth: 3,
	borderRadius: 100,
	borderColor: '#fff'
};
const imageStyleBorderOutside = {
	borderWidth: 5,
	borderRadius: 100,
	borderColor: '#00cd00'
};

const h2Styles = {
    textAlign: 'center', 
    fontFamily: "Rubik-Medium",
    marginTop: 15,
    marginBottom: 15
};

const contentStyles = {
    backgroundColor: 'transparent'
};

const tabBarStyles = {
    backgroundColor: '#2B2B2B'
};

const iconTabBarStyles = {
    color: '#E4FF33'
};


const blackButtonStyle = {
    height: 50, 
    marginBottom: 20,
    marginLeft: 20, 
    marginRight: 20,
    flex: 1,
    justifyContent: 'center'
};


const slideBFormStyle = {
    flex: 1,
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center'
};

const slideCFormStyle = {
    flex: 2,
};

export { 
    rowCenterStyle,
    columnCenterStyle,
    noBorderStyle,
    imageStyleBorderOutside,
    imageStyleBorder,
    paddingTopiOS,
    centerColumn,
    leftRow,
    rightRow,
    centerRow,
	titleText,
	noBorderBottomWidth,
	textBlack,
	textWhite,
	textGray,
	backgroundBlack,
	backgroundWhite,
	backgroundGray,
	slideCFormStyle,
	slideBFormStyle,
	h2Styles,
	contentStyles,
	tabBarStyles,
	iconTabBarStyles,
	blackButtonStyle,
};
