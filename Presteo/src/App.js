import React, { Component } from 'react';
import SplashScreen from 'react-native-splash-screen';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/lib/integration/react';
import Loader from './components/common/Loader';
import { persistor, store } from './Store';
import Router from './Router';
import ModalRoot from './ModalRoot';

class App extends Component {
  componentDidMount() {
		SplashScreen.hide();
		
  }
  
  render() {
    return (
      <Provider store={store}>
        <PersistGate persistor={persistor} >
          <Loader />
          <Router />
          <ModalRoot />
        </PersistGate>
      </Provider>
    );
  }
}

export default App;
