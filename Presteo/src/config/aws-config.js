// WARNING: DO NOT EDIT. This file is Auto-Generated by AWS Mobile Hub. It will be overwritten.

// Copyright 2017-2018 Amazon.com, Inc. or its affiliates (Amazon). All Rights Reserved.
// Code generated by AWS Mobile Hub. Amazon gives unlimited permission to
// copy, distribute and modify it.

// AWS Mobile Hub Project Constants
var aws_app_analytics = 'enable';
var aws_cognito_identity_pool_id = 'us-east-1:a5473993-c6d1-400f-8148-13495226ea30';
var aws_cognito_region = 'us-east-1';
var aws_mobile_analytics_app_id = '94dc805dca9e4665ad203820045d4e7d';
var aws_mobile_analytics_app_region = 'us-east-1';
var aws_project_id = '73464e22-938e-4104-b672-ccdc09a535dd';
var aws_project_name = 'Presteo';
var aws_project_region = 'us-east-1';
var aws_resource_name_prefix = 'presteo-mobilehub-363838696';

AWS.config.region = aws_project_region;
AWS.config.credentials = new AWS.CognitoIdentityCredentials({
    IdentityPoolId: aws_cognito_identity_pool_id
  }, {
    region: aws_cognito_region
  });
AWS.config.update({customUserAgent: 'MobileHub v0.1'});
